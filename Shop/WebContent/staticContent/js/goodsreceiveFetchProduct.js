	function fetchproductname()
	{
		var input1 = document.getElementById('fk_cat_id'), list = document
	.getElementById('cat_drop'), i, fk_cat_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_cat_id = list.options[i].getAttribute('data-value');
		}
	}
	
	var fk_cat_id = fk_cat_id;
	$("#proName").empty();
	$("#proName").append(
	$("<option></option>").attr("value", "").text("Select product"));
	var params = {};
	
	params["methodName"] = "getAllProductByCategoriesForAdvance";
	
	params["fk_cat_id"] = fk_cat_id;
	
	$.post(
			'/Shop/jsp/utility/controller.jsp',
	params,
	function(data) {
		var count = 1;
	
		var jsonData = $.parseJSON(data);
		$.each(jsonData, function(i, v) {
			$("#proName").append(
		$("<option></option>").attr("value", count).text(
				v.product + "," + v.manufacturer + ","
						+ v.weight));
	// $("#batchNo").append($("<option></option>").attr("value",v.batchNo).text("Batch
	// No = "+v.batchNo + " " +" Stock = "+ v.quantityForBatchNo
	// ));
				count++;
			});
		}).error(function(jqXHR, textStatus, errorThrown) {
	if (textStatus === "timeout") {
	
			}
		});
	
	}
	
	function getAllProduct() {
	
		var input1 = document.getElementById('fk_cat_id'), list = document
	.getElementById('cat_drop'), i, fk_cat_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_cat_id = list.options[i].getAttribute('data-value');
		}
	}
	
	var fk_cat_id = fk_cat_id;
	$("#proName").empty();
	$("#proName").append(
	$("<option></option>").attr("value", "").text("Select product"));
	var params = {};
	
	params["methodName"] = "getAllProductByCategories";
	
	params["fk_cat_id"] = fk_cat_id;
	
	$.post(
			'/Shop/jsp/utility/controller.jsp',
	params,
	function(data) {
		var jsonData = $.parseJSON(data);
		$.each(jsonData, function(i, v) {
			$("#proName").append(
		$("<option></option>").attr("value", i).text(
								v.product));
	
			});
		}).error(function(jqXHR, textStatus, errorThrown) {
	if (textStatus === "timeout") {
	
			}
		});
	
	}
	
	function getAllProductForBooked() {
	
		var input1 = document.getElementById('fk_cat_id1'), list = document
	.getElementById('cat_drop1'), i, fk_cat_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_cat_id = list.options[i].getAttribute('data-value');
		}
	}
	
	var input1 = document.getElementById('supplier1'), list = document
	.getElementById('sup_drop1'), i, fk_sup_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_sup_id = list.options[i].getAttribute('data-value');
		}
	}
	
	var fk_cat_id = fk_cat_id;
	var fk_sup_id = fk_sup_id;
	$("#proName1").empty();
	$("#proName1").append(
	$("<option></option>").attr("value", "").text("Select product"));
	var params = {};
	
	params["methodName"] = "getAllProductByCategoriesAndBySupplierForBookedGoodsReceiveNew";
	
	params["fk_cat_id"] = fk_cat_id;
	params["fk_sup_id"] = fk_sup_id;
	
	$.post(
			'/Shop/jsp/utility/controller.jsp',
	params,
	function(data) {
		var count = 1;
		var jsonData = $.parseJSON(data);
		$.each(jsonData, function(i, v) {
			$("#proName1").append(
		$("<option></option>").attr("value", count).text(
				v.product + "," + v.manufacturer + ","
						+ v.weight + "," + v.unitName));
				count++;
			});
		}).error(function(jqXHR, textStatus, errorThrown) {
	if (textStatus === "timeout") {
	
			}
		});
	
	}
	
	function prodetail() {
	
		var itemparams = {};
		var productId = $('#proName').val();
	
	itemparams["productId"] = productId;
	itemparams["methodName"] = "getpreviousDetsil";
	
	$.post(
			'/Shop/jsp/utility/controller.jsp',
	itemparams,
	function(data) {
		var jsonData = $.parseJSON(data);
		$.each(jsonData, function(i, v) {
			// $("#proName").append($("<option></option>").attr("value",i).text(v.product));
	
	alert("Previous Purchase Detail Of " + v.productName
		+ "\nProduct" + "\n\n\nProduct Name : "
		+ v.productName + "\nBuy Price    : " + v.buyPrice
		+ "\nSale Price   : " + v.salePrice
		+ "\nQuantity     : " + v.quantity
		+ "\nWeight       : " + v.weight
		+ "\nMRP          : " + v.mrp);
	
			});
		}).error(function(jqXHR, textStatus, errorThrown) {
	if (textStatus === "timeout") {
	
			}
		});
	
	}
	
	function getdetails() {
	
		var productList = "";
	this.getProductdetails = getProductdetails;
	this.getAllProductDetailsForBooked = getAllProductDetailsForBooked;
	
	function getProductdetails() {
		// Grid for Retail if it is checked
	if (document.getElementById('retail').checked) {
	jQuery("#jqGrid").trigger('reloadGrid');
	
	var params = {};
	var itemparams = {};
	var productId = $('#proName').val();
	
	itemparams["productId"] = productId;
	itemparams["methodName"] = "getProductDetailsPO";
	$
			.post(
					'/Shop/jsp/utility/controller.jsp',
		itemparams,
		function(data) {
			var jsonData = $.parseJSON(data);
	
			function sumFmatter(cellvalue, options,
					rowObject) {
	
				var jam = 0;
				var jam1 = "";
				var tot = (options.rowData.quantity * options.rowData.buyPrice);
				// var shree =
				// document.poferti.grossTotal.value;// to
				// get gross total
	
				var count = jQuery("#jqGrid").jqGrid(
						'getGridParam', 'records');
				var allRowsInGrid1 = $('#jqGrid')
						.getGridParam('data');
				var AllRows = JSON
						.stringify(allRowsInGrid1);
				for (var i = 0; i < count; i++) {
	
					var quantity = allRowsInGrid1[i].quantity;
					params["quantity" + i] = quantity;
	
					var buyPrice = allRowsInGrid1[i].buyPrice;
					params["buyPrice" + i] = buyPrice;
	
					var totals1 = ((buyPrice) * (quantity));
	
					jam = jam + totals1;
	
				}
				if (count == 0) {
					document.getElementById("total").value = tot;
				} else {
					document.getElementById("total").value = jam;
				}
				// to check whether bill is with or without
				// VAT
				if (document.getElementById('retail').checked) {
	
					/*
					 * Gross total calculation without VAT
					 * for cash customer
					 */
					var expence = document
							.getElementById("extraExpence").value;
					totalWithExpence = Number(jam)
							+ Number(expence);
					var discount = $('#discount').val();
					var discountAmount = ((discount / 100) * totalWithExpence);
					var totalminusDiscount = totalWithExpence
							- discountAmount;
					document
							.getElementById("discountAmount").value = discountAmount;
	
					document.getElementById("grossTotal").value = totalminusDiscount;
	
					return tot;
				}
	
				else {
					/*
					 * Gross total calculation with VAT for
					 * cash customer
					 */
					/* Vat Amount calculation */
					var expence = document
							.getElementById("extraExpence").value;
					totalWithExpence = Number(jam)
							+ Number(expence);
					var discount = $('#discount').val();
					var discountAmount = ((discount / 100) * totalWithExpence);
					var totalminusDiscount = totalWithExpence
							- discountAmount;
					document
							.getElementById("discountAmount").value = discountAmount;
	
					document.getElementById("grossTotal").value = totalminusDiscount;
	
					return tot;
				}
			}
	
			$
					.each(
							jsonData,
							function(i, v) {
	
								$("#jqGrid")
										.jqGrid(
												{
	
													datatype : "local",
	
													colNames : [
															"Product ID",
															"Product Name",
															"Buy Price",
															"M.R.P",
															"Sale Price",
															"Packing",
															"Quantity",
															"Total",
															"BatchNo",
															"ExpiryDate" ],
	
													colModel : [
															{
																name : "productID",
																hidden : true
															// resizable:
															// true,
	
															},
															{
																name : "productName",
																width : 100,
	
															},
															{
																name : "buyPrice",
																width : 140,
																editable : true
															},
															{
																name : "mrp",
																width : 140,
																editable : true
															},
	
															{
																name : "salePrice",
																width : 140,
																editable : true
															},
	
															{
																name : "weight",
																width : 100,
	
															},
	
															{
																name : "quantity",
																width : 140,
																editable : true
	
															},
	
															{
																name : 'Total',
																// name:
																// "",
																formatter : sumFmatter,
																width : 150,
	
															},
															{
																label : 'batchNo',
																name : "batchNo",
																width : 150,
																editable : true,
	
															},
															{
																label : 'expiryDate',
																name : "expiryDate",
																index : 'Date',
																width : 200,
																editable : true,
																edittype : "text",
																editrules : {
																	date : true,
																	minValue : 0
																},
																datefmt : 'Y-m-d',
																editoptions : {
																	dataInit : function(
																			elem) {
																		$(
																				elem)
																				.datepicker(
																						{
																							dateFormat : "yy-mm-dd"
																						});
																	}
																}
															} ],
	
													sortorder : 'desc',
	
													multiselect : false,
													loadonce : false,
													rownumbers : true,
													'cellEdit' : true,
													afterSaveCell : function grossTotal() {
														/*
														 * Calculation
														 * of
														 * total
														 * after
														 * editing
														 * quantity
														 */
	
														// $(this).trigger('reloadGrid');
														var rowId = $(
																"#jqGrid")
																.jqGrid(
																		'getGridParam',
																		'selrow');
														var rowData = jQuery(
																"#jqGrid")
																.getRowData(
																		rowId);
														var quantity = rowData['quantity'];
														var buyPrice = rowData['buyPrice'];
	
														var tota = quantity* buyPrice;
														$(
																"#jqGrid")
																.jqGrid(
																		"setCell",
																		rowId,
																		"Total",
																		tota);
													},
	
													viewrecords : true,
													width : 1200,
													height : 250,
													rowNum : 10,
													pager : "#jqGridPager",
													sortorder : "desc",
													onSelectRow : function(
															productID) {
														if (productID
																&& productID !== lastsel) {
															jQuery(
																	'#jqGrid')
																	.jqGrid(
																			'restoreRow',
																			lastsel);
															jQuery(
																	'#jqGrid')
																	.jqGrid(
																			'editRow',
																			productID,
																			true);
															// jQuery('#jqGrid').jqGrid('editRow',productID,true,pickdates);
															lastsel = productID;
														}
													},
	
												});
	
								$("#jqGrid").addRowData(i,
										jsonData[i]);
								function pickdates(
										productID) {
									jQuery(
											"#"
													+ productID
													+ "_expiryDate",
											"#jqGrid")
											.datepicker(
													{
														dateFormat : "yyyy-mm-dd"
													});
								}
								/* jQuery("#jqGrid").jqGrid('navGrid',"#jqGridPager",{edit:false,add:false,del:true}); */
	
								$('#jqGrid')
										.navGrid(
												'#jqGridPager',
												// the
												// buttons
												// to appear
												// on the
												// toolbar
												// of the
												// grid
												{
													edit : true,
													add : false,
													del : true,
													search : true,
													refresh : true,
													view : true,
													position : "left",
													cloneToTop : false
												},
												// options
												// for the
												// Edit
												// Dialog
	
												{
	
													afterSaveCell : function() {
														$(
																this)
																.trigger(
																		'reloadGrid');
													},
													editCaption : "The Edit Dialog",
													recreateForm : true,
													checkOnUpdate : true,
													checkOnSubmit : true,
													closeAfteredit : true,
													errorTextFormat : function(
															data) {
														return 'Error: '
																+ data.responseText
													}
	
												},
	
												// options
												// for the
												// Delete
												// Dialogue
												{
													closeAfterdel : true,
													recreateForm : true,
													errorTextFormat : function(
															data) {
														return 'Error: '
																+ data.responseText
													},
	
													onSelectRow : function(
															id) {
														if (id
																&& id !== lastSel) {
															jQuery(
																	"#jqGrid")
																	.saveRow(
																			lastSel,
																			true,
																			'clientArray');
															jQuery(
																	"#jqGrid")
																	.editRow(
																			id,
																			true);
	
															lastSel = id;
															console
																	.log(id);
														}
													}
	
												});
	
								// grid refresh code
	
											});
	
						});
	
	}
	
	// Grid for Tax if retail is not checked
	else if (document.getElementById('vat').checked) {
	
	jQuery("#jqGrid").trigger('reloadGrid');
	
	var params = {};
	itemparams = {};
	productId = $('#proName').val();
	
	itemparams["productId"] = productId;
	itemparams["methodName"] = "getProductDetailsForGoodsReceiveForTax";
	$
			.post(
					'/Shop/jsp/utility/controller.jsp',
		itemparams,
	
		function(data) {
			var jsonData = $.parseJSON(data);
	
			function sumFmatter(cellvalue, options,
					rowObject) {
				var jam = 0;
				var jam1 = "";
				var tot = ((options.rowData.quantity) * ((options.rowData.buyPrice) + ((options.rowData.taxPercentage / 100) * options.rowData.buyPrice)));
				// var shree =
				// document.poferti.grossTotal.value;// to
				// get gross total
	
				var count = jQuery("#jqGrid").jqGrid(
						'getGridParam', 'records');
				var allRowsInGrid1 = $('#jqGrid')
						.getGridParam('data');
				var AllRows = JSON
						.stringify(allRowsInGrid1);
				for (var i = 0; i < count; i++) {
	
					var quantity = allRowsInGrid1[i].quantity;
					params["quantity" + i] = quantity;
	
					var buyPrice = allRowsInGrid1[i].buyPrice;
					params["buyPrice" + i] = buyPrice;
	
					var taxPercentage = allRowsInGrid1[i].taxPercentage;
					params["taxPercentage" + i] = taxPercentage;
	
					var taxAmount = ((taxPercentage / 100) * buyPrice);
	
					var priceWithTaxamount = taxAmount
							+ buyPrice;
	
					var totals1 = ((priceWithTaxamount) * (quantity));
	
					jam = jam + totals1;
	
					document.getElementById("total").value = jam;
				}
				if (count == 0) {
					document.getElementById("total").value = tot;
				} else {
					document.getElementById("total").value = jam;
				}
	
				// to check whether bill is with or without
				// VAT
				if (document.getElementById('retail').checked) {
	
					/*
					 * Gross total calculation without VAT
					 * for cash customer
					 */
					var expence = document
							.getElementById("extraExpence").value;
					totalWithExpence = Number(jam)
							+ Number(expence);
					var discount = $('#discount').val();
					var discountAmount = ((discount / 100) * totalWithExpence);
					var totalminusDiscount = totalWithExpence
							- discountAmount;
					document
							.getElementById("discountAmount").value = discountAmount;
	
					document.getElementById("grossTotal").value = totalminusDiscount;
	
					return tot;
				}
	
				else {
					/*
					 * Gross total calculation with VAT for
					 * cash customer
					 */
					/* Vat Amount calculation */
					var expence = document
							.getElementById("extraExpence").value;
					totalWithExpence = Number(jam)
							+ Number(expence);
					var discount = $('#discount').val();
					var discountAmount = ((discount / 100) * totalWithExpence);
					var totalminusDiscount = totalWithExpence
							- discountAmount;
					document
							.getElementById("discountAmount").value = discountAmount;
	
					document.getElementById("grossTotal").value = totalminusDiscount;
	
					return tot;
				}
			}
	
			$
					.each(
							jsonData,
							function(i, v) {
	
								var productName = v.productName;
								var buyPrice = v.buyPrice;
								var salePrice = v.salePrice;
								var quantity = v.quantity;
								var weight = v.weight;
								var productID = v.productID;
								var lastsel = productID;
	
								$("#jqGrid")
										.jqGrid(
												{
	
													datatype : "local",
	
													colNames : [
															"Product ID",
															"Product Name",
															"Buy Price",
															"Tax %",
															"M.R.P",
															"Sale Price",
															"Packing",
															"Quantity",
															"Total",
															"BatchNo",
															"ExpiryDate" ],
	
													colModel : [
															{
																name : "productID",
																hidden : true
															// resizable:
															// true,
	
															},
															{
																name : "productName",
																width : 100,
	
															},
															{
																name : "buyPrice",
																width : 120,
																editable : true
															},
															{
																name : "taxPercentage",
																width : 140,
																editable : true
															},
															{
																name : "mrp",
																width : 140,
																editable : true
															},
															{
																name : "salePrice",
																width : 140,
																editable : true
															},
	
															{
																name : "weight",
																width : 100,
	
															},
	
															{
																name : "quantity",
																width : 120,
																editable : true
	
															},
	
															{
																name : 'Total',
																// name:
																// "",
																formatter : sumFmatter,
																width : 150,
	
															},
															{
																label : 'batchNo',
																name : "batchNo",
																width : 150,
																editable : true,
	
															},
															{
																label : 'expiryDate',
																name : "expiryDate",
																index : 'Date',
																width : 200,
																editable : true,
																edittype : "text",
																editrules : {
																	date : true,
																	minValue : 0
																},
																datefmt : 'Y-m-d',
																editoptions : {
																	dataInit : function(
																			elem) {
																		$(
																				elem)
																				.datepicker(
																						{
																							dateFormat : "yy-mm-dd"
																						});
																	}
																}
															} ],
	
													sortorder : 'desc',
	
													multiselect : false,
													loadonce : false,
													rownumbers : true,
													'cellEdit' : true,
													afterSaveCell : function grossTotal() {
														/*
														 * Calculation
														 * of
														 * total
														 * after
														 * editing
														 * quantity
														 */
	
														// $(this).trigger('reloadGrid');
														var rowId = $(
																"#jqGrid")
																.jqGrid(
																		'getGridParam',
																		'selrow');
														var rowData = jQuery(
																"#jqGrid")
																.getRowData(
																		rowId);
														var quantity = rowData['quantity'];
														var buyPrice = rowData['buyPrice'];
														var taxPercentage = rowData['taxPercentage'];
														var taxAmount = ((taxPercentage / 100) * buyPrice);
														var BuyPriceWithTaxAmount = taxAmount
																+ buyPrice;
														var tota = quantity
																* BuyPriceWithTaxAmount;
														$(
																"#jqGrid")
																.jqGrid(
																		"setCell",
																		rowId,
																		"Total",
																		tota);
													},
	
													viewrecords : true,
													width : 1200,
													height : 250,
													rowNum : 10,
													pager : "#jqGridPager",
													sortorder : "desc",
													onSelectRow : function(
															productID) {
														if (productID
																&& productID !== lastsel) {
															jQuery(
																	'#jqGrid')
																	.jqGrid(
																			'restoreRow',
																			lastsel);
															jQuery(
																	'#jqGrid')
																	.jqGrid(
																			'editRow',
																			productID,
																			true);
															// jQuery('#jqGrid').jqGrid('editRow',productID,true,pickdates);
															lastsel = productID;
														}
													},
	
												});
	
								$("#jqGrid").addRowData(i,
										jsonData[i]);
								function pickdates(
										productID) {
									jQuery(
											"#"
													+ productID
													+ "_expiryDate",
											"#jqGrid")
											.datepicker(
													{
														dateFormat : "yyyy-mm-dd"
													});
								}
								/* jQuery("#jqGrid").jqGrid('navGrid',"#jqGridPager",{edit:false,add:false,del:true}); */
	
								$('#jqGrid')
										.navGrid(
												'#jqGridPager',
												// the
												// buttons
												// to appear
												// on the
												// toolbar
												// of the
												// grid
												{
													edit : true,
													add : false,
													del : true,
													search : true,
													refresh : true,
													view : true,
													position : "left",
													cloneToTop : false
												},
												// options
												// for the
												// Edit
												// Dialog
	
												{
	
													afterSaveCell : function() {
														$(
																this)
																.trigger(
																		'reloadGrid');
													},
													editCaption : "The Edit Dialog",
													recreateForm : true,
													checkOnUpdate : true,
													checkOnSubmit : true,
													closeAfteredit : true,
													errorTextFormat : function(
															data) {
														return 'Error: '
																+ data.responseText
													}
	
												},
	
												// options
												// for the
												// Delete
												// Dialogue
												{
													closeAfterdel : true,
													recreateForm : true,
													errorTextFormat : function(
															data) {
														return 'Error: '
																+ data.responseText
													},
	
													onSelectRow : function(
															id) {
														if (id
																&& id !== lastSel) {
															jQuery(
																	"#jqGrid")
																	.saveRow(
																			lastSel,
																			true,
																			'clientArray');
															jQuery(
																	"#jqGrid")
																	.editRow(
																			id,
																			true);
	
															lastSel = id;
															console
																	.log(id);
														}
													}
	
												});
	
								// grid refresh code
	
												});
	
							});
	
		}
	
	}
	
	/** ************** Grid for Booked Goods Receive new ******* */
	
	function getAllProductDetailsForBooked() {
	
		/*
	 * //Grid for Retail if it is checked if
	 * (document.getElementById('retail1').checked) {
	 * jQuery("#jqGrid1").trigger('reloadGrid');
	 * 
	 * var input1 = document.getElementById('fk_cat_id1'), list =
	 * document.getElementById('cat_drop1'), i,fk_cat_id; for (i = 0; i <
	 * list.options.length; ++i) { if (list.options[i].value ===
	 * input1.value) { fk_cat_id =
	 * list.options[i].getAttribute('data-value'); } }
	 * 
	 * var input1 = document.getElementById('supplier1'), list =
	 * document.getElementById('sup_drop1'), i,fk_sup_id; for (i = 0; i <
	 * list.options.length; ++i) { if (list.options[i].value ===
	 * input1.value) { fk_sup_id =
	 * list.options[i].getAttribute('data-value'); } }
	 * 
	 * var fk_cat_id = fk_cat_id; var fk_sup_id = fk_sup_id;
	 * 
	 * var params= {}; itemparams={}; productId = $('#proName1').val();
	 * 
	 * $("#proName1 option:selected").each(function() { selectedVal =
	 * $(this).text(); });
	 * 
	 * var splitText = selectedVal.split(",");
	 * 
	 * var proName = splitText[0]; var company = splitText[1]; var weight =
	 * splitText[2];
	 * 
	 * itemparams["proName"]= proName; itemparams["company"]= company;
	 * itemparams["weight"]= weight;
	 * 
	 * itemparams["fk_cat_id"]= fk_cat_id; itemparams["fk_sup_id"]=
	 * fk_sup_id;
	 * 
	 * var count=0; var newrow; var rowId;
	 * 
	 * itemparams["methodName"] =
	 * "getbookedProductDetailsForGoodsReceiveNew";
	 * $.post('/Fertilizer/jsp/utility/controller.jsp',itemparams,
	 * 
	 * 
	 * function(data) { var jsonData = $.parseJSON(data);
	 * 
	 * 
	 * 
	 * function sumFmatter (cellvalue, options, rowObject) {
	 * 
	 * 
	 * 
	 * var jam=0; var jam1=""; var tot= (options.rowData.quantity *
	 * options.rowData.buyPrice); //var shree =
	 * document.poferti.grossTotal.value;// to get gross total
	 * 
	 * var count = jQuery("#jqGrid1").jqGrid('getGridParam', 'records'); var
	 * allRowsInGrid1 = $('#jqGrid1').getGridParam('data'); var
	 * AllRows=JSON.stringify(allRowsInGrid1); for (var i = 0; i < count;
	 * i++) {
	 * 
	 * var quantity = allRowsInGrid1[i].quantity; params["quantity"+i] =
	 * quantity;
	 * 
	 * var buyPrice = allRowsInGrid1[i].buyPrice; params["buyPrice"+i] =
	 * buyPrice;
	 * 
	 * 
	 * var totals1=((buyPrice)*(quantity));
	 * 
	 * jam = jam + totals1;
	 * 
	 *  } if(count == 0){ document.getElementById("total1").value = tot;
	 * }else{ document.getElementById("total1").value = jam;
	 *  }
	 * 
	 * 
	 * return tot;
	 *  }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * $.each(jsonData,function(i,v) {
	 * 
	 * $("#jqGrid1").jqGrid({
	 * 
	 * 
	 * 
	 * datatype:"local",
	 * 
	 * colNames: ["Product ID","Product Name","Company","Buy
	 * Price","M.R.P","Sale
	 * Price","Packing","Quantity","Total","BatchNo","ExpiryDate" ],
	 * 
	 * colModel: [ { name: "productID", hidden:true //resizable: true, }, {
	 * name: "productName", width:120,
	 *  }, { name: "manufacturer", width:120
	 *  }, { name: "buyPrice", width: 140, editable: true }, { name: "mrp",
	 * width: 140, editable: true }, { name: "salePrice", width: 140,
	 * editable: true },
	 *  { name: "weight", width: 100,
	 *  },
	 *  { name: "quantity", width: 140, editable: true
	 * 
	 *  },
	 *  { name : 'Total', // name: "", formatter: sumFmatter, width: 150,
	 *  }, { label : 'batchNo', name: "batchNo", width: 150, editable:true,
	 * 
	 *  }, { label : 'expiryDate', name: "expiryDate", index:'Date', width:
	 * 200, editable:true, edittype:"text", editrules:{date:true,
	 * minValue:0}, datefmt:'Y-m-d', editoptions:{dataInit: function (elem)
	 * {$(elem).datepicker({dateFormat:"yy-mm-dd"});} } } ],
	 * 
	 * 
	 * sortorder : 'desc',
	 * 
	 * multiselect: false, loadonce: false, rownumbers:true,
	 * 'cellEdit':true, afterSaveCell: function grossTotal() { Calculation
	 * of total after editing quantity
	 *  // $(this).trigger('reloadGrid'); var rowId
	 * =$("#jqGrid1").jqGrid('getGridParam','selrow'); var rowData =
	 * jQuery("#jqGrid1").getRowData(rowId); var quantity =
	 * rowData['quantity']; var buyPrice = rowData['buyPrice'];
	 * 
	 * var tota = quantity * buyPrice; $("#jqGrid1").jqGrid("setCell",
	 * rowId, "Total", tota); },
	 * 
	 * 
	 * 
	 * 
	 * viewrecords: true, width: 1200, height: 250, rowNum: 10, pager:
	 * "#jqGridPager1", sortorder: "desc", onSelectRow: function(productID){
	 * if(productID && productID!==lastsel){
	 * jQuery('#jqGrid1').jqGrid('restoreRow',lastsel);
	 * jQuery('#jqGrid1').jqGrid('editRow',productID,true);
	 * //jQuery('#jqGrid').jqGrid('editRow',productID,true,pickdates);
	 * lastsel=productID; } },
	 * 
	 * });
	 * 
	 * count = jQuery("#jqGrid1").jqGrid('getGridParam', 'records'); var
	 * rowdata =$("#jqGrid1").jqGrid('getGridParam','data'); var ids =
	 * jQuery("#jqGrid1").jqGrid('getDataIDs');
	 * 
	 * 
	 * var prodName,com,packing; for (var j = 0; j < count; j++) { prodName =
	 * rowdata[j].productName; com = rowdata[j].manufacturer; packing =
	 * rowdata[j].weight;
	 * 
	 * var rowId = ids[j]; var rowData = jQuery('#jqGrid1').jqGrid
	 * ('getRowData', rowId);
	 * 
	 * if (prodName == jsonData[i].productName && com ==
	 * jsonData[i].manufacturer && packing == jsonData[i].weight) {
	 * 
	 * newrow=false; alert("Product Name Already Inserted !!!"); var grid =
	 * jQuery("#jqGrid1"); grid.trigger("reloadGrid"); break; } else {
	 * newrow = true; } }
	 * 
	 * if(newrow == true) {
	 *  // $("#credit").addRowData(i,jsonData[i]);
	 * $("#jqGrid1").addRowData(count,jsonData[i]);
	 *  }
	 * 
	 * if(count==0 || count==null) { //
	 * $("#credit").addRowData(i,jsonData[i]);
	 * $("#jqGrid1").addRowData(0,jsonData[i]); }
	 * //$("#jqGrid1").addRowData(i,jsonData[i]); function
	 * pickdates(productID){
	 * jQuery("#"+productID+"_expiryDate","#jqGrid1").datepicker({dateFormat:"yy-mm-dd"}); }
	 * jQuery("#jqGrid").jqGrid('navGrid',"#jqGridPager",{edit:false,add:false,del:true});
	 * 
	 * 
	 * 
	 * $('#jqGrid1').navGrid('#jqGridPager1', // the buttons to appear on
	 * the toolbar of the grid {edit: true, add: false,del: true, search:
	 * true, refresh: true, view: true, position: "left", cloneToTop: false }, //
	 * options for the Edit Dialog
	 * 
	 * 
	 * 
	 * 
	 *  {
	 * 
	 * afterSaveCell: function () { $(this).trigger('reloadGrid'); },
	 * editCaption: "The Edit Dialog", recreateForm: true, checkOnUpdate :
	 * true, checkOnSubmit : true, closeAfteredit: true, errorTextFormat:
	 * function (data) { return 'Error: ' + data.responseText }
	 * 
	 * 
	 *  },
	 * 
	 * 
	 *  // options for the Delete Dialogue { closeAfterdel:true,
	 * recreateForm: true, errorTextFormat: function (data) { return 'Error: ' +
	 * data.responseText },
	 * 
	 * onSelectRow: function(id) { if (id && id !== lastSel) {
	 * jQuery("#jqGrid1").saveRow(lastSel, true, 'clientArray');
	 * jQuery("#jqGrid1").editRow(id, true);
	 * 
	 * lastSel = id; console.log(id); } }
	 * 
	 * 
	 * });
	 *  // grid refresh code
	 * 
	 * });
	 * 
	 * 
	 * });
	 *  }
	 */
	/*
	 * //Grid for Tax if retail is not checked else if
	 * (document.getElementById('vat1').checked) {
	 */
	
	jQuery("#jqGrid1").trigger('reloadGrid');
	
	var input1 = document.getElementById('fk_cat_id1'), list = document
	.getElementById('cat_drop1'), i, fk_cat_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_cat_id = list.options[i].getAttribute('data-value');
		}
	}
	
	var input1 = document.getElementById('supplier1'), list = document
	.getElementById('sup_drop1'), i, fk_sup_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_sup_id = list.options[i].getAttribute('data-value');
		}
	}
	
	var fk_cat_id = fk_cat_id;
	var fk_sup_id = fk_sup_id;
	
	var params = {};
	itemparams = {};
	productId = $('#proName1').val();
	
	$("#proName1 option:selected").each(function() {
		selectedVal = $(this).text();
	});
	
	var splitText = selectedVal.split(",");
	
	var proName = splitText[0];
	var company = splitText[1];
	var weight = splitText[2];
	
	itemparams["proName"] = proName;
	itemparams["company"] = company;
	itemparams["weight"] = weight;
	
	itemparams["fk_cat_id"] = fk_cat_id;
	itemparams["fk_sup_id"] = fk_sup_id;
	
	var count = 0;
	var newrow;
	var rowId;
	
	itemparams["methodName"] = "getProductDetailsForGoodsReceiveForBookedWithTaxNew";
	$
			.post(
					'/Shop/jsp/utility/controller.jsp',
	itemparams,
	
	function(data) {
		var jsonData = $.parseJSON(data);
	
		function sumFmatter(cellvalue, options, rowObject) {
	
			var jam = 0;
			var jam1 = "";
			var tot = ((options.rowData.quantity) * ((options.rowData.buyPrice) + ((options.rowData.taxPercentage / 100) * options.rowData.buyPrice)));
			// var shree =
			// document.poferti.grossTotal.value;// to get
			// gross total
	
			var count = jQuery("#jqGrid1").jqGrid(
					'getGridParam', 'records');
			var allRowsInGrid1 = $('#jqGrid1')
					.getGridParam('data');
			var AllRows = JSON.stringify(allRowsInGrid1);
			for (var i = 0; i < count; i++) {
	
				var quantity = allRowsInGrid1[i].quantity;
				params["quantity" + i] = quantity;
	
				var buyPrice = allRowsInGrid1[i].buyPrice;
				params["buyPrice" + i] = buyPrice;
	
				var taxPercentage = allRowsInGrid1[i].taxPercentage;
				params["taxPercentage" + i] = taxPercentage;
	
				var taxAmount = ((taxPercentage / 100) * buyPrice);
	
				var priceWithTaxamount = taxAmount
						+ buyPrice;
	
				var totals1 = ((priceWithTaxamount) * (quantity));
	
				jam = jam + totals1;
	
				/*
				 * document.getElementById("total").value =
				 * jam;
				 * document.getElementById("duptotal").value =
				 * jam;
				 */
			}
			if (count == 0) {
				document.getElementById("total1").value = tot;
	
			} else {
				document.getElementById("total1").value = jam;
	
			}
	
			return tot;
		}
	
		$
				.each(
						jsonData,
						function(i, v) {
	
							var productName = v.productName;
							var buyPrice = v.buyPrice;
							var salePrice = v.salePrice;
							var quantity = v.quantity;
							var weight = v.weight;
							var productID = v.productID;
							var manufacturer = v.manufacturer;
							var lastsel = productID;
	
							$("#jqGrid1")
									.jqGrid(
											{
	
												datatype : "local",
	
												colNames : [
														"Product ID",
														"Product Name",
														"Company",
														"Buy Price",
														"M.R.P",
														"Tax %",
														"Sale Price",
														"Packing",
														"Quantity",
														"Total",
														"BatchNo",
														"ExpiryDate" ],
	
												colModel : [
														{
															name : "productID",
															hidden : true
														// resizable:
														// true,
	
														},
														{
															name : "productName",
															width : 120,
	
														},
														{
															name : "manufacturer",
															width : 120
	
														},
														{
															name : "buyPrice",
															width : 120,
															editable : true
														},
														{
															name : "mrp",
															width : 120,
															editable : true
														},
														{
															name : "taxPercentage",
															width : 140,
															editable : true
														},
	
														{
															name : "salePrice",
															width : 140,
															editable : true
														},
	
														{
															name : "weight",
															width : 100,
	
														},
	
														{
															name : "quantity",
															width : 120,
															editable : true
	
														},
	
														{
															name : 'Total',
															// name:
															// "",
															formatter : sumFmatter,
															width : 150,
	
														},
														{
															label : 'batchNo',
															name : "batchNo",
															width : 150,
															editable : true,
	
														},
														{
															label : 'expiryDate',
															name : "expiryDate",
															index : 'Date',
															width : 200,
															editable : true,
															edittype : "text",
															editrules : {
																date : true,
																minValue : 0
															},
															datefmt : 'Y-m-d',
															editoptions : {
																dataInit : function(
																		elem) {
																	$(
																			elem)
																			.datepicker(
																					{
																						dateFormat : "yy-mm-dd"
																					});
																}
															}
														} ],
	
												sortorder : 'desc',
	
												multiselect : false,
												loadonce : false,
												rownumbers : true,
												'cellEdit' : true,
	
												afterSaveCell : function grossTotal() {
													/*
													 * Calculation
													 * of
													 * total
													 * after
													 * editing
													 * quantity
													 */
	
													// $(this).trigger('reloadGrid');
													var rowId = $(
															"#jqGrid1")
															.jqGrid(
																	'getGridParam',
																	'selrow');
													var rowData = jQuery(
															"#jqGrid1")
															.getRowData(
																	rowId);
													var quantity = rowData['quantity'];
													var buyPrice = rowData['buyPrice'];
													var taxPercentage = rowData['taxPercentage'];
													var taxAmount = ((taxPercentage / 100) * buyPrice);
													var BuyPriceWithTaxAmount = taxAmount
															+ buyPrice;
													var tota = quantity
															* BuyPriceWithTaxAmount;
													$(
															"#jqGrid1")
															.jqGrid(
																	"setCell",
																	rowId,
																	"Total",
																	tota);
												},
	
												viewrecords : true,
												width : 1200,
												height : 250,
												rowNum : 10,
												pager : "#jqGridPager1",
												sortorder : "desc",
	
												onSelectRow : function(
														productID) {
													if (productID
															&& productID !== lastsel) {
														jQuery(
																'#jqGrid1')
																.jqGrid(
																		'restoreRow',
																		lastsel);
														jQuery(
																'#jqGrid1')
																.jqGrid(
																		'editRow',
																		productID,
																		true);
														// jQuery('#jqGrid').jqGrid('editRow',productID,true,pickdates);
														lastsel = productID;
													}
												},
	
											});
	
							count = jQuery("#jqGrid1")
									.jqGrid('getGridParam',
											'records');
							var rowdata = $("#jqGrid1")
									.jqGrid('getGridParam',
											'data');
							var ids = jQuery("#jqGrid1")
									.jqGrid('getDataIDs');
	
							var prodName, com, packing;
							for (var j = 0; j < count; j++) {
								prodName = rowdata[j].productName;
								com = rowdata[j].manufacturer;
								packing = rowdata[j].weight;
	
								var rowId = ids[j];
								var rowData = jQuery(
										'#jqGrid1')
										.jqGrid(
												'getRowData',
												rowId);
	
								if (prodName == jsonData[i].productName
										&& com == jsonData[i].manufacturer
										&& packing == jsonData[i].weight) {
	
									newrow = false;
									alert("Product Name Already Inserted !!!");
									var grid = jQuery("#jqGrid1");
									grid
											.trigger("reloadGrid");
									break;
								} else {
									newrow = true;
								}
							}
	
							if (newrow == true) {
	
								// $("#credit").addRowData(i,jsonData[i]);
								$("#jqGrid1").addRowData(
										count, jsonData[i]);
	
							}
	
							if (count == 0 || count == null) {
								// $("#credit").addRowData(i,jsonData[i]);
								$("#jqGrid1").addRowData(0,
										jsonData[i]);
							}
							// $("#jqGrid1").addRowData(i,jsonData[i]);
							function pickdates(productID) {
								jQuery("#"
												+ productID
												+ "_expiryDate",
										"#jqGrid1")
										.datepicker(
												{
													dateFormat : "yyyy-mm-dd"
												});
							}
							/* jQuery("#jqGrid").jqGrid('navGrid',"#jqGridPager",{edit:false,add:false,del:true}); */
	
							$('#jqGrid1')
									.navGrid(
											'#jqGridPager1',
											// the buttons
											// to appear on
											// the toolbar
											// of the grid
											{
												edit : true,
												add : false,
												del : true,
												search : true,
												refresh : true,
												view : true,
												position : "left",
												cloneToTop : false
											},
											// options for
											// the Edit
											// Dialog
	
											{
	
												afterSaveCell : function() {
													$(this).trigger('reloadGrid');
												},
												editCaption : "The Edit Dialog",
												recreateForm : true,
												checkOnUpdate : true,
												checkOnSubmit : true,
												closeAfteredit : true,
												errorTextFormat : function(data) 
												{
													return 'Error: '+ data.responseText
												}
	
											},
	
											// options for
											// the Delete
											// Dialogue
											{
												closeAfterdel : true,
												recreateForm : true,
												errorTextFormat : function(data) 
												{
													return 'Error: '+ data.responseText
												},
	
												onSelectRow : function(id) 
												{
													if (id&& id !== lastSel) {
														jQuery("#jqGrid1").saveRow(lastSel,true,'clientArray');
														jQuery("#jqGrid1").editRow(id,true);
	                                                    lastSel = id;
														console.log(id);
													}
												}
	
											});
	
							// grid refresh code
	
												});
	
							});
	
		}
	
	}
	
	var prodetails = new getdetails();
	
	// calculation for Without booked
	
	// Calculations for Booked goods receive
	function discountCalculationForBooked() {
		var total = document.getElementById("total1").value;
	var discount = $('#discount1').val();
	var discountAmount = ((discount / 100) * Number(total));
	var totalminusDiscount = Number(total) - discountAmount;
	document.getElementById("discountAmount1").value = discountAmount;
	document.getElementById("grossTotal1").value = totalminusDiscount;
	}
	
	function hamaliExpenseAddingToGrossForBooked() {
		var hamaliExpence = document.getElementById("hamaliExpence1").value;
	// Gross total calculation
	var discount = $('#discount1').val();
	var transExpence = document.getElementById("transExpence1").value;
	
	if (discount == "") {
	
	if (transExpence == "") {
	var total = document.getElementById("total1").value;
	var totalWithExpense = Number(total) + Number(hamaliExpence);
	document.getElementById("grossTotal1").value = totalWithExpense;
	}
	if (transExpence != "") {
	var total = document.getElementById("total1").value;
	var totalWithExpense = Number(total) + Number(transExpence);
	var totalWithExpense1 = Number(totalWithExpense)
			+ Number(hamaliExpence);
	document.getElementById("grossTotal1").value = totalWithExpense1;
		}
	}
	
	if (discount != "") {
	var transExpence = document.getElementById("transExpence1").value;
	if (transExpence == "") {
	var total = document.getElementById("total1").value;
	var discountAmount = ((discount / 100) * Number(total));
	var totalminusDiscount = Number(total) - Number(discountAmount);
	var totalWithExpense = Number(totalminusDiscount)
			+ Number(hamaliExpence);
	document.getElementById("grossTotal1").value = totalWithExpense;
	}
	if (transExpence != "") {
	var total = document.getElementById("total1").value;
	var discountAmount = ((discount / 100) * Number(total));
	var totalminusDiscount = Number(total) - discountAmount;
	var totalwithTrans = Number(totalminusDiscount)
			+ Number(transExpence);
	var totalWithExpense = Number(totalwithTrans)
			+ Number(hamaliExpence);
	document.getElementById("grossTotal1").value = totalWithExpense;
			}
		}
	
	}
	
	function transExpenseAddingToGrossTotalForBooked() {
		var transExpence = document.getElementById("transExpence1").value;
	var hamaliExpence = document.getElementById("hamaliExpence1").value;
	var discount = $('#discount1').val();
	
	if (discount == "") {
	if (hamaliExpence == "") {
	var total = document.getElementById("total1").value;
	var totalWithExpense = Number(total) + Number(transExpence);
	document.getElementById("grossTotal1").value = totalWithExpense;
	}
	
	if (hamaliExpence != "") {
	var total = document.getElementById("total1").value;
	var hamaliTotal = Number(total) + Number(hamaliExpence);
	var totalWithExpense = Number(transExpence) + Number(hamaliTotal);
	document.getElementById("grossTotal1").value = totalWithExpense;
		}
	}
	
	if (discount != "") {
	if (hamaliExpence == "") {
	var total = document.getElementById("total1").value;
	var discountAmount = ((discount / 100) * Number(total));
	var totalminusDiscount = Number(total) - Number(discountAmount);
	var totalWithExpense = Number(totalminusDiscount)
			+ Number(transExpence);
	document.getElementById("grossTotal1").value = totalWithExpense;
	}
	if (hamaliExpence != "") {
	var total = document.getElementById("total1").value;
	var discountAmount = ((discount / 100) * Number(total));
	var totalminusDiscount = Number(total) - Number(discountAmount);
	var totalwithTrans = Number(totalminusDiscount)
			+ Number(hamaliExpence);
	var totalWithExpense = Number(totalwithTrans)
			+ Number(transExpence);
	document.getElementById("grossTotal1").value = totalWithExpense;
			}
	
		}
	
	}
	
	/* ++++++++ Grid code for Without Advance Booking by shrimant ++++++++++ */
	
	function productDetailInGrid()
	{
		/* if (document.getElementById('retail').checked) { */
	var params = {};
	itemparams = {};
	
	var input1 = document.getElementById('fk_cat_id'),
	list = document.getElementById('cat_drop'),
	i,fk_cat_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_cat_id= list.options[i].getAttribute('data-value');
		}
	}
	//alert("fk_cat_id---------"+fk_cat_id);
	
	toDate = $('#purchaseDate').val();
	
	var d = toDate.split("-");
	
	var todayDate = d[2]+"/"+d[1]+"/"+d[0];
	
	
	productId = $('#subCat').val();
	
	/*$("#subCat option:selected").each(function() {
		selectedVal = $(this).text();
	});*/
	var splitText = productId.split(",");
	
	
	/*var splitText = selectedVal.split(",");*/
	var fk_subCat_id = splitText[0];
	
	
	productId = $('#proName').val();
	
	/*$("#proName option:selected").each(function() {
		selectedVal = $(this).text();
	});*/
	
	var splitText = productId.split(",");
	
	var proName = splitText[0];
	
	
	/*alert("proName---------"+proName);
	alert("productId---------"+productId);
	*/
	itemparams["proName"] = proName;
	itemparams["fk_cat_id"] = fk_cat_id;
	itemparams["fk_subCat_id"] = fk_subCat_id;
	itemparams["productId"] = productId;
	
	var count = 0;
	var newrow;
	var rowId;
	var taxPercentage;
	itemparams["methodName"] = "getProductDetailsForGoodsReceiveForTax";
	$.post('/Shop/jsp/utility/controller.jsp',itemparams,
	
			function(data) {
		var jsonData = $.parseJSON(data);
	
		/*
	 * function sumFmatter (cellvalue, options, rowObject) {
	 * 
	 * var jam=0; var jam1=""; var tot=
	 * ((options.rowData.quantity) *
	 * ((options.rowData.buyPrice)+((options.rowData.taxPercentage/100)*options.rowData.buyPrice)));
	 * //var shree = document.poferti.grossTotal.value;// to
	 * get gross total
	 * 
	 * var count = jQuery("#jqGrid").jqGrid('getGridParam',
	 * 'records'); var allRowsInGrid1 =
	 * $('#jqGrid').getGridParam('data'); var
	 * AllRows=JSON.stringify(allRowsInGrid1); for (var i =
	 * 0; i < count; i++) {
	 * 
	 * var quantity = allRowsInGrid1[i].quantity;
	 * params["quantity"+i] = quantity;
	 * 
	 * var buyPrice = allRowsInGrid1[i].buyPrice;
	 * params["buyPrice"+i] = buyPrice;
	 * 
	 * var cGst = allRowsInGrid1[i].cGst; params["cGst"+i] =
	 * cGst;
	 * 
	 * var sGst = allRowsInGrid1[i].sGst; params["sGst"+i] =
	 * sGst;
	 * 
	 * var iGst = allRowsInGrid1[i].iGst; params["iGst"+i] =
	 * iGst;
	 * 
	 * if(iGst == 0){ taxPercentage = cGst + sGst;
	 * 
	 * var taxAmount = ((taxPercentage/100)*buyPrice);
	 * 
	 * var priceWithTaxamount = taxAmount+buyPrice;
	 * 
	 * var totals1=((priceWithTaxamount)*(quantity));
	 * 
	 * jam = jam + totals1; } else if (iGst != 0){
	 * taxPercentage = iGst; var taxAmount =
	 * ((taxPercentage/100)*buyPrice);
	 * 
	 * var priceWithTaxamount = taxAmount+buyPrice;
	 * 
	 * var totals1=((priceWithTaxamount)*(quantity));
	 * 
	 * jam = jam + totals1; }
	 * document.getElementById("total").value = jam;
	 * document.getElementById("duptotal").value = jam; }
	 * if(count == 0){
	 * document.getElementById("total").value = tot;
	 * document.getElementById("duptotal").value = tot; }
	 * else{ document.getElementById("total").value = jam;
	 * document.getElementById("duptotal").value = jam; }
	 * 
	 * 
	 * 
	 * return tot; }
	 */
	
	$.each(
					jsonData,
					function(i, v) {
						var catIDforVAt = v.catIDforVAt;
						var productName = v.productName;
						var buyPrice = v.buyPrice;
						var salePrice = v.salePrice;
						var quantity = v.quantity;
						var weight = v.weight;
						var productID = v.productID;
						var manufacturer = v.manufacturer;
						var hsn = v.hsn;
						var lastsel = productID;
						var subCatId = v.subCatId;
						var unitName = v.unitName;
						var gst = v.gst;
	
						$("#jqGrid").jqGrid(
										{datatype : "local",
	
											colNames : [
													"Cat ID",
													"Product ID",
													"Product<br>Name",
													"Company",
													"HSN",
													"Buy<br>Price Per<br>Packing",
													"Buy<br>Price Ex<br>Tax",
													"MRP<br>Per<br>Unit",
													"Sale<br>Price per<br>Unit",
													/*"sale<br>Price<br>Ex.Tax",*/
													"Packing",
													"Unit",
													/*"Shortage",*/
													"Total<br>Packing",
													"Discount",
													"Discount<br>Amount",
													"Total<br>Ex<br>Tax",
													"GST%",
													"IGST%",
													"Qty",
													"Batch<br>No.",
													"Expiry<br>Date",
													"Total",
													"SubCatid" ],
											// colNames: [
											// "Cat
											// ID","Product
											// ID","Product
											// Name","Company","HSN","Buy
											// Price","M.R.P","Sale
											// Price","Weight","Shortage","Total
											// Weight","GST
											// %","IGST
											// %","Quantity","Total"
											// ],
	
											colModel : [
													{
														name : "catIDforVAt",
														width : 120,
														hidden : true,
														align : 'center',
													// resizable:
													// true,
													},
													{
														name : "productID",
														width : 120,
														hidden : true,
														align : 'center',
													// resizable:
													// true,
													},
													{
														name : "productName",
														width : 120,
														align : 'center',
	
													},
	
													{
														name : "manufacturer",
														width : 140,
														align : 'center',
	
													},
													{
														name : "hsn",
														width : 120,
														align : 'center',
	
													},
													{
														name : "buyPrice",
														width : 120,
														editable : true,
														align : 'center',
													},
													
													{
														name : "buyPriceEx",
														width : 120,
														editable : false,
														align : 'center',
													},
													
													
	
													{
														name : "mrp",
														width : 90,
														editable : true,
														align : 'center',
													},
													{
														name : "salePrice",
														width : 115,
														editable : true,
														align : 'center',
													},
													
													/*{
														name : "salepriceEx",
														width : 115,
														//editable : true,
														align : 'center',
													},
	*/
													
													{
														name : "weight",
														width : 100,
														align : 'center',
													},
													{
														name : 'unitName',
														// name:
														// "",
														// formatter:
														// sumFmatter,
														width : 70,
														// hidden:true,
														align : 'center',
													},
	
													/*{
														name : "dcNum", // dcnum
																		// used
																		// for
																		// shortage
														width : 140,
														editable : true,
														align : 'center',
													},*/
	
													{
														name : "weightAftShortMinus",
														width : 100,
														align : 'center',
													},
													
													{
														name : "Discount",
														width : 80,
														align : 'center',
														editable : true
													},
													
													

													{
														name : "DiscountAmount",
														width : 80,
														align : 'center',
														editable : false
													},
													
													{
														name : "TotalExTax",
														width : 80,
														align : 'center',
														editable : true
													},
													
													{
														name : "gst",
														width : 80,
														align : 'center',
														editable : true
													},
													/*
													 * {
													 * name:
													 * "sGst",
													 * width:
													 * 80,
													 * editable:
													 * true },
													 */
													{
														name : "igst",
														width : 85,
														align : 'center',
														editable : true
													},
													{
														name : "quantity",
														width : 90,
														editable :true,
														align : 'center',
													},
													{
														id: "batchNo",
														name : "batchNo",
														width : 110,
														editable : true,
														align : 'center',
													},
													{
											          id: 'expiryDate',
											          name: 'expiryDate', 
											          index: 'PremiumDate',
											          align: 'left', 
											          sortable: true,
											          editable: true, 
											          
											          //formatter: 'date',																         
											        /*  formatoptions:
											          { 
											              srcformat: 'ISO8601Long', 
											              newformat: 'd/m/Y', 
											              defaultValue:null 
											          },
											          width : 140,
											          editable: true, 
											          edittype: 'text', 
											          editoptions:
											          {
											              size: 12, 
											              maxlengh: 12, 
											              dataInit: function (element)
											              { 
											                  $(element).datepicker({ dateFormat: 'dd/mm/yy' })
											              }
											          }, 
											          editrules: {date: true} */
											          
											          editrules:{date:true, minValue:0}, datefmt:'Y-m-d',
										        	   editoptions:{dataInit: function (elem) {$(elem).datepicker({dateFormat:"yy-mm-dd",minDate :new Date()});} }
	
											          
												     },
													 {
														name : 'Total',
														// name:
														// "",
														// formatter:
														// sumFmatter,
														width : 150,
														align : 'center',
													},
	
													{
														name : 'subCatId',
														// name:
														// "",
														// formatter:
														// sumFmatter,
														width : 150,
														hidden : true,
														align : 'center',
													},
											/*
											 * { label :
											 * 'batchNo',
											 * name:
											 * "batchNo",
											 * width: 150,
											 * editable:true,
											 * align:
											 * 'center', },
											 */
											/*
											 * { label :
											 * 'expiryDate',
											 * name:
											 * "expiryDate",
											 * index:'Date',
											 * width: 200,
											 * editable:true,
											 * edittype:"text",
											 * editrules:{date:true,
											 * minValue:0},
											 * datefmt:'Y-m-d',
											 * editoptions:{dataInit:
											 * function
											 * (elem)
											 * {$(elem).datepicker({dateFormat:"yy-mm-dd"});} } }
											 */
											],
	
											sortorder : 'desc',
											loadonce : false,
											viewrecords : true,
											width : 1300,
											shrinkToFit : true,
											hoverrows : true,
											rownumbers : true,
											rowNum : 10,
											'cellEdit' : true,
											autowidth : true,
											pgbuttons : false,
											pginput : false,
	
											afterSaveCell : function grossTotal() 
										{
												//alert("In savecell");
												document.getElementById("discount").value = "";
												document.getElementById("discountAmount").value = "";
												
												
										
												/*document.getElementById("transExpence3").value = "";
												document.getElementById("hamaliExpence3").value = "";
												document.getElementById("transExpence").value = "";
												document.getElementById("hamaliExpence").value = "";
*/
												/*
												 * Calculation
												 * of total
												 * after
												 * editing
												 * quantity
												 */
	
												// $(this).trigger('reloadGrid');
												var rowId = $("#jqGrid").jqGrid('getGridParam','selrow');
												var rowData = jQuery("#jqGrid").getRowData(rowId);
												var weight = rowData['weight'];
												var dcNum = rowData['dcNum'];
												var mrp = rowData['mrp'];
												var salePrice = rowData['salePrice'];
												var quantity = rowData['quantity'];
												var buyPrice = rowData['buyPrice'];
												var dcNum = rowData['dcNum'];
												var expiryDate = rowData['expiryDate'];
												var buyPriceEx= rowData['buyPriceEx'];
												
												
												/*if(expiryDate == null || expiryDate == "" || expiryDate == undefined || expiryDate == " " || expiryDate >=todayDate)
												{
													
													
												}
												else
												{
													var splitText = expiryDate.split("/");
													var eD=splitText[0];
													var eM=splitText[1] - 1;
													var eY=splitText[2];
													var q = new Date();
													var m = q.getMonth();
													var d = q.getDate();
													var y = q.getFullYear();
													var date = new Date(y,m,d);
													var mydate=new Date(eY,eM,eD);
													if(date>mydate)
													{	
														
													msg = "Please Enter Correct Expiry Date";
									           		 	customAlert(msg)
									           		 	var setExpDate;
									           		 	$("#jqGrid").jqGrid("setCell",rowId,"expiryDate",setExpDate);
									           		 	return false;	
													}
												}
												
*/												
												/*var checkDcNum = /^[0-9]+$/;
	
												if (dcNum.match(checkDcNum)|| dcNum == "")
												{}
												else
												{
													
													var msg = "Please Enter valid Shortage";
													var dialog = bootbox
															.dialog({
																// title:
																// "Embel
																// Technologies
																// Says
																// :",
																
																 * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																 * src="/Shop/staticContent/images/s1.jpg"
																 * height="50"
																 * width="50"/></p>',
																 
																message : '<p class="text-center">'
																		+ msg
																				.fontcolor(
																						"red")
																				.fontsize(
																						5)
																		+ '</p>',
																closeButton : false
															});
	
													setTimeout(
															function() {
																dialog
																		.modal('hide');
															},
															1500);
													
													var setZero=0;
													$("#jqGrid").jqGrid("setCell",rowId,"dcNum",setZero);
	
													return false;
	
												}*/
	                //////quantity checking///
										//var checkQuantity = /^[0-9]+$/;
										var checkQuantity = /^[0-9]+\.?[0-9]*$/;
										
										if (quantity.match(checkQuantity)|| quantity == "") 
										{
	
										} else {
										var msg = "Please Enter valid Quantity";
										var dialog = bootbox.dialog({
																// title:
																// "Embel
																// Technologies
																// Says
																// :",
																
																/* * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																 * src="/Shop/staticContent/images/s1.jpg"
																 * height="50"
																 * width="50"/></p>',
	*/															 
										message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
										closeButton : false
										});
	
										setTimeout(
										function() 
										{
										dialog.modal('hide');
										},
															
										1500);
	
										var setZero = 0;
										$("#jqGrid").jqGrid("setCell",rowId,"quantity",setZero);
													
	
										var Total = 0;
										var Total1 = 0;
										var count = jQuery("#jqGrid").jqGrid('getGridParam','records');
										var allRowsInGrid1 = $('#jqGrid').getGridParam('data');
										var AllRows = JSON.stringify(allRowsInGrid1);
											
										for (var k = 0; k < count; k++) 
										{
										  if (allRowsInGrid1[k].Total == undefined
																|| allRowsInGrid1[k].Total == ""
																|| allRowsInGrid1[k].Total == "NaN") 
										    {
															
												allRowsInGrid1[k].Total = 0;
													
											}
	
											    Total1 = allRowsInGrid1[k].Total;
												Total = +Total+ +Total1;
													
											}
	
											document.getElementById("total").value = Total.toFixed(2);
											document.getElementById("duptotal").value = Total.toFixed(2);
											document.getElementById("grossTotal").value = Total.toFixed(2);
	
												return false;
	
												}
												
												
	       //////////Buy Price validation///////////
										if (buyPrice != "") 
										{
										var numbers = /^[0-9]+\.?[0-9]*$/;
											if (buyPrice.match(numbers)) 
												{
	
												} 
												else 
												{
													var msg = "Please Enter Valid Buy Price";
													var dialog = bootbox.dialog({
																	// title:
																	// "Embel
																	// Technologies
																	// Says
																	// :",
																	
																	 /** message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																	 * src="/Shop/staticContent/images/s1.jpg"
																	 * height="50"
																	 * width="50"/></p>',
																	 */
											message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
											closeButton : false
											
												});
	
											setTimeout(
													function()
													{dialog.modal('hide');
														},
													1500);
	
												var setValue;
									           		 	$("#jqGrid").jqGrid("setCell",rowId,"buyPrice",setValue);
									           		
									           		 	return false;
														
													}
												}
	
								//Mrp validation//		
											if (mrp != "") 
											{
												var numbers = /^[0-9]+\.?[0-9]*$/;
											if (mrp.match(numbers)) 
											{
	
											} 
											else 
											{
												var msg = "Please Enter Valid MRP Price";
												var dialog = bootbox.dialog({
																	// title:
																	// "Embel
																	// Technologies
																	// Says
																	// :",
																	/*
																	 * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																	 * src="/Shop/staticContent/images/s1.jpg"
																	 * height="50"
																	 * width="50"/></p>',
	*/																 
												message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
												closeButton : false
												});
	
												setTimeout(
													function() {
													dialog.modal('hide');
														},
														1500);
														
													var setValue;
													$("#jqGrid").jqGrid("setCell",rowId,"mrp",setValue);
	
													return false;
													}
												}
								//sale price validation//		
	
										if (salePrice != "") 
										{
											var numbers = /^[0-9]+\.?[0-9]*$/;
										if (salePrice.match(numbers)) 
										{
	
										} else 
										{
											 var msg = "Please Enter Valid Sale Price";
											 var dialog = bootbox.dialog({
																	// title:
																	// "Embel
																	// Technologies
																	// Says
																	// :",
																	
																	/* * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																	 * src="/Shop/staticContent/images/s1.jpg"
																	 * height="50"
																	 * width="50"/></p>',
	*/																 
										message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
										closeButton : false
										});
	
										setTimeout(
											function()
											{
												dialog.modal('hide');
											},
											1500);
														
											var setValue;
											$("#jqGrid").jqGrid("setCell",rowId,"salePrice",setValue);
	
											return false;
											}
												}
							//mrp and sale price validation//				
									if (mrp != ""&& salePrice != "") 
									{
									if (Number(salePrice) > Number(mrp)) 
									{
									var msg = "MRP must be Greater Than Sale price Per Unit";
									var dialog = bootbox.dialog({
																	// title:
																	// "Embel
																	// Technologies
																	// Says
																	// :",
																	
																	/* * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																	 * src="/Shop/staticContent/images/s1.jpg"
																	 * height="50"
																	 * width="50"/></p>',
																	 */
										message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
										closeButton : false
										});
	
										setTimeout(
										 function() 
										 {
											dialog.modal('hide');
										},
														1500);
	
											var setZero = 0;
											$("#jqGrid").jqGrid("setCell",rowId,"salePrice",setZero);
											$("#jqGrid").jqGrid("setCell",rowId,"Total",setZero);
	
										var Total = 0;
										var Total1 = 0;
										var count = jQuery("#jqGrid").jqGrid('getGridParam','records');
										var allRowsInGrid1 = $('#jqGrid').getGridParam('data');
										var AllRows = JSON.stringify(allRowsInGrid1);
										for (var k = 0; k < count; k++) {
										if (allRowsInGrid1[k].Total == undefined
																	|| allRowsInGrid1[k].Total == ""
																	|| allRowsInGrid1[k].Total == "NaN")
										{
																
											allRowsInGrid1[k].Total = 0;
															
										}
	
											Total1 = allRowsInGrid1[k].Total;
											Total = +Total+ +Total1;
											}
	
											document.getElementById("total").value = +Total.toFixed(2);
										    document.getElementById("duptotal").value = +Total.toFixed(2);
											document.getElementById("grossTotal").value = +Total.toFixed(2);
	
											return false;
											}
													
												}
	
												/*if (dcNum >= 0) {
													var W = weight;
													var d = dcNum;
													var weightWithShortage = W
															- d;
													$(
															"#jqGrid")
															.jqGrid(
																	"setCell",
																	rowId,
																	"weightAftShortMinus",
																	weightWithShortage);
												}
	*/
												
												var buyPrice = rowData['buyPrice'];
												
												var gst = rowData['gst'];
												var iGst = rowData['igst'];
												var DiscountAmount= rowData['DiscountAmount'];
												var Discount= rowData['Discount'];
												

												/*/////calculation of gst////
*/														
												var checkDiscount = /^[0-9]+\.?[0-9]*$/;
												  if(Discount.match(checkDiscount))
												  {
													  
													  if(Number(Discount) >= 100)
											  	  {
														  
											  		  var setZero = 0;
											  		  alert("In discount")
											  		  myAlert("Discount Percentage Must Be Less Than 100");
											  		 
											  		  $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", setZero);
											      	
											      	  totalCalC();
											      	  totalDisC();
											  		  return false;
											  	  }
												  }
												  else
												  {
													  var setZero = 0;
													  if(Discount== "")
											  	  {
											  		
											  		  $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", setZero);
											      
											  	  }
													 else
												{
											  		  myAlert("Pleaee Enter Valid Discount value");
											  		 
											      	  $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", setZero);
											      	
											      	  totalCalC();
											      	  totalDisC();
											      	  return false;
													  }
												  }			
												  
												//Buy price excluding tax calc//
												  var checkbuyPrice= /^[0-9]+\.?[0-9]*$/;
												  if(buyPrice.match(checkbuyPrice))
											    {
											      	  if(Number(buyPrice) > 0)
											  		  {
											      		  /*alert("Buy price");*/
											      		  BpwTax = (buyPrice/(1+(gst/100)));
											      		  $("#jqGrid").jqGrid("setCell", rowId, "buyPriceEx",  BpwTax.toFixed(2));
											      	  
											     /*if(Number(Discount) > 0)
											      {*/
											      	if(Number(gst) > 0)
											      	{  
											      			 
											      		 DiscountAmount= (BpwTax*(Discount/100));
											      		 finalBp = BpwTax- DiscountAmount;
											      		 newTaxAmt = (((finalBp*quantity)*gst)/100);
											      			  
											      		 var oneProTax = (((finalBp)*gst)/100);
											      		 newFinalBp = finalBp+ oneProTax;
											      		 tota = newFinalBp* quantity;
											      		//DiscountAmount = DiscountAmount * quantity;
											      			 
											      		$("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));	  
													    $("#jqGrid").jqGrid("setCell", rowId, "TotalExTax", finalBp.toFixed(2));
													   /* $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));*/
													    $("#jqGrid").jqGrid("setCell", rowId, "Total", tota.toFixed(2));
													 
													     totalCalC();
													     totalDisC();	
											      		}
											      	else if(Number(gst) == 0)
											      	{
											      		 var setZero = 0;
											      		BpwTax= buyprice - 0;
											      		DiscountAmount = (BpwTax*(Discount/100));
											      		finalBp= BpwTax- DiscountAmount;
											      		newTaxAmt = (((finalBp*quantity)*gst)/100);				        			  
											      		var oneProTax = (((finalBp)*gst)/100);
											      		newFinalBp =  finalBp+ oneProTax;	        			  
											      		tota = newFinalBp * quantity;
											      		DiscountAmount = DiscountAmount* quantity;
											      			 	
											      			  
											      $("#jqGrid").jqGrid("setCell", rowId, "TotalExTax", finalBp.toFixed(2));
												  $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));
												  $("#jqGrid").jqGrid("setCell", rowId, "Total", tota.toFixed(2));
												
												  totalCalC();
												  totalDisC();
											      	}
											      	  
											      	// }
										else
											    {
											     var setZero = 0; 
												  $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", setZero);
											   
											     }
											        }
										else
									         {
											  	var setZero = 0;
											 
											    $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", setZero);
											    $("#jqGrid").jqGrid("setCell", rowId, "Total", setZero);
											
											    return false;
												}
											    }				        	  
										else
											{
												var setZero = 0;
												alert("Pleae Enter Valid Buy Price");
												$("#jqGrid").jqGrid("setCell", rowId, "buyPrice", setZero);
												$("#jqGrid").jqGrid("setCell", rowId, "buyPriceEx", setZero);
											  	$("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", setZero);
											  	$("#jqGrid").jqGrid("setCell", rowId, "Total", setZero);
											
													  return false;
												 }
												
												/*if(Gst!=0)
												{
												var taxpercentage = Gst;
												var salepriceEx=saleprice(1+(taxpercentage/100)));
												
												
											$("#jqGrid").jqGrid("setCell",rowId,"salepriceEx",salepriceEx);
											
											return false;
								                         }
												*/
												  //out of grid calc//
												
										function totalCalC()
										{	
										       var Total = 0;
										       var totAmtWithTax = 0;
										       var count = jQuery("#list4").jqGrid('getGridParam', 'records');
										       var allRowsInGrid1 = $('#list4').getGridParam('data');
										        var AllRows=JSON.stringify(allRowsInGrid1);
										        	  
										       for (var k = 0; k < count; k++)
										       {
										          var Total1 = allRowsInGrid1[k].Total;//grid total 

										        if(Total1 != undefined)
										        {
										        		Total = +Total + +Total1;
										        		  }
										        	  }
										        	  for (var j = 0; j < count; j++)
										        	  {
										        		  var Total2 = allRowsInGrid1[j].taxAmount;
										        		  var Total3 = allRowsInGrid1[j].Total;
										        		  if(Total2 != undefined)
										        		  {
										        			  totAmtWithTax = +totAmtWithTax + +Total2 + +Total3;
										        		  }
										        	  }
										        	  document.getElementById("total").value = Total.toFixed(2);//Math.round(Total);
										        	  document.getElementById("grossTotal").value = Total.toFixed(2);//Math.round(Total);
										        	  var totAmount = Total.toFixed(2);//Math.round(Total);
											        }
										        	  
										      function totalDisC()
											   {
												    //TOTAL DISCOUNT AMOUNT
												       var TotalDisAmt = 0;
												        var TotalBPAmt = 0;
												        var disPer = 0;
												        var count = jQuery("#list4").jqGrid('getGridParam', 'records');
												        var allRowsInGrid1 = $('#list4').getGridParam('data');
												        var AllRows=JSON.stringify(allRowsInGrid1);
												        for (var l = 0; l < count; l++)
												        {
												          var TotalDisAmt1 = allRowsInGrid1[l].DiscountAmount;
												          var TotalBPAmt1 = allRowsInGrid1[l].buyPriceEx;
												        		  
												        if(TotalBPAmt1 != undefined)
												        {
												        	TotalBPAmt = (+TotalBPAmt + +TotalBPAmt1).toFixed(2);
												        }
												        if(TotalDisAmt1 != undefined)
												        {
												        	TotalDisAmt = (+TotalDisAmt + +TotalDisAmt1).toFixed(2);
												        	disPer = ((TotalDisAmt/TotalBPAmt)*100).toFixed(2);
												        }						        	 
												        	  }
												        	  /*document.getElementById("discount1").value = disPer;*/
												        	 // document.getElementById("discount").value = TotalDisAmt;
											        	 }
												  
												var gt = v.gst;
												
												
												if (gst != "") {
													var checkGstIgst = /^[0-9]+$/;
													if (gst.match(checkGstIgst)) 
													{
														
													} else 
													{
													    var msg = "Please Enter Valid GST %";
														var dialog = bootbox.dialog({
																	// title:
																	// "Embel
																	// Technologies
																	// Says
																	// :",
																	/*
																	 * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																	 * src="/Shop/staticContent/images/s1.jpg"
																	 * height="50"
																	 * width="50"/></p>',
																	 */
													message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
													closeButton : false
													});
	
												setTimeout(
														function() {
														dialog.modal('hide');
																
														},
																1500);
	
														igst = 0;
														
												$("#jqGrid").jqGrid("setCell",rowId,"gst",gt);
												return false;
													
													}
												}
	
											var igst = v.igst;
												
											if (igst != "") 
											{
												var checkGstIgst = /^[0-9]+$/;
												if (igst.match(checkGstIgst))
												{
													} else
													{
														var msg = "Please Enter Valid IGST %";
														var dialog = bootbox
																.dialog({
																	// title:
																	// "Embel
																	// Technologies
																	// Says
																	// :",
																	/*
																	 * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																	 * src="/Shop/staticContent/images/s1.jpg"
																	 * height="50"
																	 * width="50"/></p>',
																	 */
											message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
										    closeButton : false
													});
	
											setTimeout(
												function() 
												{
														
													dialog.modal('hide');
													},
													1500);
	
												gst = 0;
														
											    $("#jqGrid").jqGrid("setCell",rowId,"igst",igt);
														
														return false;
													}
												}
	
								          if (gst == ""&& igst == "") 
								          {
										         var msg = "Please Enter Either GST OR IGST";
										         var dialog = bootbox.dialog({
																// title:
																// "Embel
																// Technologies
																// Says
																// :",
																/*
																 * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																 * src="/Shop/staticContent/images/s1.jpg"
																 * height="50"
																 * width="50"/></p>',
																 */
										       message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
										       closeButton : false
												});
	
										       setTimeout(
											   function() 
											   {
												dialog.modal('hide');
											   },
												1500);
												}
								
	
									         if ((gst > Number(0) && igst > Number(0)) /*
																							 * ||
																							 * (iGst >
																							 * 0 &&
																							 * Gst !=
																							 * 0)
																							 */) 
									          {
	
												var msg = "Please Enter Either GST OR IGST";
												var dialog = bootbox
															.dialog({
																// title:
																// "Embel
																// Technologies
																// Says
																// :",
																/*
																 * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
																 * src="/Shop/staticContent/images/s1.jpg"
																 * height="50"
																 * width="50"/></p>',
																 */
												message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
												closeButton : false
													});
	
												setTimeout(
													function() {
														dialog.modal('hide');
															},
															1500);
	
												}
	
											if (gst > 0&& igst == "")
											{
													igst = 0;
											}
	
											if (igst > 0&& gst == "")
											{
													gst = 0;
											}
	
											if (igst != 0)
											{
													
												return false;
												} 
											else if (igst == 0) 
											{
													
	
												}
	
											var Total = 0;
											var Total1 = 0;
											var count = jQuery("#jqGrid").jqGrid('getGridParam','records');
											var allRowsInGrid1 = $('#jqGrid').getGridParam('data');
											var AllRows = JSON.stringify(allRowsInGrid1);
												for (var k = 0; k < count; k++) 
												{
													if (allRowsInGrid1[k].Total == undefined
															|| allRowsInGrid1[k].Total == ""
															|| allRowsInGrid1[k].Total == "NaN") 
													{
														
														allRowsInGrid1[k].Total = 0;
													}
	
													Total1 = allRowsInGrid1[k].Total;
													Total = +Total+ +Total1;
												}
	                               
											document.getElementById("total").value = +Total.toFixed(2);
											document.getElementById("duptotal").value = +Total.toFixed(2);
											document.getElementById("grossTotal").value = +Total.toFixed(2);
	
											},
	
											viewrecords : true,
											width : 1200,
											rowNum : 10,
											pager : "#jqGridPager",
											sortorder : "desc",
											onSelectRow : function(productID) 
											{
												if (productID&& productID !== lastsel) 
												{
													jQuery('#jqGrid').jqGrid('restoreRow',lastsel);
													jQuery('#jqGrid').jqGrid('editRow',productID,true);
												 // jQuery('#jqGrid').jqGrid('editRow',productID,true,pickdates);
													lastsel = productID;
												}
											},
	
										});
	
						count = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
						var rowdata = $("#jqGrid").jqGrid('getGridParam', 'data');
						var ids = jQuery("#jqGrid").jqGrid('getDataIDs');
	
						var prodName, com, packing;
						for (var j = 0; j < count; j++) 
						{
							prodName = rowdata[j].productName;
							com = rowdata[j].manufacturer;
							packing = rowdata[j].weight;
	
							var rowId = ids[j];
							var rowData = jQuery('#jqGrid').jqGrid('getRowData',rowId);
	
						if (prodName == jsonData[i].productName&& com == jsonData[i].manufacturer
									&& packing == jsonData[i].weight) 
						{
	
							         newrow = false;
	
							           var msg = "Product Name Already Inserted !!!";
							           var dialog = bootbox.dialog({
											// title: "Embel
											// Technologies
											// Says :",
											/*
											 * message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img
											 * src="/Shop/staticContent/images/s1.jpg"
											 * height="50"
											 * width="50"/></p>',
											 */
						          message : '<p class="text-center">'+ msg.fontcolor("red").fontsize(5)+ '</p>',
						          closeButton : false
										});
	
								   setTimeout(function() 
								   {
									dialog.modal('hide');
								    }, 1500);
	
								   var grid = jQuery("#jqGrid");
								   grid.trigger("reloadGrid");
								   break;
							      } else 
							     {
								   newrow = true;
							     }
						      }
	
						if (newrow == true) 
						{
	
						 // $("#credit").addRowData(i,jsonData[i]);
							$("#jqGrid").addRowData(count,jsonData[i]);
	
						}
	
						// $("#jqGrid").addRowData(i,jsonData[i]);
						if (count == 0 || count == null) 
						{
							// $("#credit").addRowData(i,jsonData[i]);
							$("#jqGrid").addRowData(0,jsonData[i]);
						}
	
						// $("#jqGrid").addRowData(i,jsonData[i]);
						function pickdates(productID) 
						{
							jQuery("#" + productID+ "_expiryDate","#jqGrid").datepicker({
							dateFormat : "yyyy-mm-dd"
						});
						}
						/* jQuery("#jqGrid").jqGrid('navGrid',"#jqGridPager",{edit:false,add:false,del:true}); */
	
						$('#jqGrid')
								.navGrid(
										'#jqGridPager',
										// the buttons to
										// appear on the
										// toolbar of the
										// grid
										{
											edit : true,
											add : false,
											del : true,
											search : true,
											refresh : true,
											view : true,
											position : "left",
											cloneToTop : false,
										},
										// options for the
										// Edit Dialog
	
										{
	
											/*
											 * afterSaveCell:
											 * function () {
											 * $("#jqGrid").trigger('reloadGrid'); },
											 */
											editCaption : "The Edit Dialog",
											recreateForm : true,
											checkOnUpdate : true,
											checkOnSubmit : true,
											closeAfteredit : true,
											
	
											afterSubmit : function() {
												$('#jqGrid')
														.trigger('reloadGrid');
											},
											errorTextFormat : function(data) 
											{
												return 'Error: '+ data.responseText
											}
	
										},
	
										{
	
										},
	
										// options for the
										// Delete Dialogue
										{
	
											// closeAfterdel:true,
											// recreateForm:
											// true,
											closeAfterdel : false,
											checkOnUpdate : true,
											checkOnSubmit : true,
											recreateForm : true,
	
											errorTextFormat : function(
													data) {
												return 'Error: '
														+ data.responseText
											},
	
											// change the
											// below
											// afterComplete
											// Method to
											// afterSubmit
											// if code work
											// unexpected {
											afterComplete : function() {
												
												var total = document.getElementById("total").value;
												document.getElementById("grossTotal").value = Number(total).toFixed(2);
												document.getElementById("discount").value = "";
												document.getElementById("discountAmount").value = "";
												/*document.getElementById("transExpence3").value = "";
												document.getElementById("hamaliExpence3").value = "";
												document.getElementById("transExpence").value = "";
												document.getElementById("hamaliExpence").value = "";*/
	
												$('#jqGrid').trigger('reloadGrid');
												/*
												 * Calculation
												 * of total
												 * after
												 * editing
												 * quantity
												 */
	
												// $(this).trigger('reloadGrid');
												var rowId = $("#jqGrid").jqGrid('getGridParam','selrow');
												var rowData = jQuery("#jqGrid").getRowData(rowId);
												var weight = rowData['weight'];
												/*var dcNum = rowData['dcNum'];*/
	
												/*if (dcNum >= 0) {
													var W = weight;
													var d = dcNum;
													var weightWithShortage = W
															- d;
													$("#jqGrid")
								                     .jqGrid("setCell",rowId,
																	"weightAftShortMinus",
																	weightWithShortage);
												}*/
	
												var quantity = rowData['quantity'];
												var buyPrice = rowData['buyPrice'];
												var iGst = rowData['igst'];
												var Gst = rowData['gst'];
	
												if (iGst != 0) {
													var taxPercentage = iGst;
													/*
													 * var
													 * taxAmount =
													 * ((taxPercentage/100)*buyPrice);
													 * var
													 * BuyPriceWithTaxAmount =
													 * Number(taxAmount) +
													 * Number(buyPrice);
													 */
													var tota = quantity
															* buyPrice;
													$("#jqGrid").jqGrid("setCell",rowId,"Total",tota);
												} else if (iGst == 0) 
												{
													
													var taxPercentage = Number(Gst);
													/*
													 * var
													 * taxAmount =
													 * ((taxPercentage/100)*buyPrice);
													 * var
													 * BuyPriceWithTaxAmount =
													 * Number(taxAmount) +
													 * Number(buyPrice);
													 */
													var tota = quantity
															* buyPrice;
													$("#jqGrid").jqGrid("setCell",rowId,"Total",tota);
	
												}
												/*
												 * var count =
												 * 0;
												 */
												var Total = 0;
												var count = jQuery("#jqGrid").jqGrid('getGridParam','records');
												/*
												 * count =
												 * count -
												 * 1;
												 */
												var allRowsInGrid1 = $('#jqGrid').getGridParam('data');
												var AllRows = JSON.stringify(allRowsInGrid1);
	
												for (var k = 0; k < count; k++) 
												{
													var Total1 = allRowsInGrid1[k].Total;
													Total = +Total+ +Total1;
												}
	
											document.getElementById("total").value = Total.toFixed(2);
											document.getElementById("duptotal").value = Total.toFixed(2);
											document.getElementById("grossTotal").value = Total.toFixed(2);
	
											},
	
										onSelectRow : function(id) 
										{
											if (id&& id !== lastSel) 
											{
												jQuery("#jqGrid").saveRow(lastSel,true,'clientArray');
												jQuery("#jqGrid").editRow(id,true);
	
												lastSel = id;
												console.log(id);
												}
											},
										});
						
	
						// grid refresh code
	
						});
	
	});
	
	/*
	 * }
	 * 
	 * 
	 * 
	 * +++++++++++ grid for Tax ++++++++++++ else
	 * if(document.getElementById('vat').checked){
	 * 
	 * var params= {}; itemparams={}; productId = $('#proName').val();
	 * 
	 * $("#proName option:selected").each(function() { selectedVal =
	 * $(this).text(); });
	 * 
	 * var splitText = selectedVal.split(",");
	 * 
	 * var proName = splitText[0]; var company = splitText[1]; var weight =
	 * splitText[2];
	 * 
	 * itemparams["proName"]= proName; itemparams["company"]= company;
	 * itemparams["weight"]= weight;
	 * 
	 * itemparams["productId"]= productId;
	 * 
	 * var count=0; var newrow; var rowId;
	 * 
	 * itemparams["methodName"] = "getProductDetailsForGoodsReceiveForTax";
	 * $.post('/Fertilizer/jsp/utility/controller.jsp',itemparams,
	 * function(data) { var jsonData = $.parseJSON(data);
	 * 
	 * 
	 * 
	 * function sumFmatter (cellvalue, options, rowObject) {
	 * 
	 * var jam=0; var jam1=""; var tot= ((options.rowData.quantity) *
	 * ((options.rowData.buyPrice)+((options.rowData.taxPercentage/100)*options.rowData.buyPrice)));
	 * //var shree = document.poferti.grossTotal.value;// to get gross total
	 * 
	 * var count = jQuery("#jqGrid").jqGrid('getGridParam', 'records'); var
	 * allRowsInGrid1 = $('#jqGrid').getGridParam('data'); var
	 * AllRows=JSON.stringify(allRowsInGrid1); for (var i = 0; i < count; i++) {
	 * 
	 * var quantity = allRowsInGrid1[i].quantity; params["quantity"+i] =
	 * quantity;
	 * 
	 * var buyPrice = allRowsInGrid1[i].buyPrice; params["buyPrice"+i] =
	 * buyPrice;
	 * 
	 * var taxPercentage = allRowsInGrid1[i].taxPercentage;
	 * params["taxPercentage"+i] = taxPercentage;
	 * 
	 * var taxAmount = ((taxPercentage/100)*buyPrice);
	 * 
	 * var priceWithTaxamount = taxAmount+buyPrice;
	 * 
	 * var totals1=((priceWithTaxamount)*(quantity));
	 * 
	 * jam = jam + totals1;
	 * 
	 * document.getElementById("total").value = jam;
	 * document.getElementById("duptotal").value = jam; } if(count == 0){
	 * document.getElementById("total").value = tot;
	 * document.getElementById("duptotal").value = tot; } else{
	 * document.getElementById("total").value = jam;
	 * document.getElementById("duptotal").value = jam; }
	 * 
	 * 
	 * 
	 * return tot; }
	 * 
	 * $.each(jsonData,function(i,v) {
	 * 
	 * var productName = v.productName; var buyPrice = v.buyPrice; var salePrice
	 * =v.salePrice; var quantity = v.quantity; var weight = v.weight ; var
	 * productID = v.productID ; var manufacturer = v.manufacturer ; var lastsel =
	 * productID;
	 * 
	 * $("#jqGrid").jqGrid({
	 * 
	 * 
	 * 
	 * datatype:"local",
	 * 
	 * colNames: [ "Cat ID","Product ID","Product Name","Company","Buy
	 * Price","Tax %","M.R.P","Sale
	 * Price","Packing","Quantity","Total","BatchNo","ExpiryDate" ],
	 * 
	 * colModel: [ { name: "catIDforVAt", hidden:true //resizable: true,
	 * 
	 *  }, { name: "productID", hidden:true //resizable: true,
	 * 
	 *  }, { name: "productName", width:120,
	 *  }, { name: "manufacturer", width:120
	 *  }, { name: "buyPrice", width: 120, editable: true }, { name:
	 * "taxPercentage", width: 140, editable: true }, { name: "mrp", width: 140,
	 * editable: true }, { name: "salePrice", width: 140, editable: true },
	 *  { name: "weight", width: 100,
	 *  },
	 *  { name: "quantity", width: 120, editable: true
	 * 
	 *  },
	 *  { name : 'Total', // name: "", formatter: sumFmatter, width: 150,
	 *  }, { label : 'batchNo', name: "batchNo", width: 150, editable:true,
	 * 
	 *  }, { label : 'expiryDate', name: "expiryDate", index:'Date', width: 200,
	 * editable:true, edittype:"text", editrules:{date:true, minValue:0},
	 * datefmt:'Y-m-d', editoptions:{dataInit: function (elem)
	 * {$(elem).datepicker({dateFormat:"yy-mm-dd"});} } } ],
	 * 
	 * 
	 * sortorder : 'desc',
	 * 
	 * multiselect: false, loadonce: false, rownumbers:true, 'cellEdit':true,
	 * afterSaveCell: function grossTotal() { Calculation of total after editing
	 * quantity
	 *  // $(this).trigger('reloadGrid'); var rowId
	 * =$("#jqGrid").jqGrid('getGridParam','selrow'); var rowData =
	 * jQuery("#jqGrid").getRowData(rowId); var quantity = rowData['quantity'];
	 * var buyPrice = rowData['buyPrice']; var taxPercentage =
	 * rowData['taxPercentage']; var taxAmount = ((taxPercentage/100)*buyPrice);
	 * var BuyPriceWithTaxAmount = taxAmount + buyPrice; var tota = quantity *
	 * BuyPriceWithTaxAmount; $("#jqGrid").jqGrid("setCell", rowId, "Total",
	 * tota); },
	 * 
	 * 
	 * 
	 * 
	 * viewrecords: true, width: 1200, height: 250, rowNum: 10, pager:
	 * "#jqGridPager", sortorder: "desc", onSelectRow: function(productID){
	 * if(productID && productID!==lastsel){
	 * jQuery('#jqGrid').jqGrid('restoreRow',lastsel);
	 * jQuery('#jqGrid').jqGrid('editRow',productID,true);
	 * //jQuery('#jqGrid').jqGrid('editRow',productID,true,pickdates);
	 * lastsel=productID; } },
	 * 
	 * });
	 * 
	 * count = jQuery("#jqGrid").jqGrid('getGridParam', 'records'); var rowdata
	 * =$("#jqGrid").jqGrid('getGridParam','data'); var ids =
	 * jQuery("#jqGrid").jqGrid('getDataIDs');
	 * 
	 * 
	 * var prodName,com,packing; for (var j = 0; j < count; j++) { prodName =
	 * rowdata[j].productName; com = rowdata[j].manufacturer; packing =
	 * rowdata[j].weight;
	 * 
	 * var rowId = ids[j]; var rowData = jQuery('#jqGrid').jqGrid ('getRowData',
	 * rowId);
	 * 
	 * if (prodName == jsonData[i].productName && com ==
	 * jsonData[i].manufacturer && packing == jsonData[i].weight) {
	 * 
	 * newrow=false; alert("Product Name Already Inserted !!!"); var grid =
	 * jQuery("#jqGrid"); grid.trigger("reloadGrid"); break; } else { newrow =
	 * true; } }
	 * 
	 * if(newrow == true) {
	 *  // $("#credit").addRowData(i,jsonData[i]);
	 * $("#jqGrid").addRowData(count,jsonData[i]);
	 *  }
	 * 
	 * 
	 * 
	 * 
	 * //$("#jqGrid").addRowData(i,jsonData[i]); if(count==0 || count==null) { //
	 * $("#credit").addRowData(i,jsonData[i]);
	 * $("#jqGrid").addRowData(0,jsonData[i]); }
	 * 
	 * 
	 * 
	 * 
	 * //$("#jqGrid").addRowData(i,jsonData[i]); function pickdates(productID){
	 * jQuery("#"+productID+"_expiryDate","#jqGrid").datepicker({dateFormat:"yyyy-mm-dd"}); }
	 * jQuery("#jqGrid").jqGrid('navGrid',"#jqGridPager",{edit:false,add:false,del:true});
	 * 
	 * 
	 * 
	 * $('#jqGrid').navGrid('#jqGridPager', // the buttons to appear on the
	 * toolbar of the grid {edit: true, add: false,del: true, search: true,
	 * refresh: true, view: true, position: "left", cloneToTop: false }, //
	 * options for the Edit Dialog
	 * 
	 * 
	 * 
	 * 
	 *  {
	 * 
	 * afterSaveCell: function () { $(this).trigger('reloadGrid'); },
	 * editCaption: "The Edit Dialog", recreateForm: true, checkOnUpdate : true,
	 * checkOnSubmit : true, closeAfteredit: true, errorTextFormat: function
	 * (data) { return 'Error: ' + data.responseText }
	 * 
	 * 
	 *  },
	 * 
	 * 
	 *  // options for the Delete Dialogue { closeAfterdel:true, recreateForm:
	 * true, errorTextFormat: function (data) { return 'Error: ' +
	 * data.responseText },
	 * 
	 * onSelectRow: function(id) { if (id && id !== lastSel) {
	 * jQuery("#jqGrid").saveRow(lastSel, true, 'clientArray');
	 * jQuery("#jqGrid").editRow(id, true);
	 * 
	 * lastSel = id; console.log(id); } }
	 * 
	 * 
	 * });
	 *  // grid refresh code
	 * 
	 * });
	 * 
	 * 
	 * });
	 *  }
	 */
	
	}
	
	function batchNoValidate() {
		var rowId = $("#jqGrid").jqGrid('getGridParam', 'selrow');
	var rowData = jQuery("#jqGrid").getRowData(rowId);
	var count = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#jqGrid').getGridParam('data');// to get all rows
										// of grid
	var AllRows = JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++) {
		var catId = allRowsInGrid[i].catId;
		if (catId != 1) {
			var batchNo = allRowsInGrid[i].batchNo;
			var expiryDate = allRowsInGrid[i].expiryDate;
			/*
	 * var batchNo = rowData['batchNo'+i]; var expiryDate =
	 * rowData['expiryDate'+i];
	 */
	if (batchNo == undefined || batchNo == "") {
	alert("Please Enter Batch number At Column No :: " + ++i);
		return true;
	} else {
		if (expiryDate == undefined || expiryDate == "") {
	alert("Please Enter Expiry Date At Column No :: " + ++i);
						return true;
					}
				}
			}
		}
		addingGoodsReceive()
	}
	
	function batchNoValidateForBooked() {
		var rowId = $("#jqGrid1").jqGrid('getGridParam', 'selrow');
	var rowData = jQuery("#jqGrid1").getRowData(rowId);
	var count = jQuery("#jqGrid1").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#jqGrid1').getGridParam('data');// to get all rows
										// of grid
	var AllRows = JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++) {
		var catId = allRowsInGrid[i].catId;
		if (catId != 1) {
			var batchNo = allRowsInGrid[i].batchNo;
			var expiryDate = allRowsInGrid[i].expiryDate;
			/*
	 * var batchNo = rowData['batchNo'+i]; var expiryDate =
	 * rowData['expiryDate'+i];
	 */
	if (batchNo == undefined || batchNo == "") {
	alert("Please Enter Batch number At Column No :: " + ++i);
		return true;
	} else {
		if (expiryDate == undefined || expiryDate == "") {
	alert("Please Enter Expiry Date At Column No :: " + ++i);
						return true;
					}
				}
			}
		}
		addBookedGoodsReceive()
	}
	
	function customAlert(msg)
	{
		var msg=msg;
		var dialog = bootbox.dialog({
		message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	closeButton: false
	});			
	setTimeout(function() {
		dialog.modal('hide');
		}, 1500);
	}