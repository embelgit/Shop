//function to checkForDuplicateCategoryEntry
function checkForDuplicateCategoryEntry(){
          			
	var input = document.getElementById('categoryName'),
	     list = document.getElementById('cat_drop2'),
	     	i,categoryName;
		 		for (i = 0; i < list.options.length; ++i) {
				     if (list.options[i].value === input.value) {
				    	 categoryName = list.options[i].getAttribute('value');
				     }
		 		}
	
		

		 		if(document.getElementById("categoryName").value == categoryName)
		{
			var msg="Category already exist...Duplicate Not allowed";
		     alert(msg);
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
			
		}, 1500);

		document.getElementById("categoryName").value = "";
		
		//document.setElementById("categoryName").value ="";
			return false;
		}
	
		}
	
//function checkForDuplicateCategoryEntry
function checkForDuplicateSubCategoryEntry(){
		
	var cat = document.getElementById('fk_cat_id3');
	var subCat = document.getElementById('cat_drop4');
	     	i,subCat;
		 		for (i = 0; i < list.options.length; ++i) {
				     if (list.options[i].value === input.value) {
				    	 subCat = list.options[i].getAttribute('value');
				     }
		 		}
	
		

		 		/*var subCat = "<%=bean.getSubcategoryName()%>";
				var cat = "<%=bean.getCategoryName()%>";*/
		 		
				var subcatName=document.getElementById("subcategoryName").value;
				var catName=document.getElementById("fk_cat_id3").value;

				if(subcatName == subCat && cat == catName){
					alert("subcategory already exist...Duplicate Not allowed");
					location.reload();
					return false;
				}
	
		}
	
	
function reset()
{
document.catd.reset();	
}

function catDetails(){
	document.catd.save.disabled =true;
	
	if(document.catd.categoryName.value == "")
	{
		var msg="Please Enter Category Name";
		
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		
		return false;
	}	
	//var letterNumber = /^[a-zA-Z, ]+$/;
	var letterNumber = /^[a-zA-Z0-9]*$/;
	//var letterNumber =/^[a-zA-Z0-9%*#]*$/;
	
	if(document.catd.categoryName.value.match(letterNumber))
	{
		categoryDetails();
	}
	
	else
	{
		var msg="Please Enter only alphabets in Category name";
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		
		return false;
	}	
}

function categoryDetails()
{	
	
		var categoryName = $('#categoryName').val();
		/*var subCategoryName = $('#subCategoryName').val();*/

		var params = {};
		
		params["categoryName"] = categoryName;
		/*params["subCategoryName"] =subCategoryName;*/
		
		
		params["methodName"] = "categoryDetails";
	
	 	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
	 	{
				var msg=data;
				var dialog = bootbox.dialog({
			    message: '<p class="text-center">'+msg.fontcolor("green").fontsize(5)+'</p>',
			    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
					location.reload();
				}, 1500);
	 			document.catd.btn.disabled =false;
	 	}).error(function(jqXHR, textStatus, errorThrown){
	 	    		if(textStatus==="timeout") {
	 	    			$(loaderObj).hide();
	 	    			$(loaderObj).find('#errorDiv').show();
	 	    		}
	 	    	});

		
}

/*sub category method by RK*/
function subcat()
{
	document.subCat.save.disabled =true;
	if(document.subCat.fk_cat_id.value == "")
	{
		var msg="Please Enter Category Name";
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		
		return false;
	}	
	else if(document.subCat.subcategoryName.value == "")
	{
		var msg="Please Enter Sub Category Name";
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		
		return false;
	}	
	//var letterNumber = /^[a-zA-Z, ]+$/;
	var letterNumber = /^[a-zA-Z0-9]*$/;
	if(document.subCat.subcategoryName.value.match(letterNumber))
	{
		subCategoryDetails();
	}
	else
	{
		var msg="Please Enter Valid Sub Category name";
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		
		return false;
		
		
		alert("Enter Alphabates Only in Sub Category name field");
		return false;
	}	
}

function subCategoryDetails(){
	 
	document.subCat.save1.disabled =true;
	
	var input = document.getElementById('fk_cat_id'),
    list1 = document.getElementById('cat_drop'),
    i,fk_cat_id;

	for (i = 0; i < list1.options.length; ++i) {
    if (list1.options[i].value === input.value) {
    	fk_cat_id = list1.options[i].getAttribute('data-value');
    }
	}
	//var fk_cat_id = $('#fk_cat_id').val();
	var subcategoryName = $('#subcategoryName').val();
	
	var params = {};
	
	params["fk_cat_id"] =fk_cat_id;
	params["subcategoryName"] =subcategoryName;
	
	
	params["methodName"] = "subCategoryDetails";

	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
	{
		var msg=data;
		var dialog = bootbox.dialog({
	    message: '<p class="text-center">'+msg.fontcolor("green").fontsize(5)+'</p>',
	    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
			location.reload();
		}, 1500);
		document.catd.btn.disabled =false;
	}).error(function(jqXHR, textStatus, errorThrown){
			if(textStatus==="timeout") {
				$(loaderObj).hide();
				$(loaderObj).find('#errorDiv').show();
			}
		});
	}



function reset1()
{
   document.catd.reset();	
}

function catlist1()
{

	 window.location ="catlist1.jsp";

}
