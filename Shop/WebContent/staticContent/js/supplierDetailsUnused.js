
function supplierDetailValidate()
{
	
	var dealerName = $("#dealerName").val();
	var personName = $("#personName").val();
	var city = $("#city").val();
	var tinNo = $("#tinNo").val();
	var contactNo = $("#contactNo").val();
	var contactNoLength = contactNo.length;
	var landline = $("#landline").val();
	var landlineLength = landline.length;
	var emailId = $("#emailId").val();
	var address = $("#address").val();
	
	
	var letterNumber = /^[a-zA-Z, / ]+$/;
	if(dealerName != null && dealerName != "" && dealerName != " ")
		{
		if(dealerName.match(letterNumber))
			{
			var letterNumber = /^[a-zA-Z, / ]+$/;
			if(personName != null && personName != "" && personName != " ")
				{
				if(personName.match(letterNumber))
					{
					if(contactNoLength>Number(0))
						{
						if(contactNoLength > Number(10) || contactNoLength < Number(10))
							{
							
								var msg="Please Enter Valid 10 Digit Mobile Number";
								var dialog = bootbox.dialog({
									//title: "Embel Technologies Says :",
								    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
								    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
								    closeButton: false
								});
								
								setTimeout(function() {
									dialog.modal('hide');
								}, 1500);
								return false;							
							}
						}
					
					if(landlineLength>Number(0))
					{
					if(landlineLength > Number(10) || landlineLength < Number(10))
					{
						
						var msg="Please Enter Valid 10 Digit Landline Number";
						var dialog = bootbox.dialog({
							//title: "Embel Technologies Says :",
						    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
						    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
						    closeButton: false
						});
						
						setTimeout(function() {
							dialog.modal('hide');
						}, 1500);
						
						return false;
						}
					}
					
					if(emailId == "" || emailId == undefined || emailId == null || emailId == " ")
					{
						
					}
					else
					{
						var letterNumber = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						if(emailId.match(letterNumber))
						{
							
						}
						else
						{
							var msg="Please Enter Valid E-mail Address";
							var dialog = bootbox.dialog({
								//title: "Embel Technologies Says :",
							    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
							    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
							    closeButton: false
							});
							
							setTimeout(function() {
								dialog.modal('hide');
							}, 1500);
							
							return false;
						}
					}
						if(city != null && city != "" && city != " ")
						{
						var letterNumber = /^[a-zA-Z, ]+$/;
						if(city.match(letterNumber))
							{
							/*if(tinNo != null && tinNo != "")
								
							{
									
								var gstMatch = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/g;
									
								if(tinNo.match(gstMatch))
									
								{	*/					
									
									if(address != "")
									{
										var letterNumber = /^[0-9a-zA-Z ]+$/;
										if(address.match(letterNumber))
										{
											
										}
										else
										{											
											var msg="Please Enter Customer's Valid Address";
											var dialog = bootbox.dialog({
												//title: "Embel Technologies Says :",
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
											    closeButton: false
											});
											
											setTimeout(function() {
												dialog.modal('hide');
											}, 1500);
											
											return false;											
										}
									}								
								
									supDetails();
								//}
									/*else
									{
										var msg="Please Enter Valid GST Number";
										var dialog = bootbox.dialog({
											//title: "Embel Technologies Says :",
										    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
										    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
										    closeButton: false
										});
										
										setTimeout(function() {
											dialog.modal('hide');
										}, 1500);
										
										return false;
									}
								}
							else
								{
								
									var msg="please Enter GST No";
									var dialog = bootbox.dialog({
										//title: "Embel Technologies Says :",
									    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
									    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
									    closeButton: false
									});
									
									setTimeout(function() {
										dialog.modal('hide');
									}, 1500);
									
									return false;
								}*/
							}
						else
							{							
								var msg="Please Enter Valid City Name";
								var dialog = bootbox.dialog({
									//title: "Embel Technologies Says :",
								    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
								    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
								    closeButton: false
								});
								
								setTimeout(function() {
									dialog.modal('hide');
								}, 1500);
								
								return false;
							}
						
						}
					else
						{						
							var msg="Please Enter City";
							var dialog = bootbox.dialog({
								//title: "Embel Technologies Says :",
							    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
							    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
							    closeButton: false
							});
							
							setTimeout(function() {
								dialog.modal('hide');
							}, 1500);
							
							return false;
						}
					}
				else
				{
					var msg="Please Enter Valid Person Name";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);
					
					return false;
					
				}
				}else
					{
						var msg="Please Enter Person Name";
						var dialog = bootbox.dialog({
							//title: "Embel Technologies Says :",
						    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
						    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
						    closeButton: false
						});
						
						setTimeout(function() {
							dialog.modal('hide');
						}, 1500);
						
						return false;
						
					}
				}
		else
			{
			
				var msg="Please Enter Valid Supplier Name";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
				
			}
		}
	else
		{
			var msg="Please Enter Supplier Name";
			var dialog = bootbox.dialog({
				//title: "Embel Technologies Says :",
			    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
			    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			    closeButton: false
			});
			
			setTimeout(function() {
				dialog.modal('hide');
			}, 1500);
			
			return false;
		}
					
}
function supplierDetail()
{
		
	/*if(document.spld.dealerName.value == "")
	{
		document.write("Value of Supplier Name ===> "+document.spld.personName.value);
		alert("Enter Supplier  Name.");
		return false;
	}	
	var letterNumber = /^[a-zA-Z, ]+$/;
	if(document.spld.dealerName.value.match(letterNumber))
	{
		if(document.spld.personName.value == "")
		{			
			alert("Enter Person Name.");
			return false;
		}	
			var letterNumber = /^[a-zA-Z, ]+$/;
			if(document.spld.personName.value.match(letterNumber))
			{
				
				 if ( document.spld.contactNo.value == "" )
       			 {
		  	       		alert("Please Enter Contact Number");
		  	       		return false;
       			 }
       			
				 var letterNumber = /^[0-9]{10}$/;
       			 if(document.spld.contactNo.value.match(letterNumber))
       			 {
       				 if ( document.spld.landline.value == "" )
           			 {
    		  	       alert("Please Enter Landline Number");
    		  	       return false;
           			 }
           			
    				 var letterNumber = /^[0-9]+$/;
           			 if(document.spld.landline.value.match(letterNumber))
           			 {
       				  if(document.spld.emailId.value == ""  )
           				 {
       					  	alert("Please Enter Email .");
	 			             return false;
           				 }
		   	        	 var letterNumber = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		   	        	 if(document.spld.emailId.value.match(letterNumber) || document.spld.emailId.value==null || document.spld.emailId.value=="")
		   	        	 
				{
	   	        		
	   	        				
		   	        		if(document.spld.city.value == "")
	   	        				{
	   	        					alert("Please Enter City.");
	   	        					return false;
	   	        				}	
	   	        				var letterNumber = /^[a-zA-Z, ]+$/; 
	   	        				if(document.spld.city.value.match(letterNumber))
	   	        				{
	   	        					if ( document.spld.tinNo.value == "" )
								    {								         
								  	      alert("Please Enter GST Number");
								          return false;
								    }
									var letterNumber = /^[0-9a-zA-Z,]+$/;
									if(document.spld.tinNo.value.match(letterNumber))
									{
										if(document.spld.address.value == "")
			   	        				{
			   	        					alert("Please Enter address.");
			   	        					return false;
			   	        				}
				   	        				var letterNumber = /^[a-zA-Z0-9, ]+$/;
				   	        				if(document.spld.address.value.match(letterNumber))
				   	        				{
											
;
											}
										
			   	        				
			   	        				else
										{
												alert("Enter Alphabates Only in Address field..!!");
												return false;
											}
										}
			   	        				
			   	        				
			   	        				else
										{
												alert("Enter Numbers Only in Tin number field..!!");
												return false;
											}
										}
									
	   	        				else
									{
										alert("Enter Alphabates Only in city field..!!");
										return false;
									}	
	   	        				}
										
		   	        	 else
							{
								alert("Enter a Valid email adress (example = abc@xyz.com)");
								return false;
							}
						}
//			}
           		 else
						{
							alert("Enter Numbers Only in Landline number field..!!");
							return false;
							}	
						}								
           			 
       			 else
						{
							alert("Enter 10 digit Numbers Only in contact number field..!!");
							return false;
							}	
						}
				else
			{
				alert("Enter Alphabets Only in Person name field..!!");
				return false;
			}
		}										
	else
		{
			alert("Enter Alphabets Only in Suppliers name field..!!");
			return false;
		}
}
*/
}



function supDetails(){

	//document.spld.btn.disabled = true;
	//document.spld.save.disabled =false;
	//document.getElementById("save").disabled = true;
	document.getElementById("save").disabled = true;
	
				var dealerName = $('#dealerName').val();
				var personName = $('#personName').val();
				var contactNo = $('#contactNo').val();
				var landline = $('#landline').val();
				var emailId = $('#emailId').val();
				var tinNo = $('#tinNo').val();
				var city = $('#city').val();
				var address = $('#address').val();
				
				
				var params = {};
				
				if(dealerName=="" |dealerName==null |dealerName=="undefined" )
				{
					dealerName="N/A";
				}
				
				if(contactNo=="" |contactNo==null |contactNo=="undefined" )
				{
					contactNo="00";
				}
				if(landline=="" |landline==null |landline=="undefined" )
				{
					landline="00";
				}
				if(emailId=="" |emailId==null |emailId=="undefined" )
				{
					emailId="N/A";
				}
				if(address=="" |address==null |address=="undefined" )
				{
					address="N/A";
				}
				params["dealerName"] = dealerName;
				params["personName"] =personName;
				params["contactNo"] = contactNo;
				params["landline"] =landline;
				params["emailId"] = emailId;
				params["tinNo"] = tinNo;
				params["city"] = city;
				params["address"] = address;
				
				
				params["methodName"] = "supplierDetails";

				$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
			 	{
					var msg=data;
					var dialog = bootbox.dialog({
				    message: '<p class="text-center">'+msg.fontcolor("green").fontsize(5)+'</p>',
				   
				    closeButton: false
					});
					/* redirect to good Receive*/
					 
					
					setTimeout(function() {
						dialog.modal('hide');
						location.reload();
					}, 1500);
						
						document.getElementById("save").disabled = false;
			 			document.spld.btn.disabled =false;
			 	}).error(function(jqXHR, textStatus, errorThrown){
			 	    		if(textStatus==="timeout") {
			 	    			$(loaderObj).hide();
			 	    			$(loaderObj).find('#errorDiv').show();
			 	    		}
			 	    	});
	
}


function reset()
{
   document.spld.reset();	

}


/********* Edit Supplier Details ************/
function getSupplierDetails(){
	var params= {};
	
	var input = document.getElementById('supplier'),
     list = document.getElementById('sup_drop'),
     	i,fkRootSupId;
	 		for (i = 0; i < list.options.length; ++i) {
			     if (list.options[i].value === input.value) {
			    	 fkRootSupId = list.options[i].getAttribute('data-value');
			     }
	 		}
	
	$("#dealerName").append($("<input/>").attr("value","").text());
	$("#personName").append($("<input/>").attr("value","").text());
	$("#contactNo").append($("<input/>").attr("value","").text());
	$("#landline").append($("<input/>").attr("value","").text());
	$("#emailId").append($("<input/>").attr("value","").text());
	$("#city").append($("<input/>").attr("value","").text());
	$("#tinNo").append($("<input/>").attr("value","").text());
	$("#address").append($("<input/>").attr("value","").text());
	
	
	
	params["SupplierId"]= fkRootSupId;
	params["methodName"] = "getSupplierDetailsToEdit";
	
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data){
		
		var jsonData = $.parseJSON(data);
		var catmap = jsonData.list;
		$.each(jsonData,function(i,v)
				{
				  document.getElementById("dealerName").value = v.dealerName;
			      document.getElementById("personName").value = v.personName;
			      document.getElementById("contactNo").value = v.contactNo;
			      document.getElementById("landline").value = v.landline;
			      document.getElementById("emailId").value = v.email;
			      document.getElementById("city").value = v.city;
			      document.getElementById("tinNo").value = v.tin;
			      document.getElementById("address").value = v.address;
			   
		      
				});
			}).error(function(jqXHR, textStatus, errorThrown){
				if(textStatus==="timeout") {

				}
			});
 	    	
}
function updateSupplierDetails()
{
	
	if(document.spld1.supplier.value == "")
	{
		alert("Please Select Supplier Supplier Name To Update Details");
		return false;
	}
	else
	{
		/*document.spld1.address.value = 'N/A';*/
		
		addUpdateSupplierDetails();
	}
	
		/*addUpdateSupplierDetails();*/
			
}
function addUpdateSupplierDetails(){

	
	//document.spld1.btn.disabled = true;
	
	var input = document.getElementById('supplier'),
    list = document.getElementById('sup_drop'),
    	i,fkRootSupId;
	 		for (i = 0; i < list.options.length; ++i) {
			     if (list.options[i].value === input.value) {
			    	 fkRootSupId = list.options[i].getAttribute('data-value');
			     }
	 		}
	
	//var customerId = document.getElementById("customerId").value;
	
	var dealerName = $('#dealerName').val();
	var personName = $('#personName').val();
	var contactNo = $('#contactNo').val();				
	var landline = $('#landline').val();
	var emailId = $('#emailId').val();
	var city = $('#city').val();
	var tinNo = $('#tinNo').val();
	var address = $('#address').val();
	var gstMatch = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/g;

	
	
	var params = {};
	
	if(dealerName=="" |dealerName==null |dealerName=="undefined" )
	{
		alert("Enter Supplier Name");
		dealerName="N/A";
	}
	
	if(personName=="" |personName==null |personName=="undefined" )
	{
		alert("Enter Person Name");
		personName="N/A";
	}
	
	if(city=="" |city==null |city=="undefined" )
	{
		alert("Enter City");
		city="N/A";
		return false;
	}
	if(tinNo != null && tinNo != "" && tinNo.match(gstMatch))
	{
		
	}	
	else
	{
		var msg="Please Enter Valid GST Number";
		alert(msg);
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		
		return false;
	}	
	
	params["supplierId"] = fkRootSupId;
	
	params["dealerName"] = dealerName;	
	params["personName"] = personName;
	params["contactNo"] = contactNo;
	params["landline"] = landline;
	params["emailId"] =emailId;
	params["city"] = city;
	params["tinNo"] = tinNo;
	params["address"] = address;
	
	
	
	params["methodName"] = "updateSupplierDetails";

	$.post('/Shop/jsp/utility/controller.jsp',params,function(data){
		
		
		
				alert(data);
				if(document.spld1)
				{
					document.spld1.reset();
				}	
				document.spld1.btn.disabled =false;
			}
 	    	).error(function(jqXHR, textStatus, errorThrown){
 	    		
 	    		/*alert("Data Added Successfully..");
 	    		location.reload();
 				document.ccd.btn.disabled =false;*/
 	    		
 	    		if(textStatus==="timeout") {
 	    			$(loaderObj).hide();
 	    			$(loaderObj).find('#errorDiv').show();
 	    		}
 	    	});
}
