//get Product detail as Per barcode for cash customer
function getitemData(){ 
		var value = document.getElementById("key").value;
		
		var params= {};
		var count=0;
		var newrow;
		var rowId;

		params["methodName"] ="fetchCust";
		params["key"]=value;
		

		$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
				{
			  var jsonData = $.parseJSON(data);
				
		      // $("#list4").jqGrid("clearGridData", true).trigger("reloadGrid");
			
	        
		     $.each(jsonData,function(i,v)
			{	 
		        function sumFmatter (cellvalue, options, rowObject)
		        {
		            
		        	var tax = options.rowData.vatPercentage;
		        	
		        	if(tax == 0){
		        		var tot= (options.rowData.quantity * options.rowData.salePrice);
		        		if(isNaN(tot)){
		        			tot = 0;
						}
		        	}
		        	if(tax != 0){
		        		
		        		var taxcalculation = (tax/100)* Number(options.rowData.salePrice);
		        		var newSalePrice = Number(taxcalculation) + Number(options.rowData.salePrice)
		        		var tot= (Number(options.rowData.quantity) * Number(newSalePrice));
		        		if(isNaN(tot)){
		        			tot = 0;
						}
		        	}
		        	var jam=0;
		        	
		        	
		        	count = jQuery("#list4").jqGrid('getGridParam', 'records');
		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
		        	var AllRows=JSON.stringify(allRowsInGrid1);
		        	for (var i = 0; i < count; i++) {
		        		
		            	var quantity = allRowsInGrid1[i].quantity;
		             	params["quantity"+i] = quantity;
		             	
		             	var salePrice = allRowsInGrid1[i].salePrice;
		            	params["salePrice"+i] = salePrice;
		            	
		            	var vatPercentage = allRowsInGrid1[i].vatPercentage;
		            	params["vatPercentage"+i] = vatPercentage;
		            	
		            	if(vatPercentage == 0){
		            		
		            		var totals1=(salePrice)*(quantity);
		            		if(isNaN(totals1)){
			             		totals1 = 0;
							}
			            	jam = jam + totals1;
		            	}
		            	
		                if(vatPercentage != 0){
		                	
		                	var taxcal = (vatPercentage/100) * salePrice;
		                	var newSalePrice = Number(salePrice) + Number(taxcal);
		                	var totals1=(Number(newSalePrice)*Number(quantity));
		                	if(isNaN(totals1)){
			             		totals1 = 0;
							}
			            	jam = jam + totals1;
		                }                	
		            	
	            	
	        	    }
		        	
		        		
		        		 document.getElementById("totalWithExpense").value = jam;
		        	
		            	 return tot;

		        }
		        
		         count = jQuery("#list4").jqGrid('getGridParam', 'records'); 
			     var rowdata =$("#list4").jqGrid('getGridParam','data');
			     var ids = jQuery("#list4").jqGrid('getDataIDs');
				 
				
				  var prodName,com,packing,unit;
				  for (var j = 0; j < count; j++) 
				  {
					  prodName = rowdata[j].itemName;
					  com = rowdata[j].companyName;
					  packing = rowdata[j].weight;
					  unit = rowdata[j].unitName;
					
					 var rowId = ids[j];
					 var rowData = jQuery('#list4').jqGrid ('getRowData', rowId);
					
					if (prodName == jsonData.offer.itemName && com == jsonData.offer.companyName && packing == jsonData.offer.weight && unit == jsonData.offer.unitName) {
				    	/*ori_quantity = +rowdata[j].quantity+1;
				    	
				    	$("#list4").jqGrid("setCell", rowId, "quantity", ori_quantity);
				    	var grid = jQuery("#list4");
				    	grid.trigger("reloadGrid");*/
				    	newrow=false;
						alert("Product Name Already Inserted !!!");
						var grid = jQuery("#list4");
					    grid.trigger("reloadGrid");
				    	break;
					}
					else
					{
						newrow = true;
					}
				 }
				  
				  if(newrow == true)
					 {
						
					  //$("#list4").addRowData(i,jsonData[i]);
					  $("#list4").addRowData(count,jsonData.offer);
						
					 }
			
			
			$("#list4").jqGrid({
				datatype: "local",
				
				colNames:['cat_id','ItemName','CompanyName','Packing','Unit','Quantity', 'UnitPrice','MRP','TaxPercentage' ,'Total'],
				colModel:[ 
						     {
						    	 name:'cat_id',
						    	 hidden:true,
						     },
				          
	               
				     {	name:'itemName',
				    	 width:150,
						
					},
					
				     {	name:'companyName',
				    	 width:150,
						
					},
				           
				   
					{	name:'weight',
						width:100,
						
						
					},
					{	name:'unitName',
						width:100,
						
						
					},
					{	name:'quantity',
						width:100,
						editable: true
						
					},

					{	name:'salePrice',
						width:150,
						editable: true
						
					},
					{	name:'mrp',
						width:140,
						editable: true
						
					},
					
					{	name:'vatPercentage',
						width:100,
						editable: true
						
					},
					{	name:'total',
						width:150,
						formatter: sumFmatter
					},
					
					
				],
					
				
				sortorder : 'desc',
				loadonce: false,
				viewrecords: true,
				width: 1200,
	            height: 350,
	            rowheight: 300,
	            hoverrows: true,
		        rownumbers: true,
	            rowNum: 10,
	            'cellEdit':true,
		           afterSaveCell: function () {
		        	   // $(this).trigger('reloadGrid');
		        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');  
	                var rowData = jQuery("#list4").getRowData(rowId);
	             	var quantity = rowData['quantity'];
	             	var salePrice = rowData['salePrice'];
	             	
	             	var tota = quantity * salePrice;
	             	
	             	$("#list4").jqGrid("setCell", rowId, "total", tota);
		        	},
	           
				pager: "#jqGridPager",
				
				
				
			});
			
		
			//$("#list4").addRowData(i+1,jsonData[i]);
			if(count==0 || count==null)
			{
				 // $("#list4").addRowData(i,jsonData[i]);
				  $("#list4").addRowData(0,jsonData.offer);
			}
			

	     
			 $('#list4').navGrid('#jqGridPager',
		                
		                { edit: true, add: false, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: false },
		                
		                {
		                    editCaption: "The Edit Dialog",
		                   
		                    afterSubmit: function () {
								
		                      var grid = $("#list4"),
							  intervalId = setInterval(
								 function() {
								         grid.trigger("reloadGrid",[{current:true}]);
								   },
								   500);
		                         
		                      
							},
							
							 recreateForm: true,
							 checkOnUpdate : true,
							 checkOnSubmit : true,
			                 closeAfterEdit: true,
							
		                    errorTextFormat: function (data) {
		                        return 'Error: ' + data.responseText
		                    }
		                },
		                
		                {
		                    closeAfterAdd: true,
		                    recreateForm: true,
		                    errorTextFormat: function (data) {
		                        return 'Error: ' + data.responseText
		                    }
		                },
		                
		                {
		                	closeAfterdel:true,
		                	checkOnUpdate : true,
							checkOnSubmit : true,
							recreateForm: true,
		                	
							reloadAftersubmit:true,	
		                    errorTextFormat: function (data) {
		                        return 'Error: ' + data.responseText
		                    }
		                		
		                });			 
			 
				   });
				
			})

}


//get Product detail as per Barcode for credit customer
function getProDetailsAsPerBarcode(){
	 
	var value = document.getElementById("barcode1").value;
	
	var params= {};
	var count=0;
	var newrow;
	var rowId;

	params["methodName"] ="fetchCust";
	params["key"]=value;
	

	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
			{
		  var jsonData = $.parseJSON(data);
			
	      // $("#list4").jqGrid("clearGridData", true).trigger("reloadGrid");
		 
	     $.each(jsonData,function(i,v)
			{
	    	 
	    	 
	        function sumFmatter (cellvalue, options, rowObject)
	        {
	            
	        	var tax = options.rowData.vatPercentage;
	        	
	        	if(tax == 0){
	        		var tot= (options.rowData.quantity * options.rowData.salePrice);
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	if(tax != 0){
	        		
	        		var taxcalculation = (tax/100)* Number(options.rowData.salePrice);
	        		var newSalePrice = Number(taxcalculation) + Number(options.rowData.salePrice);
	        		var tot= (Number(options.rowData.quantity) * Number(newSalePrice));
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	var jam=0;
	        	
	        	
	        	var count = jQuery("#credit").jqGrid('getGridParam', 'records');
	        	var allRowsInGrid1 = $('#credit').getGridParam('data');
	        	var AllRows=JSON.stringify(allRowsInGrid1);
	        	for (var i = 0; i < count; i++) {
	        		
	            	var quantity = allRowsInGrid1[i].quantity;
	             	params["quantity"+i] = quantity;
	             	
	             	var salePrice = allRowsInGrid1[i].salePrice;
	            	params["salePrice"+i] = salePrice;
	            	
	            	var vatPercentage = allRowsInGrid1[i].vatPercentage;
	            	params["vatPercentage"+i] = vatPercentage;
	            	
	            	if(vatPercentage == 0){
	            		
	            		var totals1=(salePrice)*(quantity);
	            		if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	            	}
	            	
	                if(vatPercentage != 0){
	                	
	                	var taxcal = (vatPercentage/100) *Number(salePrice);
	                	var newSalePrice = Number(salePrice) + Number(taxcal);
	                	var totals1=(Number(newSalePrice)*Number(quantity));
	                	if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	                }                	
	            	
          	
      	    }
	        	
	        		
	        		 document.getElementById("totalWithExpense1").value = jam;
	        	
	            	 return tot;

	        }
	        
	        count = jQuery("#credit").jqGrid('getGridParam', 'records'); 
		     var rowdata =$("#credit").jqGrid('getGridParam','data');
		     var ids = jQuery("#credit").jqGrid('getDataIDs');
			 
			
			  var prodName,com,packing,unit;
			  for (var j = 0; j < count; j++) 
			  {
				  prodName = rowdata[j].itemName;
				  com = rowdata[j].companyName;
				  packing = rowdata[j].weight;
				  unit = rowdata[j].unitName;
				
				 var rowId = ids[j];
				 var rowData = jQuery('#credit').jqGrid ('getRowData', rowId);
				
				if (prodName == jsonData.offer.itemName && com == jsonData.offer.companyName && packing == jsonData.offer.weight && unit == jsonData.offer.unitName) {
			    	/*ori_quantity = +rowdata[j].quantity+1;
			    	
			    	$("#list4").jqGrid("setCell", rowId, "quantity", ori_quantity);
			    	var grid = jQuery("#list4");
			    	grid.trigger("reloadGrid");*/
			    	newrow=false;
					alert("Product Name Already Inserted !!!");
					var grid = jQuery("#list4");
				    grid.trigger("reloadGrid");
			    	break;
				}
				else
				{
					newrow = true;
				}
			 }
			  
			  if(newrow == true)
				 {
					
				 // $("#credit").addRowData(i,jsonData[i]);
				  $("#credit").addRowData(count,jsonData.offer);
					
				 }
		
		
		$("#credit").jqGrid({
			datatype: "local",
			
			colNames:['cat_id','ItemName','CompanyName','Packing','Unit','Quantity', 'UnitPrice','MRP','TaxPercentage' ,'Total'],
			colModel:[ 
					     {
					    	 name:'cat_id',
					    	 hidden:true,
					     },
			          
             
			     {	name:'itemName',
			    	 width:150,
					
				},
				
			     {	name:'companyName',
			    	 width:150,
					
				},
			           
			   
				{	name:'weight',
					width:100,
					
				},
				{	name:'unitName',
					width:100,
					
					
				},
				{	name:'quantity',
					width:100,
					editable: true
					
				},

				{	name:'salePrice',
					width:150,
					editable: true
					
				},
				{	name:'mrp',
					width:140,
					editable: true
					
				},
				
				{	name:'vatPercentage',
					width:100,
					editable: true
					
				},
				{	name:'total',
					width:150,
					formatter: sumFmatter
				},
				
				
			],
				
			
			sortorder : 'desc',
			loadonce: false,
			viewrecords: true,
			width: 1200,
          height: 350,
          rowheight: 300,
          hoverrows: true,
	        rownumbers: true,
          rowNum: 10,
          'cellEdit':true,
	           afterSaveCell: function () {
	        	   // $(this).trigger('reloadGrid');
	        	   var rowId =$("#credit").jqGrid('getGridParam','selrow');  
              var rowData = jQuery("#credit").getRowData(rowId);
           	var quantity = rowData['quantity'];
           	var salePrice = rowData['salePrice'];
           	
           	var tota = quantity * salePrice;
           	
           	$("#credit").jqGrid("setCell", rowId, "total", tota);
	        	},
         
			pager: "#jqGridPager",
			
			
			
		});
		
	
		//$("#credit").addRowData(i+1,jsonData[i]);
		if(count==0 || count==null)
		{
			// $("#credit").addRowData(i,jsonData[i]);
			 $("#credit").addRowData(0,jsonData.offer);
		}
		
      
   
		 $('#credit').navGrid('#jqGridPager',
	                
	                { edit: true, add: false, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: false },
	                
	                {
	                    editCaption: "The Edit Dialog",
	                   
	                    afterSubmit: function () {
							
	                      var grid = $("#credit"),
						  intervalId = setInterval(
							 function() {
							         grid.trigger("reloadGrid",[{current:true}]);
							   },
							   500);
	                         
	                      
						},
						
						 recreateForm: true,
						 checkOnUpdate : true,
						 checkOnSubmit : true,
		                 closeAfterEdit: true,
						
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                    closeAfterAdd: true,
	                    recreateForm: true,
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                	closeAfterdel:true,
	                	checkOnUpdate : true,
						checkOnSubmit : true,
						recreateForm: true,
	                	
						reloadAftersubmit:true,	
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                		
	                });
		 
			   });
			
		})

}

/*+++++++++++++  Fetcing Data from goods receive by product name for cash customer 20-5-17++++++++++++*/
function fetchDataByProductName(){
	var params= {};
	//var itemparams={};
	productId = $('#proName').val();
	
	$("#proName option:selected").each(function() {
		   selectedVal = $(this).text();
		});
	
	var splitText = selectedVal.split(",");
	
	var proName = splitText[0];
	var company = splitText[1];
	var weight = splitText[2];
	
	params["proName"]= proName;
	params["company"]= company;
	params["weight"]= weight;
	
	params["methodName"] = "fetchDetailsAsPerProductNameInFertiBill";
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
 	    	{
			  var jsonData = $.parseJSON(data);
				
		      // $("#list4").jqGrid("clearGridData", true).trigger("reloadGrid");
			  var grid = jQuery("#list4");
		    	grid.trigger("reloadGrid");
			  
	        
		     $.each(jsonData,function(i,v)
				{
		    	 
		    	 
		        function sumFmatter (cellvalue, options, rowObject)
		        {
		            
		        	var tax = options.rowData.vatPercentage;
		        	
		        	if(tax == 0){
		        		var tot= (options.rowData.quantity * options.rowData.salePrice);
		        	}
		        	if(tax != 0){
		        		
		        		var taxcalculation = (tax/100)* options.rowData.salePrice;
		        		var tot= (options.rowData.quantity * options.rowData.salePrice) + taxcalculation;
		        	}
		        	var jam=0;
		        	
		        	
		        	var count = jQuery("#list4").jqGrid('getGridParam', 'records');
		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
		        	var AllRows=JSON.stringify(allRowsInGrid1);
		        	for (var i = 0; i < count; i++) {
		        		
		            	var quantity = allRowsInGrid1[i].quantity;
		             	params["quantity"+i] = quantity;
		             	
		             	var salePrice = allRowsInGrid1[i].salePrice;
		            	params["salePrice"+i] = salePrice;
		            	
		            	var vatPercentage = allRowsInGrid1[i].vatPercentage;
		            	params["vatPercentage"+i] = vatPercentage;
		            	
		            	if(vatPercentage == 0){
		            		
		            		var totals1=(salePrice)*(quantity);
			            	jam = jam + totals1;
		            	}
		            	
		                if(vatPercentage != 0){
		                	
		                	var taxcal = (vatPercentage/100) * salePrice;
		                	var totals1=((salePrice)*(quantity)) + taxcal;
			            	jam = jam + totals1;
		                }                	
		            	
	            	
	        	    }
		        	
		        		
		        		 document.getElementById("totalWithExpense").value = jam;
		        	
		            	 return tot;

		        }
		        
		  	
			
			
			$("#list4").jqGrid({
				datatype: "local",
				
				colNames:['pk_goodre_id','supp_id','cat_id','BarcodeNO','ItemName','CompanyName','Packing', 'Quantity', 'UnitPrice','MRP','TaxPercentage' ,'Total'],
				colModel:[ 
				          
				          
	                 {
		                name:'PkGoodreceiveId',
		                hidden:true,
		              
	                 },    
				     {
				    	 name:'supplier_id',
				    	 hidden:true,
				     },
				     {
				    	 name:'cat_id',
				    	 hidden:true,
				     },
				    
				 	{
				    	 name:'barcodeNo',
				    	 width:100,				    	
				    	 
				     },
				     {	name:'itemName',
				    	 width:150,
						
					},
					
				     {	name:'companyName',
				    	 width:150,
						
					},
				           
				   
					{	name:'weight',
						width:100,
						editable: true
						
					},
					
					{	name:'quantity',
						width:100,
						editable: true
						
					},

					{	name:'salePrice',
						width:150,
						editable: true
						
					},
					{	name:'mrp',
						width:140,
						editable: true
						
					},
					
					{	name:'vatPercentage',
						width:100,
						editable: true
						
					},
					{	name:'total',
						width:150,
						formatter: sumFmatter
					},
					
					
				],
					
				
				sortorder : 'desc',
				loadonce: false,
				viewrecords: true,
				width: 1200,
	            height: 350,
	            rowheight: 300,
	            hoverrows: true,
		        rownumbers: true,
	            rowNum: 10,
	            'cellEdit':true,
		           afterSaveCell: function () {
		        	   // $(this).trigger('reloadGrid');
		        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');  
	                var rowData = jQuery("#list4").getRowData(rowId);
	             	var quantity = rowData['quantity'];
	             	var salePrice = rowData['salePrice'];
	             	
	             	var tota = quantity * salePrice;
	             	
	             	$("#list4").jqGrid("setCell", rowId, "total", tota);
		        	},
	           
				pager: "#jqGridPager",
				
				
				
			});
			
		
			$("#list4").addRowData(i+1,jsonData[i]);
	     
			 $('#list4').navGrid('#jqGridPager',
		                
		                { edit: true, add: false, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: false },
		                
		                {
		                    editCaption: "The Edit Dialog",
		                   
		                    afterSubmit: function () {
								
		                      var grid = $("#list4"),
							  intervalId = setInterval(
								 function() {
								         grid.trigger("reloadGrid",[{current:true}]);
								   },
								   500);
		                         
		                      
							},
							
							 recreateForm: true,
							 checkOnUpdate : true,
							 checkOnSubmit : true,
			                 closeAfterEdit: true,
							
		                    errorTextFormat: function (data) {
		                        return 'Error: ' + data.responseText
		                    }
		                },
		                
		                {
		                    closeAfterAdd: true,
		                    recreateForm: true,
		                    errorTextFormat: function (data) {
		                        return 'Error: ' + data.responseText
		                    }
		                },
		                
		                {
		                	closeAfterdel:true,
		                	checkOnUpdate : true,
							checkOnSubmit : true,
							recreateForm: true,
		                	
							reloadAftersubmit:true,	
		                    errorTextFormat: function (data) {
		                        return 'Error: ' + data.responseText
		                    }
		                		
		                });
			 
			 
				   });
				
				
 				
			
				
 			})
}



/*++++++++++++++ Fetch product details by Barcode No for CREDIT customer 22-5-17 ++++++++++++++++++++++++++*/
function getProductDetailsByBarcodeNo1(){

	var params= {};
	
	var barcodeNo1 = $('#barcodeNo1').val();
	
		params["barcodeNo"]= barcodeNo1;
	
	var count=0;
	var newrow;
	var rowId;
	
	params["methodName"] = "getProductDetailsByBarcode";
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
			{
		  var jsonData = $.parseJSON(data);
			
	      // $("#list4").jqGrid("clearGridData", true).trigger("reloadGrid");
		
    
	     $.each(jsonData,function(i,v)
			{
	    	 
	    	 
	        function sumFmatter (cellvalue, options, rowObject)
	        {
	            
	        	var tax = options.rowData.vatPercentage;
	        	
	        	if(tax == 0){
	        		var tot= (options.rowData.quantity * options.rowData.salePrice);
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	if(tax != 0){
	        		
	        		var taxcalculation = (tax/100)* Number(options.rowData.salePrice);
	        		var newSalePrice = Number(taxcalculation) + Number(options.rowData.salePrice)
	        		var tot= (Number(options.rowData.quantity) * Number(newSalePrice));
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	var jam=0;
	        	
	        	
	        	count = jQuery("#credit").jqGrid('getGridParam', 'records');
	        	var allRowsInGrid1 = $('#credit').getGridParam('data');
	        	var AllRows=JSON.stringify(allRowsInGrid1);
	        	for (var i = 0; i < count; i++)
	        	{
	        		var kg = allRowsInGrid1[i].kg;
	        		params["kg"+i] = kg;
	        		
	        		var grams = allRowsInGrid1[i].grams;
	        		params["grams"+i] = grams;
	        		
	            	var quantity = allRowsInGrid1[i].quantity;
	             	params["quantity"+i] = quantity;
	             	
	             	/*var unitName = allRowsInGrid1[i].unitName;
	            	params["unitName"+i] = unitName;*/
	             	
	             	var salePrice = allRowsInGrid1[i].salePrice;
	            	params["salePrice"+i] = salePrice;
	            	
	            	var vatPercentage = allRowsInGrid1[i].vatPercentage;
	            	params["vatPercentage"+i] = vatPercentage;
	            	
	            	if(vatPercentage == 0){
	            		
	            		var totals1=(salePrice)*(quantity);
	            		if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	            	}
	            	
	                if(vatPercentage != 0){
	                	
	                	var taxcal = (vatPercentage/100) * salePrice;
	                	var newSalePrice = Number(salePrice) + Number(taxcal);
	                	var totals1=(Number(newSalePrice)*Number(quantity));
	                	if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	                }                	
	            	
        	
    	    }
	        	
	        		
	        		 document.getElementById("totalWithExpense").value = jam;
	        	
	            	 return tot;

	        }
	        
	         count = jQuery("#credit").jqGrid('getGridParam', 'records'); 
		     var rowdata =$("#credit").jqGrid('getGridParam','data');
		     var ids = jQuery("#credit").jqGrid('getDataIDs');
			 
			
			  var prodName,com,packing,unit, expiryDate;
			  for (var j = 0; j < count; j++) 
			  {
				  prodName = rowdata[j].itemName;
				  com = rowdata[j].companyName;
				  packing = rowdata[j].weight;
				  unit = rowdata[j].unitName;
				  expiryDate = rowdata[j].expiryDate;
				
				 var rowId = ids[j];
				 var rowData = jQuery('#credit').jqGrid ('getRowData', rowId);
				
				if (prodName == jsonData.offer.itemName && com == jsonData.offer.companyName && packing == jsonData.offer.weight && unit == jsonData.offer.unitName && expiryDate == jsonData.offer.expiryDate) {
			    	/*ori_quantity = +rowdata[j].quantity+1;
			    	
			    	$("#list4").jqGrid("setCell", rowId, "quantity", ori_quantity);
			    	var grid = jQuery("#list4");
			    	grid.trigger("reloadGrid");*/
			    	newrow=false;
					alert("Product Name Already Inserted !!!");
					var grid = jQuery("#credit");
				    grid.trigger("reloadGrid");
			    	break;
				}
				else
				{
					newrow = true;
				}
			 }
			  
			  if(newrow == true)
				 {
					
				  //$("#list4").addRowData(i,jsonData[i]);
				  $("#credit").addRowData(count,jsonData.offer);
					
				 }
		
		
		$("#credit").jqGrid({
			datatype: "local",
			
			//colNames:['cat_id','sub_cat_id','ItemName','CompanyName',"HSN",'Packing','Unit', 'UnitPrice','MRP','GST','IGST','Quantity','Total'],
			//colNames:['cat_id','sub_cat_id','ItemName','CompanyName', 'Packing',"HSN",'Unit', 'UnitPrice','MRP','GST','IGST', 'Kg/Ltr', 'Gram/Mili', 'Quantity','Total','StockInGrams','StockInMili','TotalQuantity'],
			colNames:['cat_id','sub_cat_id',"Barcode<br>No",'Item<br>Name','Company<br>Name',"HSN","Batch No.","Packing", "Unit<br>Price",'MRP','GST%','IGST%','Qty','Free<br>Qty',"Unit","Expiry<br>Date",'Total','TotalKgLtrQuantity', 'stockPerEntry'],
			colModel:[ 
					     {
					    	 name:'cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },
					     {
					    	 name:'sub_cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },
							
           
					     {	name:'barcodeNo',
					    	 width:150,
					    	 align:'center',
							
						},
					     {	name:'itemName',
					    	 width:150,
					    	 align:'center',
							
						},

					     {	name:'companyName',
					    	 width:150,
					    	 align:'center',
							
						},
						{	name:'hsn',
							width:80,
							align:'center',
						},
						{	 name:'batchNumber',
							 width:125,
					    	 align:'center',
						},						   
						{	name:'weight',
							width:70,
							hidden:true,
							align:'center',
						},
						
						{	name:'salePrice',
							width:100,
							editable: true,
					    	 align:'center',
						},
						{	name:'mrp',
							width:100,
							editable: true,
							align:'center',
						},
						{	name:'vatPercentage',
							width:80,
							editable: true,
							align:'center',
						},				
						
						/*{	name:'gst',
							width:80,
							editable: true,
							
						},*/
						/*{	name:'sGst',
							width:80,
							editable: true
							
						},*/
						{	name:'igst',
							//width:80,
							//editable: true,
							align:'center',
							hidden:true
							
						},
						{	name:'quantity',
							width:100,
							editable: true,
							align:'center',
						},
						{	
							id:'freeQuantity',
							name:'freeQuantity',
							width:100,
							editable: true,
							align:'center',
						},
						{	name:'unitName',
							width:50,
							align:'center',
						},
						{				
							id:'expiryDate',
							name:'expiryDate',
							width:140,
							editable: false,
							align:'center',
						},						
						{	name:'total',
							width:150,
							align:'center',
							/*formatter: sumFmatter*/
							
						},		
						{	name:'totalKgLtrPiece',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						},
						{	name:'stockPerEntry',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						}
				
			],
				
			
			sortorder : 'desc',
			loadonce: false,
			viewrecords: true,
			width: 1200,
			shrinkToFit:true,
			rowheight: 300,
			hoverrows: true,
	        rownumbers: true,
	        rowNum: 10,
	        'cellEdit':true,
	        autowidth:true,
	        pgbuttons : false,
		    pginput : false,
      	
	        afterSaveCell: function  grossTotal()
			{
				       /* 	Calculation of total after editing quantity*/
				        	   
	        		document.getElementById("discount1").value = 0;
	     			document.getElementById("discountAmount1").value = 0;
	     			document.getElementById("hamaliExpence3").value = 0;
	     			document.getElementById("hamaliExpence1").value = 0;
	        		 
	        		 
				        	   // $(this).trigger('reloadGrid');
				        	   var rowId =$("#credit").jqGrid('getGridParam','selrow');  
		                       var rowData = jQuery("#credit").getRowData(rowId);
		                       var productName = rowData['itemName'];
		                       var quantity = rowData['quantity'];
		                       var freeQuantity = rowData['freeQuantity'];
		                       var salePrice = rowData['salePrice'];
		                       var mrp = rowData['mrp'];
		                       var iGst = rowData['igst'];
		                       var Gst = rowData['gst'];
		                       var unit = rowData['unitName'];
		                       var vatPercentage = rowData['vatPercentage']; 
		                       var stockPerEntry = rowData['stockPerEntry'];
		                       var expiryDate = rowData['expiryDate'];
		                       
		                       if(salePrice != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(salePrice.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid Sale Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
                    				   var setZero = 0;
	 		                    	   $("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		
		 		                    	  gridTotalCalculation();

	                    				return false;
		                    	   }
		                       }
		                       
		                       if(mrp != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(mrp.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid MRP";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    			   var setZero = 0;
	 		                    	   $("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		
	 		                    	  gridTotalCalculation();

	                    				return false;
		                    	   }
		                       }
		                       
		                       if(vatPercentage != "")
		                       {
		                    	   var checkTax = /^[0-9]+$/;
		                    	   if(vatPercentage.match(checkTax))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid GST %";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       if(mrp == "" || mrp == "0")
		                       {}
		                       else
		                       {
			                       if(Number(salePrice) > Number(mrp))
			                       {
			                    	   var msg="MRP must be Greater Than Unit Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				return false;
			                       }
		                       }
		                       
		                       var userKgOrLtrInGramsOrMili = Number(quantity) * 1000;
							                			                    	
		                       var salePricePerGram = 0;
		                    	salePricePerGram = (Number)(salePrice/1000);                    	
		                    	
		                    	//KG CALCULATIONS
		                    	if(unit == 'kg')
	                    		{	
	                    	   		var setFq = 0;
	                    			$("#credit").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    		
	                    			var rowId =$("#credit").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#credit").getRowData(rowId);
	                    			var stockInKg = rowData['totalKgLtrPiece'];
	                    			var stockInGrams;
	                    			if(Number(stockInKg) > 1)
	                    			{
		                    			stockInGrams = stockInKg * 1000;
	                    			}
	                    			else
	                    			{
	                    				stockInGrams = stockInKg;
	                    			}
	                    			
	                    		if(quantity != '')
	                    		{
	                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
	                    			if(quantity.match(checkKiloLtr))
	                    			{
	                    				if(quantity == 0)
	                    				{
	                    					quantity = 0;
		                    				
		                    				var msg="Please Enter Valid kg Value";
		                    				var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
		                    				
		                    				setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
		                    				
/*		                    				var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);*/
				                    		
			 		                    	  gridTotalCalculation();
				                    		
				                    		return false;
	                    				}
	                    			}
	                    			else
	                    			{
	                    				quantity = 0;
	                    				
	                    				var msg="Please Enter Valid kg Value";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		
		 		                    	  gridTotalCalculation();
			                    		return false;
	                    			}
	                    		}
	                    		
	                    		if(expiryDate == 'N/A')
                    			{		                    			
	                    			if(Number(userKgOrLtrInGramsOrMili) > Number(stockInGrams))
			                    	{
		                    			if(Number(stockInGrams) >1000)
		                    				{
		                    					var convertedStockInKg = Number(stockInGrams)/1000;
		                    					//alert(productName+" Available stock = "+convertedStockInKg+" Kg");
		                    					
		                    					 var msg = productName+" Available stock = "+convertedStockInKg+" Kg";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                   	}
		                    				else
		                    				{
		                    					//alert(productName+" Available stock = "+stockInGrams+" Grams");
		                    					 var msg = productName+" Available stock = "+stockInGrams+" Grams";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
		                    				}
		                    			
		                    			var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    					                    		
		 		                    	gridTotalCalculation();
		 		                    	return false;
		                    			
			                    	}
                    			}
	                    		else if(expiryDate !='N/A')
	                    			{
	                    				var stockperEntryInKg = rowData['stockPerEntry'];
		                    			var stockperEntryInGrams;
		                    			if(Number(stockperEntryInKg) > 1)
		                    			{
		                    				stockperEntryInGrams = stockperEntryInKg * 1000;
		                    			}
		                    			else
		                    			{
		                    				stockperEntryInGrams = stockperEntryInKg;
		                    			}
	                    				
	                    			
	                    				if(Number(userKgOrLtrInGramsOrMili) > Number(stockperEntryInGrams))
				                    	{
			                    			if(Number(stockInGrams) >1000)
			                    				{
			                    					var convertedStockInKg = Number(stockperEntryInKg)/1000;
			                    					//alert(productName+" Available stock = "+convertedStockInKg+" Kg");
			                    					
			                    					 var msg = productName+" Available stock = "+stockperEntryInKg+" Kg";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);
					 		                   	}
			                    				else
			                    				{
			                    					//alert(productName+" Available stock = "+stockInGrams+" Grams");
			                    					 var msg = productName+" Available stock = "+stockperEntryInGrams+" Grams";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);
			                    				}
			                    			
			                    			var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
				                    					                    		
			 		                    	gridTotalCalculation();
			 		                    	return false;
			                    	}
	                    			
	                    		}
		                    			KgGramsLtrMiliCalculation();
		                    		
	                    		}
	                    		
		                    	//LTR CALCULATIONS
		                    	if(unit == 'ltr')
	                    		{
		                    		var setFq = 0;
	                    			$("#credit").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    		
		                    		var rowId =$("#credit").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#credit").getRowData(rowId);
	                    			var stockInLtr = rowData['totalKgLtrPiece'];
	                    			var stockInMili = stockInLtr * 1000;
	                    			
	                    			if(quantity != '')
		                    		{	
	                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
	                    			if(quantity.match(checkKiloLtr))
	                    			{
	                    				if(quantity == 0)
	                    				{
	                    					quantity = 0;
		                    				
		                    				var msg="Please Enter Valid kg Value";
		                    				var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
		                    				
		                    				setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
		                    				
		                    				var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
				                    		
			 		                    	gridTotalCalculation();		
				                    		return false;	                    				
	                    				}
	                    			}
	                    			else
	                    			{
	                    				quantity = 0;
	                    				
	                    				   var msg = "Please Enter Valid ltr Value";
		 		                    	   var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
		 		                    	   });
	                    				
		 		                    	   setTimeout(function() {
	                    					dialog.modal('hide');
		 		                    	   }, 1500);
		 		                    	   
	                    				var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    		KgGramsLtrMiliCalculation();
	                    				return false;
	                    			}
	                    		}
	                    			if(expiryDate =='N/A')
	                    			{	                    			
			                    		if(Number(userKgOrLtrInGramsOrMili) > Number(stockInMili))
				                    	{
			                    			if(Number(stockInMili) >1000)
			                    				{
			                    					var convertedStockInLtr = Number(stockInMili)/1000;
			                    					
			                    					 var msg = productName+" Available stock = "+convertedStockInLtr+" Ltr";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);		                    					
			                    				}
			                    				else
			                    				{		                    					
			                    					 var msg = productName+" Available stock = "+stockInMili+" mili";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);
			                    				}
			                    			
			                    			var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
				                    		
				                    		gridTotalCalculation();			        		        	
				        		        	return false;
			                    	}
	                    		}
	                    		else if(expiryDate !='N/A')
	                    		{	
	                    			var stockperEntryInLtr = rowData['stockPerEntry'];
	                    			var stockperEntryInMili;
	                    			if(Number(stockperEntryInLtr) > 1)
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr * 1000;
	                    			}
	                    			else
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr;
	                    			}	                    			
	                    		
	                    			if(Number(userKgOrLtrInGramsOrMili) > Number(stockperEntryInMili))
			                    	{
		                    			if(Number(stockInMili) >1000)
		                    				{
		                    					var convertedStockInLtr = Number(stockperEntryInMili)/1000;
		                    					
		                    					 var msg = productName+" Available stock = "+convertedStockInLtr+" Ltr";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);		                    					
		                    				}
		                    				else
		                    				{		                    					
		                    					 var msg = productName+" Available stock = "+stockperEntryInMili+" mili";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
		                    				}
		                    			
		                    			var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    		
			                    		gridTotalCalculation();			        		        	
			        		        	return false;
			                    	}
	                    		}
		                    		
		                    		KgGramsLtrMiliCalculation();
	                    		}
		                    	
		                    	function KgGramsLtrMiliCalculation()
		                    	{
		                    		if((Number(quantity)>0 || Number(quantity) != '' || quantity != '0'))
		                    		{
			                    		var salePriceKgTota = 0;
			                    		salePriceKgTota = userKgOrLtrInGramsOrMili*salePricePerGram;
			                    		salePriceKgTota = Math.round(salePriceKgTota * 100) / 100;			                    		
	                    				$("#credit").jqGrid("setCell", rowId, "total", salePriceKgTota);
	                    			}
			                    	
			                    	else if(Number(quantity) == 0  || Number(quantity)== '')
			                    	{		
	                    				var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		
		 		                    	gridTotalCalculation();
	 		                    	   
	 		                    	   return false;
			                    	}
		                    	}
		                    	
		                    	if(unit == 'pcs')
				                {
			                    		if(freeQuantity == "" || freeQuantity == "0")
			                    		{
			                    			freeQuantity = 0;
			                    			var setZero = 0;
	                                 		$("#credit").jqGrid("setCell", rowId, "freeQuantity", setZero);
	                                 		quantity = Number(quantity) + Number(freeQuantity);
			                    		}
				                    	else if(freeQuantity != '' || freeQuantity != "0")
			                    		{
											var checkFreeQuantity = /^[0-9]+$/;
								    		if(freeQuantity.match(checkFreeQuantity))
								    		{
								    			quantity = Number(quantity) + Number(freeQuantity);
								    		}
								    		else
								    		{
							    				 var msg="Please Enter Valid Free Quantity";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
				 		                    	  setTimeout(function() {
				                    					dialog.modal('hide');
				                    				}, 1500);
			                    				 
			                    				 quantity = 0;
			                    				 var setZero = 0;
		                                   		 $("#credit").jqGrid("setCell", rowId, "quantity", setZero);
		 			                    		 $("#credit").jqGrid("setCell", rowId, "total", setZero);
		                                   		 $("#credit").jqGrid("setCell", rowId, "freeQuantity", setZero);
		 			                    		 
		                                   		finalCalculation();
		                                   		 	
			                    				return false;			                    			
								    		}
			                    		}
			                    		
			                    		var rowId =$("#credit").jqGrid('getGridParam','selrow');
		                    			var rowData = jQuery("#credit").getRowData(rowId);
		                    			var stockInTotalPieceQuantity = rowData['totalKgLtrPiece'];	                    			
		                    			
		                    			if(quantity != '')
			                    		{
			                    			 var checkQuantity = /^[0-9]+$/;
			                    			 if(String(quantity).match(checkQuantity))
			                    			 {}
			                    			 else
			                    			 {
			                    				 var msg = productName+"Please Enter Valid Quantity";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                    	   
			                    				 quantity = 0;
			                    				 var setZero = 0;
		                                  		 $("#credit").jqGrid("setCell", rowId, "quantity", setZero);
		                                  		 $("#credit").jqGrid("setCell", rowId, "total", setZero);
		                                  		 $("#credit").jqGrid("setCell", rowId, "freeQuantity", setZero);
		                                  		 
				 		                    	  gridTotalCalculation();                                  		 
			                    				 return false;
			                    			 } 
			                    		}
		                    			
		                    			if(expiryDate =='N/A')
			                    		{		
		                    				if ( Number(quantity) > Number(stockInTotalPieceQuantity))
					                    	{					                    		
					                    		var msg = productName+" Available Stock For Expiry Date : "+expiryDate+" = "+stockInTotalPieceQuantity+" Pieces";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function()
				 		                    		{
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                    	   
				 		                    	   var setFq = 0;
				 		                    	   $("#credit").jqGrid("setCell", rowId, "freeQuantity", setFq);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "quantity", setFq);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "total", setFq);
					                    		
				 		                    	  gridTotalCalculation();
				 		                    	  return false;
					                    	}
			                    		}
		                    			if(expiryDate !='N/A')
			                    		{		
		                    				if ( Number(quantity) > Number(stockPerEntry))
					                    	{				                    		
					                    		var msg = productName+" Available stock = "+stockPerEntry+" Pieces";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                    	   
				 		                    	   var zeroQuantity = 0;
				 		                    	   $("#credit").jqGrid("setCell", rowId, "freeQuantity", zeroQuantity);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "quantity", zeroQuantity);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "total", zeroQuantity);
					                    		
				 		                    	  gridTotalCalculation();
				 		                    	  return false;
					                    	}
		                    			}
				                    	
			                    	if (iGst != 0 || iGst != undefined)
			                    	{
		                    			var taxPercentage = iGst;
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
				                    }
			                    	else if(iGst == 0 || iGst == undefined)
			                    	{
			                    		Gst = 0;
			                    		var  taxPercentage = Number(Gst);
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
			                    	}
				                }
			                    		                    	
		                    	gridTotalCalculation();
		                    	
		                    	function gridTotalCalculation()
		                    	{
			                    	var Total =0;
			                    	var Total1 = 0;
		                    		var count = jQuery("#credit").jqGrid('getGridParam', 'records');
		        		        	var allRowsInGrid1 = $('#credit').getGridParam('data');
		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
		        		        	for (var k = 0; k < count; k++)
		        		        	{
		        		        		Total1 = allRowsInGrid1[k].total;
		        		        		if(Total1 == undefined)
	        		        			{
		        		        			Total1 = 0;
	        		        			}
		        		        		Total = +Total + +Total1;
		        		        		Total = Math.round(Total * 100) / 100;
		        		        	}
		        		        	document.getElementById("totalWithExpense1").value = Total;
		        		        	document.getElementById("grossTotal1").value = Total;
	        		        	}  
	                    	
			},
			pager: "#jqGridPager1",
			
		});
		
	
		//$("#list4").addRowData(i+1,jsonData[i]);
		if(count==0 || count==null)
		{
			 // $("#list4").addRowData(i,jsonData[i]);
			  $("#credit").addRowData(0,jsonData.offer);
		}
		

 
		 $('#credit').navGrid('#jqGridPager1',
	                
	                { edit: true, add: false, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: false },
	                
	                {
	                    editCaption: "The Edit Dialog",
	                 
	                    afterSubmit: function () {
							
	                      var grid = $("#credit"),
						  intervalId = setInterval(
						  function()
						  {
					         grid.trigger("reloadGrid",[{current:true}]);
						  },500);
	                         
	                      
						},
						
						 recreateForm: true,
						 checkOnUpdate : true,
						 checkOnSubmit : true,
		                 closeAfterEdit: true, 
		                						
	                    errorTextFormat: function (data)
	                    {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                    closeAfterAdd: true,
	                    recreateForm: true,
	                    errorTextFormat: function (data)
	                    {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                	closeAfterdel:true,
	                	checkOnUpdate : true,
						checkOnSubmit : true,
						recreateForm: true,
	                	
						afterComplete: function() {
							
							document.getElementById("discount1").value = 0;
			     			document.getElementById("discountAmount1").value = 0;
			     			document.getElementById("hamaliExpence3").value = 0;
			     			document.getElementById("hamaliExpence1").value = 0;
							
	                		$('#credit').trigger( 'reloadGrid' );


	 				       /* 	Calculation of total after editing quantity*/
	 				        	   
	 				        	   // $(this).trigger('reloadGrid');
	 				        	   var rowId =$("#credit").jqGrid('getGridParam','selrow');  
	 		                       var rowData = jQuery("#credit").getRowData(rowId);
	 		                    	var quantity = rowData['quantity'];
	 		                    	var salePrice = rowData['salePrice'];
	 		                    	
	 		                    	var iGst = rowData['igst'];
	 		                    	var Gst = rowData['gst'];
	 		                    	
	 		                    	productId = $('#proName1').val();
	 		                    	
	 		                    	$("#proName1 option:selected").each(function() {
	 		                    		   selectedVal = $(this).text();
	 		                    		});
	 		                    	
	 		                    	var splitText = selectedVal.split(",");
	 		                    	
	 		                    	var stock = splitText[4];
	 		                    	
	 		                    	if ( Number(quantity) > Number(stock))
	 		                    	{
	 		                    		alert("Available stock = "+stock);
	 		                    		
	 		                    		var tota = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
			                    		//delete if next line if grid didn't work properly 
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", tota);
			                    		
			                    		
			                    		return false;
			                        }
			                    	//if grid not work properly then just dlt else statement
			                    	else
			                    	{
			                    		var tota = quantity * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
			                    	//}
			                    		
			                    		var Total =0;
			                    		var count = jQuery("#credit").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#credit').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++) {
			        		        		var Total1 = allRowsInGrid1[k].total;
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        	}
			        		        	document.getElementById("totalWithExpense1").value = Total;
			        		        	document.getElementById("grossTotal1").value = Total;
		        		        		//document.getElementById("duptotal").value = Total;
			                    	}
	 		                    		
	 		                    		
	 	                    		
	 	                    	      //  }
	 		                    	
	 			                    		var tota = quantity * salePrice;
	 			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
	 		                    	
	 		                    		
	 		                    		var Total =0;
	 		                    		var count = jQuery("#credit").jqGrid('getGridParam', 'records');
	 		        		        	var allRowsInGrid1 = $('#credit').getGridParam('data');
	 		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
	 		        		        	for (var k = 0; k < count; k++) {
	 		        		        		var Total1 = allRowsInGrid1[k].total;
	 		        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
	 		        		        	}
	 		        		        	document.getElementById("totalWithExpense1").value = Total;
	 		        		        	document.getElementById("grossTotal1").value = Total;
	 	        		        		//document.getElementById("duptotal").value = Total;	 		        		        	
						//}	
						},
						reloadAftersubmit:true,	
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                		
	                });
		 
		 
			   });
			
			
			
		
			
		})
	
}



/*++++++++++++++ Fetch product details by product name for CREDIT customer 22-5-17 ++++++++++++++++++++++++++*/
function getProductDetailsByProductNameForCredit(){

	var params= {};
	
	var productIdCredit = $('#productIdCredit').val();
	
	var splitText = productIdCredit.split(" => ");
	
	/*var proName = splitText[0];
	var company = splitText[1];
	var weight = splitText[2];
	var catId = splitText[5];
	var subCatId=splitText[7];
	var expiryDate=splitText[10];*/
	
	var proName = splitText[0];
	var expiryDate = splitText[1];
	var weight = splitText[2];
	var company = splitText[3];
	var subCatId=splitText[5];
	var catId=splitText[6];
	
	
	/*
	var proName = splitText[0];
	var expiryDate = splitText[1];
	var weight = splitText[2];
	var company = splitText[5];
	var subCatId=splitText[7];
	var catId=splitText[10];*/
	
	
	params["proName"]= proName;
	params["company"]= company;
	params["weight"]= weight;
	params["catId"]= catId;
	params["subCatId"]= subCatId;
	params["expiryDate"]= expiryDate;
	
	var count=0;
	var newrow;
	var rowId;
	
	params["methodName"] = "getProductDetails2";
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
			{
		  var jsonData = $.parseJSON(data);
			
	      // $("#list4").jqGrid("clearGridData", true).trigger("reloadGrid");
		
    
	     $.each(jsonData,function(i,v)
			{
	    	 
	    	 var gstPercentage = v.vatPercentage;
	    	 
	        function sumFmatter (cellvalue, options, rowObject)
	        {
	            
	        	var tax = options.rowData.vatPercentage;
	        	
	        	if(tax == 0){
	        		var tot= (options.rowData.quantity * options.rowData.salePrice);
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	if(tax != 0){
	        		
	        		var taxcalculation = (tax/100)* Number(options.rowData.salePrice);
	        		var newSalePrice = Number(taxcalculation) + Number(options.rowData.salePrice)
	        		var tot= (Number(options.rowData.quantity) * Number(newSalePrice));
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	var jam=0;
	        	
	        	
	        	count = jQuery("#credit").jqGrid('getGridParam', 'records');
	        	var allRowsInGrid1 = $('#credit').getGridParam('data');
	        	var AllRows=JSON.stringify(allRowsInGrid1);
	        	for (var i = 0; i < count; i++)
	        	{
	        		var kg = allRowsInGrid1[i].kg;
	        		params["kg"+i] = kg;
	        		
	        		var grams = allRowsInGrid1[i].grams;
	        		params["grams"+i] = grams;
	        		
	            	var quantity = allRowsInGrid1[i].quantity;
	             	params["quantity"+i] = quantity;
	             	
	             	/*var unitName = allRowsInGrid1[i].unitName;
	            	params["unitName"+i] = unitName;*/
	             	
	             	var salePrice = allRowsInGrid1[i].salePrice;
	            	params["salePrice"+i] = salePrice;
	            	
	            	var vatPercentage = allRowsInGrid1[i].vatPercentage;
	            	params["vatPercentage"+i] = vatPercentage;
	            	
	            	if(vatPercentage == 0){
	            		
	            		var totals1=(salePrice)*(quantity);
	            		if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	            	}
	            	
	                if(vatPercentage != 0){
	                	
	                	var taxcal = (vatPercentage/100) * salePrice;
	                	var newSalePrice = Number(salePrice) + Number(taxcal);
	                	var totals1=(Number(newSalePrice)*Number(quantity));
	                	if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	                }                	
	            	
        	
    	    }
	        	
	        		 document.getElementById("totalWithExpense").value = jam;
	        	
	            	 return tot;

	        }
	        
	         count = jQuery("#credit").jqGrid('getGridParam', 'records'); 
		     var rowdata =$("#credit").jqGrid('getGridParam','data');
		     var ids = jQuery("#credit").jqGrid('getDataIDs');
			 
			
			  var prodName,com,packing,unit, expiryDate;
			  for (var j = 0; j < count; j++) 
			  {
				  prodName = rowdata[j].itemName;
				  com = rowdata[j].companyName;
				  packing = rowdata[j].weight;
				  unit = rowdata[j].unitName;
				  expiryDate = rowdata[j].expiryDate;
				
				 var rowId = ids[j];
				 var rowData = jQuery('#credit').jqGrid ('getRowData', rowId);
				
				if (prodName == jsonData.offer.itemName && com == jsonData.offer.companyName && packing == jsonData.offer.weight && unit == jsonData.offer.unitName && expiryDate == jsonData.offer.expiryDate) {
			    	/*ori_quantity = +rowdata[j].quantity+1;
			    	
			    	$("#list4").jqGrid("setCell", rowId, "quantity", ori_quantity);
			    	var grid = jQuery("#list4");
			    	grid.trigger("reloadGrid");*/
			    	newrow=false;
					alert("Product Name Already Inserted !!!");
					var grid = jQuery("#credit");
				    grid.trigger("reloadGrid");
			    	break;
				}
				else
				{
					newrow = true;
				}
			 }
			  
			  if(newrow == true)
				 {
					
				  //$("#list4").addRowData(i,jsonData[i]);
				  $("#credit").addRowData(count,jsonData.offer);
					
				 }
		
		
		$("#credit").jqGrid({
			datatype: "local",
			
			//colNames:['cat_id','sub_cat_id','ItemName','CompanyName',"HSN",'Packing','Unit', 'UnitPrice','MRP','GST','IGST','Quantity','Total'],
			//colNames:['cat_id','sub_cat_id','ItemName','CompanyName', 'Packing',"HSN",'Unit', 'UnitPrice','MRP','GST','IGST', 'Kg/Ltr', 'Gram/Mili', 'Quantity','Total','StockInGrams','StockInMili','TotalQuantity'],
			colNames:['cat_id','sub_cat_id',"Barcode<br>No",'Item<br>Name','Company<br>Name',"HSN","Batch No.",
				      "Packing", "Unit<br>Price",'MRP','Sale<br>Price<br>Ex','GST%','IGST%','Discount%','Discount<br>Amount',
				      'Total<br>Ex<br>Tax', 'Qty','Free<br>Qty',"Unit","Expiry<br>Date",'Total','TotalKgLtrQuantity', 
				      'stockPerEntry'],
			colModel:[ 
					     {
					    	 name:'cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },
					     {
					    	 name:'sub_cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },
							
           
					     {	name:'barcodeNo',
					    	 width:150,
					    	 align:'center',
							
						},
					     {	name:'itemName',
					    	 width:150,
					    	 align:'center',
							
						},

					     {	name:'companyName',
					    	 width:150,
					    	 align:'center',
							
						},
						{	name:'hsn',
							width:80,
							align:'center',
						},
						{	 name:'batchNumber',
							 width:125,
					    	 align:'center',
						},						   
						{	name:'weight',
							width:70,
							hidden:true,
							align:'center',
						},
						
						{	name:'salePrice',
							width:100,
							editable: true,
					    	 align:'center',
						},
						{	name:'mrp',
							width:100,
							editable: true,
							align:'center',
						},
						
						{	 name:'salePriceEx',
							 width:100,
							 editable: true,
					    	 align:'center',
						},
						
						
						{	name:'vatPercentage',
							width:80,
							editable: true,
							align:'center',
						},				
						
						/*{	name:'gst',
							width:80,
							editable: true,
							
						},*/
						/*{	name:'sGst',
							width:80,
							editable: true
							
						},*/
						{	name:'igst',
							//width:80,
							//editable: true,
							align:'center',
							hidden:true
							
						},
						
						{	 name:'Discount',
							 width:100,
							 hidden:false,
							 editable:true,
					    	 align:'center',
						},
						{	 name:'DiscountAmount',
							 width:100,
							 hidden:false,
					    	 align:'center',
						},
						{	 name:'TotalEx',
							 width:100,
							 //editable: true,
					    	 align:'center',
						},
						
						{	name:'quantity',
							width:100,
							editable: true,
							align:'center',
						},
						{	
							id:'freeQuantity',
							name:'freeQuantity',
							width:100,
							editable: true,
							align:'center',
						},
						{	name:'unitName',
							width:50,
							align:'center',
						},
						{				
							id:'expiryDate',
							name:'expiryDate',
							width:140,
							editable: false,
							align:'center',
						},						
						{	name:'total',
							width:150,
							align:'center',
							/*formatter: sumFmatter*/
							
						},		
						{	name:'totalKgLtrPiece',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						},
						{	name:'stockPerEntry',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						}
				
			],
				
			
			sortorder : 'desc',
			loadonce: false,
			viewrecords: true,
			width: 1200,
			shrinkToFit:true,
			rowheight: 300,
			hoverrows: true,
	        rownumbers: true,
	        rowNum: 10,
	        'cellEdit':true,
	        autowidth:true,
	        pgbuttons : false,
		    pginput : false,
      	
	        afterSaveCell: function  grossTotal()
			{
				       /* 	Calculation of total after editing quantity*/
				        	   
	        		document.getElementById("discount1").value = 0;
	     			document.getElementById("discountAmount1").value = 0;
	     			document.getElementById("hamaliExpence3").value = 0;
	     			document.getElementById("hamaliExpence1").value = 0;
	        		 
	        		 
				        	   // $(this).trigger('reloadGrid');
				        	   var rowId =$("#credit").jqGrid('getGridParam','selrow');  
		                       var rowData = jQuery("#credit").getRowData(rowId);
		                       var productName = rowData['itemName'];
		                       var quantity = rowData['quantity'];
		                       var freeQuantity = rowData['freeQuantity'];
		                       var salePrice = rowData['salePrice'];
		                       var mrp = rowData['mrp'];
		                       var iGst = rowData['igst'];
		                    
		                       var unit = rowData['unitName'];
		                       var gst = rowData['vatPercentage']; 
		                       var stockPerEntry = rowData['stockPerEntry'];
		                       var expiryDate = rowData['expiryDate'];
		                     
		                       
		                       if(salePrice != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(salePrice.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid Sale Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
                    				   var setZero = 0;
	 		                    	   $("#credit").jqGrid("setCell", rowId, "salePrice", setZero);
			                    		
		 		                    	  gridTotalCalculation();

	                    				return false;
		                    	   }
		                       }
		                       
		                       if(mrp != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(mrp.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid MRP";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    			   var setZero = 0;
	 		                    	   $("#credit").jqGrid("setCell", rowId, "mrp", setZero);
			                    		
	 		                    	  gridTotalCalculation();

	                    				return false;
		                    	   }
		                       }
		                       
		                       
		                       
		                       /*if(vatPercentage != "")
		                       {
		                    	   var checkTax = /^[0-9]+$/;
		                    	   if(vatPercentage.match(checkTax))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid GST %";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				// var setZero = 0;
		 		                    	 $("#credit").jqGrid("setCell", rowId, "vatPercentage", gstPercentage);
	                    				
	                    				return false;
		                    	   }
		                       }*/
		                       
		                       if(mrp == "" || mrp == "0")
		                       {}
		                       else
		                       {
			                       if(Number(salePrice) > Number(mrp))
			                       {
			                    	   var msg="MRP must be Greater Than Unit Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				return false;
			                       }
		                       }
		                       
		                       var userKgOrLtrInGramsOrMili = Number(quantity) * 1000;
							                			                    	
		                       var salePricePerGram = 0;
		                    	salePricePerGram = (Number)(salePrice/1000);                    	
		                    	
		                    	//KG CALCULATIONS
		                    	if(unit == 'kg')
	                    		{	
	                    	   		var setFq = 0;
	                    			$("#credit").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    		
	                    			var rowId =$("#credit").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#credit").getRowData(rowId);
	                    			var stockInKg = rowData['totalKgLtrPiece'];
	                    			var stockInGrams;
	                    			if(Number(stockInKg) > 1)
	                    			{
		                    			stockInGrams = stockInKg * 1000;
	                    			}
	                    			else
	                    			{
	                    				stockInGrams = stockInKg;
	                    			}
	                    			
	                    		if(quantity != '')
	                    		{
	                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
	                    			if(quantity.match(checkKiloLtr))
	                    			{
	                    				if(quantity == 0)
	                    				{
	                    					quantity = 0;
		                    				
		                    				var msg="Please Enter Valid kg Value";
		                    				var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
		                    				
		                    				setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
		                    				
/*		                    				var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);*/
				                    		
			 		                    	  gridTotalCalculation();
				                    		
				                    		return false;
	                    				}
	                    			}
	                    			else
	                    			{
	                    				quantity = 0;
	                    				
	                    				var msg="Please Enter Valid kg Value";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		
		 		                    	  gridTotalCalculation();
			                    		return false;
	                    			}
	                    		}
	                    		
	                    		if(expiryDate == 'N/A')
                    			{		                    			
	                    			if(Number(userKgOrLtrInGramsOrMili) > Number(stockInGrams))
			                    	{
		                    			if(Number(stockInGrams) >1000)
		                    				{
		                    					var convertedStockInKg = Number(stockInGrams)/1000;
		                    					//alert(productName+" Available stock = "+convertedStockInKg+" Kg");
		                    					
		                    					 var msg = productName+" Available stock = "+convertedStockInKg+" Kg";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                   	}
		                    				else
		                    				{
		                    					//alert(productName+" Available stock = "+stockInGrams+" Grams");
		                    					 var msg = productName+" Available stock = "+stockInGrams+" Grams";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
		                    				}
		                    			
		                    			var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    					                    		
		 		                    	gridTotalCalculation();
		 		                    	return false;
		                    			
			                    	}
                    			}
	                    		else if(expiryDate !='N/A')
	                    			{
	                    				var stockperEntryInKg = rowData['stockPerEntry'];
		                    			var stockperEntryInGrams;
		                    			if(Number(stockperEntryInKg) > 1)
		                    			{
		                    				stockperEntryInGrams = stockperEntryInKg * 1000;
		                    			}
		                    			else
		                    			{
		                    				stockperEntryInGrams = stockperEntryInKg;
		                    			}
	                    				
	                    			
	                    				if(Number(userKgOrLtrInGramsOrMili) > Number(stockperEntryInGrams))
				                    	{
			                    			if(Number(stockInGrams) >1000)
			                    				{
			                    					var convertedStockInKg = Number(stockperEntryInKg)/1000;
			                    					//alert(productName+" Available stock = "+convertedStockInKg+" Kg");
			                    					
			                    					 var msg = productName+" Available stock = "+stockperEntryInKg+" Kg";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);
					 		                   	}
			                    				else
			                    				{
			                    					//alert(productName+" Available stock = "+stockInGrams+" Grams");
			                    					 var msg = productName+" Available stock = "+stockperEntryInGrams+" Grams";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);
			                    				}
			                    			
			                    			var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
				                    					                    		
			 		                    	gridTotalCalculation();
			 		                    	return false;
			                    	}
	                    			
	                    		}
		                    			KgGramsLtrMiliCalculation();
		                    		
	                    		}
	                    		
		                    	//LTR CALCULATIONS
		                    	if(unit == 'ltr')
	                    		{
		                    		var setFq = 0;
	                    			$("#credit").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    		
		                    		var rowId =$("#credit").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#credit").getRowData(rowId);
	                    			var stockInLtr = rowData['totalKgLtrPiece'];
	                    			var stockInMili = stockInLtr * 1000;
	                    			
	                    			if(quantity != '')
		                    		{	
	                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
	                    			if(quantity.match(checkKiloLtr))
	                    			{
	                    				if(quantity == 0)
	                    				{
	                    					quantity = 0;
		                    				
		                    				var msg="Please Enter Valid kg Value";
		                    				var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
		                    				
		                    				setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
		                    				
		                    				var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
				                    		
			 		                    	gridTotalCalculation();		
				                    		return false;	                    				
	                    				}
	                    			}
	                    			else
	                    			{
	                    				quantity = 0;
	                    				
	                    				   var msg = "Please Enter Valid ltr Value";
		 		                    	   var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
		 		                    	   });
	                    				
		 		                    	   setTimeout(function() {
	                    					dialog.modal('hide');
		 		                    	   }, 1500);
		 		                    	   
	                    				var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    		KgGramsLtrMiliCalculation();
	                    				return false;
	                    			}
	                    		}
	                    			if(expiryDate =='N/A')
	                    			{	                    			
			                    		if(Number(userKgOrLtrInGramsOrMili) > Number(stockInMili))
				                    	{
			                    			if(Number(stockInMili) >1000)
			                    				{
			                    					var convertedStockInLtr = Number(stockInMili)/1000;
			                    					
			                    					 var msg = productName+" Available stock = "+convertedStockInLtr+" Ltr";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);		                    					
			                    				}
			                    				else
			                    				{		                    					
			                    					 var msg = productName+" Available stock = "+stockInMili+" mili";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
					 		                    	   });
				                    				
					 		                    	   setTimeout(function() {
				                    					dialog.modal('hide');
					 		                    	   }, 1500);
			                    				}
			                    			
			                    			var setZero = 0;
				                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
				                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
				                    		
				                    		gridTotalCalculation();			        		        	
				        		        	return false;
			                    	}
	                    		}
	                    		else if(expiryDate !='N/A')
	                    		{	
	                    			var stockperEntryInLtr = rowData['stockPerEntry'];
	                    			var stockperEntryInMili;
	                    			if(Number(stockperEntryInLtr) > 1)
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr * 1000;
	                    			}
	                    			else
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr;
	                    			}	                    			
	                    		
	                    			if(Number(userKgOrLtrInGramsOrMili) > Number(stockperEntryInMili))
			                    	{
		                    			if(Number(stockInMili) >1000)
		                    				{
		                    					var convertedStockInLtr = Number(stockperEntryInMili)/1000;
		                    					
		                    					 var msg = productName+" Available stock = "+convertedStockInLtr+" Ltr";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);		                    					
		                    				}
		                    				else
		                    				{		                    					
		                    					 var msg = productName+" Available stock = "+stockperEntryInMili+" mili";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
		                    				}
		                    			
		                    			var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", setZero);
			                    		
			                    		gridTotalCalculation();			        		        	
			        		        	return false;
			                    	}
	                    		}
		                    		
		                    		KgGramsLtrMiliCalculation();
	                    		}
		                    	
		                    	function KgGramsLtrMiliCalculation()
		                    	{
		                    		if((Number(quantity)>0 || Number(quantity) != '' || quantity != '0'))
		                    		{
			                    		var salePriceKgTota = 0;
			                    		salePriceKgTota = userKgOrLtrInGramsOrMili*salePricePerGram;
			                    		salePriceKgTota = Math.round(salePriceKgTota * 100) / 100;			                    		
	                    				$("#credit").jqGrid("setCell", rowId, "total", salePriceKgTota);
	                    			}
			                    	
			                    	else if(Number(quantity) == 0  || Number(quantity)== '')
			                    	{		
	                    				var setZero = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", setZero);
			                    		
		 		                    	gridTotalCalculation();
	 		                    	   
	 		                    	   return false;
			                    	}
		                    	}
		                    	
		                    	if(unit == 'pcs')
				                {
			                    		if(freeQuantity == "" || freeQuantity == "0")
			                    		{
			                    			freeQuantity = 0;
			                    			var setZero = 0;
	                                 		$("#credit").jqGrid("setCell", rowId, "freeQuantity", setZero);
	                                 		quantity = Number(quantity) + Number(freeQuantity);
			                    		}
				                    	else if(freeQuantity != '' || freeQuantity != "0")
			                    		{
											var checkFreeQuantity = /^[0-9]+$/;
								    		if(freeQuantity.match(checkFreeQuantity))
								    		{
								    			quantity = Number(quantity) + Number(freeQuantity);
								    		}
								    		else
								    		{
							    				 var msg="Please Enter Valid Free Quantity";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
				 		                    	  setTimeout(function() {
				                    					dialog.modal('hide');
				                    				}, 1500);
			                    				 
			                    				 quantity = 0;
			                    				 var setZero = 0;
		                                   		 $("#credit").jqGrid("setCell", rowId, "quantity", setZero);
		 			                    		 $("#credit").jqGrid("setCell", rowId, "total", setZero);
		                                   		 $("#credit").jqGrid("setCell", rowId, "freeQuantity", setZero);
		 			                    		 
		                                   		finalCalculation();
		                                   		 	
			                    				return false;			                    			
								    		}
			                    		}
			                    		
			                    		var rowId =$("#credit").jqGrid('getGridParam','selrow');
		                    			var rowData = jQuery("#credit").getRowData(rowId);
		                    			var stockInTotalPieceQuantity = rowData['totalKgLtrPiece'];	                    			
		                    			
		                    			if(quantity != '')
			                    		{
			                    			 var checkQuantity = /^[0-9]+$/;
			                    			 if(String(quantity).match(checkQuantity))
			                    			 {}
			                    			 else
			                    			 {
			                    				 var msg = productName+"Please Enter Valid Quantity";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                    	   
			                    				 quantity = 0;
			                    				 var setZero = 0;
		                                  		 $("#credit").jqGrid("setCell", rowId, "quantity", setZero);
		                                  		 $("#credit").jqGrid("setCell", rowId, "total", setZero);
		                                  		 $("#credit").jqGrid("setCell", rowId, "freeQuantity", setZero);
		                                  		 
				 		                    	  gridTotalCalculation();                                  		 
			                    				 return false;
			                    			 } 
			                    		}
		                    			
		                    			if(expiryDate =='N/A')
			                    		{		
		                    				if ( Number(quantity) > Number(stockInTotalPieceQuantity))
					                    	{					                    		
					                    		var msg = productName+" Available Stock For Expiry Date : "+expiryDate+" = "+stockInTotalPieceQuantity+" Pieces";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function()
				 		                    		{
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                    	   
				 		                    	   var setFq = 0;
				 		                    	   $("#credit").jqGrid("setCell", rowId, "freeQuantity", setFq);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "quantity", setFq);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "total", setFq);
					                    		
				 		                    	  gridTotalCalculation();
				 		                    	  return false;
					                    	}
			                    		}
		                    			if(expiryDate !='N/A')
			                    		{		
		                    				if ( Number(quantity) > Number(stockPerEntry))
					                    	{				                    		
					                    		var msg = productName+" Available stock = "+stockPerEntry+" Pieces";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
				 		                    	   });
			                    				
				 		                    	   setTimeout(function() {
			                    					dialog.modal('hide');
				 		                    	   }, 1500);
				 		                    	   
				 		                    	   var zeroQuantity = 0;
				 		                    	   $("#credit").jqGrid("setCell", rowId, "freeQuantity", zeroQuantity);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "quantity", zeroQuantity);
				 		                    	   $("#credit").jqGrid("setCell", rowId, "total", zeroQuantity);
					                    		
				 		                    	  gridTotalCalculation();
				 		                    	  return false;
					                    	}
		                    			}
				                    	
			                    	/*if (iGst != 0 || iGst != undefined)
			                    	{
		                    			var taxPercentage = iGst;
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
				                    }
			                    	else if(iGst == 0 || iGst == undefined)
			                    	{
			                    		Gst = 0;
			                    		var  taxPercentage = Number(Gst);
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
			                    	}*/
				                }
			                    		                    	
		                    	gridTotalCalculation();
		                    	
		                    	
		                    	
		                    	var quantity = rowData['quantity'];
                                var DiscountAmount= rowData['DiscountAmount'];
								var Discount= rowData['Discount'];
								var TotalEx= rowData['TotalEx'];
								var salePriceEx= rowData['salePriceEx'];
								

								/*/////calculation of gst////
*/														
								var checkDiscount = /^[0-9]+\.?[0-9]*$/;
								  if(Discount.match(checkDiscount))
								  {
									  if(Number(Discount) >= 100)
							  	  {
							  		  var setZero = 0;
							  		  //alert("In discount")
							  		  myAlert("Discount Percentage Must Be Less Than 100");
							  	      $("#credit").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							      	  totalCalC();
							      	  totalDisC();
							  		  return false;
							  	  }
								  }
								  else
								  {
									  var setZero = 0;
									   if(Discount== "")
									   {
							  		 $("#credit").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							      	  }
									  else
									  {
							  		  myAlert("Pleaee Enter Valid Discount value");
							  		  $("#credit").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							      	  totalCalC();
							      	  totalDisC();
							      	  return false;
									  }
								  }			
								  
								//sale price excluding tax calc//
								  var checksalePrice= /^[0-9]+\.?[0-9]*$/;
								  if(salePrice.match(checksalePrice))
							    {
							      	  if(Number(salePrice) > 0)
							  		  {
							      		 
							      		  spwTax = (salePrice/(1+(gst/100)));
							      		  $("#credit").jqGrid("setCell", rowId, "salePriceEx",  spwTax.toFixed(2));
							      	  
							      	/*if(Number(Discount) > 0)
							      	 {*/
							      		 if(Number(gst) > 0)
							      		 {  
							      			 
							      		     DiscountAmount= (spwTax*(Discount/100));
							      			  finalsp = spwTax- DiscountAmount;
							      			  newTaxAmt = (((finalsp*quantity)*gst)/100);
							      			  var oneProTax = (((finalsp)*gst)/100);
							      			  newFinalsp = finalsp+ oneProTax;
							      			  tota = newFinalsp* quantity;
							      			  //DiscountAmount = DiscountAmount * quantity;
							      			 
							      		
							      		
									    $("#credit").jqGrid("setCell", rowId, "TotalEx", finalsp.toFixed(2));
									    $("#credit").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));	
									    $("#credit").jqGrid("setCell", rowId, "total", tota.toFixed(2));
									 /* $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", newTaxAmt.toFixed(2));*/
									     totalCalC();
									     totalDisC();	
							      		}
							      	else if(Number(gst) == 0)
							      	{
							      		 var setZero = 0;
							      		spwTax= saleprice - 0;
							      		DiscountAmount = (spwTax*(Discount/100));
							      		finalsp= spwTax- DiscountAmount;
							      		newTaxAmt = ((( finalsp*quantity)*gst)/100);				        			  
							      		var oneProTax = (((finalsp)*gst)/100);
							      		newFinalsp =  finalsp+ oneProTax;	        			  
							      		tota = newFinalsp * quantity;
							      		DiscountAmount = DiscountAmount* quantity;
							      			 	
							      			  
							      $("#credit").jqGrid("setCell", rowId, "TotalEx", finalsp.toFixed(2));
								  $("#credit").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));
								  $("#credit").jqGrid("setCell", rowId, "total", tota.toFixed(2));
								 /* $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", newTaxAmt.toFixed(2));*/
								  totalCalC();
								  totalDisC();
							      	}
							      	  
							      	 //}
						
							      	else
							    {
							     var setZero = 0; 
								  $("#credit").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							   /* $("#list4").jqGrid("setCell", rowId, "Total", tota.toFixed(2));*/
							  /*  $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", setZero);*/
							     }
							        }
						else
					         {
							  	var setZero = 0;
							  /*$("#list4").jqGrid("setCell", rowId, "sPWithoutTax", setZero);*/
							    $("#credit").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							    $("#credit").jqGrid("setCell", rowId, "total", setZero);
							 /* $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", setZero);*/
							    return false;
								}
							    }				        	  
						else
							{
								var setZero = 0;
								alert("Pleae Enter Valid Buy Price");
								$("#credit").jqGrid("setCell", rowId, "salePrice", setZero);
								$("#credit").jqGrid("setCell", rowId, "salePriceEx", setZero);
							  	$("#credit").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							  	$("#credit").jqGrid("setCell", rowId, "total", setZero);
							 /*  $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", setZero);*/
									  return false;
								 }
								
								/*if(Gst!=0)
								{
								var taxpercentage = Gst;
								var salepriceEx=saleprice(1+(taxpercentage/100)));
								
								
							$("#jqGrid").jqGrid("setCell",rowId,"salepriceEx",salepriceEx);
							
							return false;
				                         }
								*/
								  //out of grid calc//
								
									function totalCalC()
									{	
									       var Total = 0;
									       var totAmtWithTax = 0;
									       var count = jQuery("#credit").jqGrid('getGridParam', 'records');
									       var allRowsInGrid1 = $('#credit').getGridParam('data');
									        var AllRows=JSON.stringify(allRowsInGrid1);
									       //alert("total...." +total);
									       for (var k = 0; k < count; k++)
									       {
									          var Total1 = allRowsInGrid1[k].total;//grid total 

									        if(Total1 != undefined)
									        {
									        		Total = +Total + +Total1;
									        		  }
									        	  }
									        	  /*for (var j = 0; j < count; j++)
									        	  {
									        		  var Total2 = allRowsInGrid1[j].taxAmount;
									        		  var Total3 = allRowsInGrid1[j].Total;
									        		  if(Total2 != undefined)
									        		  {
									        			  totAmtWithTax = +totAmtWithTax + +Total2 + +Total3;
									        		  }
									        	  }*/
									        	  document.getElementById("totalWithExpense1").value = +Total.toFixed(2);
//									        	  document.getElementById("totalAmount").value = Total.toFixed(2);//Math.round(Total);
									        	  document.getElementById("grossTotal").value = Total.toFixed(2);//Math.round(Total);
									        	  var totAmount = Total.toFixed(2);//Math.round(Total);
										        }
									        	  
									      function totalDisC()
										   {
											    //TOTAL DISCOUNT AMOUNT
											       var TotalDisAmt = 0;
											        var TotalsPAmt = 0;
											        var disPer = 0;
											        var count = jQuery("#credit").jqGrid('getGridParam', 'records');
											        var allRowsInGrid1 = $('#credit').getGridParam('data');
											        var AllRows=JSON.stringify(allRowsInGrid1);
											        for (var l = 0; l < count; l++)
											        {
											          var TotalDisAmt1 = allRowsInGrid1[l].DiscountAmount;
											          var TotalsPAmt1 = allRowsInGrid1[l].salePriceEx;
											        		  
											        if(TotalsPAmt1 != undefined)
											        {
											        	TotalsPAmt = (+TotalsPAmt + +TotalsPAmt1).toFixed(2);
											        }
											        if(TotalDisAmt1 != undefined)
											        {
											        	TotalDisAmt = (+TotalDisAmt + +TotalDisAmt1).toFixed(2);
											        	disPer = ((TotalDisAmt/TotalsPAmt)*100).toFixed(2);
											        }						        	 
											        	  }
											        	  /*document.getElementById("discount1").value = disPer;*/
											        	 // document.getElementById("discount").value = TotalDisAmt;
										        	 }
		                    	
		                    	function gridTotalCalculation()
		                    	{
			                    	var Total =0;
			                    	var Total1 = 0;
		                    		var count = jQuery("#credit").jqGrid('getGridParam', 'records');
		        		        	var allRowsInGrid1 = $('#credit').getGridParam('data');
		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
		        		        	for (var k = 0; k < count; k++)
		        		        	{
		        		        		Total1 = allRowsInGrid1[k].total;
		        		        		if(Total1 == undefined)
	        		        			{
		        		        			Total1 = 0;
	        		        			}
		        		        		Total = +Total + +Total1;
		        		        		Total = Math.round(Total * 100) / 100;
		        		        	}
		        		        	document.getElementById("totalWithExpense1").value = Total;
		        		        	document.getElementById("grossTotal1").value = Total;
	        		        	}  
	                    	
			},
			pager: "#jqGridPager1",
			
		});
		
	
		//$("#list4").addRowData(i+1,jsonData[i]);
		if(count==0 || count==null)
		{
			 // $("#list4").addRowData(i,jsonData[i]);
			  $("#credit").addRowData(0,jsonData.offer);
		}
		

 
		 $('#credit').navGrid('#jqGridPager1',
	                
	                { edit: true, add: false, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: false },
	                
	                {
	                    editCaption: "The Edit Dialog",
	                 
	                    afterSubmit: function () {
							
	                      var grid = $("#credit"),
						  intervalId = setInterval(
						  function()
						  {
					         grid.trigger("reloadGrid",[{current:true}]);
						  },500);
	                         
	                      
						},
						
						 recreateForm: true,
						 checkOnUpdate : true,
						 checkOnSubmit : true,
		                 closeAfterEdit: true, 
		                						
	                    errorTextFormat: function (data)
	                    {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                    closeAfterAdd: true,
	                    recreateForm: true,
	                    errorTextFormat: function (data)
	                    {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                	closeAfterdel:true,
	                	checkOnUpdate : true,
						checkOnSubmit : true,
						recreateForm: true,
	                	
						afterComplete: function() {
							
							document.getElementById("discount1").value = 0;
			     			document.getElementById("discountAmount1").value = 0;
			     			document.getElementById("hamaliExpence3").value = 0;
			     			document.getElementById("hamaliExpence1").value = 0;
							
	                		$('#credit').trigger( 'reloadGrid' );


	 				       /* 	Calculation of total after editing quantity*/
	 				        	   
	 				        	   // $(this).trigger('reloadGrid');
	 				        	   var rowId =$("#credit").jqGrid('getGridParam','selrow');  
	 		                       var rowData = jQuery("#credit").getRowData(rowId);
	 		                    	var quantity = rowData['quantity'];
	 		                    	var salePrice = rowData['salePrice'];
	 		                    	
	 		                    	var iGst = rowData['igst'];
	 		                    	var Gst = rowData['gst'];
	 		                    	
	 		                    	productId = $('#proName1').val();
	 		                    	
	 		                    	$("#proName1 option:selected").each(function() {
	 		                    		   selectedVal = $(this).text();
	 		                    		});
	 		                    	
	 		                    	var splitText = selectedVal.split(",");
	 		                    	
	 		                    	var stock = splitText[4];
	 		                    	
	 		                    	if ( Number(quantity) > Number(stock))
	 		                    	{
	 		                    		alert("Available stock = "+stock);
	 		                    		
	 		                    		var tota = 0;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
			                    		//delete if next line if grid didn't work properly 
			                    		$("#credit").jqGrid("setCell", rowId, "quantity", tota);
			                    		
			                    		
			                    		return false;
			                        }
			                    	//if grid not work properly then just dlt else statement
			                    	else
			                    	{
			                    		var tota = quantity * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
			                    	//}
			                    		
			                    		var Total =0;
			                    		var count = jQuery("#credit").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#credit').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++) {
			        		        		var Total1 = allRowsInGrid1[k].total;
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        	}
			        		        	document.getElementById("totalWithExpense1").value = Total;
			        		        	document.getElementById("grossTotal1").value = Total;
		        		        		//document.getElementById("duptotal").value = Total;
			                    	}
	 		                    		
	 		                    		
	 		                    		
	 		                    		
	 		                    		
	 		                    		
	 	                    		
	 	                    	      //  }
	 		                    	
	 			                    		var tota = quantity * salePrice;
	 			                    		$("#credit").jqGrid("setCell", rowId, "total", tota);
	 		                    	
	 		                    		
	 		                    		var Total =0;
	 		                    		var count = jQuery("#credit").jqGrid('getGridParam', 'records');
	 		        		        	var allRowsInGrid1 = $('#credit').getGridParam('data');
	 		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
	 		        		        	for (var k = 0; k < count; k++) {
	 		        		        		var Total1 = allRowsInGrid1[k].total;
	 		        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
	 		        		        	}
	 		        		        	document.getElementById("totalWithExpense1").value = Total;
	 		        		        	document.getElementById("grossTotal1").value = Total;
	 	        		        		//document.getElementById("duptotal").value = Total;	 		        		        	
						//}	
						},
						reloadAftersubmit:true,	
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                		
	                });
		 
		 
			   });
			
			
			
		
			
		})
	
}


// Get Details By Barcode No
function getProductDetailsByBarcodeNo()
{	
	var params= {};
	
	var barcodeNo = $('#barcodeNo').val();


	params["barcodeNo"]= barcodeNo;
	
	var count=0;
	var newrow;
	var rowId;
	
	params["methodName"] = "getProductDetailsByBarcode";
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
 	   {
		  var jsonData = $.parseJSON(data);
			
	      // $("#list4").jqGrid("clearGridData", true).trigger("reloadGrid");
		
      
	     $.each(jsonData,function(i,v)
		 {
	    	 
	    	 
	        function sumFmatter (cellvalue, options, rowObject)
	        {	            
	        	var tax = options.rowData.vatPercentage;
	        	
	        	if(tax == 0){
	        		var tot= (options.rowData.quantity * options.rowData.salePrice);
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	if(tax != 0)
	        	{	        		
	        		var taxcalculation = (tax/100)* Number(options.rowData.salePrice);
	        		var newSalePrice = Number(taxcalculation) + Number(options.rowData.salePrice)
	        		var tot= (Number(options.rowData.quantity) * Number(newSalePrice));
	        		if(isNaN(tot))
	        		{
	        			tot = 0;
					}
	        	}
	        	var jam=0;
	        	
	        	
	        	count = jQuery("#list4").jqGrid('getGridParam', 'records');
	        	var allRowsInGrid1 = $('#list4').getGridParam('data');
	        	var AllRows=JSON.stringify(allRowsInGrid1);
	        	for (var i = 0; i < count; i++)
	        	{	        		
	        		var kg = allRowsInGrid1[i].kg;
	        		params["kg"+i] = kg;
	        		
	        		var grams = allRowsInGrid1[i].grams;
	        		params["grams"+i] = grams;
	        		
	        		var defQuantity = 1;	        		
	            	var quantity = allRowsInGrid1[i].quantity;
	             	params["quantity"+i] = quantity;
	             	
	             	var salePrice = allRowsInGrid1[i].salePrice;
	            	params["salePrice"+i] = salePrice;
	            	
	            	var vatPercentage = allRowsInGrid1[i].vatPercentage;
	            	params["vatPercentage"+i] = vatPercentage;
	            		            	
	            	/*var stockInGrams = allRowsInGrid1[i].stockInGrams;
	            	params["stockInGrams"+i] = stockInGrams;
	            	
	   	    	 	var stockInMili = allRowsInGrid1[i].stockInMili;
	            	params["stockInMili"+i] = stockInMili;
	            	
	   	    	 	var totalPieceQuantity = allRowsInGrid1[i].totalPieceQuantity;
	            	params["totalPieceQuantity"+i] = totalPieceQuantity;*/
	            	
	            	if(vatPercentage == 0){
	            		
	            		var totals1=(salePrice)*(quantity);
	            		if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	            	}
	            	
	                if(vatPercentage != 0){
	                	
	                	var taxcal = (vatPercentage/100) * salePrice;
	                	var newSalePrice = Number(salePrice) + Number(taxcal);
	                	var totals1=(Number(newSalePrice)*Number(quantity));
	                	if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	                }                	
	           }

	        	document.getElementById("totalWithExpense").value = jam;
	        	
	            return tot;

	        }
	        
	         count = jQuery("#list4").jqGrid('getGridParam', 'records'); 
		     var rowdata =$("#list4").jqGrid('getGridParam','data');
		     var ids = jQuery("#list4").jqGrid('getDataIDs');
			
			  var prodName,com,packing,unit, expiryDate;
			  
			  for (var j = 0; j < count; j++) 
			  {
				  prodName = rowdata[j].itemName;
				  com = rowdata[j].companyName;
				  packing = rowdata[j].weight;
				  unit = rowdata[j].unitName;
				  expiryDate = rowdata[j].expiryDate;
				
				 var rowId = ids[j];
				 var rowData = jQuery('#list4').jqGrid ('getRowData', rowId);
				
				if (prodName == jsonData.offer.itemName && com == jsonData.offer.companyName && packing == jsonData.offer.weight && unit == jsonData.offer.unitName && expiryDate == jsonData.offer.expiryDate)
				{
			    	/*ori_quantity = +rowdata[j].quantity+1;
			    	
			    	$("#list4").jqGrid("setCell", rowId, "quantity", ori_quantity);
			    	var grid = jQuery("#list4");
			    	grid.trigger("reloadGrid");*/
			    	newrow=false;
					alert("Product Name Already Inserted !!!");
					var grid = jQuery("#list4");
				    grid.trigger("reloadGrid");
			    	break;
				}
				else
				{
					newrow = true;
				}
			 }					 
			  
			  if(newrow == true)
				 {
					
				  //$("#list4").addRowData(i,jsonData[i]);
				  $("#list4").addRowData(count,jsonData.offer);
				  for(var i=0; i<count; i++)
					  {
					  	if(count[i] == 10)
					  	{
					  								  		
					  	}					  
					  }					
				 }
/*			  var quantity = 1;*/ 
		
			  
		$("#list4").jqGrid({
			datatype: "local",
			
			/*colNames:['cat_id','sub_cat_id','ItemName','CompanyName',"HSN",'Packing','Unit', 'UnitPrice','MRP','GST','IGST','Quantity','Total'],*/
			/*colNames:['cat_id','sub_cat_id','Item<br>Name','Company<br>Name',"HSN","Packing", "Unit<br>Price",'MRP','GST%','IGST%','kg/ltr','gram/mili','Qty',"Unit",'Total','StockInGrams','StockInMili','TotalQuantity'],*/
			colNames:['cat_id','sub_cat_id',"Barcode<br>No",'Item<br>Name','Company<br>Name',"HSN","Batch No.", "Unit<br>Price",'MRP','GST%',"Packing","Unit",'Qty','Free<br>Qty','Expiry<br>Date','Total','totalKgLtrPiece','stockPerEntry'],
			
			colModel:[ 
					     {
					    	 name:'cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },		
					     {
					    	 name:'sub_cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },	
					     
					     {	 name:'barcodeNo',
					    	 width:150,
					    	 align:'center',
							
						 },	
					     
					     {	 name:'itemName',
					    	 width:150,
					    	 align:'center',
							
						 },						
					     {	 name:'companyName',
					    	 width:150,
					    	 align:'center',
							
						},
						{	 name:'hsn',
							 width:80,
					    	 align:'center',
						},
						{	 name:'batchNumber',
							 width:125,
					    	 align:'center',
						},	
					
					
						
						{	 name:'salePrice',
							 width:100,
							 editable: true,
					    	 align:'center',
						},
						
						{	 name:'mrp',
							 width:100,
							 editable: true,
					    	 align:'center',
							
						},
						{	 name:'vatPercentage',
							 width:80,
							 editable: true,
					    	 align:'center',
						},
						
						{	 name:'gst',
							 width:80,
							 editable: true
							
						},
						/*{	name:'sGst',
							width:80,
							editable: true
							
						},*/
						/*{	name:'igst',
							width:80,
							//editable: true,
					    	align:'center',
					    	hidden:true
							
						},*/
						
						{	 name:'weight',
							 width:100,
							 hidden:false,
					    	 align:'center',
						},
						
						{	id:'unitName', 
							name:'unitName',
							width:50,
							align:'center',
						},
						{	
							id:'quantity',
							name:'quantity',
							width:100,
							editable: true,
							align:'center',
						},
						{	
							id:'freeQuantity',
							name:'freeQuantity',
							width:100,
							editable: true,
							align:'center',							
						},						
					
						{	id:'expiryDate', 
							name:'expiryDate',
							width:140,
							align:'center',
							editable: false,
						},						
						{	name:'total',
							width:150,
							align:'center',
							//formatter: sumFmatter
						},						
						{	name:'totalKgLtrPiece',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						},
						{	name:'stockPerEntry',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						}
			],
			
			
			sortorder : 'desc',
			loadonce: false,
			viewrecords: true,
			//width: 1200,
			shrinkToFit:true,
			rowheight: 300,
          	hoverrows: true,
	        rownumbers: true,
	        rowNum: 10,
           'cellEdit':true,
            autowidth:true,
            pgbuttons : false,
            pginput : false,
           
           /*beforeEditCell: function(rowid, cellname, value, irow, icol)
           { 
        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');
               var rowData = jQuery("#list4").getRowData(rowId);
               var unit =  rowData['unitName'];
               
	        	if(unit == 'pcs')
	           	{
	           		$(this).jqGrid('setColProp', 'quantity', {editable:true});
	           		$(this).jqGrid('setColProp', 'kg', {editable:false});
	           		$(this).jqGrid('setColProp', 'grams', {editable:false});			                    		
	           	}
	           	else
	           	{
	           		$(this).jqGrid('setColProp', 'quantity', {editable:false});
	           		$(this).jqGrid('setColProp', 'kg', {editable:true});
	           		$(this).jqGrid('setColProp', 'grams', {editable:true});
	           	}
           },*/
            
            afterSaveCell: function grossTotal()
			{
            	       	document.getElementById("discount").value = 0;
		 				document.getElementById("discountAmount").value = 0;
		 				document.getElementById("hamaliExpence2").value = 0;
		 				document.getElementById("hamaliExpence").value = 0;
            	
            	
            		   /* 	Calculation of total after editing quantity*/
				        	   
				        	   // $(this).trigger('reloadGrid');
				        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');
				        	   //alert("rowId ==========> "+rowId);
		                       var rowData = jQuery("#list4").getRowData(rowId);
		                       productName = rowData['itemName'];
		                       var quantity = rowData['quantity'];
		                       var freeQuantity = rowData['freeQuantity'];
		                       //alert("ENTERED QUANTITY =====> "+quantity);
		                       var salePrice = rowData['salePrice'];
		                       var mrp = rowData['mrp'];
		                       //alert("grams entered = "+grams);
		                       var iGst = rowData['igst'];
		                       var Gst = rowData['gst'];
		                       var unit = rowData['unitName'];
		                       var vatPercentage = rowData['vatPercentage'];
		                       var stockPerEntry = rowData['stockPerEntry'];
		                       var expiryDate = rowData['expiryDate'];
		                       
		                       if(freeQuantity == "")
		                       {
		                    	   	freeQuantity = "0";
                   					var setZero = 0;
                   					$("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
		                       }
		                       
		                 
		                     if(salePrice != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(salePrice.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid Sale Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				//var setZero = salePrice;
			                    		$("#list4").jqGrid("setCell", rowId, "salePrice", v.salePrice);
			                    		
			                    		finalCalculation();
			                    		
			                    		/*var Total = 0;
			                    		var Total1 = 0;
			                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++)
			        		        	{
			        		        		Total1 = allRowsInGrid1[k].total;
			        		        		if(Total1 == undefined)
			        		        			{
			        		        				Total1 = 0;
			        		        			}
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        		
			        		        	}
			        		        	
			        		        	document.getElementById("totalWithExpense").value = Total;
			        		        	document.getElementById("grossTotal").value = Total;*/
	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       if(mrp != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(mrp.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid MRP";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				var setZero = 0;
	                    				$("#list4").jqGrid("setCell", rowId, "mrp", setZero);
	                    				
	                    				finalCalculation();
	                    				
	                    				/*var setZero = 0;
			                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
			                    		
			                    		var Total = 0;
			                    		var Total1 = 0;
			                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++)
			        		        	{
			        		        		Total1 = allRowsInGrid1[k].total;
			        		        		if(Total1 == undefined)
			        		        			{
			        		        				Total1 = 0;
			        		        			}
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        		
			        		        	}
			        		        	
			        		        	document.getElementById("totalWithExpense").value = Total;
			        		        	document.getElementById("grossTotal").value = Total;*/
	                    				
	                    				
	                    				/*document.getElementById("mrp").value = "";
	                    			
	                    				
	                    				var allRowsInGrid = $('#list4').getGridParam('data');//to get all rows of grid
	                    				var AllRows=JSON.stringify(allRowsInGrid);
	                    				
	                    				allRowsInGrid[i].mrp="";
	                    				*/

	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       var vPercentage = v.vatPercentage;
		                       if(vatPercentage != "")
		                       {
		                    	   var checkTax = /^[0-9]+$/;
		                    	   if(vatPercentage.match(checkTax))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid GST %";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				$("#list4").jqGrid("setCell", rowId, "vatPercentage", vPercentage);
	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       if(quantity != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(quantity.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid Quantity";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				var setValue;
	                    				$("#list4").jqGrid("setCell", rowId, "quantity", setValue);
	                    				
	                    				finalCalculation();
	                    				
	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       if(freeQuantity != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(freeQuantity.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid Free Quantity";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				var setValue;
	                    				$("#list4").jqGrid("setCell", rowId, "freeQuantity", setValue);
	                    				
	                    				finalCalculation();
	                    				
	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       
		                       
		                       if(mrp == "" || mrp == "0")
		                       {}
		                       else
		                       {
			                       if(Number(salePrice) > Number(mrp))
			                       {
			                    	   var msg="MRP must be Greater Than Unit Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				return false;
			                       }
		                       }
		                       
		                    	/*if(unit == 'pcs')
			                    	{
			                    		$(this).jqGrid('setColProp', 'quantity', {editable:true});
			                    		$(this).jqGrid('setColProp', 'kg', {editable:false});
			                    		$(this).jqGrid('setColProp', 'grams', {editable:false});			                    		
			                    	}
			                    	else
			                    	{
			                    		$(this).jqGrid('setColProp', 'quantity', {editable:false});
			                    		$(this).jqGrid('setColProp', 'kg', {editable:true});
			                    		$(this).jqGrid('setColProp', 'grams', {editable:true});
			                    	}*/
		                    	
		                    	/*productId = $('#proName').val();
		                    	
		                    	$("#proName option:selected").each(function()
		                    		{
		                    		   selectedVal = $(this).text();
		                    		});
		                    	
		                    	var splitText = selectedVal.split(",");
		                    	
		                    	//var stockArray = {};
		                    	
		                    		var stock = splitText[4];*/
		                    		
		                    	 	/*var stockInGrams = splitText[11];
		                    		var stockInMili = splitText[12];
		                    		var stockInTotalPieceQuantity = splitText[13];*/
		                    	           	
		                    	
		                    	var salePricePerGramOrMili = salePricePerGram = (Number)(salePrice/1000);
	                    		var userTotalGramsOrMili = Number(quantity) * 1000;
		                    	//alert("Sale Price Per Gram = "+salePricePerGram);
		                    	
		                    	if(unit == 'kg')
		                    	{		
 		                    	   		var setFq = 0;
		                    			$("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    			
		                    			var rowId =$("#list4").jqGrid('getGridParam','selrow');
		                    			var rowData = jQuery("#list4").getRowData(rowId);
		                    			var stockInKg = rowData['totalKgLtrPiece'];
		                    			var stockInGrams;
		                    			if(Number(stockInKg) > 1)
		                    			{
			                    			stockInGrams = stockInKg * 1000;
		                    			}
		                    			else
		                    			{
		                    				stockInGrams = stockInKg;
		                    			}
		                    					                    			
		                    			if(quantity != '')
		                    			{
		                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
		                    			//var checkKiloLtr = /^[0-9]+\.?[0-9]*$/;
		                    			if(quantity.match(checkKiloLtr))
		                    			{
		                    				if(quantity == 0)
		                    				{
			                    				var msg="Please Enter Valid kg Value";
			                    				var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
			                    				
			                    				setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);

			 		                    	   	var setZero = 0;
					                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);					                    		
					                    		
					                    		finalCalculation();
					                    					                    				 		                    	   
			 		                    	   return false;
		                    				}		                    				
		                    			}
		                    			else
		                    			{
		                    				var setZero = 0;
		                    				
		                    				var msg="Please Enter Valid kg Value";
		                    				var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
		                    				
		                    				setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
		                    						                    				
		                    				var setZero = 0;
				                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
				                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
				                    		
				                    		finalCalculation();
				                    						                    				                    		
				                    		return false;
		                    				}
		                    			}
		                    			
		                    			if(expiryDate =='N/A')
		                    			{
			                    			if(Number(userTotalGramsOrMili) > Number(stockInGrams))
					                    	{ 		                    				
				                    			if(Number(stockInGrams) >1000)
				                    				{
				                    					var convertedStockInKg = Number(stockInGrams)/1000;
				                    					
				                    					var msg=productName+" Available stock = "+convertedStockInKg+" Kg";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    				else
				                    				{			                    					
				                    					var msg=productName+" Available stock = "+stockInGrams+" Grams";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    			
					                    		var setZero = 0;
					                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
					                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
					                    		
					                    		finalCalculation();
					                    		
					                    	}
		                    			}
		                    			else if(expiryDate !='N/A')
		                    			{
		                    				var stockperEntryInKg = rowData['stockPerEntry'];
			                    			var stockperEntryInGrams;
			                    			if(Number(stockperEntryInKg) > 1)
			                    			{
			                    				stockperEntryInGrams = stockperEntryInKg * 1000;
			                    			}
			                    			else
			                    			{
			                    				stockperEntryInGrams = stockperEntryInKg;
			                    			}
		                    			
		                    				if(Number(userTotalGramsOrMili) > Number(stockperEntryInGrams))
					                    	{ 		                    				
				                    			if(Number(stockperEntryInGrams) >1000)
				                    				{
				                    					var convertedStockInKg = Number(stockperEntryInGrams)/1000;
				                    					
				                    					var msg=productName+" Available stock = "+convertedStockInKg+" Kg";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    				else
				                    				{			                    					
				                    					var msg=productName+" Available stock = "+stockperEntryInGrams+" Grams";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    			
					                    		var setZero = 0;
					                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
					                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
					                    		
					                    		finalCalculation();
					                    		
					                    	}
		                    			}
			                    		
			                    			KgGramsLtrMiliCalculation();
			                 		}
		                    	
		                    	if(unit == 'ltr')
	                    		{
	                    	   		var setFq = 0;
	                    			$("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    		
		                    		var rowId =$("#list4").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#list4").getRowData(rowId);
	                    			var stockInLtr = rowData['totalKgLtrPiece'];
	                    			var stockInMili = stockInLtr * 1000;
	                    			
	                    			var stockperEntryInLtr = rowData['stockPerEntry'];
	                    			var stockperEntryInMili;
	                    			if(Number(stockperEntryInLtr) > 1)
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr * 1000;
	                    			}
	                    			else
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr;
	                    			}
	                    			
	                    			if(quantity != '')
	                    			{
	                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
	                    			if(quantity.match(checkKiloLtr))
	                    			{
	                    				if(quantity == "0")
	                    				{
			 		                    	   var msg="Please Enter ltr OR mili";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
			 		                    	   
			 		                    	   var setZero = 0;
			 		                    	   $("#list4").jqGrid("setCell", rowId, "total", setZero);
					                    		
			 		                    	  finalCalculation();
			 		                    	   
			 		                    	  return false;
	                    				}
	                    			}
	                    			else
	                    			{
	                    				quantity = 0;
	                    				
	                    				var msg="Please Enter Valid ltr Value";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);				
	                    				
	                    				var setZero = 0;
			                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
			                    		
			                    		finalCalculation();
			                    					                    			                    		
	                    				return false;
	                    				}
	                    			}
	                    			
	                    			
	                    			if(expiryDate =='N/A')
	                    			{
		                    		if(Number(userTotalGramsOrMili) > Number(stockInMili))
			                    	{
		                    			if(Number(stockInMili) >1000)
		                    				{
		                    					var convertedStockInLtr = Number(stockInMili)/1000;
		                    					
		                    					var msg=productName+" Available stock = "+convertedStockInLtr+" Ltr";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
				 		                    	  setTimeout(function() {
				                    					dialog.modal('hide');
				                    				}, 1500);
		                    					
		                    					
		                    				}
		                    				else
		                    				{		                    					
		                    					var msg=productName+" Available stock = "+stockInMili+" mili";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
				 		                    	  setTimeout(function() {
				                    					dialog.modal('hide');
				                    				}, 1500);
		                    				}
		                    			
			                    		var setZero = 0;
			                    		$("#list4").jqGrid("setCell", rowId, "total", setZero); 
			                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
			                    		
			                    		finalCalculation();
			                    		
			        		        	return false;
			                    	}
	                    			}
		                    		else if(expiryDate !='N/A')
	                    			{
		                    			if(Number(userTotalGramsOrMili) > Number(stockperEntryInMili))
				                    	{
			                    			if(Number(stockperEntryInMili) >1000)
			                    				{
			                    					var convertedStockInLtr = Number(stockperEntryInMili)/1000;
			                    					
			                    					var msg=productName+" Available stock = "+convertedStockInLtr+" Ltr";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
				                    				});
					 		                    	  setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
			                    					
			                    					
			                    				}
			                    				else
			                    				{		                    					
			                    					var msg=productName+" Available stock = "+stockperEntryInMili+" mili";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
				                    				});
					 		                    	  setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
			                    				}
			                    			
				                    		var setZero = 0;
				                    		$("#list4").jqGrid("setCell", rowId, "total", setZero); 
				                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
				                    		
				                    		finalCalculation();
				                    		
				        		        	return false;
	                    			}		                    		
	                    		}
                    				KgGramsLtrMiliCalculation();
                    		}
		                    	
	                    	function KgGramsLtrMiliCalculation()
	                    	{
	                    		if((Number(quantity)>0 || Number(quantity) != '' || quantity != '0'))
                    			{
	                    			salePricePerGram 
		                    		var salePriceKgTota = 0;
		                    		salePriceKgTota = userTotalGramsOrMili * salePricePerGramOrMili ;
		                    		salePriceKgTota = Math.round(salePriceKgTota * 100) / 100;			                    		
                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceKgTota);
                    			}				                    	
		                    	          	
		                    	else if(Number(quantity) == 0  || Number(quantity)== '')
                    			{
		                    		
		                    		var setZero = 0;
		                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
		                    		
		                    		finalCalculation();
		                    	   
 		                    	   return false;
                    			}
	                    	}
			                   
			                    if(unit == 'pcs')
			                    {	
			                    	if(freeQuantity == "" || freeQuantity == "0")
		                    		{
		                    			freeQuantity = 0;
		                    			var setZero = 0;
                                 		$("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
                                 		quantity = Number(quantity) + Number(freeQuantity);
		                    		}
			                    	else if(freeQuantity != '' || freeQuantity != "0")
		                    		{
										var checkFreeQuantity = /^[0-9]+$/;
							    		if(freeQuantity.match(checkFreeQuantity))
							    		{
							    			quantity = Number(quantity) + Number(freeQuantity);
							    		}
							    		else
							    		{
						    				 var msg="Please Enter Valid Free Quantity";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
		                    				 
		                    				 quantity = 0;
		                    				 var setZero = 0;
	                                   		 $("#list4").jqGrid("setCell", rowId, "quantity", setZero);
	 			                    		 $("#list4").jqGrid("setCell", rowId, "total", setZero);
	                                   		 $("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
	 			                    		 
	                                   		finalCalculation();
	                                   		 	
		                    				return false;			                    			
							    		}
		                    		}
			                    	
		                    		var rowId =$("#list4").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#list4").getRowData(rowId);
	                    			var stockInTotalPieceQuantity = rowData['totalKgLtrPiece'];	
	                    			
	                    			if(quantity != '')
	                    			{
	                    			 var checkQuantity = /^[0-9]+$/;
	                    			 if(String(quantity).match(checkQuantity) || String(quantity) == "")
	                    			 {}
	                    			 else
	                    			 {	                    				 
	                    				 var msg="Please Enter Valid Quantity";
		 		                    	   var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
		 		                    	  setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
	                    				 
	                    				 quantity = 0;
	                    				 var setZero = 0;
                                   		 $("#list4").jqGrid("setCell", rowId, "quantity", setZero);
 			                    		 $("#list4").jqGrid("setCell", rowId, "total", setZero);
                                   		 $("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
 			                    		 
                                   		finalCalculation();
                                   		 	
	                    				return false;
	                    			 	}
	                    			}
	                    			
	                    			 
		                    		if(expiryDate =='N/A')
		                    		{	
		                    			if ( Number(quantity) > Number(stockInTotalPieceQuantity))
					                    	{
	                    					   var msg = productName+" Available Stock For Expiry Date : "+expiryDate+" = "+stockInTotalPieceQuantity+" Pieces";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
			 		                    	   });
		                    				
			 		                    	   setTimeout(function()
			 		                    		{
		                    					dialog.modal('hide');
			 		                    	   }, 1500);
			 		                    	   
			 		                    	   var setFq = 0;
			 		                    	   $("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
			 		                    	   $("#list4").jqGrid("setCell", rowId, "quantity", setFq);
					                    	   $("#list4").jqGrid("setCell", rowId, "total", setFq);
					                    	   
					                    	   finalCalculation();					                    		
			 		                    	   return false;
				                    	}	                    			 
		                    		}
	                    			else if(expiryDate !='N/A')
	                    			{	
	                    				if ( Number(quantity) > Number(stockPerEntry))
				                    	{
				                    		   var msg = productName+" Available Stock For Expiry Date : "+expiryDate+" = "+stockPerEntry+" Pieces";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
				                    		
				                    		var tota = 0;
				                    		var quantity = 0;
				                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
				                    		$("#list4").jqGrid("setCell", rowId, "quantity", quantity);
			 		                    	$("#list4").jqGrid("setCell", rowId, "freeQuantity", quantity);
				                    		
			 		                    	finalCalculation();
				                    	}
	                    			}
			                    	if (iGst != 0 || iGst != undefined || Number(quantity) > 0 || quantity == '')
			                    	{
		                    			var taxPercentage = iGst;
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
				                    }
			                    	else if(iGst == 0 || iGst == undefined || Number(quantity) > 0 || quantity == '')
			                    	{
			                    		Gst = 0;
			                    		var  taxPercentage = Number(Gst);
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
			                    				                    		
			                    	}
			                    	
			                    	/*if(freeQuantity == "")
			                    	{}
			                    	else
			                    	{
			                    		var checkFreeQuantity = /^[0-9]+$/;
			                    		if(freeQuantity.match(checkFreeQuantity))
			                    		{}
			                    		else
			                    		{	
			                    			 var msg="Please Enter Valid Free Quantity";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
			 		                    	  
			 		                    	  	var setFq = 0;
		                                   		$("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
			                    		}
			                    	}*/
			                    }
			                    
			                    finalCalculation();
			                    
			                    function finalCalculation()
			                    {
			                    		var Total = 0;
			                    		var Total1 = 0;
			                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++)
			        		        	{
			        		        		Total1 = allRowsInGrid1[k].total;
			        		        		if(Total1 == undefined)
			        		        			{
			        		        				Total1 = 0;
			        		        			}
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        		
			        		        	}
			        		        	
			        		        	document.getElementById("totalWithExpense").value = Total;
			        		        	document.getElementById("grossTotal").value = Total;
		        		        		
			                    }
	                    },
			//},
	        	
         
			pager: "#jqGridPager",
			
			
			
		});
		
	
		//$("#list4").addRowData(i+1,jsonData[i]);
		if(count==0 || count==null)
		{
			 // $("#list4").addRowData(i,jsonData[i]);
			  $("#list4").addRowData(0,jsonData.offer);
		}
		

   
		 $('#list4').navGrid('#jqGridPager',
	                
	                { edit: true, add: false, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: false },
	                
	                {
	                    editCaption: "The Edit Dialog",
	                   
	                    afterSubmit: function () {
							
	                      var grid = $("#list4"),
						  intervalId = setInterval(
							 function() {
							         grid.trigger("reloadGrid",[{current:true}]);
							   },
							   500);
	                         
	                      
						},
						
						 recreateForm: true,
						 checkOnUpdate : true,
						 checkOnSubmit : true,
		                 closeAfterEdit: true,
						
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                    closeAfterAdd: true,
	                    recreateForm: true,
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                              		
	                {
	                	closeAfterdel:true,
	                	checkOnUpdate : true,
						checkOnSubmit : true,
						recreateForm: true,
	                	
						

						afterComplete: function() {
	                		$('#list4').trigger( 'reloadGrid' );

	                		
	                		document.getElementById("discount").value = 0;
			 				document.getElementById("discountAmount").value = 0;
			 				document.getElementById("hamaliExpence2").value = 0;
			 				document.getElementById("hamaliExpence").value = 0;	                		
	                		
	 				       /* 	Calculation of total after editing quantity*/
	 				        	   
	 				        	   // $(this).trigger('reloadGrid');
	 				        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');  
	 		                       var rowData = jQuery("#list4").getRowData(rowId);
	 		                    	var quantity = rowData['quantity'];
	 		                    	var salePrice = rowData['salePrice'];
	 		                    	var kg = rowData['kg'];
			                    	//alert("kg entered = "+kg);
			                    	var grams = rowData['grams'];
			                    	//alert("grams entered = "+grams);
	 		                    	var iGst = rowData['igst'];
	 		                    	var Gst = rowData['gst'];
	 		                    	
	 		                    	productId = $('#proName').val();
	 		                    	
	 		                    	
	 		                    	$("#proName option:selected").each(function() {
	 		                    		   selectedVal = $(this).text();
	 		                    		});
	 		                    	
	 		                    	var splitText = selectedVal.split(",");
	 		                    	
	 		                    	var stock = splitText[4];
	 		                    	
	 		                    	var salePricePerGram = 0;
			                    	salePricePerGram = (Number)(salePrice/1000);
			                    	
			             
		                    	if(Number(grams) != "" || Number(kgValue) != "")
		                    	{
			                    	if(Number(kg)>0 && Number(grams) == 0)
	                    			{
			                    		var salePriceKgTota = 0;
			                    		salePriceKgTota = Number(salePrice)*kg;
			                    		salePriceKgTota = Math.round(salePriceKgTota * 100) / 100;
	                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceKgTota);
	                    			}
			                    	
			                    	if(Number(grams)>0 && Number(kg) == 0)
	                    			{
			                    		var salePriceGramTota = 0;
			                    		salePriceGramTota = Number(salePricePerGram)*grams;
			                    		salePriceGramTota = Math.round(salePriceGramTota * 100) / 100;
	                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceGramTota);
	                    			}
			                    	
			                    	if(Number(kg) > 0 && Number(grams) > 0)
	                    			{
			                    		var salePriceKgTota = 0;
			                    		var salePriceGramTota = 0;
			                    		var salePriceKgPlusGrams = 0;
			                    		salePriceKgTota = Number(salePrice)*kg;
			                    		salePriceGramTota = Number(salePricePerGram)*grams;                    		
			                    		salePriceKgPlusGrams = Number(salePriceKgTota) + Number(salePriceGramTota);
			                    		salePriceKgPlusGrams = Math.round(salePriceKgPlusGrams * 100) / 100;
	                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceKgPlusGrams);
		                    		}
			                    	
			                    	var Total =0;
 		                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
 		        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
 		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
 		        		        	for (var k = 0; k < count; k++) {
 		        		        		var Total1 = allRowsInGrid1[k].total;
 		        		        		Total = +Total + +Total1;
 		        		        		Total = Math.round(Total * 100) / 100;
 		        		        	}
 		        		        	document.getElementById("totalWithExpense").value = Total;
 		        		        	document.getElementById("grossTotal").value = Total;
 		                        	
		                    	}
	 		                    	if ( Number(quantity) > Number(stock))
	 		                    	{
	 		                    		
	 		                    		alert("Available stock = "+stock);
	 		                    		
	 		                    		
	 		                    		
	 	                    		
	 	                    	        }
	 		                    	
	 		                    		if (iGst != 0){
	 		                    		/*	var taxPercentage = iGst;
	 			                    		var taxAmount = ((taxPercentage/100)*salePrice);
	 			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);*/
	 			                    		var tota = quantity * salePrice;
	 			                    		tota = Math.round(tota * 100) / 100;
	 			                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
		 		                    	}
		 		                    	else if(iGst == 0){
	 		                    		/*var  taxPercentage = Number(Gst);
	 		                    		var taxAmount = ((taxPercentage/100)*salePrice);
	 		                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);*/
	 		                    		var tota = quantity * salePrice;
	 		                    		tota = Math.round(tota * 100) / 100;
	 		                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
		 		                    		
		 		                    	}
		 		                    		
		 		                    		var Total =0;
		 		                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
		 		        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
		 		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
		 		        		        	for (var k = 0; k < count; k++) {
		 		        		        		var Total1 = allRowsInGrid1[k].total;
		 		        		        		Total = +Total + +Total1;
		 		        		        		Total = Math.round(Total * 100) / 100;
		 		        		        	}
		 		        		        	document.getElementById("totalWithExpense").value = Total;
		 		        		        	document.getElementById("grossTotal").value = Total;
		 	        		        		//document.getElementById("duptotal").value = Total;
		 		                    	
	 		        	
						},
						reloadAftersubmit:true,	
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                		
	                });
		 
		 
			   });
		
		})
}

	

/*++++++++++++++ Fetch product details by product name for cash customer 22-5-17 ++++++++++++++++++++++++++*/
function getProductDetailsByProductName()
{	
	var params= {};
	itemparams = {};
	
	/*var input1 = document.getElementById('productIdCash'),
	list = document.getElementById('product_dropCash'),
	i,productIdCash;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			productIdCash= list.options[i].getAttribute('data-value');
		}
	}*/
	
	var productId = $('#productIdCash').val();
	
	var splitText = productId.split(" => ");
	
	var proName = splitText[0];
	var expiryDate = splitText[1];
	var weight = splitText[2];
	var company = splitText[3];
	var subCatId=splitText[5];
	var catId=splitText[6];
	
	
	/*
	var proName = splitText[0];
	var expiryDate = splitText[1];
	var weight = splitText[2];
	var company = splitText[5];
	var subCatId=splitText[8];
	var catId=splitText[11];*/
	
	
	/*var input = catName;
    list1 = document.getElementById('proName'),
    i,cat;

	for (i = 0; i < list1.options.length; ++i) {
    if (list1.options[i].value === input.value) {
    	catId = list1.options[i].getAttribute('data-value');
    }
	}
	
	alert(catId)*/
	/*var input = document.getElementById('fk_cat_id'),
    list1 = document.getElementById('cat_drop'),
    i,fk_cat_id;

	for (i = 0; i < list1.options.length; ++i) {
    if (list1.options[i].value === input.value) {
    	fk_cat_id = list1.options[i].getAttribute('data-value');
    }
	}*/
	
	
	params["proName"]= proName;
	params["productId"]= productId;
	params["company"]= company;
	params["weight"]= weight;
	params["catId"]= catId;
	params["subCatId"]= subCatId;
	params["expiryDate"]= expiryDate;
	
	var count=0;
	var newrow;
	var rowId;
	
	params["methodName"] = "getProductDetails1";
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
 	   {
		  var jsonData = $.parseJSON(data);
			
	      // $("#list4").jqGrid("clearGridData", true).trigger("reloadGrid");
		
      
	     $.each(jsonData,function(i,v)
		 { 
	        function sumFmatter (cellvalue, options, rowObject)
	        {	            
	        	/*var tax = options.rowData.vatPercentage;
	        	
	        	if(tax == 0){
	        		var tot= (options.rowData.quantity * options.rowData.salePrice);
	        		if(isNaN(tot)){
	        			tot = 0;
					}
	        	}
	        	if(tax != 0)
	        	{	        		
	        		var taxcalculation = (tax/100)* Number(options.rowData.salePrice);
	        		var newSalePrice = Number(taxcalculation) + Number(options.rowData.salePrice)
	        		var tot= (Number(options.rowData.quantity) * Number(newSalePrice));
	        		if(isNaN(tot))
	        		{
	        			tot = 0;
					}
	        	}*/
	        	var jam=0;
	        	
	        	
	        	count = jQuery("#list4").jqGrid('getGridParam', 'records');
	        	var allRowsInGrid1 = $('#list4').getGridParam('data');
	        	var AllRows=JSON.stringify(allRowsInGrid1);
	        	for (var i = 0; i < count; i++)
	        	{	        		
	        		var kg = allRowsInGrid1[i].kg;
	        		params["kg"+i] = kg;
	        		
	        		var grams = allRowsInGrid1[i].grams;
	        		params["grams"+i] = grams;
	        		
	        		var defQuantity = 1;	        		
	            	var quantity = allRowsInGrid1[i].quantity;
	            	alert("quantity =============== "+quantity )
	             	params["quantity"+i] = quantity;
	             	
	             	var salePrice = allRowsInGrid1[i].salePrice;
	            	params["salePrice"+i] = salePrice;
	            	
	            	var vatPercentage = allRowsInGrid1[i].vatPercentage;
	            	params["vatPercentage"+i] = vatPercentage;
	            		            	
	            	/*var stockInGrams = allRowsInGrid1[i].stockInGrams;
	            	params["stockInGrams"+i] = stockInGrams;
	            	
	   	    	 	var stockInMili = allRowsInGrid1[i].stockInMili;
	            	params["stockInMili"+i] = stockInMili;
	            	
	   	    	 	var totalPieceQuantity = allRowsInGrid1[i].totalPieceQuantity;
	            	params["totalPieceQuantity"+i] = totalPieceQuantity;*/
	            	
	            	if(vatPercentage == 0){
	            		
	            		var totals1=(salePrice)*(quantity);
	            		if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	            	}
	            	
	                if(vatPercentage != 0){
	                	
	                	var taxcal = (vatPercentage/100) * salePrice;
	                	var newSalePrice = Number(salePrice) + Number(taxcal);
	                	var totals1=(Number(newSalePrice)*Number(quantity));
	                	if(isNaN(totals1)){
		             		totals1 = 0;
						}
		            	jam = jam + totals1;
	                }               	
	           }

	        	document.getElementById("totalWithExpense").value = jam;
	        	
	            return tot;

	        }
	        
	         count = jQuery("#list4").jqGrid('getGridParam', 'records'); 
		     var rowdata =$("#list4").jqGrid('getGridParam','data');
		     var ids = jQuery("#list4").jqGrid('getDataIDs');
			
			  var prodName,com,packing,unit, expiryDate;
			  
			  for (var j = 0; j < count; j++) 
			  {
				  prodName = rowdata[j].itemName;
				  com = rowdata[j].companyName;
				  packing = rowdata[j].weight;
				  unit = rowdata[j].unitName;
				  expiryDate = rowdata[j].expiryDate;
				
				 var rowId = ids[j];
				 var rowData = jQuery('#list4').jqGrid ('getRowData', rowId);
				
				if (prodName == jsonData.offer.itemName && com == jsonData.offer.companyName && packing == jsonData.offer.weight && unit == jsonData.offer.unitName && expiryDate == jsonData.offer.expiryDate)
				{
			    	/*ori_quantity = +rowdata[j].quantity+1;
			    	
			    	$("#list4").jqGrid("setCell", rowId, "quantity", ori_quantity);
			    	var grid = jQuery("#list4");
			    	grid.trigger("reloadGrid");*/
			    	newrow=false;
					alert("Product Name Already Inserted !!!");
					var grid = jQuery("#list4");
				    grid.trigger("reloadGrid");
			    	break;
				}
				else
				{
					newrow = true;
				}
			 }					 
			  
			  if(newrow == true)
				 {
					
				  //$("#list4").addRowData(i,jsonData[i]);
				  $("#list4").addRowData(count,jsonData.offer);
				  for(var i=0; i<count; i++)
					  {
					  	if(count[i] == 10)
					  	{
					  								  		
					  	}					  
					  }					
				 }
/*			  var quantity = 1;*/ 
		
		$("#list4").jqGrid({
			datatype: "local",
			
			/*colNames:['cat_id','sub_cat_id','ItemName','CompanyName',"HSN",'Packing','Unit', 'UnitPrice','MRP','GST','IGST','Quantity','Total'],*/
			/*colNames:['cat_id','sub_cat_id','Item<br>Name','Company<br>Name',"HSN","Packing", "Unit<br>Price",'MRP','GST%','IGST%','kg/ltr','gram/mili','Qty',"Unit",'Total','StockInGrams','StockInMili','TotalQuantity'],*/
			colNames:['cat_id','sub_cat_id','Barcode<br>No','Item<br>Name','Company<br>Name','HSN','Batch No', 'Unit<br>Price',
				'MRP','Sale<br>Price<br>Ex','Gst%','igst','Discount%','Discount<br>Amount','Total<br>Ex<br>Tax','Packing',
				'Unit','Qty','Free<br>Qty','Expiry<br>Date','Total','totalKgLtrPiece','stockPerEntry'],
			
			colModel:[ 
					     {
					    	 name:'cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },		
					     {
					    	 name:'sub_cat_id',
					    	 hidden:true,
					    	 align:'center',
					     },	
					     
					     {	 name:'barcodeNo',
					    	 width:150,
					    	 align:'center',
							
						 },	
					     
					     {	 name:'itemName',
					    	 width:150,
					    	 align:'center',
							
						 },						
					     {	 name:'companyName',
					    	 width:150,
					    	 align:'center',
							
						},
						{	 name:'hsn',
							 width:80,
					    	 align:'center',
						},
						{	 name:'batchNumber',
							 width:125,
					    	 align:'center',
						},	
					
						{	 name:'salePrice',
							 width:100,
							 editable: true,
					    	 align:'center',
						},
						
						{	 name:'mrp',
							 width:100,
							 editable: true,
					    	 align:'center',
							
						},
						
						{	 name:'salePriceEx',
							 width:100,
							 editable: true,
					    	 align:'center',
						},
						
						
						
						{	 name:'vatPercentage',
							 width:80,
							 editable: true,
					    	 align:'center',
						},
						
						/*{	name:'sGst',
							width:80,
							editable: true
							
						},*/
						{	name:'igst',
							width:80,
							//editable: true,
					    	align:'center',
					    	hidden:true
							
						},
						
						{	 name:'Discount',
							 width:100,
							 hidden:false,
							 editable:true,
					    	 align:'center',
						},
						{	 name:'DiscountAmount',
							 width:100,
							 hidden:false,
					    	 align:'center',
						},
						{	 name:'TotalEx',
							 width:100,
							 //editable: true,
					    	 align:'center',
						},
						
						{	 name:'weight',
							 width:100,
							 hidden:false,
					    	 align:'center',
						},
						
						{	id:'unitName', 
							name:'unitName',
							width:50,
							align:'center',
						},
						{	
							id:'quantity',
							name:'quantity',
							width:100,
							editable: true,
							align:'center',
						},
						{	
							id:'freeQuantity',
							name:'freeQuantity',
							width:100,
							editable: true,
							align:'center',							
						},						
					
						{	id:'expiryDate', 
							name:'expiryDate',
							width:140,
							align:'center',
							editable: false,
						},						
						{	name:'total',
							width:150,
							align:'center',
							//formatter: sumFmatter
						},						
						{	name:'totalKgLtrPiece',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						},
						{	name:'stockPerEntry',
							width:150,
							hidden:true,
							align:'center',
							//formatter: sumFmatter
						}
			],
				
			sortorder : 'desc',
			loadonce: false,
			viewrecords: true,
			//width: 1200,
			shrinkToFit:true,
			rowheight: 300,
          	hoverrows: true,
	        rownumbers: true,
	        rowNum: 10,
           'cellEdit':true,
            autowidth:true,
            pgbuttons : false,
            pginput : false,
           
           /*beforeEditCell: function(rowid, cellname, value, irow, icol)
           { 
        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');
               var rowData = jQuery("#list4").getRowData(rowId);
               var unit =  rowData['unitName'];
               
	        	if(unit == 'pcs')
	           	{
	           		$(this).jqGrid('setColProp', 'quantity', {editable:true});
	           		$(this).jqGrid('setColProp', 'kg', {editable:false});
	           		$(this).jqGrid('setColProp', 'grams', {editable:false});			                    		
	           	}
	           	else
	           	{
	           		$(this).jqGrid('setColProp', 'quantity', {editable:false});
	           		$(this).jqGrid('setColProp', 'kg', {editable:true});
	           		$(this).jqGrid('setColProp', 'grams', {editable:true});
	           	}
           },*/
           
            afterSaveCell: function grossTotal()
			{
            	       	document.getElementById("discount").value = 0;
		 				document.getElementById("discountAmount").value = 0;
		 				document.getElementById("hamaliExpence2").value = 0;
		 				document.getElementById("hamaliExpence").value = 0;
            	
            	//alert("In afterSave cell");
            		   /* 	Calculation of total after editing quantity*/
				        	   
				        	   // $(this).trigger('reloadGrid');
				        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');
				        	   
		                       var rowData = jQuery("#list4").getRowData(rowId);
		                       productName = rowData['itemName'];
		                       var quantity = rowData['quantity'];
		                       var freeQuantity = rowData['freeQuantity'];
		                       
		                       var salePrice = rowData['salePrice'];
		                       var mrp = rowData['mrp'];
		                      var iGst = rowData['igst'];
		                       //var gst = rowData['gst'];
		                       var unit = rowData['unitName'];
		                       var gst = rowData['vatPercentage'];
		                       var stockPerEntry = rowData['stockPerEntry'];
		                       var expiryDate = rowData['expiryDate'];
		                       
		                       if(freeQuantity == "")
		                       {
		                    	   	freeQuantity = "0";
                   					var setZero = 0;
                   					$("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
		                       }
		                       
		                       
		                       var sPrice = v.salePrice;
		                 
		                       
		                       if(salePrice != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(salePrice.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid Sale Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    			/*	var setZero = 0;
			                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);*/
	                    				
			                    		//var setZero="0"
	                    				
			                    		$("#list4").jqGrid("setCell", rowId, "salePrice", sPrice);
			                    		
			                    		finalCalculation();
			                    		
			                    		
			                    		/*var Total = 0;
			                    		var Total1 = 0;
			                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++)
			        		        	{
			        		        		Total1 = allRowsInGrid1[k].total;
			        		        		if(Total1 == undefined)
			        		        			{
			        		        				Total1 = 0;
			        		        			}
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        		
			        		        	}
			        		        	
			        		        	document.getElementById("totalWithExpense").value = Total;
			        		        	document.getElementById("grossTotal").value = Total;*/
	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       if(mrp != "")
		                       {
		                    	   var checkprice = /^[0-9]+\.?[0-9]*$/;
		                    	   if(mrp.match(checkprice))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid MRP";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				var setZero = 0;
	                    				$("#list4").jqGrid("setCell", rowId, "mrp", setZero);
	                    				
	                    				
	                    				finalCalculation();
	                    				
	                    				/*var setZero = 0;
			                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
			                    		
			                    		var Total = 0;
			                    		var Total1 = 0;
			                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++)
			        		        	{
			        		        		Total1 = allRowsInGrid1[k].total;
			        		        		if(Total1 == undefined)
			        		        			{
			        		        				Total1 = 0;
			        		        			}
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        		
			        		        	}
			        		        	
			        		        	document.getElementById("totalWithExpense").value = Total;
			        		        	document.getElementById("grossTotal").value = Total;*/
	                    				
	                    				return false;
		                    	   }
		                       }
		                       
		                       /*var vPercentage = v.vatPercentage; 
		                       
		                     
		                       if(vatPercentage != "")
		                       {
		                    	   
		                    	   var checkTax = /^[0-9]+$/;
		                    	   if(vatPercentage.match(checkTax))
		                    	   {}
		                    	   else
		                    	   {
		                    		   var msg="Please Enter Valid GST %";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				
	                    				
			                    		$("#list4").jqGrid("setCell", rowId, "vatPercentage", vPercentage);
	                    				
	                    				return false;
		                    	   }
		                       }*/
		                       
		                       if(mrp == "" || mrp == "0")
		                       {}
		                       else
		                       {
			                       if(Number(salePrice) > Number(mrp))
			                       {
			                    	   var msg="MRP must be Greater Than Unit Price";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);
	                    				
	                    				
	                    				return false;
			                       }
		                       }
		                       
		                    	/*if(unit == 'pcs')
			                    	{
			                    		$(this).jqGrid('setColProp', 'quantity', {editable:true});
			                    		$(this).jqGrid('setColProp', 'kg', {editable:false});
			                    		$(this).jqGrid('setColProp', 'grams', {editable:false});			                    		
			                    	}
			                    	else
			                    	{
			                    		$(this).jqGrid('setColProp', 'quantity', {editable:false});
			                    		$(this).jqGrid('setColProp', 'kg', {editable:true});
			                    		$(this).jqGrid('setColProp', 'grams', {editable:true});
			                    	}*/
		                    	
		                    	/*productId = $('#proName').val();
		                    	
		                    	$("#proName option:selected").each(function()
		                    		{
		                    		   selectedVal = $(this).text();
		                    		});
		                    	
		                    	var splitText = selectedVal.split(",");
		                    	
		                    	//var stockArray = {};
		                    	
		                    		var stock = splitText[4];*/
		                    		
		                    	 	/*var stockInGrams = splitText[11];
		                    		var stockInMili = splitText[12];
		                    		var stockInTotalPieceQuantity = splitText[13];*/
		                    	           	
		                    	
		                    	var salePricePerGramOrMili = salePricePerGram = (Number)(salePrice/1000);
	                    		var userTotalGramsOrMili = Number(quantity) * 1000;
		                    	//alert("Sale Price Per Gram = "+salePricePerGram);
		                    	
		                    	if(unit == 'kg')
		                    	{		
 		                    	   		var setFq = 0;
		                    			$("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    			
		                    			var rowId =$("#list4").jqGrid('getGridParam','selrow');
		                    			var rowData = jQuery("#list4").getRowData(rowId);
		                    			var stockInKg = rowData['totalKgLtrPiece'];
		                    			var stockInGrams;
		                    			if(Number(stockInKg) > 1)
		                    			{
			                    			stockInGrams = stockInKg * 1000;
		                    			}
		                    			else
		                    			{
		                    				stockInGrams = stockInKg;
		                    			}
		                    					                    			
		                    			if(quantity != '')
		                    			{
		                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
		                    			//var checkKiloLtr = /^[0-9]+\.?[0-9]*$/;
		                    			if(quantity.match(checkKiloLtr))
		                    			{
		                    				if(quantity == 0)
		                    				{
			                    				var msg="Please Enter Valid kg Value";
			                    				var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
			                    				
			                    				setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);

			 		                    	   	var setZero = 0;
					                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);					                    		
					                    		
					                    		finalCalculation();
					                    					                    				 		                    	   
			 		                    	   return false;
		                    				}		                    				
		                    			}
		                    			else
		                    			{
		                    				var setZero = 0;
		                    				
		                    				var msg="Please Enter Valid kg Value";
		                    				var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
		                    				
		                    				setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
		                    						                    				
		                    				var setZero = 0;
				                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
				                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
				                    		
				                    		finalCalculation();
				                    						                    				                    		
				                    		return false;
		                    				}
		                    			}
		                    			
		                    			if(expiryDate =='N/A')
		                    			{
			                    			if(Number(userTotalGramsOrMili) > Number(stockInGrams))
					                    	{ 		                    				
				                    			if(Number(stockInGrams) >1000)
				                    				{
				                    					var convertedStockInKg = Number(stockInGrams)/1000;
				                    					
				                    					var msg=productName+" Available stock = "+convertedStockInKg+" Kg";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    				else
				                    				{			                    					
				                    					var msg=productName+" Available stock = "+stockInGrams+" Grams";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    			
					                    		var setZero = 0;
					                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
					                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
					                    		
					                    		finalCalculation();
					                    		
					                    	}
		                    			}
		                    			else if(expiryDate !='N/A')
		                    			{
		                    				var stockperEntryInKg = rowData['stockPerEntry'];
			                    			var stockperEntryInGrams;
			                    			if(Number(stockperEntryInKg) > 1)
			                    			{
			                    				stockperEntryInGrams = stockperEntryInKg * 1000;
			                    			}
			                    			else
			                    			{
			                    				stockperEntryInGrams = stockperEntryInKg;
			                    			}
		                    			
		                    				if(Number(userTotalGramsOrMili) > Number(stockperEntryInGrams))
					                    	{ 		                    				
				                    			if(Number(stockperEntryInGrams) >1000)
				                    				{
				                    					var convertedStockInKg = Number(stockperEntryInGrams)/1000;
				                    					
				                    					var msg=productName+" Available stock = "+convertedStockInKg+" Kg";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    				else
				                    				{			                    					
				                    					var msg=productName+" Available stock = "+stockperEntryInGrams+" Grams";
					                    				var dialog = bootbox.dialog({
					                    					//title: "Embel Technologies Says :",
					                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					                    				    closeButton: false
					                    				});
					                    				setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
				                    				}
				                    			
					                    		var setZero = 0;
					                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
					                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
					                    		
					                    		finalCalculation();
					                    		
					                    	}
		                    			}
			                    		
			                    			KgGramsLtrMiliCalculation();
			                 		}
		                    	
		                    	if(unit == 'ltr')
	                    		{
	                    	   		var setFq = 0;
	                    			$("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
		                    		
		                    		var rowId =$("#list4").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#list4").getRowData(rowId);
	                    			var stockInLtr = rowData['totalKgLtrPiece'];
	                    			var stockInMili = stockInLtr * 1000;
	                    			
	                    			var stockperEntryInLtr = rowData['stockPerEntry'];
	                    			var stockperEntryInMili;
	                    			if(Number(stockperEntryInLtr) > 1)
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr * 1000;
	                    			}
	                    			else
	                    			{
	                    				stockperEntryInMili = stockperEntryInLtr;
	                    			}
	                    			
	                    			if(quantity != '')
	                    			{
	                    			var checkKiloLtr = /^[0-9]+\.?[0-9]{0,3}$/;
	                    			if(quantity.match(checkKiloLtr))
	                    			{
	                    				if(quantity == "0")
	                    				{
			 		                    	   var msg="Please Enter ltr OR mili";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
			 		                    	   
			 		                    	   var setZero = 0;
			 		                    	   $("#list4").jqGrid("setCell", rowId, "total", setZero);
					                    		
			 		                    	  finalCalculation();
			 		                    	   
			 		                    	  return false;
	                    				}
	                    			}
	                    			else
	                    			{
	                    				quantity = 0;
	                    				
	                    				var msg="Please Enter Valid ltr Value";
	                    				var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
	                    				setTimeout(function() {
	                    					dialog.modal('hide');
	                    				}, 1500);				
	                    				
	                    				var setZero = 0;
			                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
			                    		
			                    		finalCalculation();
			                    					                    			                    		
	                    				return false;
	                    				}
	                    			}
	                    			
	                    			
	                    			if(expiryDate =='N/A')
	                    			{
		                    		if(Number(userTotalGramsOrMili) > Number(stockInMili))
			                    	{
		                    			if(Number(stockInMili) >1000)
		                    				{
		                    					var convertedStockInLtr = Number(stockInMili)/1000;
		                    					
		                    					var msg=productName+" Available stock = "+convertedStockInLtr+" Ltr";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
				 		                    	  setTimeout(function() {
				                    					dialog.modal('hide');
				                    				}, 1500);
		                    					
		                    					
		                    				}
		                    				else
		                    				{		                    					
		                    					var msg=productName+" Available stock = "+stockInMili+" mili";
				 		                    	   var dialog = bootbox.dialog({
			                    					//title: "Embel Technologies Says :",
			                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			                    				    closeButton: false
			                    				});
				 		                    	  setTimeout(function() {
				                    					dialog.modal('hide');
				                    				}, 1500);
		                    				}
		                    			
			                    		var setZero = 0;
			                    		$("#list4").jqGrid("setCell", rowId, "total", setZero); 
			                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
			                    		
			                    		finalCalculation();
			                    		
			        		        	return false;
			                    	}
	                    			}
		                    		else if(expiryDate !='N/A')
	                    			{
		                    			if(Number(userTotalGramsOrMili) > Number(stockperEntryInMili))
				                    	{
			                    			if(Number(stockperEntryInMili) >1000)
			                    				{
			                    					var convertedStockInLtr = Number(stockperEntryInMili)/1000;
			                    					
			                    					var msg=productName+" Available stock = "+convertedStockInLtr+" Ltr";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
				                    				});
					 		                    	  setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
			                    					
			                    					
			                    				}
			                    				else
			                    				{		                    					
			                    					var msg=productName+" Available stock = "+stockperEntryInMili+" mili";
					 		                    	   var dialog = bootbox.dialog({
				                    					//title: "Embel Technologies Says :",
				                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				                    				    closeButton: false
				                    				});
					 		                    	  setTimeout(function() {
					                    					dialog.modal('hide');
					                    				}, 1500);
			                    				}
			                    			
				                    		var setZero = 0;
				                    		$("#list4").jqGrid("setCell", rowId, "total", setZero); 
				                    		$("#list4").jqGrid("setCell", rowId, "quantity", setZero);
				                    		
				                    		finalCalculation();
				                    		
				        		        	return false;
	                    			}		                    		
	                    		}
                    				KgGramsLtrMiliCalculation();
                    		}
		                    	
	                    	function KgGramsLtrMiliCalculation()
	                    	{
	                    		if((Number(quantity)>0 || Number(quantity) != '' || quantity != '0'))
                    			{
	                    			salePricePerGram 
		                    		var salePriceKgTota = 0;
		                    		salePriceKgTota = userTotalGramsOrMili * salePricePerGramOrMili ;
		                    		salePriceKgTota = Math.round(salePriceKgTota * 100) / 100;			                    		
                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceKgTota);
                    			}				                    	
		                    	          	
		                    	else if(Number(quantity) == 0  || Number(quantity)== '')
                    			{
		                    		
		                    		var setZero = 0;
		                    		$("#list4").jqGrid("setCell", rowId, "total", setZero);
		                    		
		                    		finalCalculation();
		                    	   
 		                    	   return false;
                    			}
	                    	}
			                   
			                    if(unit == 'pcs')
			                    {	
			                    	if(freeQuantity == "" || freeQuantity == "0")
		                    		{
		                    			freeQuantity = 0;
		                    			var setZero = 0;
                                 		$("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
                                 		quantity = Number(quantity) + Number(freeQuantity);
		                    		}
			                    	else if(freeQuantity != '' || freeQuantity != "0")
		                    		{
										var checkFreeQuantity = /^[0-9]+$/;
							    		if(freeQuantity.match(checkFreeQuantity))
							    		{
							    			quantity = Number(quantity) + Number(freeQuantity);
							    		}
							    		else
							    		{
						    				 var msg="Please Enter Valid Free Quantity";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
		                    				 
		                    				 quantity = 0;
		                    				 var setZero = 0;
	                                   		 $("#list4").jqGrid("setCell", rowId, "quantity", setZero);
	 			                    		 $("#list4").jqGrid("setCell", rowId, "total", setZero);
	                                   		 $("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
	 			                    		 
	                                   		finalCalculation();
	                                   		 	
		                    				return false;			                    			
							    		}
		                    		}
			                    	
		                    		var rowId =$("#list4").jqGrid('getGridParam','selrow');
	                    			var rowData = jQuery("#list4").getRowData(rowId);
	                    			var stockInTotalPieceQuantity = rowData['totalKgLtrPiece'];	
	                    			
	                    			if(quantity != '')
	                    			{
	                    			 var checkQuantity = /^[0-9]+$/;
	                    			 if(String(quantity).match(checkQuantity) || String(quantity) == "")
	                    			 {}
	                    			 else
	                    			 {	                    				 
	                    				 var msg="Please Enter Valid Quantity";
		 		                    	   var dialog = bootbox.dialog({
	                    					//title: "Embel Technologies Says :",
	                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
	                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	                    				    closeButton: false
	                    				});
		 		                    	  setTimeout(function() {
		                    					dialog.modal('hide');
		                    				}, 1500);
	                    				 
	                    				 quantity = 0;
	                    				 var setZero = 0;
                                   		 $("#list4").jqGrid("setCell", rowId, "quantity", setZero);
 			                    		 $("#list4").jqGrid("setCell", rowId, "total", setZero);
                                   		 $("#list4").jqGrid("setCell", rowId, "freeQuantity", setZero);
 			                    		 
                                   		finalCalculation();
                                   		 	
	                    				return false;
	                    			 	}
	                    			}
	                    			
	                    			 
		                    		if(expiryDate =='N/A')
		                    		{	
		                    			if ( Number(quantity) > Number(stockInTotalPieceQuantity))
					                    	{
	                    					   var msg = productName+" Available Stock For Expiry Date : "+expiryDate+" = "+stockInTotalPieceQuantity+" Pieces";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
			 		                    	   });
		                    				
			 		                    	   setTimeout(function()
			 		                    		{
		                    					dialog.modal('hide');
			 		                    	   }, 1500);
			 		                    	   
			 		                    	   var setFq = 0;
			 		                    	   $("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
			 		                    	   $("#list4").jqGrid("setCell", rowId, "quantity", setFq);
					                    	   $("#list4").jqGrid("setCell", rowId, "total", setFq);
					                    	   
					                    	   finalCalculation();					                    		
			 		                    	   return false;
				                    	}	                    			 
		                    		}
	                    			else if(expiryDate !='N/A')
	                    			{	
	                    				if ( Number(quantity) > Number(stockPerEntry))
				                    	{
				                    		   var msg = productName+" Available Stock For Expiry Date : "+expiryDate+" = "+stockPerEntry+" Pieces";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
				                    		
				                    		var tota = 0;
				                    		var quantity = 0;
				                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
				                    		$("#list4").jqGrid("setCell", rowId, "quantity", quantity);
			 		                    	$("#list4").jqGrid("setCell", rowId, "freeQuantity", quantity);
				                    		
			 		                    	finalCalculation();
				                    	}
	                    			}
			                    	/*if (iGst != 0 || iGst != undefined || Number(quantity) > 0 || quantity == '')
			                    	{
		                    			var taxPercentage = iGst;
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
				                    }
			                    	else if(iGst == 0 || iGst == undefined || Number(quantity) > 0 || quantity == '')
			                    	{
			                    		Gst = 0;
			                    		var  taxPercentage = Number(Gst);
			                    		var taxAmount = ((taxPercentage/100)*salePrice);
			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);
			                    		var tota = Number(quantity - freeQuantity) * salePrice;
			                    		tota = Math.round(tota * 100) / 100;
			                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
			                    				                    		
			                    	}*/
			                    	
			                    	/*if(freeQuantity == "")
			                    	{}
			                    	else
			                    	{
			                    		var checkFreeQuantity = /^[0-9]+$/;
			                    		if(freeQuantity.match(checkFreeQuantity))
			                    		{}
			                    		else
			                    		{	
			                    			 var msg="Please Enter Valid Free Quantity";
			 		                    	   var dialog = bootbox.dialog({
		                    					//title: "Embel Technologies Says :",
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
		                    				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		                    				    closeButton: false
		                    				});
			 		                    	  setTimeout(function() {
			                    					dialog.modal('hide');
			                    				}, 1500);
			 		                    	  
			 		                    	  	var setFq = 0;
		                                   		$("#list4").jqGrid("setCell", rowId, "freeQuantity", setFq);
			                    		}
			                    	}*/
			                    }
			                    
			                    finalCalculation();
			                    
			                    
			                    var quantity = rowData['quantity'];
								var iGst = rowData['igst'];
								var DiscountAmount= rowData['DiscountAmount'];
								var Discount= rowData['Discount'];
								var TotalEx= rowData['TotalEx'];
								

								/*/////calculation of gst////
*/														
								var checkDiscount = /^[0-9]+\.?[0-9]*$/;
								  if(Discount.match(checkDiscount))
								  {
									  if(Number(Discount) >= 100)
							  	  {
							  		  var setZero = 0;
							  		  //alert("In discount")
							  		  myAlert("Discount Percentage Must Be Less Than 100");
							  	      $("#list4").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							      	  totalCalC();
							      	  totalDisC();
							  		  return false;
							  	  }
								  }
								  else
								  {
									  var setZero = 0;
									  
									  if(Discount== "")
							  	  {
							  		 
							  		  $("#list4").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							      	  
							  	  }
									  else
									  {
							  		  myAlert("Pleaee Enter Valid Discount value");
							  		  $("#list4").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							      	  totalCalC();
							      	  totalDisC();
							      	  return false;
									  }
								  }			
								  
								//sale price excluding tax calc//
								  var checksalePrice= /^[0-9]+\.?[0-9]*$/;
								  if(salePrice.match(checksalePrice))
							    {
							      	  if(Number(salePrice) > 0)
							  		  {
							      		 
							      		  spwTax = (salePrice/(1+(gst/100)));
							      		  $("#list4").jqGrid("setCell", rowId, "salePriceEx",  spwTax.toFixed(2));
							      	  
							      	/*if(Number(Discount) > 0)
							      	 {*/
							      		 if(Number(gst) > 0)
							      		 {  
							      			 
							      		     DiscountAmount= (spwTax*(Discount/100));
							      			  finalsp = spwTax- DiscountAmount;
							      			  newTaxAmt = (((finalsp*quantity)*gst)/100);
							      			  
							      			  var oneProTax = (((finalsp)*gst)/100);
							      			  newFinalsp = finalsp+ oneProTax;
							      			  tota = newFinalsp* quantity;
							      			  //DiscountAmount = DiscountAmount * quantity;
							      			 
							      		$("#list4").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));	  
									    $("#list4").jqGrid("setCell", rowId, "TotalEx", finalsp.toFixed(2));
									   /* $("#jqGrid").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));*/
									    $("#list4").jqGrid("setCell", rowId, "total", tota.toFixed(2));
									 /* $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", newTaxAmt.toFixed(2));*/
									     totalCalC();
									     totalDisC();	
							      		}
							      	else if(Number(gst) == 0)
							      	{
							      		 var setZero = 0;
							      		spwTax= saleprice - 0;
							      		DiscountAmount = (spwTax*(Discount/100));
							      		finalsp= spwTax- DiscountAmount;
							      		newTaxAmt = ((( finalsp*quantity)*gst)/100);				        			  
							      		var oneProTax = (((finalsp)*gst)/100);
							      		newFinalsp =  finalsp+ oneProTax;	        			  
							      		tota = newFinalsp * quantity;
							      		DiscountAmount = DiscountAmount* quantity;
							      			 	
							      			  
							      $("#list4").jqGrid("setCell", rowId, "TotalEx", finalsp.toFixed(2));
								  $("#list4").jqGrid("setCell", rowId, "DiscountAmount", DiscountAmount.toFixed(2));
								  $("#list4").jqGrid("setCell", rowId, "total", tota.toFixed(2));
								 /* $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", newTaxAmt.toFixed(2));*/
								  totalCalC();
								  totalDisC();
							      	}
							      	  
							      	// }
						else
							    {
							     var setZero = 0; 
								  $("#list4").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							   /* $("#list4").jqGrid("setCell", rowId, "Total", tota.toFixed(2));*/
							  /*  $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", setZero);*/
							     }
							        }
						else
					         {
							  	var setZero = 0;
							  /*$("#list4").jqGrid("setCell", rowId, "sPWithoutTax", setZero);*/
							    $("#list4").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							    $("#list4").jqGrid("setCell", rowId, "total", setZero);
							 /* $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", setZero);*/
							    return false;
								}
							    }				        	  
						else
							{
								var setZero = 0;
								alert("Pleae Enter Valid Buy Price");
								$("#list4").jqGrid("setCell", rowId, "salePrice", setZero);
								$("#list4").jqGrid("setCell", rowId, "salePriceEx", setZero);
							  	$("#list4").jqGrid("setCell", rowId, "DiscountAmount", setZero);
							  	$("#list4").jqGrid("setCell", rowId, "total", setZero);
							 /*  $("#list4").jqGrid("setCell", rowId, "taxAmountAfterDis", setZero);*/
									  return false;
								 }
								
								/*if(Gst!=0)
								{
								var taxpercentage = Gst;
								var salepriceEx=saleprice(1+(taxpercentage/100)));
								
								
							$("#jqGrid").jqGrid("setCell",rowId,"salepriceEx",salepriceEx);
							
							return false;
				                         }
								*/
								  //out of grid calc//
								
									function totalCalC()
									{	
									       var Total = 0;
									       var totAmtWithTax = 0;
									       var count = jQuery("#list4").jqGrid('getGridParam', 'records');
									       var allRowsInGrid1 = $('#list4').getGridParam('data');
									        var AllRows=JSON.stringify(allRowsInGrid1);
									        	  
									       for (var k = 0; k < count; k++)
									       {
									          var Total1 = allRowsInGrid1[k].total;//grid total 

									        if(Total1 != undefined)
									        {
									        		Total = +Total + +Total1;
									        		  }
									        	  }
									        	  for (var j = 0; j < count; j++)
									        	  {
									        		  var Total2 = allRowsInGrid1[j].taxAmount;
									        		  var Total3 = allRowsInGrid1[j].total;
									        		  if(Total2 != undefined)
									        		  {
									        			  totAmtWithTax = +totAmtWithTax + +Total2 + +Total3;
									        		  }
									        	  }
									        	 document.getElementById("totalWithExpense").value = Total.toFixed(2);//Math.round(Total);
									        	  document.getElementById("grossTotal").value = Total.toFixed(2);//Math.round(Total);
									        	  var totAmount = Total.toFixed(2);//Math.round(Total);
										        }
									        	  
									      function totalDisC()
										   {
											    //TOTAL DISCOUNT AMOUNT
											       var TotalDisAmt = 0;
											        var TotalsPAmt = 0;
											        var disPer = 0;
											        var count = jQuery("#list4").jqGrid('getGridParam', 'records');
											        var allRowsInGrid1 = $('#list4').getGridParam('data');
											        var AllRows=JSON.stringify(allRowsInGrid1);
											        for (var l = 0; l < count; l++)
											        {
											          var TotalDisAmt1 = allRowsInGrid1[l].DiscountAmount;
											          var TotalsPAmt1 = allRowsInGrid1[l].salePriceEx;
											        		  
											        if(TotalsPAmt1 != undefined)
											        {
											        	TotalsPAmt = (+TotalsPAmt + +TotalsPAmt1).toFixed(2);
											        }
											        if(TotalDisAmt1 != undefined)
											        {
											        	TotalDisAmt = (+TotalDisAmt + +TotalDisAmt1).toFixed(2);
											        	disPer = ((TotalDisAmt/TotalsPAmt)*100).toFixed(2);
											        }						        	 
											        	  }
											        	  /*document.getElementById("discount1").value = disPer;*/
											        	 // document.getElementById("discount").value = TotalDisAmt;
										        	 }
			                    
			                    
			                    function finalCalculation()
			                    {
			                    		var Total = 0;
			                    		var Total1 = 0;
			                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
			        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
			        		        	var AllRows=JSON.stringify(allRowsInGrid1);
			        		        	for (var k = 0; k < count; k++)
			        		        	{
			        		        		Total1 = allRowsInGrid1[k].total;
			        		        		if(Total1 == undefined)
			        		        			{
			        		        				Total1 = 0;
			        		        			}
			        		        		Total = +Total + +Total1;
			        		        		Total = Math.round(Total * 100) / 100;
			        		        		
			        		        	}
			        		        	
			        		        	document.getElementById("totalWithExpense").value = Total;
			        		        	document.getElementById("grossTotal").value = Total;
		        		        		
			                    }
	                    },
			//},
	        	
         
			pager: "#jqGridPager",
			
		});
		
	
		//$("#list4").addRowData(i+1,jsonData[i]);
		if(count==0 || count==null)
		{
			 // $("#list4").addRowData(i,jsonData[i]);
			  $("#list4").addRowData(0,jsonData.offer);
		}
		

   
		 $('#list4').navGrid('#jqGridPager',
	                
	                { edit: true, add: false, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: false },
	                
	                {
	                    editCaption: "The Edit Dialog",
	                   
	                    afterSubmit: function () {
							
	                      var grid = $("#list4"),
						  intervalId = setInterval(
							 function() {
							         grid.trigger("reloadGrid",[{current:true}]);
							   },
							   500);
	                         
	                      
						},
						
						 recreateForm: true,
						 checkOnUpdate : true,
						 checkOnSubmit : true,
		                 closeAfterEdit: true,
						
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                
	                {
	                    closeAfterAdd: true,
	                    recreateForm: true,
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                },
	                              		
	                {
	                	closeAfterdel:true,
	                	checkOnUpdate : true,
						checkOnSubmit : true,
						recreateForm: true,
	                	
						

						afterComplete: function() {
	                		$('#list4').trigger( 'reloadGrid' );

	                		
	                		document.getElementById("discount").value = 0;
			 				document.getElementById("discountAmount").value = 0;
			 				document.getElementById("hamaliExpence2").value = 0;
			 				document.getElementById("hamaliExpence").value = 0;	                		
	                		
	 				       /* 	Calculation of total after editing quantity*/
	 				        	   
	 				        	   // $(this).trigger('reloadGrid');
	 				        	   var rowId =$("#list4").jqGrid('getGridParam','selrow');  
	 		                       var rowData = jQuery("#list4").getRowData(rowId);
	 		                    	var quantity = rowData['quantity'];
	 		                    	var salePrice = rowData['salePrice'];
	 		                    	var kg = rowData['kg'];
			                    	//alert("kg entered = "+kg);
			                    	var grams = rowData['grams'];
			                    	//alert("grams entered = "+grams);
	 		                    	var iGst = rowData['igst'];
	 		                    	var Gst = rowData['gst'];
	 		                    	
	 		                    	productId = $('#proName').val();
	 		                    	
	 		                    	
	 		                    	$("#proName option:selected").each(function() {
	 		                    		   selectedVal = $(this).text();
	 		                    		});
	 		                    	
	 		                    	var splitText = selectedVal.split(",");
	 		                    	
	 		                    	var stock = splitText[4];
	 		                    	
	 		                    	var salePricePerGram = 0;
			                    	salePricePerGram = (Number)(salePrice/1000);
			                    	
			             
		                    	if(Number(grams) != "" || Number(kgValue) != "")
		                    	{
			                    	if(Number(kg)>0 && Number(grams) == 0)
	                    			{
			                    		var salePriceKgTota = 0;
			                    		salePriceKgTota = Number(salePrice)*kg;
			                    		salePriceKgTota = Math.round(salePriceKgTota * 100) / 100;
	                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceKgTota);
	                    			}
			                    	
			                    	if(Number(grams)>0 && Number(kg) == 0)
	                    			{
			                    		var salePriceGramTota = 0;
			                    		salePriceGramTota = Number(salePricePerGram)*grams;
			                    		salePriceGramTota = Math.round(salePriceGramTota * 100) / 100;
	                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceGramTota);
	                    			}
			                    	
			                    	if(Number(kg) > 0 && Number(grams) > 0)
	                    			{
			                    		var salePriceKgTota = 0;
			                    		var salePriceGramTota = 0;
			                    		var salePriceKgPlusGrams = 0;
			                    		salePriceKgTota = Number(salePrice)*kg;
			                    		salePriceGramTota = Number(salePricePerGram)*grams;                    		
			                    		salePriceKgPlusGrams = Number(salePriceKgTota) + Number(salePriceGramTota);
			                    		salePriceKgPlusGrams = Math.round(salePriceKgPlusGrams * 100) / 100;
	                    				$("#list4").jqGrid("setCell", rowId, "total", salePriceKgPlusGrams);
		                    		}
			                    	
			                    	var Total =0;
 		                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
 		        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
 		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
 		        		        	for (var k = 0; k < count; k++) {
 		        		        		var Total1 = allRowsInGrid1[k].total;
 		        		        		Total = +Total + +Total1;
 		        		        		Total = Math.round(Total * 100) / 100;
 		        		        	}
 		        		        	document.getElementById("totalWithExpense").value = Total;
 		        		        	document.getElementById("grossTotal").value = Total;
 		                        	
		                    	}
	 		                    	if ( Number(quantity) > Number(stock))
	 		                    	{
	 		                    		
	 		                    		alert("Available stock = "+stock);
	 		                    		
	 		                    		
	 		                    		
	 	                    		
	 	                    	        }
	 		                    	
	 		                    		if (iGst != 0){
	 		                    		/*	var taxPercentage = iGst;
	 			                    		var taxAmount = ((taxPercentage/100)*salePrice);
	 			                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);*/
	 			                    		var tota = quantity * salePrice;
	 			                    		tota = Math.round(tota * 100) / 100;
	 			                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
		 		                    	}
		 		                    	else if(iGst == 0){
	 		                    		/*var  taxPercentage = Number(Gst);
	 		                    		var taxAmount = ((taxPercentage/100)*salePrice);
	 		                    		var BuyPriceWithTaxAmount = Number(taxAmount) + Number(salePrice);*/
	 		                    		var tota = quantity * salePrice;
	 		                    		tota = Math.round(tota * 100) / 100;
	 		                    		$("#list4").jqGrid("setCell", rowId, "total", tota);
		 		                    		
		 		                    	}
		 		                    		
		 		                    		var Total =0;
		 		                    		var count = jQuery("#list4").jqGrid('getGridParam', 'records');
		 		        		        	var allRowsInGrid1 = $('#list4').getGridParam('data');
		 		        		        	var AllRows=JSON.stringify(allRowsInGrid1);
		 		        		        	for (var k = 0; k < count; k++) {
		 		        		        		var Total1 = allRowsInGrid1[k].total;
		 		        		        		Total = +Total + +Total1;
		 		        		        		Total = Math.round(Total * 100) / 100;
		 		        		        	}
		 		        		        	document.getElementById("totalWithExpense").value = Total;
		 		        		        	document.getElementById("grossTotal").value = Total;
		 	        		        		//document.getElementById("duptotal").value = Total;
		 		                    	
	 		        	
						},
						reloadAftersubmit:true,	
	                    errorTextFormat: function (data) {
	                        return 'Error: ' + data.responseText
	                    }
	                		
	                });
		 
		 
			   });
		
		})
}

				
						/*************	Cash Customer Bill adding to fertilizerBilling table ************************/




function fertilizerBill()
{	
	var customerName = $("#customerName").val();
	var shopName = $("#shopName").val();
	var contactNo = $("#contactNo").val();
	var proName  = $("#proName").val();
	
	if (customerName == "")
	{
		/*$.getScript('/Shop/staticContent/js/bootbox.min.js', function() */ 
		{		
				var msg="Please Enter Customer Name";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
				
					}//);
		
		/*bootbox.alert("Hello world!", function() {
			  Example.show("Hello world callback");
			});*/
		
	 }
	if(customerName != "")
	{
	 var letterNumber = /^[a-zA-Z ]+$/;
	 if(customerName.match(letterNumber))
	 {
		 
	 }
	 else
		{
		 var msg="Please Enter Valid Customer Name";
			var dialog = bootbox.dialog({
				//title: "Embel Technologies Says :",
			   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			    closeButton: false
			});
			
			setTimeout(function() {
				dialog.modal('hide');
			}, 1500);
			
			return false;
		}
	 }
	  if(contactNo != "")
	  {
		  var letterNumber = /^[0-9]{10}$/;
			 if(contactNo.match(letterNumber))
			 {				
				
			 }
			 else
			 {
				    var msg="Please Enter Valid 10 Digit Contact Number";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);
					
					return false;
			 }
	  }
	  if(proName == "" || proName == "Select product")
	  {
						
			var msg="Please Select Product";
			var dialog = bootbox.dialog({
				//title: "Embel Technologies Says :",
			   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			    closeButton: false
			});
			
			setTimeout(function() {
				dialog.modal('hide');
			}, 1500);
			
			return false;
	  }
	  
	  if(document.fertiBill.paymentMode.value == ""  || document.fertiBill.paymentMode.value == "selected")
        {					
			var msg="Please Select Payment Mode";
			var dialog = bootbox.dialog({
				//title: "Embel Technologies Says :",
			   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
			    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
			    closeButton: false
			});
			
			setTimeout(function() {
				dialog.modal('hide');
			}, 1500);
			
			return false;
        }
    	if(document.fertiBill.paymentMode.value == "cheque")
    	{
        	if(document.fertiBill.chequeNum.value == "")
        	{					
				var msg="Please Enter Cheque Number";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
        		}
        	
        	if(document.fertiBill.nameOnCheck.value == "")
    		{					
				var msg="Please Enter Name on Cheque";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
    		}
    	}
    	
    	if(document.fertiBill.paymentMode.value == "card")
    	{	
    		
        	if(document.fertiBill.cardNum.value == "")
        	{					
				var msg="Please Enter Card Number";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
        		}
        	if(document.fertiBill.cardNum.value != "")
    		{
        		var numbers = /^[0-9]+$/;
        		if(document.fertiBill.cardNum.value.match(numbers))
        		{
        			
        		}
        		else
        		{					
					var msg="Please Enter Valid Card Number";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);
					
					return false;
         		}
    		}
    	}
    	
    	
    	if(document.fertiBill.paymentMode.value == "neft")
    	{
        	if(document.fertiBill.accNum.value == "")
        	{
				var msg="Please Enter Account Number";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
				
        	}
        	if(document.fertiBill.accNum.value != "")
    		{
        		var numbers = /^[0-9]+$/;
        		if(document.fertiBill.accNum.value.match(numbers))
        		{
        			
        		}
        		else
        		{
					var msg="Please Enter Valid Acount Number";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);
					
					return false;
        						
        		}

    		}
    	
        	if(document.fertiBill.bankName.value == "")
    		{
				var msg="Please Enter bank Name";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
    		}
        	if(document.fertiBill.bankName.value != "")
    		{
   	        	var letters = /^[A-Za-z]+$/;
   	        	if(document.fertiBill.bankName.value.match(letters))
   	        	{
   	        		
   	        	}
   	        	else
   	        	{					
					var msg="Please Enter Valid Bank Name";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);
					
					return false;        						
           		}
        		
    		}
    	}	   	        				 
			fertilizerBilling();
}


function fertilizerBilling()
{	
	//document.fertiBill.btn.disabled = true;	
	document.getElementById("billingCashbtn").disabled = true;
	var params = {};
	
	var customerType = "NORMALCUSTOMER";
	
	var customerName = $('#customerName').val();
	var village = $('#village').val();
	var contactNo = $('#contactNo').val();
	//var aadhar = $('#aadharNo').val();
	/*var transExpense = $('#transExpence').val();*/
	var hamaliExpense = $('#hamaliExpence').val();
	/*var transExpenseWithoutGST = $('#transExpence2').val();*/
	var hamaliExpenseWithourGST = $('#hamaliExpence2').val();
	var grossTotal = $('#grossTotal').val();
	var total = $('#totalWithExpense').val();
	var paymentMode = $('#paymentMode').val();
	var chequeNum = $('#chequeNum').val();
	var saleDate = $('#saleDate').val();
	var input = document.getElementById('shopName'),
    list = document.getElementById('shop_drop1'),
    i,shopName;

	for (i = 0; i < list.options.length; ++i) {
    if (list.options[i].value === input.value) {
    	shopName = list.options[i].getAttribute('data-value');
    	}
	}
	var gstNo = $('#gstNo').val();
	var nameOnCheck = $('#nameOnCheck').val();
	var cardNum = $('#cardNum').val();
	var accNum = $('#accNum').val();
	var bankName = $('#bankName').val();
	
	var discount = $('#discount').val();
	var discountAmount = $('#discountAmount').val();
	
    productId = $('#proName').val();
	
	$("#proName option:selected").each(function() {
		   selectedVal = $(this).text();
		});
	
	/*var splitText = selectedVal.split(",");
	
	var fk_shop_id = splitText[12];*/
	
	var count = jQuery("#list4").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#list4').getGridParam('data');//to get all rows of grid
	var AllRows=JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++)
	{		
		var cat_id = allRowsInGrid[i].cat_id;
		params["cat_id"+i] = cat_id;
		
		var sub_cat_id = allRowsInGrid[i].sub_cat_id;
		params["sub_cat_id"+i] = sub_cat_id;
	/*alert(sub_cat_id);*/
		
		var itemName = allRowsInGrid[i].itemName;
		params["itemName"+i] = itemName;
		
		var companyName = allRowsInGrid[i].companyName;
		params["companyName"+i] = companyName;
		
		var hsn = allRowsInGrid[i].hsn;
		params["hsn"+i] = hsn;
		
		var weight = allRowsInGrid[i].weight;
		params["weight"+i] = weight;
		
		var unitName = allRowsInGrid[i].unitName;
		params["unitName"+i] = unitName;
		
		var salePrice = allRowsInGrid[i].salePrice;
		var mrp = allRowsInGrid[i].mrp;
			
		if(salePrice != '' || salePrice == '0')
		{
			var numbers = /^[0-9]+\.?[0-9]*$/;
			if(String(salePrice).match(numbers))
			{	
				if(salePrice == '0' || salePrice == undefined || salePrice == '' || salePrice == null)
				{
					var msg="Please Enter Unit Price For :: "+(i+1)+" "+itemName;
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);
	 				document.getElementById("billingCashbtn").disabled = false;	
					return false;
				}
			}
			else
			{
				var msg="Please Enter Valid Unit Price For :: "+(i+1)+" "+itemName;
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
 				document.getElementById("billingCashbtn").disabled = false;	
				return false;
			}
		}
		
		if(mrp != '')
		{
			var numbers = /^[0-9]+\.?[0-9]*$/;
			if(String(mrp).match(numbers))
				{		
					if(mrp == '' || mrp == '0' || mrp == undefined || mrp == null)
					{
						mrp = 0;
					}
					else if(Number(salePrice) > Number(mrp))
					{
						var msg="MRP per Unit Must be Greater Than Unit Price";
						var dialog = bootbox.dialog({
							//title: "Embel Technologies Says :",
						    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
						    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
						    closeButton: false
						});
						
						setTimeout(function() {
							dialog.modal('hide');
						}, 1500);
		 				document.getElementById("billingCashbtn").disabled = false;	
						return false;
					}
				}
			else
			{
				var msg="Please Enter Valid MRP Price";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
 				document.getElementById("billingCashbtn").disabled = false;	
				return false;
			}
		}
		else
		{
			mrp = 0;
		}
		
		params["salePrice"+i] = salePrice;
		params["mrp"+i] = mrp;
		
		var quantity = allRowsInGrid[i].quantity;
		if(unitName == 'kg')
		{	
			if(quantity == "0" || quantity == undefined || quantity == "" || quantity == null)
			{
				var msg="Please Enter Kg OR grams :: "+(i+1)+" "+itemName;;
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
 				document.getElementById("billingCashbtn").disabled = false;	
				return false;
			}
			else
			{
				params["quantity"+i] = quantity;
			}
		}
		
		if(unitName == 'ltr')
		{
			if(quantity == "0" || quantity == undefined || quantity == "" || quantity == null)
			{
				var msg="Please Enter Ltr OR mili :: "+(i+1)+" "+itemName;;
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
 				document.getElementById("billingCashbtn").disabled = false;	
				return false;
			}
			else
			{
				params["quantity"+i] = quantity;
			}
		}		
		
		if(unitName == 'pcs')
		{			
			if(quantity == "0" || quantity == undefined || quantity == "" || quantity == null)
			{
				var msg="Please Enter Quantity :: "+(i+1)+" "+itemName;
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
 				document.getElementById("billingCashbtn").disabled = false;	
				return false;
			}
			else
			{
				params["quantity"+i] = quantity;
			}
			
			var freeQuantity = allRowsInGrid[i].freeQuantity;
			if(freeQuantity == "0" || freeQuantity == undefined || freeQuantity == "" || freeQuantity == null)
			{
				params["freeQuantity"+i] = 0;
			}
			else
			{
				params["freeQuantity"+i] = freeQuantity;
			}	
		}
		
		var gst = allRowsInGrid[i].vatPercentage;
		params["gst"+i] = gst;
		
		/*var gst = allRowsInGrid[i].gst;
		params["gst"+i] = gst;*/
		
		var igst = allRowsInGrid[i].igst;
		params["igst"+i] = igst;
		
		var total = allRowsInGrid[i].total;
		params["total"+i] = total;
		
		var expiryDate = allRowsInGrid[i].expiryDate;
		params["expiryDate"+i] = expiryDate;
		
	}
	
	if(count == '' || count == '0' || count == undefined || count == null)
	{
		var msg="Please Select Product";
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		document.getElementById("billingCashbtn").disabled = false;	
		return false;
	}
	
	params["customerName"] = customerName;
	params["count"] = count;
	params["village"] = village;
	params["contactNo"] = contactNo;
	//params["aadhar"] = aadhar;
	/*params["transExpense"] = transExpense;*/
	params["total"] = total;
	params["fk_shop_id"] = shopName;
	params["gstNo"] = gstNo;
	params["grossTotal"] = grossTotal;
	params["hamaliExpense"] = hamaliExpense;
	/*params["transExpenseWithoutGST"] = transExpenseWithoutGST;*/
	params["hamaliExpenseWithourGST"] = hamaliExpenseWithourGST;
	params["paymentMode"] = paymentMode;
	params["chequeNum"] = chequeNum;
	params["saleDate"] = saleDate;
	params["nameOnCheck"] = nameOnCheck;
	params["cardNum"] = cardNum;
	params["accNum"] = accNum;
	params["discount"] = discount;
	params["discountAmount"] = discountAmount;
	params["bankName"] = bankName;
	params["customerType"] = customerType;
	
	params["methodName"] = "addingFertilizerBill";
	
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
 	 {
		window.open("FertilizerBillPDF.jsp");
		location.reload();
		document.getElementById("billingCashbtn").disabled = false;	
	}
 	 ).error(function(jqXHR, textStatus, errorThrown){
 	    		if(textStatus==="timeout") {
 	    			$(loaderObj).hide();
 	    			$(loaderObj).find('#errorDiv').show();
 	    		}
 	    	});


}

/*************					Cash Customer Bill adding to fertilizerBilling table    28-5-17  ************************/
function fertilizerBill_28_5_17(){
	
	document.fertiBill.btn.disabled = true;
	var params = {};

	var customerName = $('#customerName').val();
	var village = $('#village').val();
	var contactNo = $('#contactNo').val();
	var aadhar = $('#aadharNo').val();
	var transExpense = $('#transExpence').val();
	var hamaliExpense = $('#hamaliExpence').val();
	var grossTotal = $('#grossTotal').val();
	var total = $('#totalWithExpense').val();
	var paymentMode = $('#paymentMode').val();
	var chequeNum = $('#chequeNum').val();
	var nameOnCheck = $('#nameOnCheck').val();
	var cardNum = $('#cardNum').val();
	var accNum = $('#accNum').val();
	var bankName = $('#bankName').val();
	
	
	
	var count = jQuery("#list4").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#list4').getGridParam('data');//to get all rows of grid
	var AllRows=JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++) {

		var itemName = allRowsInGrid[i].itemName;
		params["itemName"+i] = itemName;
		
		var companyName = allRowsInGrid[i].companyName;
		params["companyName"+i] = companyName;
		
		var weight = allRowsInGrid[i].weight;
		params["weight"+i] = weight;
		
		var quantity = allRowsInGrid[i].quantity;
		params["quantity"+i] = quantity;
		
		var salePrice = allRowsInGrid[i].salePrice;
		params["salePrice"+i] = salePrice;
		
		var mrp = allRowsInGrid[i].mrp;
		params["mrp"+i] = mrp;
		
		var vatPercentage = allRowsInGrid[i].vatPercentage;
		params["vatPercentage"+i] = vatPercentage;
		
		var total = allRowsInGrid[i].total;
		params["total"+i] = total;
	}
	
	params["customerName"] = customerName;
	params["count"] = count;
	params["village"] = village;
	params["contactNo"] = contactNo;
	params["aadhar"] = aadhar;
	params["transExpense"] = transExpense;
	params["total"] = total;
	params["grossTotal"] = grossTotal;
	params["hamaliExpense"] = hamaliExpense;

	params["paymentMode"] = paymentMode;
	params["chequeNum"] = chequeNum;
	params["nameOnCheck"] = nameOnCheck;
	params["cardNum"] = cardNum;
	params["accNum"] = accNum;
	params["bankName"] = bankName;
	params["discount"] = discount;
	params["discountAmount"] = discountAmount;
	
	params["methodName"] = "addingFertilizerBill_28_5_17";
	
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
 	    	{
 				alert(data);
 				window.open("FertilizerBillPDF.jsp");
 				location.reload();
 	    	}
 	    	).error(function(jqXHR, textStatus, errorThrown){
 	    		if(textStatus==="timeout") {
 	    			$(loaderObj).hide();
 	    			$(loaderObj).find('#errorDiv').show();
 	    		}
 	    	});	
}



/*************					Credit Customer Bill adding to fertilizerBilling table      ************************/
//validation for submit button
function creditCustFertilizerBill()
{	
	if (document.creditFertiBill1.creditCustomer.value == "" )
	 {
	    alert("Please Select Customer Name");
	    return false;
	 }
	/*var letterNumber = /^[a-zA-Z, ]+$/;
	 if(document.creditfertiBill.creditCustomer.value.match(letterNumber))
	 {*/
		 if(document.creditFertiBill1.shopName2.value == "" )
		 {
			alert("Please Select Shop Name")
			return false;
		 }
	

	/*if ( document.creditFertiBill1.village1.value == "" )
	 {
    alert("Please Enter Village");
    return false;
	 }*/

	/* var letterNumber = /^[a-zA-Z, ]+$/;
	 if(document.creditFertiBill1.village1.value.match(letterNumber))
	 {
	
	if ( document.creditFertiBill1.contactNo1.value == "" )
	 {
     alert("Please Enter Contact Number");
     return false;
	 }*/
	 
	 /*var letterNumber = /^[0-9]{10}$/;
	 if(document.creditFertiBill1.contactNo1.value.match(letterNumber))
	 {*/
	if ( document.creditFertiBill1.saleDate2.value == "" )
		 {
	       alert("Please Enter Sale Date");
	       return false;
		 }
		 var letterNumber = /^[0-9]{12}$/;
		/* if(document.creditFertiBill1.aadharNo1.value.match(letterNumber))
		 {*/
						   	    if ( document.creditFertiBill1.proName2.value == "" )
							   	       {
											alert("Please Select Product Name");
											return false;
							   	        }
					   	        				 
					   	        				/*if ( document.creditFertiBill1.transExpence1.value == "" )
						   	        			 {
										  	       alert("Please Enter Transportation Expense");
										  	       return false;
						   	        			 }*/
						   	        			/* var letterNumber = /^\s*-?[0-9]\d*(\.\d{1,2})?\s*$/;
						   	        			 var num = /^\d+$/;
						   	        			 if(document.creditFertiBill1.transExpence1.value.match(letterNumber) || document.creditFertiBill1.transExpence1.value.match(num))
						   	        			 {*/
						   	        				/*if ( document.creditFertiBill1.hamaliExpence1.value == "" )
							   	        			 {
											  	       alert("Please Enter Hamali Expense");
											  	       return false;
							   	        			 }*/
							   	        			/* var letterNumber = /^\s*-?[0-9]\d*(\.\d{1,2})?\s*$/;
							   	        			 var num = /^\d+$/;
							   	        			 if(document.creditFertiBill1.hamaliExpence1.value.match(letterNumber) || document.creditFertiBill1.hamaliExpence1.value.match(num) )
							   	        			 {*/
						   	        				 
							   	        				fertilizerBillForCredit();
														}

							   	        			/*else
													{
														alert("Enter only Numbers upto 2 decimal in Hamali Expense field..!!");
														return false;
													} 
						   	        			 }*/
							   	       /* else
											{
												alert("Enter only Numbers upto 2 decimal in Transportation Expense field..!!");
												return false;
											}
										}
		  else
			{
				alert("Enter valid 12 digit Aadhar Number");
				return false;
			}
		}*/
	 /*else
		{
			alert("Enter valid 10 digit Contact Number");
			return false;
		}
	}
	 else
		{
			alert("Enter only Alphanumeric in Village field");
			return false;
		}
	}*/

/*}*/


function fertilizerBillForCreditCustomer()
{
	if (document.creditFertiBill1.creditCustomer.value == "" )
	 {
		var msg="Please Enter Credit Customer Name";
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
			document.getElementById("billingCashbtn").disabled = false;	
		return false;
	 }
	else
	{
		if (document.creditFertiBill1.creditCustomer.value != "" )
		{
		 var letterNumber = /^[a-zA-Z ]+$/;
		 if(document.creditFertiBill1.creditCustomer.value.match(letterNumber))
		 {
			 
		 }
		 else
			{
			 var msg="Please Enter Valid Customer Name";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
			}
		}
	}
	
	if(document.creditFertiBill1.contactNo1.value == "" )
	{
		
	}
	else
	{
		if(document.creditFertiBill1.contactNo1.value != "" )
		{
			
			 var letterNumber = /^[0-9]{10}$/;
			 var contactForZero = /^[0-0]{1}$/;
			 if(document.creditFertiBill1.contactNo1.value.match(letterNumber) || document.creditFertiBill1.contactNo1.value.match(contactForZero))
			 {				
				
			 }
			 else
			 {
				    var msg="Please Enter Valid 10 Digit Contact Number";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);
					
					return false;
			 }			
		}
	}
	
	
		/* var letterNumber = /^[0-9]{12}$/;
			if ( document.creditFertiBill1.productIdCredit.value == "" )
	   	     {
				var msg="Please Select Product";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);
				
				return false;
	   	      }*/
						   	var netpayAmount = $("#firstPaidAmtCreditCustomer").val();
						   	var grossTotal1 = $("#grossTotal1").val();
						   	
						   	if((netpayAmount != "" || netpayAmount != "0" || netpayAmount != undefined || netpayAmount != null) && (grossTotal1 != "" || grossTotal1 != "0" || grossTotal1 != undefined || grossTotal1 != null) && Number(netpayAmount) > 0 && Number(grossTotal1) > 0)
						   	{
						   		if(Number(netpayAmount) > Number(grossTotal1))
						   		{
						   			var msg="Net Pay Amount Must not be greater That Gross Total";
						   			customAlert(msg);
									return false;
						   		}
						   	}
						   							   	        				 
						   	if(netpayAmount != "" && netpayAmount != "0")
						   	{
						   		var numbers = /^[0-9]+\.?[0-9]*$/;
						   		if(netpayAmount.match(numbers))
						   		{
						   			if(document.creditFertiBill1.paymentModeCredit.value == ""  || document.creditFertiBill1.paymentModeCredit.value == "selected")
						   			{
						   				var msg="Please Select Payment Mode";
										var dialog = bootbox.dialog({
											//title: "Embel Technologies Says :",
										   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
										    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
										    closeButton: false
										});
										
										setTimeout(function() {
											dialog.modal('hide');
										}, 1500);
										
										return false;
						   			}
						   		}
						   		else
						   		{
						   			var msg="Please Enter Valid Net Pay Amount\nEg : 100.50 ";
									var dialog = bootbox.dialog({
										//title: "Embel Technologies Says :",
									   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
									    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
									    closeButton: false
									});
									
									setTimeout(function() {
										dialog.modal('hide');
									}, 1500);
									
									return false;
						   			
						   		}
						   	 
				   	        	if(document.creditFertiBill1.paymentModeCredit.value == "cheque1")
				   	        	{
					   	        	if(document.creditFertiBill1.chequeNum.value == "")
					   	        		{
					   	        			var msg="Please Enter Cheque Number";
											var dialog = bootbox.dialog({
												//title: "Embel Technologies Says :",
											   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
											    closeButton: false
											});
											
											setTimeout(function() {
												dialog.modal('hide');
											}, 1500);
											
											return false;
					   	        		}
					   	        	
					   	        	if(document.creditFertiBill1.nameOnCheck.value == "")
				   	        		{				   	        			
				   	        			var msg="Please Enter Name On Check Number";
										var dialog = bootbox.dialog({
											//title: "Embel Technologies Says :",
										   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
										    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
										    closeButton: false
										});
										
										setTimeout(function() {
											dialog.modal('hide');
										}, 1500);
										
										return false;
				   	        		}
				   	        	}
				   	        	
				   	        	if(document.creditFertiBill1.paymentModeCredit.value == "card1")
				   	        	{	
				   	        		
					   	        	if(document.creditFertiBill1.cardNum.value == "")
					   	        	{					   	        			
					   	        			var msg="Please Enter Card Number";
											var dialog = bootbox.dialog({
												//title: "Embel Technologies Says :",
											   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
											    closeButton: false
											});
											
											setTimeout(function() {
												dialog.modal('hide');
											}, 1500);
											
											return false;
					   	        			
					   	        		}
					   	        	if(document.creditFertiBill1.cardNum.value != "")
				   	        		{
					   	        		var numbers = /^[0-9]+$/;
					   	        		if(document.creditFertiBill1.cardNum.value.match(numbers))
					   	        		{
					   	        			
					   	        		}
					   	        		else
					   	        		{
					   	        			var msg="Please Enter Only Numbers in Card Number";
											var dialog = bootbox.dialog({
												//title: "Embel Technologies Says :",
											   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
											    closeButton: false
											});
											
											setTimeout(function() {
												dialog.modal('hide');
											}, 1500);
											
											return false;
					   	        			
					   	        		}
				   	        		}
				   	        	}
				   	        	
				   	        	
				   	        	if(document.creditFertiBill1.paymentModeCredit.value == "neft1")
				   	        	{
					   	        	if(document.creditFertiBill1.accNum.value == "")
					   	        		{					   	        			
					   	        			
					   	        			var msg="Please Enter Account Number";
											var dialog = bootbox.dialog({
												//title: "Embel Technologies Says :",
											   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
											    closeButton: false
											});
											
											setTimeout(function() {
												dialog.modal('hide');
											}, 1500);
											return false;
					   	        			
					   	        			
					   	        		}
					   	        	if(document.creditFertiBill1.accNum.value != "")
				   	        		{
					   	        		var numbers = /^[0-9]+$/;
					   	        		if(document.creditFertiBill1.accNum.value.match(numbers))
					   	        		{
					   	        			
					   	        		}
					   	        		else
					   	        		{
					   	        			var msg="Please Enter Only Numbers in Account Number";
											var dialog = bootbox.dialog({
												//title: "Embel Technologies Says :",
											   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
											    closeButton: false
											});
											
											setTimeout(function() {
												dialog.modal('hide');
											}, 1500);
											
											return false;
					   	        			
					   	        		}

				   	        		}
				   	        	
					   	        	if(document.creditFertiBill1.bankName.value == "")
				   	        		{
					   	        		
				   	        			var msg="Please Enter Bank Name";
										var dialog = bootbox.dialog({
											//title: "Embel Technologies Says :",
										   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
										    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
										    closeButton: false
										});
										
										setTimeout(function() {
											dialog.modal('hide');
										}, 1500);
										
										return false;
				   	        			
				   	        			
				   	        		}
					   	        	if(document.creditFertiBill1.bankName.value != "")
				   	        		{
						   	        	var letters = /^[A-Za-z]+$/;
						   	        	if(document.creditFertiBill1.bankName.value.match(letters))
						   	        	{
						   	        		
						   	        	}
						   	        	else
						   	        	{
					   	        			var msg="Please Enter Valid Bank Name";
											var dialog = bootbox.dialog({
												//title: "Embel Technologies Says :",
											   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
											    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
											    closeButton: false
											});
											
											setTimeout(function() {
												dialog.modal('hide');
											}, 1500);
											
											return false;
					   	        			
						   	        	}
					   	        		
				   	        		}
				   	        	}
						   	}
							   	 	fertilizerBillForCredit();
}

function fertilizerBillForCredit()
{
	//document.creditFertiBill1.btn2.disable= true ;
	document.getElementById("billingCreditBtn").disabled = true;
	
	var params = {};
	
	var customerType = "CREDITCUSTOMER";	
	
	var firstPaidAmtCreditCustomer = $("#firstPaidAmtCreditCustomer").val();
	if(firstPaidAmtCreditCustomer == "" || firstPaidAmtCreditCustomer == null || firstPaidAmtCreditCustomer == undefined) 
	{
		firstPaidAmt = '0';
	}
	
	var input = document.getElementById('creditCustomer'),
    list = document.getElementById('cust_drop1'),
    i,creditCustomer;
	for (i = 0; i < list.options.length; ++i) {
    if (list.options[i].value === input.value) {
    	creditCustomer = list.options[i].getAttribute('data-value');
    	}
	}
	
	var fkCreditCustomerID = creditCustomer;
	var creditCustomerName = $('#creditCustomer').val();
	var creditCustomerHiddenName = $('#customerNameHidden').val();
	var village = $('#village1').val();
	var contactNo = $('#contactNo1').val();
	var saleDate = $('#saleDate2').val();
	var input = document.getElementById('shopName2'),
    list = document.getElementById('shop_drop2'),
    i,shopName;

	for (i = 0; i < list.options.length; ++i) {
    if (list.options[i].value === input.value) {
    	shopName = list.options[i].getAttribute('data-value');
    	}
	}
	var gstNo = $('#gstNo2').val();
	//var aadhar = $('#aadharNo1').val();
	/*var transExpense = $('#transExpence1').val();*/
	var hamaliExpense = $('#hamaliExpence1').val();
	/*var transExpenseWithoutGST = $('#transExpence3').val();*/
	var hamaliExpenseWithourGST = $('#hamaliExpence3').val();
	var grossTotal = $('#grossTotal1').val();
	var total = $('#totalWithExpense1').val();
	var discount = $('#discount1').val();
	var discountAmount = $('#discountAmount1').val();
	
	var paymentMode = $('#paymentModeCredit').val();
	var chequeNum = document.creditFertiBill1.chequeNum.value;
	var nameOnCheck = document.creditFertiBill1.nameOnCheck.value;
	var cardNum = document.creditFertiBill1.cardNum.value;
	var accNum = document.creditFertiBill1.accNum.value;
	var bankName = document.creditFertiBill1.bankName.value;
	 
	/*productId = $('#proName1').val();
		
		$("#proName1 option:selected").each(function() {
			   selectedVal = $(this).text();
			});
		
		var splitText = selectedVal.split(",");
		
		var fk_shop_id = splitText[12];*/
		
	var count = jQuery("#credit").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#credit').getGridParam('data');//to get all rows of grid
	var AllRows=JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++)
	{

		var cat_id = allRowsInGrid[i].cat_id;
		params["cat_id"+i] = cat_id;
		
		var sub_Cat_Id = allRowsInGrid[i].sub_Cat_Id;
		params["sub_Cat_Id"+i] = sub_Cat_Id;
		
		var itemName = allRowsInGrid[i].itemName;
		params["itemName"+i] = itemName;
		
		var companyName = allRowsInGrid[i].companyName;
		params["companyName"+i] = companyName;
		
		var hsn = allRowsInGrid[i].hsn;
		params["hsn"+i] = hsn;
		
		var unitName = allRowsInGrid[i].unitName;
		params["unitName"+i] = unitName;
		
		var weight = allRowsInGrid[i].weight;
		params["weight"+i] = weight;
		
		var expiryDate = allRowsInGrid[i].expiryDate;
		params["expiryDate"+i] = expiryDate;
		
		var salePrice = allRowsInGrid[i].salePrice;
		var mrp = allRowsInGrid[i].mrp;
		
		if(salePrice != '')
		{		
			var numbers = /^[0-9]+\.?[0-9]*$/;
			if(String(salePrice).match(numbers))
			{	
				if(salePrice == "0" || salePrice == undefined || salePrice == '' || salePrice == null)
				{
					var msg="Please Enter Unit Price";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);

						document.getElementById("billingCreditBtn").disabled = false;
						return false;
				}
			}
			else
			{
				var msg="Please Enter Valid Unit Price";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);

					document.getElementById("billingCreditBtn").disabled = false;
					return false;
			}
		}
		
		
		if(mrp != '')
		{
			var numbers = /^[0-9]+\.?[0-9]*$/;
			if(String(mrp).match(numbers))
			{		
				if(mrp == '' || mrp == '0' || mrp == undefined || mrp == null)
				{
					mrp = 0;
				}
				else if(Number(salePrice) > Number(mrp))
					{
						var msg="MRP must be greater Than Unit Price";
						var dialog = bootbox.dialog({
							//title: "Embel Technologies Says :",
						   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
						    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
						    closeButton: false
						});
						
						setTimeout(function() {
							dialog.modal('hide');
						}, 1500);
						document.getElementById("billingCreditBtn").disabled = false;
						return false;

					}
			}
			else
			{			
					var msg="Please Enter valid MRP Price";
					var dialog = bootbox.dialog({
						//title: "Embel Technologies Says :",
					   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
					    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
					    closeButton: false
					});
					
					setTimeout(function() {
						dialog.modal('hide');
					}, 1500);

					document.getElementById("billingCreditBtn").disabled = false;
					return false;
			}			
		}
		
		params["salePrice"+i] = salePrice;
		params["mrp"+i] = mrp;
		
		var quantity = allRowsInGrid[i].quantity;
				
		if(unitName == 'kg')
		{
			if(quantity == "0" || quantity == undefined || quantity == "" || quantity == null)			
			{
				var msg="Please Enter Kg OR Grams";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);

 				document.getElementById("billingCreditBtn").disabled = false;
				return false;
			}
			else
			{
				params["quantity"+i] = quantity;		
			}
		}
		
		if(unitName == 'ltr')
		{
			if(quantity == "0" || quantity == undefined || quantity == "" || quantity == null)
			{
				var msg="Please Enter Ltr OR mili";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);

 				document.getElementById("billingCreditBtn").disabled = false;
				return false;
			}
			else
			{
				params["quantity"+i] = quantity;		
			}
		}
		
		
		if(unitName == 'pcs')
		{
			if(quantity == "0" || quantity == undefined || quantity == "" || quantity == null)
			{
				var msg="Please Enter Quantity";
				var dialog = bootbox.dialog({
					//title: "Embel Technologies Says :",
				   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
				    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
				    closeButton: false
				});
				
				setTimeout(function() {
					dialog.modal('hide');
				}, 1500);

 				document.getElementById("billingCreditBtn").disabled = false;
				return false;
			}
			else
			{
				params["quantity"+i] = quantity;		
			}
			
			var freeQuantity = allRowsInGrid[i].freeQuantity;
			if(freeQuantity == "0" || freeQuantity == undefined || freeQuantity == "" || freeQuantity == null)
			{
				params["freeQuantity"+i] = 0;
			}
			else
			{
				params["freeQuantity"+i] = freeQuantity;
			}
		}
				
		var gst = allRowsInGrid[i].vatPercentage;
		params["gst"+i] = gst;
		
		/*var gst = allRowsInGrid[i].gst;
		params["gst"+i] = gst;*/
		
		var igst = allRowsInGrid[i].igst;
		params["igst"+i] = igst;
		
		var total = allRowsInGrid[i].total;
		params["total"+i] = total;
	}
	
	if(count == "0" || count == undefined || count == "" || count == null)
	{
		var msg="Please Select Product";
		var dialog = bootbox.dialog({
			//title: "Embel Technologies Says :",
		   /* message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'<img src="/Shop/staticContent/images/s1.jpg" height="50" width="50"/></p>',*/
		    message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
		    closeButton: false
		});
		
		setTimeout(function() {
			dialog.modal('hide');
		}, 1500);
		document.getElementById("billingCreditBtn").disabled = false;
		return false;
	}
	
	params["creditCustomerHiddenName"] = creditCustomerHiddenName;
	params["fkCreditCustomerID"] = fkCreditCustomerID;
	params["count"] = count;
	params["village"] = village;
	params["saleDate"] = saleDate;
	params["gstNo"] = gstNo;
	params["contactNo"] = contactNo;
	//params["aadhar"] = aadhar;
	/*params["transExpense"] = transExpense;*/
	/*params["transExpenseWithoutGST"] = transExpenseWithoutGST;*/
	params["hamaliExpenseWithourGST"] = hamaliExpenseWithourGST;
	params["total"] = total;
	params["fk_shop_id"] = shopName;
	params["grossTotal"] = grossTotal;
	params["hamaliExpense"] = hamaliExpense;
	params["creditCustomerName"] = creditCustomerName;
	params["discount"] = discount;
	params["discountAmount"] = discountAmount
	params["customerType"] = customerType;
	params["firstPaidAmtCreditCustomer"] = firstPaidAmtCreditCustomer;
	params["paymentMode"] = paymentMode;
	params["chequeNum"] = chequeNum;	
	params["nameOnCheck"] = nameOnCheck;
	params["cardNum"] = cardNum;
	params["accNum"] = accNum;
	params["bankName"] = bankName;	
	
	params["methodName"] = "addingFertilizerBill";
	
	$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
	{
		window.open("FertilizerBillPDF.jsp");
		if((firstPaidAmtCreditCustomer != "0" || firstPaidAmtCreditCustomer != "" || firstPaidAmtCreditCustomer != null || firstPaidAmtCreditCustomer != undefined) && Number(firstPaidAmtCreditCustomer) > 0)
		{
			window.open("creditNoteForCustomerPDF.jsp");
		}
		location.reload();
		document.getElementById("billingCreditBtn").disabled = false;
	}
	).error(function(jqXHR, textStatus, errorThrown){
		if(textStatus==="timeout") {
			$(loaderObj).hide();
			$(loaderObj).find('#errorDiv').show();
		}
	});
}

/*** +++ Fetching product Name FOR CASH CUSTOMER+++ *****/
function getProductName()
{				
	var shopId = $("#shopId").val();

	
	
	 	//var fk_cat_id = 1;
		$("#proName").empty();
		$("#proName").append($("<option></option>").attr("value","").text("Select Product"));
					 
		var params= {};
		
		params["shopId"] = shopId;
		params["methodName"] = "getAllProductByCategoriesForFertiBill";
		
		
		//params["fk_cat_id"]= fk_cat_id;
		
		$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
		{
			var count = 1;
				
			var jsonData = $.parseJSON(data);
			$.each(jsonData,function(i,v)
			{				
					$("#proName").append($("<option></option>").attr("value",count).text(v.product+" => "+v.manufacturer+" => "+v.weight+" "+v.unitName+" cat => "+v.catName+" => "+v.fkCatId+" subcat => "+v.subCatName+" => "+v.fkSubCatId+" => "+v.stockInTotalKgLtrPiece+" "+v.unitName+" Expiry Date => "+v.expiryDate));
					count++;
				
					$("#proName").append($("<option></option>").attr("value",count).text(v.product + ","+v.manufacturer+","+v.weight+",Kg Stock =,"+v.quantityDouble+",cat =,"+v.catName+","+v.fkCatId+",subcat =,"+v.subCatName+","+v.fkSubCatId+","+v.stockInGrams+","+v.stockInMili+","+v.stockInTotalPieceQauntity));
					count++;
			});
		}
		).error(function(jqXHR, textStatus, errorThrown){
					if(textStatus==="timeout") {

					}
				});
	}

/*** +++ Fetching product Name FOR CREDIT CUSTOMER+++ *****/
function getProductNameForCredit()
{
	var shopId = $("#shopId").val();
		
	 //var fk_cat_id = 1;
		$("#proName2").empty();
		$("#proName2").append($("<option></option>").attr("value","").text("Select product"));
		var params= {};
		params["shopId"] = shopId;
		params["methodName"] = "getAllProductByCategoriesForFertiBill";
		
		//params["fk_cat_id"]= fk_cat_id;
		
		$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
		{ var count = 1;
				
			var jsonData = $.parseJSON(data);
			$.each(jsonData,function(i,v)
			{
					$("#proName2").append($("<option></option>").attr("value",count).text(v.product+" => "+v.manufacturer+" => "+v.weight+" "+v.unitName+" cat => "+v.catName+" => "+v.fkCatId+" subcat => "+v.subCatName+" => "+v.fkSubCatId+" => "+v.stockInTotalKgLtrPiece+" "+v.unitName+" Expiry Date => "+v.expiryDate));
					count++;
					$("#proName2").append($("<option></option>").attr("value",count).text(v.product + ","+v.manufacturer+","+v.weight+",Kg Stock =,"+v.quantityDouble+",cat =,"+v.catName+","+v.fkCatId+",subcat =,"+v.subCatName+","+v.fkSubCatId)); 
					count++;
					});
				}).error(function(jqXHR, textStatus, errorThrown){
					if(textStatus==="timeout") {

					}
				});
}



/* ++++++++++++++ Get credit customer details ++++*/
function customerDetail(){
	
	this.getVillageName = getVillageName;
	this.getContactNo = getContactNo;
	this.getName = getName;
	//this.getAadhar = getAadhar;
	
	function getVillageName()
	{
		
		var input = document.getElementById('creditCustomer'),
	    list = document.getElementById('cust_drop1'),
	    i,creditCustomer;

		for (i = 0; i < list.options.length; ++i) {
	    if (list.options[i].value === input.value) {
	    	creditCustomer = list.options[i].getAttribute('data-value');
	    }
	}
		
		var creditCustomerId = creditCustomer;
		$("#village1").empty();
		$("#village1").append($("<input/>").attr("value","").text());
		var params= {};
		params["methodName"] = "getVillageNameAndContactNoAndFirstNameByCustomer";
		params["creditCustomerId"]= creditCustomerId;
		$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
				{
			var jsonData = $.parseJSON(data);
			//var jsonData = jsonData.list;
			$.each(jsonData,function(i,v)
			{
				document.getElementById("village1").value = v.village;
				document.getElementById("gstNo2").value = v.gstno;
					});
				}).error(function(jqXHR, textStatus, errorThrown){
					if(textStatus==="timeout") {

					}
				});

	}
	
	/*function getAadhar(){

		
		var input = document.getElementById('creditCustomer'),
	    list = document.getElementById('cust_drop1'),
	    i,creditCustomer;

		for (i = 0; i < list.options.length; ++i) {
	    if (list.options[i].value === input.value) {
	    	creditCustomer = list.options[i].getAttribute('data-value');
	    }
	}
		
		var creditCustomerId = creditCustomer;
		$("#aadharNo1").empty();
		$("#aadharNo1").append($("<input/>").attr("value","").text());
		var params= {};
		params["methodName"] = "getVillageNameAndContactNoAndFirstNameByCustomer";
		params["creditCustomerId"]= creditCustomerId;
		$.post('/Fertilizer/jsp/utility/controller.jsp',params,function(data)
				{
			var jsonData = $.parseJSON(data);
			//var jsonData = jsonData.list;
			$.each(jsonData,function(i,v)
					{
				document.getElementById("aadharNo1").value = v.aadhar;
				
					});
			
				}).error(function(jqXHR, textStatus, errorThrown){
					if(textStatus==="timeout") {

					}
				});

	
		
	}*/
	
	function getContactNo()
	{
		
		var input = document.getElementById('creditCustomer'),
	    list = document.getElementById('cust_drop1'),
	    i,creditCustomer;

		for (i = 0; i < list.options.length; ++i) {
	    if (list.options[i].value === input.value) {
	    	creditCustomer = list.options[i].getAttribute('data-value');
	    }
	}
		
		var creditCustomerId = creditCustomer;
		$("#contactNo1").empty();
		$("#contactNo1").append($("<input/>").attr("value","").text());
		var params= {};
		params["methodName"] = "getVillageNameAndContactNoAndFirstNameByCustomer";
		params["creditCustomerId"]= creditCustomerId;
		$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
				{
			var jsonData = $.parseJSON(data);
			//var jsonData = jsonData.list;
			$.each(jsonData,function(i,v)
					{
				document.getElementById("contactNo1").value = v.contactNo;
					});
				}).error(function(jqXHR, textStatus, errorThrown){
					if(textStatus==="timeout") {

					}
				});

	}
	
	
	
	function getName()
	{
		
		var input = document.getElementById('creditCustomer'),
	    list = document.getElementById('cust_drop1'),
	    i,creditCustomer;

		for (i = 0; i < list.options.length; ++i) {
	    if (list.options[i].value === input.value) {
	    	creditCustomer = list.options[i].getAttribute('data-value');
	    }
	}
		
		var creditCustomerId = creditCustomer;
		$("#customerNameHidden").empty();
		$("#customerNameHidden").append($("<input/>").attr("value","").text());
		var params= {};
		params["methodName"] = "getVillageNameAndContactNoAndFirstNameByCustomer";
		params["creditCustomerId"]= creditCustomerId;
		$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
				{
			var jsonData = $.parseJSON(data);
			//var jsonData = jsonData.list;
			$.each(jsonData,function(i,v)
					{
				document.getElementById("customerNameHidden").value = v.firstName;
					});
				}).error(function(jqXHR, textStatus, errorThrown){
					if(textStatus==="timeout") {

					}
				});
	}
}

var custDetail = new customerDetail();


function customAlert(msg)
{
	var msg=msg;
	var dialog = bootbox.dialog({
	message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	closeButton: false
	});			
	setTimeout(function() {
		dialog.modal('hide');
	}, 1500);
}