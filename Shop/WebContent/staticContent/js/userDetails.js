//validateing password with re-password

function customAlert(msg)
{
	var msg=msg;
	var dialog = bootbox.dialog({
	message: '<p class="text-center">'+msg.fontcolor("red").fontsize(5)+'</p>',
	closeButton: false
	});			
	setTimeout(function() {
		dialog.modal('hide');
	}, 1500);
}

function validatePassword(){
        	 var password=$('#password').val();
        	 var rePassword=$('#password2').val();
        	 
        	 if(password != rePassword){
        		 
    		 msg = "Password mismatch";
    		 customAlert(msg);
    		 return false;
        	 }
         } 
 
 //Ading user detail
 
function regUserDetails(){
        		if(document.usd.firstName.value == "")
        		{
        			msg = "Enter Employee First Name.";
           		 	customAlert(msg)
           		 	return false;
        		}	
        		var letterNumber = /^[a-zA-Z, ]+$/;
        		if(document.usd.firstName.value.match(letterNumber))
        		{
        			
        			/* var letterNumber = /^[a-zA-Z, ]+$/;
        			if(document.usd.middleName.value.match(letterNumber))
        			{
        				 */if(document.usd.lastName.value == "")
        				{
                			msg = "Enter Employee Last Name.";
                   		 	customAlert(msg)
                   		 	return false;
        				}	
        				var letterNumber = /^[a-zA-Z, ]+$/;
        				if(document.usd.lastName.value.match(letterNumber))
        				{
        					
        		   	        			 if ( document.usd.contactNo.value == "" )
        		   	        			 {
        		   	        				msg = "Please Enter Contact Number";
        		                   		 	customAlert(msg)
        		                   		 	return false;
        		   	        			 }
        		   	        			 var letterNumber = /^[0-9]{10}$/;
        		   	        			 if(document.usd.contactNo.value.match(letterNumber))
        		   	        			 {
        		   	        				if(document.usd.address1.value == "")
        		   	        				{
            		   	        				msg = "Please Enter Employee Address line 1.";
            		                   		 	customAlert(msg)
            		                   		 	return false;
        		   	        				}	
        		   	        				var letterNumber = /^[a-zA-Z0-9, ]+$/;
        		   	        				if(document.usd.address1.value.match(letterNumber))
        		   	        				{
        		   	        					
												if ( document.usd.userName.value == "" )
    										    {
	            		   	        				msg = "Please Enter user name...!!!";
	            		                   		 	customAlert(msg)
	            		                   		 	return false;
    										    }
    											var letterNumber = /^[a-zA-Z0-9, ]+$/;
    											if(document.usd.userName.value.match(letterNumber))
    											{
    												if ( document.usd.password.value == "" )
    												{
    													msg = "Please Enter password...!!!";
    	            		                   		 	customAlert(msg)
    	            		                   		 	return false;
		  										    }
														if ( document.usd.password2.value == "" )
		  										    {
    													msg = "Please Enter re-password...!!!";
    	            		                   		 	customAlert(msg)
    	            		                   		 	return false;
		  										    }
        											if ( document.usd.pan.value == "" )
        										    {
        												msg = "Please Enter Pan number...!!!";
    	            		                   		 	customAlert(msg)
    	            		                   		 	return false;
        										    }
        											var letterNumber = /^[a-zA-Z0-9]+$/;
        											if(document.usd.pan.value.match(letterNumber))
        											{
            											usrDetails();
        											
            											}
        										
            											else
            											{
            													msg = "Enter Numbers And Alphabates Only in user name field..!!";
            	            		                   		 	customAlert(msg)
            	            		                   		 	return false;
            												}
            											}
            											
            											else
        											{
        													msg = "Enter Numbers And Alphabates Only in Pan Number field..!!";
        	            		                   		 	customAlert(msg)
        	            		                   		 	return false;
        												}
        											}
        										
        		   	        				else
        										{
        											msg = "Enter Numbers and Alphabates Only in address line 1 field..!!";
	            		                   		 	customAlert(msg)
	            		                   		 	return false;
        										}	
        									}
        											
        		   	        			 else
        									{
        										msg = "Enter 10 digit Numbers Only in contact number field..!!";
            		                   		 	customAlert(msg)
            		                   		 	return false;
        										}	
        									}
				        				else
				        					{
				        						msg = "Enter Alphabets Only in last name field..!!";
					                   		 	customAlert(msg)
					                   		 	return false;
				        					}
				        				}
					        		else
					        			{
					        				msg = "Enter Alphabets Only in first name field..!!";
					               		 	customAlert(msg)
					               		 	return false;
					        			}
        		}	
      
        
        		
    function usrDetails(){

        		document.usd.btn.disabled = true;
        			
        		var params = {};
        		
        			var firstName = $('#firstName').val();
        			var city = $('#city').val();
        			var lastName  = $('#lastName').val();
        			var contactNo = $('#contactNo').val();
        			var address1 = $('#address1').val();
        			var pan = $('#pan').val();
        			var userName = $('#userName').val();
        			var password = $('#password').val();
        			var password2 = $('#password2').val();
        			var typeId= $('#typeId').val();

        			
        			
        			params["firstName"] = firstName;
        			params["city"] = city;
        			params["lastName"] = lastName;
        			params["contactNo"] = contactNo;
        			params["address1"] = address1;
        			params["pan"] =pan;
        			params["userName"] = userName;
        			params["password"] = password;
        			params["password2"] =password2;
        			params ["typeId"] = typeId;
        			
        			params["methodName"] = "regUserDetails";

        			$.post('/Shop/jsp/utility/controller.jsp',params,function(data)
        		 	{
        				var msg=data;
        				var dialog = bootbox.dialog({
        				message: '<p class="text-center">'+msg.fontcolor("green").fontsize(5)+'</p>',
        				closeButton: false
        				});			
        				setTimeout(function() {
        					dialog.modal('hide');
        					location.reload();
        				}, 1500);
		 				
		 				document.usd.btn.disabled =false;
		 			}
		 	    	).error(function(jqXHR, textStatus, errorThrown){
		 	    		if(textStatus==="timeout") {
		 	    			$(loaderObj).hide();
		 	    			$(loaderObj).find('#errorDiv').show();
		 	    		}
		 	    	});        		 			
        		}       		
        		
        	function reset()
        	{
        	document.usd.reset();	

        	}




/*function regUserDetails(){
	if(document.usd.firstName.value == "")
	{
		alert("Enter Employee First Name.");
		return false;
	}	
	var letterNumber = /^[a-zA-Z]+$/;
	if(document.usd.firstName.value.match(letterNumber))
	{
		
		var letterNumber = /^[a-zA-Z]+$/;
		if(document.usd.middleName.value.match(letterNumber))
		{
			if(document.usd.lastName.value == "")
			{
				alert("Enter Employee Last Name.");
				return false;
			}	
			var letterNumber = /^[a-zA-Z]+$/;
			if(document.usd.lastName.value.match(letterNumber))
			{
				
	   	        			 if ( document.usd.contactNo.value == "" )
	   	        			 {
					  	       alert("Please Enter Contact Number");
					  	       return false;
	   	        			 }
	   	        			 var letterNumber = /^[0-9]+$/;
	   	        			 if(document.usd.contactNo.value.match(letterNumber))
	   	        			 {
	   	        				if(document.usd.address1.value == "")
	   	        				{
	   	        					alert("Please Enter Employee Address line 1.");
	   	        					return false;
	   	        				}	
	   	        				var letterNumber = /^[a-zA-Z]+$/;
	   	        				if(document.usd.address1.value.match(letterNumber))
	   	        				{
	   	        					
	   	        					
										if ( document.usd.pan.value == "" )
									    {
									         
									  	      alert("Please Enter Pan number...!!!");
									          return false;
									    }
										var letterNumber = /^[a-zA-Z0-9]+$/;
										if(document.usd.pan.value.match(letterNumber))
										{
										usrDetails();
										}
									else
										{
												alert("Enter Numbers And Alphabates Only in Pan Number field..!!");
												return false;
											}
										}
									
	   	        				else
									{
										alert("Enter Alphabates Only in address line 1 field..!!");
										return false;
									}	
								}
										
	   	        			 else
								{
									alert("Enter Numbers Only in contact number field..!!");
									return false;
									}	
								}
	   	        		 		
			else
				{
					alert("Enter Alphabets Only in last name field..!!");
					return false;
				}
			}
																
		
												
	else
		{
			alert("Enter Alphabets Only in first name field..!!");
			return false;
		}

	}	
}
	
	
function usrDetails(){

	document.usd.btn.disabled = true;
		
	var params = {};
	
		var firstName = $('#firstName').val();
		var city = $('#city').val();
		var lastName  = $('#lastName').val();
		var contactNo = $('#contactNo').val();
		var address1 = $('#address1').val();
		var pan = $('#pan').val();
		var userName = $('#userName').val();
		var password = $('#password').val();
		var password2 = $('#password2').val();
		

		
		
		params["firstName"] = firstName;
		params["city"] = city;
		params["lastName"] = lastName;
		params["contactNo"] = contactNo;
		params["address1"] = address1;
		params["pan"] =pan;
		params["userName"] = userName;
		params["password"] = password;
		params["password2"] =password2;
		
		
		params["methodName"] = "regUserDetails";

		$.post('/Fertilizer/jsp/utility/controller.jsp',params,function(data)
	 	    	{
	 				alert(data);
	 				if(document.usd)
	 				{
	 					document.usd.reset();
	 				}	
	 				document.usd.btn.disabled =false;
	 			}
	 	    	).error(function(jqXHR, textStatus, errorThrown){
	 	    		if(textStatus==="timeout") {
	 	    			$(loaderObj).hide();
	 	    			$(loaderObj).find('#errorDiv').show();
	 	    		}
	 	    	});
	 			
	}

	
	
function reset()
{
document.usd.reset();	

}

*/