function getAllBills() {

	var input = document.getElementById('supplier'), list = document
			.getElementById('sup_drop'), i, supplier;

	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input.value) {
			supplier = list.options[i].getAttribute('data-value');
		}
	}

	var supplier = supplier;
	$("#bill_no").empty();
	$("#bill_no").append(
			$("<option></option>").attr("value", "").text("Select bill"));
	var params = {};

	params["methodName"] = "getAllBillBySuppliers";

	params["supplier"] = supplier;

	$.post('/Shop/jsp/utility/controller.jsp', params, function(data) {
		var jsonData = $.parseJSON(data);
		// var jsonData = jsonData.list;
		$.each(jsonData, function(i, v) {
			$("#bill_no").append(
					$("<option></option>").attr("value", i).text(v.billNo));

		});
	})

}

function fetchDataForPurchase() {

	var bill_no = $('#bill_no').val();

	var input = document.getElementById('supplier'), list = document
			.getElementById('sup_drop'), i, supplier;

	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input.value) {
			supplier = list.options[i].getAttribute('data-value');
		}
	}

	var params = {};

	params["methodName"] = "getAllIetmByBillNo";

	params["bill_no"] = bill_no;
	params["supplier"] = supplier;

	$
			.post(
					'/Shop/jsp/utility/controller.jsp',
					params,
					function(data) {

						var jsonData = $.parseJSON(data);
						// $("#jqGrid1").jqGrid("clearGridData", true);

						$("#jqGrid").jqGrid("clearGridData", true).trigger(
								"reloadGrid");

						/*
						 * function sumFmatter (cellvalue, options, rowObject) {
						 * 
						 * 
						 * 
						 * var jam=0; var jam1=""; var tot=
						 * (options.rowData.quantity *
						 * options.rowData.buyPrice); //var shree =
						 * document.poferti.grossTotal.value;// to get gross
						 * total
						 * 
						 * var count = jQuery("#jqGrid").jqGrid('getGridParam',
						 * 'records'); var allRowsInGrid1 =
						 * $('#jqGrid').getGridParam('data'); var
						 * AllRows=JSON.stringify(allRowsInGrid1); for (var i =
						 * 0; i < count; i++) {
						 * 
						 * var quantity = allRowsInGrid1[i].quantity;
						 * params["quantity"+i] = quantity;
						 * 
						 * var buyPrice = allRowsInGrid1[i].buyPrice;
						 * params["buyPrice"+i] = buyPrice;
						 * 
						 * 
						 * var totals1=((buyPrice)*(quantity));
						 * 
						 * jam = jam + totals1; } if(count == 0){
						 * document.getElementById("total").value = tot;
						 * document.getElementById("duptotal").value = tot;
						 * }else{ document.getElementById("total").value = jam;
						 * document.getElementById("duptotal").value = jam; }
						 * 
						 * 
						 * return tot; }
						 * 
						 * 
						 * 
						 * 
						 */

						$
		                        .each(jsonData,function(i, v) {
											$("#jqGrid").jqGrid(
											{

																datatype : "local",

																colNames : [
																		"Product ID",
																		"product_Id22",
																		"Category_Id",
																		"Supplier",
																		"Product Name",
																		"Category",
																		"Company",
																		"Batch No",
																		"Packing",
																		"Buy Price",
																		"Sale Price",
																		"M.R.P",
																		"Tax per",
																		"Quantity",
																		"Return Quantity",
																		"Tax Amount",
																		/*"Dc No",*/
																		"total",
																		"Barcode No",
																		"PurchaseDate",
																		"DuplicateQuant" ],

																colModel : [
																		{
																			name : "pk_goods_receive_id",
																			hidden : true
																		// resizable:
																		// true,
																		},
																		
																		{
																			name : "pkPOId",
																			hidden : true
																		// resizable:
																		// true,
																		},
																		{
																			name : "fkCategoryId",
																			hidden : true
																			//hidden : true
																		// resizable:
																		// true,
																		},
																		
																		{
																			name : "supplier_name",
																			width : 100,
																		// resizable:
																		// true,
																		},
																		{
																			name : "product_name",
																			width : 100,
																		// resizable:
																		// true,
																		},
																		{
																			name : "catName",
																			width : 100,
																		},
																		{
																			name : "company_Name",
																			width : 100
																		},
																		{
																			name : "batch_no",
																			width : 70
																		},
																		{
																			name : "weight",
																			width : 70
																		},
																		{
																			name : "buy_price",
																			width : 100,
																			editable:true,
																		},
																		{
																			name : "sale_price",
																			width : 100
																		},
																		{
																			name : 'mrp',
																			// formatter:
																			// sumFmatter,
																			width : 100
																		},
																		{
																			name : "tax_percentage",
																			width : 50
																		},
																		{
																			name : "dupQuantity1",
																			width : 70
																		},
																		{
																			name : "dupQuantity",//return Quantity
																			width : 70,
																			editable : true
																		},
																		
																		{
																			name : "taxAmount",
																			width : 70
																		},
																		
																		/*{
																			name : "dc_number",
																			width : 70
																		},*/
																		{
																			name : "total",
																			width : 80
																		},
																		
																		{
																			name : "barcodeNo",
																			width : 80
																		},
																		{
																			name : "purchaseDate1",
																			width : 140
																		},
																		{
																			name : "dupQuantity1",
																			hidden : true
																		} ],

																sortorder : 'desc',

																multiselect : false,
																loadonce : false,
																rownumbers : true,
																forcePlaceholderSize : true,
																'cellEdit' : true,
																viewrecords : true,
																width : 1400,
																shrinkToFit : true,
																rowNum : 10,
																pager : "#jqGridPager",
																sortorder : "desc",

																afterSaveCell : function grossTotal() {
																	/*
																	 * Calculation
																	 * of total
																	 * after
																	 * editing
																	 * quantity
																	 */

																	// $(this).trigger('reloadGrid');
																	var rowId = $("#jqGrid").jqGrid('getGridParam','selrow');
																	var rowData = jQuery("#jqGrid").getRowData(rowId);
																	var prevquantity = rowData['quantity'];
																	var quantity = rowData['dupQuantity'];
																	var buy_price= rowData['buy_price'];
																	var total = rowData['total'];
																	var tax_percentage= rowData['tax_percentage'];
																	
																	 
																	

												   if (quantity == "" || quantity == null)
													{
													alert("Please Enter Return Quantity GRID CREATION fetchDataForPurchase()");
													return false;
													quantity = "";
													//location.reload();
													}

													if (Number(quantity) > Number(prevquantity)) {
													alert("Return Quantity Must Less Than Previous GRID CREATION fetchDataForPurchase()");
													return false;
													quantity = "";
																		//location.reload();
													}
													/*
													if(Number(quantity) > Number(prevquantity))
											       	  {
											       		  alert("Available Stock ="+prevquantity);
											       		  document.custord.btnSubmit.disabled = false;
											       		  return false;
											       	  }
													*/
															
												var total=0;
											    total=Number(buy_price)*Number(quantity);
												 $("#jqGrid").jqGrid("setCell", rowId, "total", total);
												 
												 var checkbuyPrice= /^[0-9]+\.?[0-9]*$/;
												  if(buy_price.match(checkbuyPrice))
											    {
											      	  /*if(Number(buyPrice) > 0)
											  		  {*/
											      		  /*alert("Buy price");*/
											      		  BpwTax = (buy_price/(1+(tax_percentage/100)));
											      		taxAmount=(BpwTax*(tax_percentage/100));
											      		  $("#jqGrid").jqGrid("setCell", rowId, "taxAmount",  taxAmount.toFixed(2));
											    }
											     else
												{
											      var setZero = 0; 
												 $("#jqGrid").jqGrid("setCell", rowId, "taxAmount", setZero);
													   
												}
												 
												 
																},
																pager : "#jqGridPager",
															});

											     $("#jqGrid").addRowData(i,jsonData[i]);
											     $('#jqGrid').navGrid('#jqGridPager',
															// the buttons to
															// appear on the
															// toolbar of the
															// grid
															{
																edit : true,
																add : false,
																del : true,
																search : true,
																//refresh : true,
																view : true,
																position : "left",
																cloneToTop : false
															},
															// options for the
															// Edit Dialog

															{
																afterSubmit : function()
																{
																	$(this).trigger('reloadGrid');
																},
																editCaption : "The Edit Dialog",
																recreateForm : true,
																checkOnUpdate : true,
																checkOnSubmit : true,
																closeAfteredit : true,
																errorTextFormat : function(data)
																{
																	return 'Error: '+data.responseText
																}

															},
															{},
															// options for the
															// Delete Dialogue
															{
																afterSubmit : function() {
																	$(this).trigger('reloadGrid');
															},
																closeAfterdel : true,
																recreateForm : true,
																errorTextFormat : function(data) 
																{
																	return 'Error: '+ data.responseText
																},

														onSelectRow : function(id)
														{
															if (id&& id !== lastSel) {
															jQuery("#jqGrid").saveRow(lastSel,true,'clientArray');
															jQuery("#jqGrid").editRow(id,true);
															lastSel = id;
															console.log(id);
																	}
																}
															});

											// grid refresh code

										});

					});

}
/*function returnPurchaseValidate() {
	var supplier = $("#supplier").val();
	var bill_no = $("#bill_no").val();

	// if(supplier != "" || supplier != undefined || supplier != null ||
	// supplier != " ")
	if (supplier != "") {
		if (bill_no != "") {
			returntPurchase();
		} else {
			alert("Please Enter Bill No");
			return false;
		}
	} else {
		alert("Please Enter Supplier Name");
		return false;
	}
}*/

////////////////storing purchase Return data in database //////////////////

function purchaseReturnTable(){
	document.getElementById("btn").disabled = false;
	var params={};

	var supplierName=$('#supplier').val();
	var bill_no=$('#bill_no').val();
	
	var input1 = document.getElementById('supplier'),
	list = document.getElementById('sup_drop'),
	i,fk_supplier_id;
	for (i = 0; i < list.options.length; ++i) {
		if (list.options[i].value === input1.value) {
			fk_supplier_id= list.options[i].getAttribute('data-value');
		}
	}

	var count = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#jqGrid').getGridParam('data');//to get all rows of grid
	var AllRows=JSON.stringify(allRowsInGrid);
	var flag=0;
	for (var i = 0; i < count; i++)
	{

		var product_name= allRowsInGrid[i].product_name;
		//alert("Product Name------------"+product_name);
		params["product_name" + i] = product_name;

		var company_Name= allRowsInGrid[i].company_Name;
		//alert("CompanyName------------"+company_Name);
		params["company_Name" + i] = company_Name;

		
		var catName= allRowsInGrid[i].catName;
		//alert("Category Name----------"+catName);
		params["catName"+i] = catName;
		
		
		
		var pkPOId= allRowsInGrid[i].pkPOId;
		//alert("Prod id-----------"+pkPOId);
		params["pkPOId"+i] = pkPOId;
		
		
		var fkCategoryId= allRowsInGrid[i].fkCategoryId;
		//alert("fkCategoryId----------"+fkCategoryId);
		params["fkCategoryId"+i] = fkCategoryId;
		
		var batch_no= allRowsInGrid[i].batch_no;
		//alert("batchNo----------"+batch_no);
		params["batch_no"+i] = batch_no;

		var weight = allRowsInGrid[i].weight;
		//alert("weight ----------"+weight);
		params["weight" + i] = weight;
		
		var buy_price= allRowsInGrid[i].buy_price;
		//alert("buyPrice ----------"+buy_price);
		params["buy_price" + i] = buy_price;
		
		
		var sale_price= allRowsInGrid[i].sale_price;
		//alert("salePrice ----------"+sale_price);
		params["sale_price" + i] = sale_price;
		
		var mrp = allRowsInGrid[i].mrp;
		//alert("mrp ----------"+mrp);
		params["mrp" + i] = mrp;
		
		
		var tax_percentage= allRowsInGrid[i].tax_percentage;
		//alert("taxPercentage----------"+tax_percentage);
		params["tax_percentage" + i] = tax_percentage;
		
		var dupQuantity1= allRowsInGrid[i].dupQuantity1;//quantity
		//alert("quantity ----------"+dupQuantity1);
		params["dupQuantity1"+i] = dupQuantity1;
		
		var dupQuantity= allRowsInGrid[i].dupQuantity;
		//alert("returnQuantity----------" +dupQuantity);
		params["dupQuantity"+i] = dupQuantity;
		
		var taxAmount= allRowsInGrid[i].taxAmount;
		//alert("taxAmount----------"+taxAmount);
		params["taxAmount"+i] = taxAmount;
		
		var total = allRowsInGrid[i].total;
		if(total==null || total==undefined || total=="")
		{
			
			params["total"+i] = 0.0;
		}
		else
		{
			
			params["total"+i] = total;
		}
		
		if(dupQuantity1 < dupQuantity)
		{
			
			return false;
		}
		
		var barcodeNo= allRowsInGrid[i].barcodeNo;
		//alert("barcodeNo----------"+barcodeNo);
		params["barcodeNo"+i] = barcodeNo;
		
		
		var purchaseDate= allRowsInGrid[i].purchaseDate1;
		//alert("purchaseDate----------"+purchaseDate);
		params["purchaseDate" + i] = purchaseDate;

		
	}
	/*alert("supplierName--------"+supplierName);*/
	params["fk_supplier_id"] = fk_supplier_id;
	params["supplierName"] = supplierName;
	params["count"] = count;
	params["methodName"] = "PurchaseReturn";

	$.post('/Shop/jsp/utility/controller.jsp', params, function(data) {
		
		alert(data);
		// returntMinusFromStockPurchase();
	}).error(function(jqXHR, textStatus, errorThrown) {
		if (textStatus === "timeout") {
			$(loaderObj).hide();
			$(loaderObj).find('#errorDiv').show();
		}
	});
}


function returntPurchase()
{
	var supplier = $("#supplier").val();
	var bill_no = $("#bill_no").val();

/*		 if(supplier != "")
		 { 
			 if(bill_no != "")
			 { 
				 returntPurchase();
			 }
			 else
			 {
				 alert("Please Enter Bill No");
				 return false;
			 }
		 }
		 else
		 {
			 alert("Please Enter Supplier Name");
			 return false;
		 }
*/	 

	var params = {};

	var count = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#jqGrid').getGridParam('data');// to get all rows
	// of grid
	var AllRows = JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++) {
		var pk_goods_receive_id = allRowsInGrid[i].pk_goods_receive_id;
		params["pk_goods_receive_id" + i] = pk_goods_receive_id;

		//return Quantity
		var dupQuantity = allRowsInGrid[i].dupQuantity;
		if (dupQuantity == undefined)
		{
			alert("Please Enter Return Quantity FUNCTION ==> returntPurchase()")
			return false;
			dupQuantity = undefined;
			//return false;
		}
		params["dupQuantity" + i] = dupQuantity;

		var product_name = allRowsInGrid[i].product_name;
		params["product_name" + i] = product_name;

		var company_Name = allRowsInGrid[i].company_Name;
		params["company_Name" + i] = company_Name;

		var weight = allRowsInGrid[i].weight;
		params["weight" + i] = weight;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var tax_percentage = allRowsInGrid[i].tax_percentage;
		params["tax_percentage" + i] = tax_percentage;

		var buy_price = allRowsInGrid[i].buy_price;
		params["buy_price" + i] = buy_price;
		
		
	}

	params["count"] = count;
	params["methodName"] = "returntPurchase";

	$.post('/Shop/jsp/utility/controller.jsp', params, function(data) {
		 //alert(data);
		 //returntMinusFromStockPurchase();
	}).error(function(jqXHR, textStatus, errorThrown) {
		if (textStatus === "timeout") {
			$(loaderObj).hide();
			$(loaderObj).find('#errorDiv').show();
		}
	});
}

function returntMinusFromStockPurchase() {
	var params = {};

	var count = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#jqGrid').getGridParam('data');// to get all rows
	// of grid
	var AllRows = JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++) {
		var pk_goods_receive_id = allRowsInGrid[i].pk_goods_receive_id;
		params["pk_goods_receive_id" + i] = pk_goods_receive_id;
		
		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var dupQuantity = allRowsInGrid[i].dupQuantity;
		params["dupQuantity" + i] = dupQuantity;
		
		//return Quantity
		if(dupQuantity > duplicateQuantity)
			{
				alert("Return Quantity Must be Less Than Available Quantity");
				return false;
			}

		var product_name = allRowsInGrid[i].product_name;
		params["product_name" + i] = product_name;

		var company_Name = allRowsInGrid[i].company_Name;
		params["company_Name" + i] = company_Name;

		var weight = allRowsInGrid[i].weight;
		params["weight" + i] = weight;

		/*var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;*/
	}

	params["count"] = count;
	params["methodName"] = "returntMinusFromStockPurchase";

	$.post('/Shop/jsp/utility/controller.jsp', params, function(data) {
		//alert(data);
		location.reload();

	}).error(function(jqXHR, textStatus, errorThrown) {
		if (textStatus === "timeout") {
			$(loaderObj).hide();
			$(loaderObj).find('#errorDiv').show();
		}
	});
}

function registerPurchaseReturn() {

	var params = {};

	var count = jQuery("#jqGrid").jqGrid('getGridParam', 'records');
	var allRowsInGrid = $('#jqGrid').getGridParam('data');// to get all rows
	// of grid
	var AllRows = JSON.stringify(allRowsInGrid);
	for (var i = 0; i < count; i++) {

		var pk_goods_receive_id = allRowsInGrid[i].pk_goods_receive_id;
		params["pk_goods_receive_id" + i] = pk_goods_receive_id;

		var dupQuantity = allRowsInGrid[i].dupQuantity;
		params["dupQuantity" + i] = dupQuantity;

		var product_name = allRowsInGrid[i].product_name;
		params["product_name" + i] = product_name;

		var company_Name = allRowsInGrid[i].company_Name;
		params["company_Name" + i] = company_Name;

		var weight = allRowsInGrid[i].weight;
		params["weight" + i] = weight;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

		var duplicateQuantity = allRowsInGrid[i].dupQuantity1;
		params["duplicateQuantity" + i] = duplicateQuantity;

	}

	params["count"] = count;
	params["methodName"] = "returntMinusFromStockPurchase";

	$.post('/Shop/jsp/utility/controller.jsp', params, function(data)
			{
		alert(data);
		location.reload();

	}).error(function(jqXHR, textStatus, errorThrown) {
		if (textStatus === "timeout") {
			$(loaderObj).hide();
			$(loaderObj).find('#errorDiv').show();
		}
	});
}