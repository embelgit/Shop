<%@page import="com.Fertilizer.hibernate.shopDetailsBean"%>
<%@page import="com.Fertilizer.dao.shopDetailsDao"%>
<%@page import="com.Fertilizer.bean.StockDetail"%>
<%@page import="com.Fertilizer.hibernate.Stock"%>
<%@page import="com.Fertilizer.hibernate.GodownEntry"%>
<%@page import="com.Fertilizer.dao.GodownEntryDao"%>
<%@page import="com.Fertilizer.hibernate.CategoryDetailsBean"%>
<%@page import="com.Fertilizer.dao.CategoryDetailsDao"%>
<% boolean isHome=false;%>
<%@include file="commons/header.jsp"%>
<%@page import="java.util.List"%>
<head>
<script src="/Shop/staticContent/js/stockDetails.js"></script>
<!-- <script src="/Shop/staticContent/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/selectjj.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/buttom.js"></script>
<script src="/Shop/staticContent/js/jquery.min.js"></script>
<script src="/Shop/staticContent/js/jquery.jqgrid.min.js"></script>
<script src="/Shop/staticContent/js/jquery.dataTables.js" type="text/javascript"></script>
<script type="text/javascript" src="/Shop/staticContent/js/jqueryUi.js"></script>

<link href="/Shop/WebContent/staticContent/css/dataTa.css" rel="stylesheet" type="text/css" media="all" />
<link href="/Shop/staticContent/css/dataTables.jqueryui.min.css" rel="stylesheet" type="text/css" media="all">
<link href="/Shop/staticContent/css/select.css" rel="stylesheet" type="text/css" media="all">
<link href="/Shop/staticContent/css/button.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="/Shop/staticContent/css/jquery-ui.min.css">
<link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.min.css"> -->


<script src="/Shop/staticContent/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/selectjj.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/buttom.js"></script>
<script src="/Shop/staticContent/js/jquery.min.js"></script>
<script src="/Shop/staticContent/js/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/jqueryUi.js"></script>
<script src="/Shop/staticContent/js/jquery.dataTables.js" type="text/javascript"></script>
<link href="/Shop/WebContent/staticContent/css/dataTa.css" rel="stylesheet" type="text/css" media="all" />
<link href="/Shop/staticContent/css/dataTables.jqueryui.min.css" rel="stylesheet" type="text/css" media="all">
<link href="/Shop/staticContent/css/select.css" rel="stylesheet" type="text/css" media="all">
<link href="/Shop/staticContent/css/button.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="/Shop/staticContent/css/jquery-ui.min.css">
<link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.min.css">

<!-- For datatable to pdf,print,excel etc conversion -->
<script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script> 
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/bootstrap.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/bootbox.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"> 
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

<script type="text/javascript">
function openBilling() {
		 window.location = '/Shop/jsp/allBilling.jsp';
}

</script>


</head>

<body onload="getProductName()">
<div class="container col-md-offset-1" style="float: left"> 
 		
 		<div class="row">
			<div align="center" style="margin-top: 75px">
				  <h2 class="form-name style_heading">Stock Reports</h2>
			</div>
				 	
			 <div class="row">
				<div class="col-sm-offset-1 col-md-10">
						<hr style="border-top-color:#c1b1b1;">
				</div>	
			  </div>
		</div>

		<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#cat"><h4 style="color:blue">Category Wise</h4></a></li>
	    <li><a data-toggle="tab" href="#productName"><h4 style="color:blue">Product Wise</h4></a></li>
	    <li><a data-toggle="tab" href="#companyName"><h4 style="color:blue">Company Wise</h4></a></li>
	    <li><a data-toggle="tab" href="#godown"><h4 style="color:blue">Shop Wise</h4></a></li>
 	 </ul>
 
 	<div class="tab-content" style="float: left">
   
				    <!------------------------------------------------- CATEGORY WISE  ------------------------------------------------------->
   
    	<div id="cat" class="tab-pane fade in active" align="center">
    		<div class="row"></div>
 		
 				<form class="form-horizontal" method="post" action="" name="fertiBill">
					<fieldset>
				         <div class="row form-group" style="margin-top: 20px">
								<label class="col-md-2 control-label" for="fk_cat_id">Product Category<sup>*</sup></label>  
			           			 <div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="	glyphicon glyphicon-hand-right"></i>
										</span>
			              				
										<!-- Following code is to get categories from "categories" table of "fertilizer" DB -->
										<!-- getAllMainCat() is implemented in  CategoryDetailsDao with return type List-->
										<%
										   CategoryDetailsDao cdd = new CategoryDetailsDao();
			           					   List cList =cdd.getAllMainCat();
										%>
										<input list="cat_drop" id="fk_cat_id" name="fk_cat_id" class="form-control">
							<datalist id="cat_drop">
										<%
								           for(int i=0;i<cList.size();i++){
								        	   CategoryDetailsBean cat=(CategoryDetailsBean)cList.get(i);
										%>
					
										<option data-value="<%=cat.getCatId()%>" value="<%=cat.getCategoryName()%>">
										<%
							      			}
							    		%>
						              	
			            			</datalist>
			            			</div>
            					</div> 
				     <div class="col-md-3">
						<div class="input-group">
									<input type="button"  id="btn" name="save" class="btn btn-lg btn-success btn-md button_hw button_margin_right" onclick="StockDetailsReportAsPerCatValidate()" value="Search"/>	
									<!-- <input type="button"  id="btn" name="save" class="btn btn-lg btn-success btn-md button_hw button_margin_right" onclick="StockDetailsReportAsPerCat()" value="Search"/> -->
												     									
						</div>
					</div>	
				</div>		
						<table class= "table table-bordered table-striped table-condensed cf" id="stockByCat" class="display">
						<thead>
							<tr>
								<th>Product Name</th>
								<th>Company Name</th>
								<th>Packing</th>
								<th>Quantity</th>
								<th>Stock Available</th>
								<th>Unit</th>							
							</tr>
						</thead>
						<tfoot>
						<!-- 
							<tr>
								  <th colspan="4" style="text-align: right">Total Quantity:</th>
								<th></th> 
							</tr>
						 -->
						</tfoot>
					</table>
					<div class="row form-group buttons_margin_top ">
								<div align="center">
								<input type="button" id="btn" style="font-size: 25" class="btn btn-large btn-success button-height-width" name="btn" onclick="openBilling()" value="Back To Billing">	
								</div>
					</div>
				</fieldset>
				</form>
    	</div>
    	
    			<!------------------------------------------------- PRODUCT NAME WISE -------------------------------------------->
    	
    	<div id="productName" class="tab-pane">
    	<div class="row"></div>
 		
 				<form class="form-horizontal" method="post" action="" name="fertiBill">
					<fieldset>
				         <div class="row form-group" style="margin-top: 20px">
								<label class="col-md-2 control-label" for="fk_godown_id">Select Product<sup>*</sup></label>  
           			 <div class="col-md-3">
						<div class="input-group">
						
							<span class="input-group-addon">
								<i class="	glyphicon glyphicon-hand-right"></i>
							</span>
							<!-- <input list="cat_drop" id="fk_cat_id" name="fk_cat_id" class="form-control">   -->
									<select class="form-control" id="proName"   name="proName"  >
									</select>
            			</div>
            		</div> 
			
					  
				     <div class="col-md-3">
						<div class="input-group">
												  
								<input type="button"  id="btn" name="save" class="btn btn-lg btn-success btn-md button_hw button_margin_right" onclick="StockDetailsReportAsPerProductNameValidate()" value="Search"/>
												     									
						</div>
					</div>	
				</div>		
						<table class= "table table-bordered table-striped table-condensed cf" id="productTable" class="display">
						<thead>
							<tr>
								<th>Product Name</th>
								<th>Company Name</th>
								<th>Packing</th>
								<th>Quantity</th>
								<th>Stock Available</th>
								<th>Unit</th>
								<!-- <th>Stock In Litre</th>
								<th>Total Quantities</th> -->
							</tr>
						</thead>
						<tfoot>
							<!-- <tr>
								  <th colspan="3" style="text-align: right">Total Quantity:</th>
								<th></th> 
							</tr> -->
						</tfoot>
					</table>
					<div class="row form-group buttons_margin_top ">
								<div align="center">
								<input type="button" id="btn" style="font-size: 25" class="btn btn-large btn-success button-height-width" name="btn" onclick="openBilling()" value="Back To Billing">	
								</div>
					</div>
				</fieldset>
				</form>
    	
    	</div>
    	
    	
		    	<!-------------------------------------------------- COMPANY NAME WISE------------------------------------------------------>
    	
    	<div id="companyName" class="tab-pane">
    	<div class="row"></div>
 		
 				<form class="form-horizontal" method="post" action="" name="fertiBill">
					<fieldset>
				         <div class="row form-group" style="margin-top: 20px">
								<label class="col-md-2 control-label" for="fk_godown_id">Select Company<sup>*</sup></label>  
           			 <div class="col-md-3">
						<div class="input-group">
						
							<span class="input-group-addon">
								<i class="	glyphicon glyphicon-hand-right"></i>
							</span>
							
							<%
							   CategoryDetailsDao cdDAO = new CategoryDetailsDao();
           						List companyName =cdDAO.getCompanyNames();
							
							%>
							<input list="company_drop" id="company_name" name="company_name"  class="form-control" >
				<datalist id="company_drop">
							<%
					           for(int i=0;i<companyName.size();i++){
					        	   Stock com = (Stock)companyName.get(i);
							%>
		
							<option data-value="<%=com.getCompanyName()%>" value="<%=com.getCompanyName()%>">
							<%
				      			}
				    		%>
			              	
            			</datalist>
            			</div>
            		</div> 
			
					  
				     <div class="col-md-3">
						<div class="input-group">
												  
								<input type="button"  id="btn" name="save" class="btn btn-lg btn-success btn-md button_hw button_margin_right" onclick="StockDetailsReportAsPerCompanyNameValidate()" value="Search"/>
												     									
						</div>
					</div>	
				</div>		
						<table class= "table table-bordered table-striped table-condensed cf" id="companyWiseTable" class="display">
						<thead>
							<tr>
								<th>Product Name</th>
								<th>Company Name</th>
								<th>Packing</th>
								<th>Quantity</th>
								<th>Stock Available</th>
								<th>Unit</th>
							<!-- 	<th>Stock In Litre</th>
								<th>Total Quantities</th> -->
							</tr>
						</thead>
						<tfoot>
							<!-- <tr>
								  <th colspan="3" style="text-align: right">Total Quantity:</th>
								<th></th> 
							</tr> -->
						</tfoot>
					</table>
					<div class="row form-group buttons_margin_top ">
								<div align="center">
								<input type="button" id="btn" style="font-size: 25" class="btn btn-large btn-success button-height-width" name="btn" onclick="openBilling()" value="Back To Billing">	
								</div>
					</div>
				</fieldset>
				</form>
    	
    	</div>
    	
		    <!----------------------------------------------------------- SHOP WISE ------------------------------------------------------------>
    	<div id="godown" class="tab-pane">
    	<div class="row"></div>
 		
 				<form class="form-horizontal" method="post" action="" name="fertiBill">
					<fieldset>
				         <div class="row form-group" style="margin-top: 20px">
								<label class="col-md-2 control-label" for="fk_godown_id">Shop Name<sup>*</sup></label>  
           			 <div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="	glyphicon glyphicon-hand-right"></i>
							</span>
              				
							<!-- Following code is to get categories from "categories" table of "fertilizer" DB -->
							<!-- getAllMainCat() is implemented in  CategoryDetailsDao with return type List-->
							<%
							shopDetailsDao cdd1 = new shopDetailsDao();
           						List cList1 =cdd1.getAllShop();
							
							%>
							<input list="Shop_drop" id="fk_shop_id" name="fk_shop_id"  class="form-control" >
				<datalist id="Shop_drop">
							<%
					           for(int i=0;i<cList1.size();i++){
					        	   shopDetailsBean sdb=(shopDetailsBean)cList1.get(i);
							%>
		
							<option data-value="<%=sdb.getShopId()%>" value="<%=sdb.getShopName()%>">
							<%
				      			}
				    		%>
			              	
            			</datalist>
            			</div>
            		</div> 
			
					  
				     <div class="col-md-3">
						<div class="input-group">
												  
								<input type="button"  id="btn" name="save" class="btn btn-lg btn-success btn-md button_hw button_margin_right" onclick="StockDetailsReportAsPerShopValidate()" value="Search"/>
												     									
						</div>
					</div>	
				</div>		
						<table class= "table table-bordered table-striped table-condensed cf" id="stockByGodown" class="display">
						<thead>
							<tr>
								<th>Product Name</th>
								<th>Company Name</th>
								<th>Packing</th>
								<th>Quantity</th>
								<th>Stock Available</th>
								<th>Unit</th>
								<!-- <th>Total Quantities</th> -->
							
							</tr>
						</thead>
						
					</table>
					<div class="row form-group buttons_margin_top ">
								<div align="center">
								<input type="button" id="btn" style="font-size: 25" class="btn btn-large btn-success button-height-width" name="btn" onclick="openBilling()" value="Billing">	
								</div>
					</div>
				</fieldset>
			</form>
    	</div>
    </div>
  </div>
</body>
<jsp:include page="commons/footer.jsp"></jsp:include>