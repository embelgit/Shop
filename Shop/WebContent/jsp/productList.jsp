
<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.Fertilizer.bean.GetProductDetails"%>
<%@page import="java.util.List"%>
  	<% boolean isHome=false;%>
	<%@include file="commons/header.jsp"%>
	
	<link href="/Shop/WebContent/staticContent/css/dataTa.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/Shop/staticContent/css/dataTables.jqueryui.min.css"  rel="stylesheet" type="text/css" media="all">
	<link rel="stylesheet" href="/Shop/staticContent/css/tabDemo.css">
 	<link rel="stylesheet" href="/Shop/staticContent/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.min.css">
    
    <script src="/Shop/staticContent/js/jquery.min.js"></script>
    <script src="/Shop/staticContent/js/jquery.jqgrid.min.js"></script>
	<script src="/Shop/staticContent/js/jquery.dataTables.js" type="text/javascript"></script>
	<script type="text/javascript" src="/Shop/staticContent/js/jqueryUi.js"></script>
	
	
	
<html>
	<head>

		<title>Suppliert List</title>
		
  		
  		<script type="text/javascript">
  			function Back()
  			{
  				window.location = "product_detail.jsp" ;
  			}
  			function deletProduct()
    		 {
    		 window.location = "DeletProduct.jsp";
    		 }
  			
  			
  		</script>
		

	</head>

	

	<script type="text/javascript"> 
		$(document).ready(function () 
		{
	         var table=$("#list").dataTable({


				    "bProcessing": true,
				    "sAutoWidth": false,
				    "bDestroy":true,
				    "sPaginationType": "bootstrap", // full_numbers
				    "iDisplayStart ": 10,
				    "iDisplayLength": 10,
				    "bPaginate": false, //hide pagination
				    //"bFilter": false, //hide Search bar
				    "bInfo": false, // hide showing entries
	    		         
	    		     });
	        
	         
			 var tableTools = new $.fn.dataTable.TableTools(table,{				 
				 'sSwfPath':'//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf',				 
				 	'aButtons':['copy','print','csv',
					 {
						 'sExtends':'xls',
						 'sFileName':'Data.xls',
						 'sButtonText': 'Save to Excel'
					 }
					],				
			});
				$(tableTools.fnContainer()).insertBefore('#list_wrapper');
		});
	</script>

<body id="dt_example">
		<div class="row" style="margin-top:70px">
				    <div align="center">
				  		<h2 class="form-name style_heading " >Product List</h2>
				  	</div>
				 	
			     <div class="row">
					     <div class="col-sm-offset-1 col-md-10">
							  		<hr style="border-top-color:#c1b1b1;">
					     </div>	
			   		 </div>
		</div>
			    
	<%
	ProductDetailsDao dao=new ProductDetailsDao();
	List list12=dao.getProductList();
	%>
	
	<div id="date">
		<label id="demo"></label>
		<script>
			var date = new Date();
			document.getElementById("demo").innerHTML = date.toDateString();
		</script>
	</div>

	<div id="demo_jui">
		<table id="list" class="display" border="1">
			<thead>
				<tr>
					<th>Product Name</th>
					<th>Category</th>
	                <th>Manufacturing Company</th>
	                 <th>Buy price</th> 
	                <th>M.R.P</th>  
	                <th>Tax Name</th>	                
					<th>Tax Percentage</th>
					<th>Unit Name</th>
					
					 <th>Sale Price</th>  
					<!-- <th>Weight</th> --> 
					 
					<!-- <th>Credit Customer Sale Price</th> -->
					
					
				</tr>
			</thead>
			
			<tbody>
   				<%
						for(int i=0;i<list12.size();i++){
						GetProductDetails sr=(GetProductDetails)list12.get(i);
						System.out.println(sr.getProduct()+" "+sr.getCat()+" "+sr.getManufacturer()+" "+sr.getTaxType()+" "+sr.getTaxPercentage()+" "+sr.getBuyPrice());
				%>
				
				<tr>
					<td class="align"><%=sr.getProduct()%></td>
					<td class="align"><%=sr.getCat()%></td>
					<td class="align"><%=sr.getManufacturer()%></td>
					  <td class="align"><%=sr.getBuyPrice()%></td> 
					<td class="align"><%=sr.getMrp()%></td>   
					<td class="align"><%=sr.getTaxType()%></td>
					<td class="align"><%=sr.getTaxPercentage()%></td>
					<td class="align"><%=sr.getUnitName()%></td>
				
					 <td class="align"><%=sr.getSalePrice()%></td>  
					<%-- <td class="align"><%=sr.getWeight()%></td>   --%>
					
					<%-- <td class="align"><%=sr.getCreditSalePrice()%></td> --%>
				</tr>
	
				<%
					}
				%>
			</tbody>
		</table>
	</div>
	
	<div class="wrapper" align="center">
		<input type="button" style="width: 100px; height: 65px; font-size: 25px" value="Back" id="listBtn" class="btn btn-primary" onclick="Back()" /> 
		<input type="button" style="width: 200px; height: 65px; font-size: 25px" value="Delete Product" id="listBtn2" class="btn btn-large btn-danger button-height-width" onclick="deletProduct()" />
	</div>
	
</body>
<jsp:include page="commons/footer.jsp"></jsp:include>
</html>

