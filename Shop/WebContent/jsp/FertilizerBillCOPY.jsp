<%@page import="com.Fertilizer.bean.GetBillDetails"%>
<%@page import="com.Fertilizer.bean.CreditCustBillNoAndName"%>
<%@page import="com.Fertilizer.dao.FertilizerBillDao"%>

<% boolean isHome=false;%>
<%@include file="commons/header.jsp"%>

<script src="/Shop/staticContent/js/generateSeedAndPesticidePDF.js"></script>

<script src="/Shop/staticContent/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/selectjj.js"></script>
<script type="text/javascript" src="/Shop/staticContent/js/buttom.js"></script>
<script src="/Shop/staticContent/js/jquery.min.js"></script>
<script src="/Shop/staticContent/js/jquery.jqgrid.min.js"></script>
<script src="/Shop/staticContent/js/jquery.dataTables.js" type="text/javascript"></script>
<script type="text/javascript" src="/Shop/staticContent/js/jqueryUi.js"></script>

<link href="/Shop/WebContent/staticContent/css/dataTa.css" rel="stylesheet" type="text/css" media="all" />
<link href="/Shop/staticContent/css/dataTables.jqueryui.min.css" rel="stylesheet" type="text/css" media="all">
<link href="/Shop/staticContent/css/select.css" rel="stylesheet" type="text/css" media="all">
<link href="/Shop/staticContent/css/button.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="/Shop/staticContent/css/jquery-ui.min.css">
<link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.min.css">

<!--  <div class="container" style="float: left" align="center">  -->

 <div class="container" style="float: none"> 
 		
 		<div class="row" align="center">
			<div style="margin-top: 75px; float: none;">
				  <!-- <h2 class="form-name style_heading" style="float: none;" align="center">Customer Bill Copy</h2> -->
				  <h2 class="form-name style_heading" align="center">Customer Bill Copy</h2>
			</div>
				 	
			 <div class="row">
				<div class="col-sm-offset-2 col-md-10">
						<hr style="border-top-color:#c1b1b1;">
				</div>	
			  </div>
		</div>
		
		
		
			 <ul class="nav nav-tabs" >
			 	<li class="active"><a data-toggle="tab" href="#NomCust"><h4 style="color:blue">Normal Customer Bill Copy</h4></a></li>
	    		<li><a data-toggle="tab" href="#creditCustomer"><h4 style="color:blue">Credit Customer Bill Copy</h4></a></li>
 	 		</ul>
 	 		
 	 		<!-- <div class="tab-content" style="float: left"> -->
 	 		
 	 		<div class="tab-content" style="float: none;" align="center">
 	 		
 	 		
 	 		
 	 		<!-- Normal Cutomer Bill COPY -->
 	 		
 	 			<div id="NomCust" class="tab-pane fade in active" align="center">
 					<form action="" method="post" name="genIn">
 						<%
							FertilizerBillDao fd = new FertilizerBillDao();
							List list = fd.getNormalCustFertilizerBillNos();
						%>
						<div class="row" style="margin-top: 25px;" align="center">
							<div class="col-md-offset-3">
								<div class="col-md-2">
									<label class="control-label"> Bill Number:</label> 
								</div>	
								<div class="col-md-4">
									<input list="seedBillNo" id="BillNo" class="form-control">
									<datalist id="seedBillNo">
									<%
					               		for(int i=0;i<list.size();i++){
					               			GetBillDetails billList=(GetBillDetails)list.get(i);
									%>
									<option data-value="<%=billList.getBillNo()%>" value="<%=billList.getBillNo()%>    <%=billList.getClientName()%>" >
									<%
										}
									%>
									</datalist>
									
								 </div>		
								 <div class="col-md-3" style="/* margin-top: 25px; *//* height: 69px; */" align="center">
										<button type="button" onclick="normalCustFertilzerBillCOPYValidate()" name="btn" id="btn" class="btn btn-success" style="width: 100px;height: 35px;font-weight: bold;">Print</button>
									</div>				
								
							</div>
							
						</div>
 					</form>
 				</div>
 				
 				<!-- Credit Cutomer Bill COPY -->
 	 		
 	 			
 	 			<div id="creditCustomer" class="tab-pane">
 					<form action="" method="post" name="genIn">
 						<%
							FertilizerBillDao ccfd = new FertilizerBillDao();
							List cclist = ccfd.getCreditCustFertilizerBillNos();
						%> 
						<div class="row" style="margin-top: 25px;">
							<div class="col-md-offset-3">
								<div class="col-md-2">
									<label class="control-label"> Bill Number:</label> 
								</div>	
								<div class="col-md-4">
									<input list="creditCustBillNo" id="CreditBillNo" class="form-control">
									<datalist id="creditCustBillNo">
									 <%
					               		for(int i=0;i<cclist.size();i++){
					               			GetBillDetails ccbillList=(GetBillDetails)cclist.get(i);
									%> 
									<option data-value="<%=ccbillList.getBillNo()%>" value="<%=ccbillList.getBillNo()%>   <%=ccbillList.getClientName()%>" >
									<%
										}
									%> 
									</datalist>
								 </div>
								 									
									 <div class="col-md-3" style="/* margin-top: 25px; *//* height: 69px; */" align="center">
										<button type="button" onclick="creditCustFertilzerBillCOPYValidate()" name="btn" id="btn" class="btn btn-success" style="width: 100px;height: 35px;font-weight: bold;">Print</button>
									</div>	
									
								</div>
							</div>
						</div>
 					</form>
 				</div> 	
 				<br/>
 				<hr style="color: black"/>
 				<jsp:include page="commons/footer.jsp"></jsp:include> 		
 	 		</div>
 	  </div>
 	  
 	  

 	  