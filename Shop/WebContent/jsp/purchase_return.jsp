 <%@page import="com.Fertilizer.dao.GoodsReceiveDao"%>
 <%@page import="com.Fertilizer.hibernate.GoodsReceiveBean"%>
 <%@page import="java.util.List" %>
<%@page import="com.Fertilizer.hibernate.SupplierDetailsBean"%>
<%@page import="com.Fertilizer.dao.SupplierDetailsDao"%>

<%boolean isHome = false;%>
<%@include file="commons/header.jsp"%>
<head>
<meta charset="utf-8">

	<!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="Shop/staticContent/js/jquery.min.js"></script> 
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="Shop/staticContent/js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="Shop/staticContent/js/grid.locale-en.js"></script>
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="/Shop/staticContent/css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="/Shop/staticContent/css/trirand/ui.jqgrid.css" />
    <meta charset="utf-8" />

     <script type="text/javascript" src="/Shop/staticContent/js/jquery-1.12.3.min.js"></script>
	 <link rel="stylesheet" href="/Shop/staticContent/css/jquery-ui.min.css">
     <link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.min.css">
    
     
     <link rel="stylesheet" href="/Shop/staticContent/y_css/jquery-ui.css">
     <link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.css">
     <script type="text/javascript" src="/Shop/staticContent/js/jquery.min.js"></script>
     <script type="text/javascript" src="/Shop/staticContent/js/jquery-ui-min.js"></script>
     <script type="text/javascript" src="/Shop/staticContent/js/jquery-ui.js"></script>
     <script type="text/javascript" src="/Shop/staticContent/js/jqueryUi.js"></script>
     <script type="text/javascript" src="/Shop/staticContent/js/jquery.jqgrid.min.js"></script>
     
     
    
     <script type="text/javascript" src="/Shop/staticContent/js/purchaseReturn.js"></script>
     
     
     <script type="text/javascript">
     function cancle(){
    		location.reload()

    		}
     
     $(document).on('change', 'input', function(){
    	 getAllBills();
    	});
     
     $(document).on('change', 'select', function(){
    	 fetchDataForPurchase();
    	});
   /*   
     $( "#bill_no" ).change(function() {
    	 fetchDataForPurchase();
    	});
      */
     </script>
    
</head>
 <div class="row header_margin_top">
			    <div align="center">
			  		<h2 class="form-name style_heading">Purchase Return</h2>
			  	</div>
			 
    </div>
     <div class="row">
		     <div class="col-sm-offset-1 col-md-10">
				  		<hr style="border-top-color:#c1b1b1;">
		     </div>	
    </div>
    
    <form class="form-horizontal" method="post" action="">
          <fieldset>
   <div class="container" >
			 	
				<div class="row form-group">
           				 <label class="col-md-3 control-label" for="supplier">Supplier <sup>*</sup></label>  
          					  <div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="	glyphicon glyphicon-hand-right"></i>
									</span>
									
							<!-- Following code is to get Supplier from "supplier_details" table of "fertilizer" DB -->
							<!-- getAllSupllier() is implemented in  SupplierDetailsDao with return type List-->
						
							<%
								SupplierDetailsDao sdd = new SupplierDetailsDao();
           						List sList =sdd.getAllSupplier();
							
							%>
							
							<input list="sup_drop" id="supplier" placeholder="Supplier Name" name="supplier" required="required" class="form-control">
				            <datalist id="sup_drop">
							
							<%
					           for(int i=0;i<sList.size();i++){
					        	   SupplierDetailsBean sup =(SupplierDetailsBean)sList.get(i);
							%>
		
							<option data-value="<%=sup.getSupId()%>" value="<%=sup.getDealerName() %>">
							<%
				      			}
				    		%>
						</datalist>           	
					</div>
           		</div>
            
           				    <label class="col-md-2 control-label" for="bill_no"> Bill No<sup>*</sup> </label>  
          					  <div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										No
									</span>
           		 					
           		 					<select class="form-control input-md" placeholder="Bill Number" required="required" id="bill_no" name="bill_no"></select>
           						 </div>
							</div>
						  </div> 
	
        </div>
             <div class="row form-group " style="padding-left: 50px;">
           		<table id="jqGrid" ></table>
				<div id="jqGridPager"></div>
            </div>
             
            <div class="form-group row" style="margin-left: 8%">
            	<div class="col-md-10 text-center">
            		<!-- <input type="button" id="btn" style="font-size: 25" class="btn btn-large btn-success button-height-width" name="btn"
            		onclick=" returntPurchase(); returntMinusFromStockPurchase();" value="Submit"> -->
            		
            		<!-- <input type="button" id="btn" style="font-size: 25" class="btn btn-large btn-success button-height-width" name="btn"
            		onclick=" returnPurchaseValidate(); returntMinusFromStockPurchase();" value="Submit"> -->
            		
            		<input type="button" id="btn" style="font-size: 25" class="btn btn-large btn-success button-height-width" name="btn"
            		onclick=" purchaseReturnTable(); returntPurchase(); returntMinusFromStockPurchase();" value="Submit">
            		
		            <input type="button" id="btn1" style="font-size: 25" class="btn btn-large btn-danger   button-height-width" name="btn1" value="Cancel" onclick="cancle()">
           		</div>
          	</div>
		</fieldset>
       </form>
    
     
<jsp:include page="commons/footer.jsp"></jsp:include>