<%@page import="com.Fertilizer.dao.PesticideBillDao"%>
<%@page import="com.Fertilizer.hibernate.PesticideBillBean"%>
<%@page import="com.Fertilizer.hibernate.SeedPesticideBillBean"%>
<%@page import="com.Fertilizer.dao.SeedPesticideBillDAO"%>
<%@page import="com.Fertilizer.dao.FertilizerBillDao"%>
 <%@page import="com.Fertilizer.hibernate.FertilizerBillBean"%>
 <%@page import="com.Fertilizer.dao.CustomerDetailsDao"%>
 <%@page import="com.Fertilizer.hibernate.CustomerDetailsBean"%>
 
 <%@page import="java.util.List" %>


<% boolean isHome=false;%>
<%@include file="commons/header.jsp"%>
<head>
<meta charset="utf-8">

       <script type="text/javascript" src="/Shop/staticContent/js/jquery-1.12.3.min.js"></script>
     <!--  <script type="text/javascript" src="/Shop/staticContent/js/jquery-1.11.1.min.js"></script> -->
	 <link rel="stylesheet" href="/Shop/staticContent/css/jquery-ui.min.css">
     <link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.min.css">
    
     
     <link rel="stylesheet" href="/Shop/staticContent/y_css/jquery-ui.css">
     <link rel="stylesheet" href="/Shop/staticContent/css/ui.jqgrid.css">
     <script type="text/javascript" src="/Shop/staticContent/js/jquery.min.js"></script>
     <!-- <script type="text/javascript" src="/Shop/staticContent/js/jquery-ui-min.js"></script> -->
     
      <script type="text/javascript" src="/Shop/staticContent/js/jquery-ui.min.js"></script>
     <script type="text/javascript" src="/Shop/staticContent/js/jquery-ui.js"></script>
     <script type="text/javascript" src="/Shop/staticContent/js/jqueryUi.js"></script>
     <script type="text/javascript" src="/Shop/staticContent/js/jquery.jqgrid.min.js"></script>
    
     <script type="text/javascript" src="/Shop/staticContent/js/fertiSaleReturn.js"></script>
     
     
     
     <script type="text/javascript">
     function cancle(){
 		location.reload()

 		}
     
      function pageLoad(){	
    		$("#CashCustDetail").show();
    		$("#CreditCustDetail").hide(); 	
    }
     
    /*  $('#bill_no').on('change','input', function() { 
    	 fetchDataForSale();  // get the current value of the input field.
    	}); */
     
     function openCashCustomerBilling() {
    		$("#CashCustDetail").show();
    		$("#CreditCustDetail").hide();
    		location.reload();
    	}
     
     function openCreditCustomerBilling() {
    		$("#CreditCustDetail").show();
    		$("#CashCustDetail").hide();
    	}
     
     
     </script>
</head>
	<body onload="pageLoad();">
	  <div class="row header_margin_top">
				    <div align="center">
				  		<h2 class="form-name style_heading">Sale Return</h2>
				  	</div>
			 
    </div>
     <div class="row">
		     <div class="col-sm-offset-1 col-md-10">
				  		<hr style="border-top-color:#c1b1b1;">
		     </div>	
    </div>
    <!-- <ul class="nav nav-tabs col-md-offset-1">
	    <li class="active"><a data-toggle="tab" href="#home"><h4 style="color:blue">Fertilizer</h4></a></li>
	    <li><a data-toggle="tab" href="#twoDates"><h4 style="color:blue">Seed</h4></a></li>
	     <li><a data-toggle="tab" href="#pro"><h4 style="color:blue">Pesticide</h4></a></li>
 	 </ul> -->
  	<div style="float: none;"></div>

<!-- Fertilizer sale return -->
	<div id="home">
  		 <form class="form-horizontal" method="post" action="">
         	 <fieldset>
         	 <div class="container col-sm-offset-2" >
                 <div class="row form-group">
           	 		<div class="col-md-6">
              			<%@include file="commons/clock.jsp" %>
           		 	</div>
				</div>
				
				 <div class="row form-group">
         		<div class="col-md-3 control-label">
         		<label  for="customertype" style="float:right">Customer Type<sup>*</sup></label>
         		</div>	
         			<div class="col-md-3">
						<div class="col-xs-6 ">									
						<input type="radio" name="customertype" id="customertype" checked="checked" onclick="openCashCustomerBilling()" 
						style=" width:  20px; height:  20px; ">
							<label class="radio-inline" style=" MARGIN-TOP: -15PX; FONT-WEIGHT: 700;padding: 0px;">
							Cash
							</label>
						</div>	
      					<div class="col-xs-6 col-md-ffset-1 ">	
						<input  type="radio" name="customertype" id="customertype" onclick="openCreditCustomerBilling()"
						style=" width:  20px; height:  20px;">
							<label class="radio-inline" style=" MARGIN-TOP: -15PX; FONT-WEIGHT: 700;padding: 0px;">
								Credit
							</label>									     				
						</div>
              		</div>
           </div>
           
           </div>
           </fieldset>
           </form>
           </div>
           
				<!---------------------- cash customer------------- -->
				<div id="CashCustDetail">
  
     <form class="form-horizontal" method="post" action="" name="fertiBill">
			<fieldset>
				
			 <div class="row form-group">
			<label class="col-md-3 control-label" for="customerName">Customer Name<sup>*</sup></label>  
           			 <div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-user"></i>
							</span>
           		 			  <input id="customerName" name="customerName" placeholder="Customer Name" class="form-control input-md" type="text" >
           		 		</div>
					</div>	
				
				
				
                 <div class="row form-group">
           			 <label class="col-md-2 control-label" for="billNo">Bill No</label>  
            			<div class="col-md-3">
							<div class="input-group " ">
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-hand-right"></i>
								</span>
								
								<%
								  FertilizerBillDao sdd = new FertilizerBillDao();
           			              List pList = sdd.getAllBillNoOnSaleReturn();
							
				                 %>
              					<input  list="bill_no_drop"  id="bill_no" class="form-control" onchange="fetchDataForSale()" >
				                <datalist id="bill_no_drop" style="overflow-x: hidden; overflow: scroll; width: 100%; height:500px">
							
							   <%
					               for(int i=0;i<pList.size();i++){
					            	FertilizerBillBean sup =(FertilizerBillBean)pList.get(i);
							    %>
		
							      <option data-value="<%=sup.getBillNo()%>" value="<%=sup.getBillNo() %> " >
							    <%
				      			  }
				    		     %>
						</datalist> 
            				</div>
            			</div>
         		  </div>
         		  
         		   <div class="row form-group" style="padding-left: 50px;" align="center">
           		<table id="jqGrid" ></table>
				<div id="jqGridPager"></div>
            </div>
         		  
         		  <div class="form-group row">
            		<div class="col-md-10 text-center" style="margin-left: 8%">
              			<!-- <button id="save" name="save" class="btn btn-large btn-success" onclick="saleReturn()"> Submit</button>
              			<button class="btn btn-large btn-danger" type="reset"> Cancel </button>
              			 -->
              			<input type="button" id="save" name="save" style="font-size: 25" class="btn btn-large btn-success button-height-width"
              			onclick="saleReturn()" value="Submit">
		            <input type="button" id="btn1" style="font-size: 25" class="btn btn-large btn-danger   button-height-width" name="btn1" value="Cancel" onclick="cancle()">
          	  		</div>
         		 </div>       
         		  
         		  
         		  </div> 
         		  </fieldset>
         		  </form>
         		  </div>
         		  
         		  
         		 
         		  
         		  
         	<!-- ----------------Credit customer Name---------------------- -->	  
         		  <div id="CreditCustDetail">
       	<form class="form-horizontal" method="post" action="" name="creditFertiBill1">
			<fieldset>
         		  
         		  <div class="row form-group">
			<label class="col-md-3 control-label" for="creditCustomerName"> Customer Name<sup>*</sup></label>  
           			 <div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-user"></i>
							</span> 
			
										
							<!-- Following code is to get customers from "customer_details" table of "fertilizer" DB -->
							<!-- getAllCustomer() is implemented in  CustomerDetailsDao with return type List-->
						
							 <%
							CustomerDetailsDao dao = new CustomerDetailsDao();
           						List cust =dao.getAllCustomer();
							
							%>
							
							<input type="text" id="creditCustomer" list="cust_drop1" class="form-control" onchange="getAllBill()">
				<datalist id="cust_drop1">
							
							<%
					           for(int i=0;i<cust.size();i++){
					        	   CustomerDetailsBean bean =(CustomerDetailsBean)cust.get(i);
							%>
							<option data-value="<%=bean.getCustId()%>"><%=bean.getFirstName() %> <%=bean.getLastName() %> </option>
							<%-- <option data-value="<%=bean.getCustId()%>"><%=bean.getFirstName()%></option> --%>
							<%
				      			}
				    		%> 
						</datalist>
						
						</div>
					</div>
         		  
         		  
         		   <div class="row form-group">
           			 <label class="col-md-2 control-label" for="billNo">Bill No</label>  
            			<div class="col-md-3">
							<div class="input-group " ">
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-hand-right"></i>
								</span>
								
								<%-- <%
								  FertilizerBillDao sdd1 = new FertilizerBillDao();
           			              List pList1 = sdd1.getAllBillNoOnSaleReturn();
							
				                 %> --%>
              					<input  list="creditBillNoDrop"  id="creditBillNo" class="form-control" onchange="fetchDataForSaleCredit()" >
				                <datalist id="creditBillNoDrop" style="overflow-x: hidden; overflow: scroll; width: 100%; height:500px">
							
							 <%--   <%
					               for(int i=0;i<pList1.size();i++){
					            	FertilizerBillBean sup =(FertilizerBillBean)pList1.get(i);
							    %>
		
							      <option data-value="<%=sup.getBillNo()%>" value="<%=sup.getBillNo() %> " >
							    <%
				      			  }
				    		     %> --%>
						</datalist> 
            				</div>
            			</div>
         		  </div>
         		
         		  
           <div class="row form-group" style="padding-left: 50px;" align="center">
           		<table id="jqGridCredit" ></table>
				<div id="jqGridPagerCredit"></div>
            </div>
           		<div class="form-group row">
            		<div class="col-md-10 text-center" style="margin-left: 8%">
              			<!-- <button id="save" name="save" class="btn btn-large btn-success" onclick="saleReturn()"> Submit</button>
              			<button class="btn btn-large btn-danger" type="reset"> Cancel </button>
              			 -->
              			<input type="button" id="save" name="save" style="font-size: 25" class="btn btn-large btn-success button-height-width"
              			onclick="saleReturn()" value="Submit">
		            <input type="button" id="btn1" style="font-size: 25" class="btn btn-large btn-danger   button-height-width" name="btn1" value="Cancel" onclick="cancle()">
          	  		</div>
         		 </div>       	    
         	 </fieldset>
          </form>
          </div>	
	 
<%-- <!-- 	 Seed return -->
	 <div id="twoDates" class="tab-pane ">
	  <form class="form-horizontal" method="post" action="">
         	 <fieldset>
         	 <div class="container col-sm-offset-2" >
                 <div class="row form-group">
           	 		<div class="col-md-6">
              			<%@include file="commons/clock.jsp" %>
           		 	</div>
				</div>
                 <div class="row form-group">
           			 <label class="col-md-2 control-label" for="seedBillNo">Bill No</label>  
            			<div class="col-md-3">
							<div class="input-group " style="overflow-x:hidden; overflow:scroll; ">
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-hand-right"></i>
								</span>
								
								<%
								SeedPesticideBillDAO seedDao = new SeedPesticideBillDAO();
           			              List seedList = seedDao.getAllSeedBillNoForSaleReturn();
							
				                 %>
              					<input list="seedBillNo_drop" id="seedBillNo" class="form-control" onchange="fetchSeedDataForSale()">
				                <datalist id="seedBillNo_drop">
							
							   <%
					               for(int i=0;i<seedList.size();i++){
					            	   SeedPesticideBillBean seedBean =(SeedPesticideBillBean)seedList.get(i);
							    %>
		
							      <option data-value="<%=seedBean.getBillNo()%>" value="<%=seedBean.getBillNo() %>">
							    <%
				      			  }
				    		     %>
						</datalist> 
            				</div>
            			</div>
         		 </div> 
         		  </div> 
           <div class="row form-group" style="padding-left: 50px;">
           		<table id="jqGridSeed" ></table>
				<div id="jqGridPagerSeed"></div>
            </div>
           		<div class="form-group row">
            		<div class="col-md-10 text-center">
              			<!-- <button id="save" name="save" class="btn btn-large btn-success" onclick="saleReturn()"> Submit</button>
              			<button class="btn btn-large btn-danger" type="reset"> Cancel </button>
              			 -->
              			<input type="button" id="save" name="save" style="font-size: 25" class="btn btn-large btn-success button-height-width"  onclick="seedReturn()" value="Submit">
		            <input type="reset" id="btn1" style="font-size: 25" class="btn btn-large btn-danger   button-height-width" name="btn1" value="Cancel">
          	  		</div>
         		 </div>       	    
         	 </fieldset>
          </form>
	 
	</div>
	
	<!-- 	 Pesticide return -->
	 <div id="pro" class="tab-pane ">
	  <form class="form-horizontal" method="post" action="">
         	 <fieldset>
         	 <div class="container col-sm-offset-2" >
                 <div class="row form-group">
           	 		<div class="col-md-6">
              			<%@include file="commons/clock.jsp" %>
           		 	</div>
				</div>
                 <div class="row form-group">
           			 <label class="col-md-2 control-label" for="pestiBillNo">Bill No</label>  
            			<div class="col-md-3">
							<div class="input-group " style="overflow-x:hidden; overflow:scroll; ">
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-hand-right"></i>
								</span>
								
								<%
								PesticideBillDao pestiDao = new PesticideBillDao();
           			              List pestiList = pestiDao.getAllPestiBillNoForSaleReturn();
							
				                 %>
              					<input list="pestiBillNo_drop" id="pestiBillNo" class="form-control" onchange="fetchPesticideDataForSale()">
				                <datalist id="pestiBillNo_drop">
							
							   <%
					               for(int i=0;i<pestiList.size();i++){
					            	   PesticideBillBean pestiBean =(PesticideBillBean)pestiList.get(i);
							    %>
		
							      <option data-value="<%=pestiBean.getBillNo()%>" value="<%=pestiBean.getBillNo() %>">
							    <%
				      			  }
				    		     %>
						</datalist> 
            				</div>
            			</div>
         		 </div> 
         		  </div> 
           <div class="row form-group" style="padding-left: 50px;">
           		<table id="jqGridPesti" ></table>
				<div id="jqGridPagerPesti"></div>
            </div>
           		<div class="form-group row">
            		<div class="col-md-10 text-center">
              			<!-- <button id="save" name="save" class="btn btn-large btn-success" onclick="saleReturn()"> Submit</button>
              			<button class="btn btn-large btn-danger" type="reset"> Cancel </button>
              			 -->
              			<input type="button" id="save" name="save" style="font-size: 25" class="btn btn-large btn-success button-height-width"  onclick="pesticideReturn()" value="Submit">
		            <input type="reset" id="btn1" style="font-size: 25" class="btn btn-large btn-danger   button-height-width" name="btn1" value="Cancel">
          	  		</div>
         		 </div>       	    
         	 </fieldset>
          </form>
	 
	</div>  --%>
	
	</body>
	</div>
<jsp:include page="commons/footer.jsp"></jsp:include>