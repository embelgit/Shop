package com.Fertilizer.helper;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jfree.util.Log;

import com.Fertilizer.bean.BillBean;
import com.Fertilizer.bean.CustomerBean;
import com.Fertilizer.bean.GetPODetailsForDcUpdate;
import com.Fertilizer.bean.GetPODetailsForGoodsReceive;
import com.Fertilizer.bean.GetProductDetails;
import com.Fertilizer.bean.GetPurchaseProduct;
import com.Fertilizer.bean.ProfitAndLoss;
import com.Fertilizer.bean.PurchaseDetailsFromGoodsReceive;
import com.Fertilizer.bean.PurchaseStockMinus;
import com.Fertilizer.bean.SaleReports;
import com.Fertilizer.bean.StockDetail;
import com.Fertilizer.dao.CustomerDetailsDao;
import com.Fertilizer.dao.GoodsReceiveDao;
import com.Fertilizer.dao.ProductDetailsDao;
import com.Fertilizer.dao.StockDao;
import com.Fertilizer.hibernate.FertilizerBillBean;
import com.Fertilizer.hibernate.GoodsReceiveBean;
import com.Fertilizer.hibernate.ProductDetailsBean;
import com.Fertilizer.hibernate.Stock;
import com.Fertilizer.hibernate.SubCategoryDetailsBean;
import com.Fertilizer.utility.HibernateUtility;
import com.itextpdf.text.log.SysoLogger;

public class GoodsReceiveHelper
{		
	Long barcodeNo;
	String catName;
	String productName;
	String companyName;
	Long PkStockId;
	
	//Double quantityFromStockTable;
	//Double quantityFromStockTable;
	Double interGST;
	Double taxPercentage;
	public void goodsReceived(HttpServletRequest request,
			HttpServletResponse response) throws ParseException {
		
		String extraExpenseGSTperc = request.getParameter("extraExpenseGSTperc");
		String totalTransportExpense = request.getParameter("totalTransportExpense");
		String totalhamaliExpence = request.getParameter("totalhamaliExpence");
		
		System.out.println(extraExpenseGSTperc+" ******************** ");
		System.out.println(totalTransportExpense+" ******************** ");
		System.out.println(totalhamaliExpence+" ******************** ");
		
		if(extraExpenseGSTperc == null || extraExpenseGSTperc.equals("") || extraExpenseGSTperc.isEmpty())
		{
			extraExpenseGSTperc = "0";
		}
		if(totalTransportExpense == null || totalTransportExpense.equals("") || totalTransportExpense.isEmpty())
		{
			totalTransportExpense = "0.0";
		}
		if(totalhamaliExpence == null || totalhamaliExpence.equals("") || totalhamaliExpence.isEmpty())
		{
			totalhamaliExpence = "0.0";
		}
		
		System.out.println("IN HELPER");
		GoodsReceiveDao dao = new GoodsReceiveDao();
		
		Integer count = Integer.parseInt(request.getParameter("count"));
		System.out.println(count);
		
		GoodsReceiveBean bean = new GoodsReceiveBean();
		
	for(int i =0 ; i<count;i++)
	{
		HttpSession session3 = request.getSession();
		GoodsReceiveDao data = new GoodsReceiveDao();
		List stkList  = data.getLastBarcodeNo();
		
		for(int j=0;j<stkList.size();j++)
		{			
			BillBean st = (BillBean)stkList.get(j);
			barcodeNo = st.getBarcodeNo();
			barcodeNo++;
		}
		
			String catIDforVAt = request.getParameter("catIDforVAt"+i);
			System.out.println("catIDforVAt @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ====> "+catIDforVAt);
			String supplier  = request.getParameter("supplier");
			//System.out.println("GoodRecieveHelper good receive supplier fk id value "+supplier);
			String fk_cat_id  = request.getParameter("fk_cat_id");
			//String fk_subCat_id  = request.getParameter("fk_subCat_id");
			String fk_subCat_id  = request.getParameter("subCatId"+i);
			//System.out.println("SUB CAT ID ::::::::::::::::::===== "+fk_subCat_id);
			String fk_shop_id  = request.getParameter("fk_shop_id");
			catName = request.getParameter("catName");
			String unitName = request.getParameter("unitName"+i);
			System.out.println("**** GOOD RECIEVE UNITNAME **** ===============> "+unitName);
			String transExpence  = request.getParameter("transExpence");
			/*String dueDate = request.getParameter("dueDate");*/
			String total = request.getParameter("total");
			/*String fk_godown_id = request.getParameter("fk_godown_id");*/
			String purchaseDate = request.getParameter("purchaseDate");
			String productID = request.getParameter("productID"+i);
			String mrp = request.getParameter("mrp"+i);
			//System.out.println("MRP ========== from grid goodReceiveHelper ln=102 =======> "+mrp);
			String gst = request.getParameter("gst"+i);
			String igst = request.getParameter("igst"+i);
			String expiryDate = request.getParameter("expiryDate"+i);			
			
			if(igst == null || igst.equalsIgnoreCase("") || igst.isEmpty())
			{
				igst = "0.0";			
			}
			
			System.out.println("GST == "+gst+"\n"+"IGST == "+igst);
			if(gst == null || gst.equalsIgnoreCase("") || gst.isEmpty())
			{
				gst = "0.0";
			}
			if(igst == null || igst.equalsIgnoreCase("") || igst.isEmpty())
			{
				igst = "0.0";			
			}
			System.out.println("IN HELPER gst=="+gst);
			System.out.println("IN HELPER igst=="+igst);
			String unit = request.getParameter("unitName"+i);
						
			//System.out.println(productID+"poID");
		if (productID == null) {	
			break;
		}
		else {
			bean.setPkPOId(Long.parseLong(productID));
		}
		
		companyName = request.getParameter("companyName"+i);
		if(companyName==null){
			break;
		}
		else{
			bean.setCompanyName(companyName);
		}
		
		productName = request.getParameter("productName"+i);
		if(productName==null){
			break;
		}
		else{
			bean.setProductName(productName);
		}
		
		String buyPrice = request.getParameter("buyPrice"+i);
		if(buyPrice==null){
			break;
		}
		else{
			bean.setBuyPrice(Double.parseDouble(buyPrice));
		}
		
		String salePrice = request.getParameter("salePrice"+i);		
		//System.out.println("Sale Price from grid == GoodsReceiveHelper ln 142 ===> "+salePrice);
		if(salePrice == null || salePrice == "" || salePrice.isEmpty()){
			salePrice = "0";
		}
		else{
			bean.setSalePrice(Double.parseDouble(salePrice));
		}
		System.out.println("SALE PRICE =================== "+salePrice);
		
		String weight = request.getParameter("weight"+i);
		//System.out.println("weight = = = =>"+weight);
		if(weight==null){
			break;
		}
		else{
			bean.setWeight(Double.parseDouble(weight));
		}
		
		String quantity = request.getParameter("quantity"+i);
		
		System.out.println("quantity = = = =>"+quantity);
		if(quantity==null){
			break;
		}
		else{
			bean.setQuantity(Double.parseDouble(quantity));
			bean.setDupQuantity(Double.parseDouble(quantity));
		}
		
			/*
			 * String dcNum = request.getParameter("dcNum"+i); if(dcNum==null ||
			 * dcNum.equalsIgnoreCase("")){ bean.setDcNum(0l); } else{
			 * bean.setDcNum(Long.parseLong(dcNum)); }
			 */

		String weightAftShortMinus = request.getParameter("weightAftShortMinus"+i);
		if(weightAftShortMinus==null){
			bean.setWeightAftShortMinus(0.0);
		}
		else{
			bean.setWeightAftShortMinus(Double.parseDouble(weightAftShortMinus));
		}
		
		//System.out.println("weightAftShortMinus = = = ="+weightAftShortMinus);
		
		String batchNo = request.getParameter("batchNo"+i);
		if(batchNo.isEmpty() || batchNo.equalsIgnoreCase("") || batchNo == null)
		{
			bean.setBatchNo("N/A");
		}
		else
		{
			bean.setBatchNo(batchNo);
		}
		
		if(expiryDate == null || expiryDate.equalsIgnoreCase("") || expiryDate.isEmpty())
		{
			if(weightAftShortMinus != null || weightAftShortMinus.isEmpty() || weightAftShortMinus.equalsIgnoreCase(""))
			{
				bean.setStockPerEntry(1.0);
			}
		}
		else
		{
			if(weightAftShortMinus != null || weightAftShortMinus.isEmpty() || weightAftShortMinus.equalsIgnoreCase(""))
			{
				bean.setStockPerEntry(Double.parseDouble(quantity)*Double.parseDouble(weight));
			}
		}
				
		String discount = request.getParameter("discount");
		if(discount == "" || discount.isEmpty() || discount == null)
		{
			discount = "0";
		}
		
		String discountAmount = request.getParameter("discountAmount");
		if(discountAmount == "" || discountAmount.isEmpty() || discountAmount == null)
		{
			discountAmount = "0";
		}
		
		String billNum = request.getParameter("billNum");		
		
		Date date = new Date();
		bean.setInsertDate(date);		
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date newDate = null;
		try {
				newDate = format.parse(purchaseDate);
			} 
		catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		bean.setPurchaseDate(newDate);
		
		if(expiryDate == null || expiryDate.equalsIgnoreCase("") || expiryDate.isEmpty())
		{
			bean.setExpiryDate(null);
		}
		else
		{
			Date newExpiryDate = null;
			System.out.println(expiryDate);
			try {
				 String expiryDate1 = expiryDate;
				 System.out.println(expiryDate1);
				 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				 
				 newExpiryDate = df.parse(expiryDate1);
				 DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
				 System.out.println(df1.format(newExpiryDate));
				 Date checkDate = newExpiryDate;
				 System.out.println("STRING CONVERTED TO DATE ==============!@@!#!$@#!@!!$@@$!#@$@@# "+checkDate);
				} 
			catch (ParseException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			bean.setExpiryDate(newExpiryDate);
		}		
		
		if(mrp == null){
			bean.setMrp(0.0);
		}else{
			bean.setMrp(Double.parseDouble(mrp));
		}
		
		Double tax = Double.parseDouble(gst);
		if(tax == null || tax == 0.0)
		{
			bean.setTaxPercentage(0.0);
		}
		else
		{
			bean.setTaxPercentage(tax);
		}
		
		Double igstx=Double.parseDouble(igst);
		if(igstx == null || igstx == 0.0){
			bean.setiGstPercentage(0.0);
		}
		else{
			bean.setiGstPercentage(igstx);
		}
		
		
		String gross = request.getParameter("grossTotal");
		if(gross != null){
			bean.setGrossTotal(Double.parseDouble(gross));
			bean.setBillPaymentPending(Double.parseDouble(total) - Double.parseDouble(discountAmount));
		}
		else{
			bean.setGrossTotal(0.0);
			bean.setBillPaymentPending(0.0);
		}
		

		/*SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		//String pDate = request.getParameter("pDate");
		Date purchaseDateq = null;
		try {
			purchaseDateq = dateFormat.parse(purchaseDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bean.setPurchaseDate(purchaseDateq);
		*/
		

		session3.setAttribute("barcodeNo", barcodeNo);
		
		if(barcodeNo == null){
			bean.setBarcodeNo(1000l);
			}
			else
			{
				bean.setBarcodeNo(barcodeNo);	
			}
		
		
		
		//String pDate = request.getParameter("pDate");
		/*Date dueDateq = null;
		try {
			dueDateq = dateFormat.parse(dueDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch bloc
			e.printStackTrace();
		}*/
		int quant = Integer.parseInt(quantity);
		
		try{
			for (int x = 0; x < quant; x++ ){
			FileInputStream fstream = new FileInputStream(
					"D:/barcose/input.prn");

			BufferedReader br = new BufferedReader(new InputStreamReader(
					fstream));
			FileWriter fw = new FileWriter("D:/barcose/Output.prn");

			BufferedWriter bw = new BufferedWriter(fw);
			String strLine;
			String str1;
			while ((strLine = br.readLine()) != null) {

				if (strLine.equals("~product")) 
				{
					str1 = br.readLine();
					strLine = str1 +"\"" +productName+ "\"";
				}
				else if(strLine.equals("~company"))
		         {
					str1=br.readLine();
		            strLine = str1 +"\"" +companyName+ "\"";
		               
		         }
				else if(strLine.equals("~quanti"))
		         {
					str1=br.readLine();
		            strLine = str1 +"\"" +quantity+ "\"";
		               
		         }
				else if(strLine.equals("~bar"))
		         {
					str1=br.readLine();
		            strLine = str1 + "\"" +barcodeNo+ "\"";
		         }
				
				//System.out.println(strLine);
				bw.write(strLine + "\r\n");

				/*// Print the content on the console
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@"+strLine);*/
			}

			bw.close();
			// Close the input stream
			br.close();
			/*List cmdAndArgs = Arrays.asList("cmd", "D:", "cd barcose",
					"printbatch.bat");*/
			//System.out.println("hello Shreeemant::");
			//File dir = new File("D:/barcose");
			
		/*	List cmdAndArgs = Arrays.asList("cmd", "/d", "printbatch.bat");
	        File dir = new File("D:/barcose");*/
	        
	        List cmdAndArgs = Arrays.asList("cmd", "/c", "printbatch.bat");
			File dir = new File("C:/barcose");


			ProcessBuilder pb = new ProcessBuilder(cmdAndArgs);
			pb.directory(dir);
			Process p = pb.start();
			}
		}catch(Exception e){
			
		}
		
	
		if(billNum == null){
			bean.setBillNum("N/A");
		}
		else{
			bean.setBillNum(billNum);
		}
		
		if(discount == ""){
			bean.setDiscount(0.0);
		}
		else{
			bean.setDiscount(Double.parseDouble(discount));
		}
		
		if(discountAmount == ""){
			bean.setDiscountAmount(0.0);
		}
		else{
			bean.setDiscountAmount(Double.parseDouble(discountAmount));
		}
		
		/*if(transExpence == ""){
			bean.setExpenses(0.0);
		}
		else{
			bean.setExpenses(Double.parseDouble(transExpence));
		}*/
		
		
		bean.setSupplier(Long.parseLong(supplier));
		bean.setTotalAmount(Double.parseDouble(total) - Double.parseDouble(discountAmount));
		//bean.setFkCategoryId(Long.parseLong(fk_cat_id));
		//System.out.println(fk_cat_id);
		bean.setFk_subCat_id(Long.parseLong(fk_subCat_id));
		bean.setFk_shop_id(Long.parseLong(fk_shop_id));
		/*bean.setFkGodownId(Long.parseLong(fk_godown_id));*/
		Long newIdForUpdate = Long.parseLong(productID);
		
		Double bp=bean.getBuyPrice();
		Double qnt=bean.getQuantity();
		Double tota=bp*qnt;

		
		
		Double gstPerc=Double.parseDouble(gst);
		Double igstPerc=Double.parseDouble(igst);
		
		if(gstPerc == 0.0 && igstPerc == 0.0)
		{
			System.out.println("========================GST=0 & IGST=0 TAX AMNOUNT CALCULATION======================");
			bean.setTaxAmount(0.0);
			System.out.println("========================GST=0 & IGST=0 TAX AMNOUNT CALCULATION======================");
		}
		
		if(gstPerc != 0.0)
		{
			System.out.println("========================GST TAX AMNOUNT CALCULATION======================");
			Double gsttaxAmnt=((gstPerc/100)*(tota));
			bean.setTaxAmount(gsttaxAmnt);
			System.out.println("gst per @@@@@@@ "+gstPerc);
			System.out.println("gst amount @@@@@@@ "+gsttaxAmnt);
			//bean.setTotalIgstAmount(0.0);
			
			tota = tota-gsttaxAmnt;
			System.out.println("========================GST TAX AMNOUNT CALCULATION======================");
		}
		else if(igstPerc != 0.0)
		{
			System.out.println("========================I-GST TAX AMNOUNT CALCULATION======================");
			Double igsttaxAmnt=((igstPerc/100)*(tota));
			bean.setTaxAmount(igsttaxAmnt);
			//bean.setTaxAmount(0.0);
			System.out.println("gst per @@@@@@@ "+igstPerc);
			System.out.println("gst amount @@@@@@@ "+igsttaxAmnt);
			tota = tota-igsttaxAmnt;
			System.out.println("========================I-GST TAX AMNOUNT CALCULATION======================");
		}
		
		
		
		bean.setPerProductTotal(tota);
				
		bean.setUnit(unit);
		
		if(totalTransportExpense != null || totalTransportExpense.equals(""))
		{
			bean.setTransExpenseIncTax(Double.parseDouble(totalTransportExpense));
			System.out.println(bean.getTransExpenseIncTax()+" **************** ");
		}
		else
		{
			bean.setTransExpenseIncTax(0.0);
		}
		
		if(totalhamaliExpence != null || totalhamaliExpence.equals(""))
		{
			bean.setHamaliExpenseIncTax(Double.parseDouble(totalhamaliExpence));
			System.out.println(bean.getHamaliExpenseIncTax()+" **************** ");
		}
		else
		{
			bean.setHamaliExpenseIncTax(0.0);
		}
		
		if(extraExpenseGSTperc != null || extraExpenseGSTperc.equals(""))
		{
			bean.setExpenseTaxPerc(Integer.parseInt(extraExpenseGSTperc));
			System.out.println(bean.getExpenseTaxPerc()+" **************** ");
		}
		else
		{
			bean.setExpenseTaxPerc(0);
		}
		
		bean.setExpenses(Double.parseDouble(totalTransportExpense) + Double.parseDouble(totalhamaliExpence));
		bean.setFkCategoryId(Long.parseLong(catIDforVAt));
		
		dao.updateProductStatus(newIdForUpdate);		
		dao.addGoodsReceive(bean);
		
		
		/*****************************************************STOCK ENTRY WHENEVER GOOD RECEIVED*********************************************/
		
		StockDao dao1 = new StockDao();
        List stkList2  = dao1.getAllStockEntry();
        
        /*If Stock Is Empty */ 
        if(stkList2.size() == 0)
        {
        	System.out.println("1ST IF().....\nWhen Stock Details List ======= 0"+stkList2.size());
        	/*System.out.println("if stock details is EMPTY =====> "+stkList2.size());
        	System.out.println("In if block of stock empty");*/
        	Stock newEntry = new Stock();
			
        	newEntry.setCatID(Long.parseLong(catIDforVAt));
			//newEntry.setCatID(Long.parseLong(fk_cat_id));
			newEntry.setSubCatId(Long.parseLong(fk_subCat_id));
			newEntry.setFk_shop_id(Long.parseLong(fk_shop_id));
			//System.out.println(fk_shop_id);
			
			newEntry.setProductName(productName);
			newEntry.setCompanyName(companyName);
			newEntry.setWeight(Double.parseDouble(weight));
			newEntry.setQuantity(Double.parseDouble(quantity));
			
				
			Double weightForConversionStock = Double.parseDouble(weight);
			Double quantityForConversionStock = Double.parseDouble(quantity);
			
			if(unitName.equalsIgnoreCase("pcs"))
			{
				Double totalPieceQuantity = weightForConversionStock*quantityForConversionStock;
				newEntry.setTotalKgLtrPieceQuantity(totalPieceQuantity);
				newEntry.setUnit(unit);
	        	newEntry.setProductId(Long.parseLong(productID));
			}
			
			if(unitName.equalsIgnoreCase("kg"))
			{	
				Double newstockInKg = weightForConversionStock*quantityForConversionStock;
				newEntry.setTotalKgLtrPieceQuantity(newstockInKg);
				newEntry.setUnit(unit);
	        	newEntry.setProductId(Long.parseLong(productID));
			}
			
			if(unitName.equalsIgnoreCase("ltr"))
			{
				Double newstockInLtr = weightForConversionStock*quantityForConversionStock;
				newEntry.setTotalKgLtrPieceQuantity(newstockInLtr);
				newEntry.setUnit(unit);
	        	newEntry.setProductId(Long.parseLong(productID));
			}
			
			/*if(batchNo != null){
			newEntry.setBatchNum(batchNo);
			}else{
				newEntry.setBatchNum("N/A");
			}*/
			StockDao dao2 = new StockDao();
			dao2.registerStock(newEntry);	
        	
        }
        
        /*To Update Stock Table If It is Not Empty */
        
        else
        {
        	//System.out.println("if stock details table is NOT EMPTY ======= >Stock List = "+stkList2.size());
		    	   for(int j=0;j<stkList2.size();j++)
		    	   {
		             	System.out.println("1ST IF's ELSE().....\nwhen Stockdetails is GREATER THAN 0 =====> "+stkList2.size());
		             	Stock st = (Stock)stkList2.get(j);
		             	String itemName = st.getProductName();
		             	Long catId = st.getCatID();
		             	Long subCatId=st.getSubCatId();
		             	Long shopId=st.getFk_shop_id();
		             	String company = st.getCompanyName();
		             	Double wight = st.getWeight();
		             	Long PkStockId = st.getPkStockId();
		             	Double oldTotalKgLtrQuantity = st.getTotalKgLtrPieceQuantity();
		             	
		             	//String batchNumber = st.getBatchNum();
		             	/*If ItemName Is Already Exists In Stock Table */ 
		             	//System.out.println("fk_cat_id from stock = = "+catId);*/
		             	//System.out.println("subCatId from stock = = "+subCatId);
		             	//System.out.println("fk_cat_id from ui = = "+fk_cat_id);
		             	//System.out.println("subCatId from ui = = "+fk_subCat_id);
		             	//System.out.println("fk_cat_id from s = = "+shopId);
		             	//System.out.println("subCatId from ui = = "+fk_shop_id);
		             	 //System.out.println("batchNumber from stock table"+batchNumber);
		         		// System.out.println("batchNo from goods receive"+batchNo);
		    	   //System.out.println("In else Part");	
		    	  
		    	   /*if(productName.equals(itemName) && company.equals(companyName) && wight==Double.parseDouble(weight) && catId== Long.parseLong(fk_cat_id) && subCatId==Long.parseLong(fk_subCat_id) && shopId==Long.parseLong(fk_shop_id))*/
		     if(productName.equals(itemName) && company.equals(companyName) && wight==Double.parseDouble(weight) && catId== Long.parseLong(catIDforVAt) && subCatId==Long.parseLong(fk_subCat_id) && shopId==Long.parseLong(fk_shop_id))   	
		    	   {
		    		   
		    		 
            		 System.out.println("IF INSIDE ELSE BLOCK ==== CONDITION TRUE ===== inside If");
            		 
            		 HibernateUtility hbu = HibernateUtility.getInstance();
            		 Session session = hbu.getHibernateSession();
            		 Transaction transaction = session.beginTransaction();
            		 
            		 Stock updateStock = (Stock) session.get(Stock.class, new Long(PkStockId));
            		 
	    		   if(unitName.equalsIgnoreCase("pcs"))
	    		   {
	    		   		 Double qunty = st.getQuantity();
	            		 Double updatequnty = (double) (qunty + Double.parseDouble(quantity));
	            		 updateStock.setQuantity(updatequnty);
	            		 Double weightForConversionStock = Double.parseDouble(weight);
		     			 Double quantityForConversionStock = Double.parseDouble(quantity);     						
		     			 Double totalPieceQuantity = weightForConversionStock*quantityForConversionStock;	     				
		     			 System.out.println("IF INSIDE IF BLOCKWEIGHT*QUANTITY =====> "+totalPieceQuantity);
		     			 Double updateTotalPieceQuantity = (oldTotalKgLtrQuantity + totalPieceQuantity);
			       		 updateStock.setTotalKgLtrPieceQuantity(updateTotalPieceQuantity);
			       		 updateStock.setUnit(unit);

			       	}
	    		   if(unitName.equalsIgnoreCase("kg"))
	    		   {
	    			   	 Double weightForConversionStock = Double.parseDouble(weight);
	    				 Double quantityForConversionStock = Double.parseDouble(quantity);   
	     				 Double newstockInKg = weightForConversionStock*quantityForConversionStock;
		     			 Double updateStockKg = (oldTotalKgLtrQuantity + newstockInKg);
		     			 Double UpdateQuantityKg = updateStockKg/Double.parseDouble(weight);
	        			 updateStock.setQuantity(UpdateQuantityKg);
	        			 updateStock.setUnit(unit);
	        			 updateStock.setTotalKgLtrPieceQuantity(updateStockKg);
	    		   }        		 
	     			
	    		   if(unitName.equalsIgnoreCase("ltr"))
	    		   {
	    			     Double weightForConversionStock = Double.parseDouble(weight);
	    				 Double quantityForConversionStock = Double.parseDouble(quantity);
	    				 Double newstockInLtr = weightForConversionStock*quantityForConversionStock;
		      		 	 Double updateStockLtr = (oldTotalKgLtrQuantity + newstockInLtr);
		      			 Double UpdateQuantityLtr = updateStockLtr/Double.parseDouble(weight);
	        			 updateStock.setQuantity(UpdateQuantityLtr);
	        			 updateStock.setUnit(unit);
	        			 updateStock.setTotalKgLtrPieceQuantity(updateStockLtr);
	      			}
	      			
            		 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            		 Date date2 = new Date();
            	     
            		 updateStock.setUpdateDate(date2);
            		 session.saveOrUpdate(updateStock);
            		 transaction.commit();
            		 
            		 System.out.println("IF INSIDE ELSE IF BLOCK.....VALUE OF J ===================>"+j);
            		break;
		            	}
		            	else
		            	{
		            		//ItemName is Not Exists
		            		if(j+1 == stkList2.size())
		            		{
		            		System.out.println("IF INSIDE ELSE IF ELSE BLOCK VALUE OF J+1 ===================>"+j);
		            			
		            			Stock newEntry = new Stock();
		            			newEntry.setCatID(Long.parseLong(catIDforVAt));
		            			//newEntry.setCatID(Long.parseLong(fk_cat_id));
		            			newEntry.setSubCatId(Long.parseLong(fk_subCat_id));
		            			newEntry.setFk_shop_id(Long.parseLong(fk_shop_id));
		            			newEntry.setProductName(productName);
		            			newEntry.setCompanyName(companyName);
		            			newEntry.setWeight(Double.parseDouble(weight));
		            			newEntry.setQuantity(Double.parseDouble(quantity));
		            			
		            			Double weightForConversionStock = Double.parseDouble(weight);
		         				Double quantityForConversionStock = Double.parseDouble(quantity);
		            			
		         				if(unitName.equalsIgnoreCase("pcs"))
		         				{
			            			Double totalPieceQuantity = weightForConversionStock*quantityForConversionStock;
			            			Double updateQuantityPiece = totalPieceQuantity/Double.parseDouble(weight);
		    	        			newEntry.setQuantity(updateQuantityPiece);		    	        			 
			            			newEntry.setTotalKgLtrPieceQuantity(totalPieceQuantity);
			    		       		newEntry.setUnit(unit);
			    		       		newEntry.setProductId(Long.parseLong(productID));
		         				}
		         				
		         				if(unitName.equalsIgnoreCase("kg"))
		         				{
			         				 Double newstockInKg = weightForConversionStock*quantityForConversionStock;
			    	     		 	 Double updateQuantityKg = newstockInKg/Double.parseDouble(weight);
		    	        			 newEntry.setQuantity(updateQuantityKg);
		    	        			 newEntry.setUnit(unit);
		    	     				 newEntry.setTotalKgLtrPieceQuantity(newstockInKg);
		    	     				 newEntry.setProductId(Long.parseLong(productID));
		         				}
		    	        		if(unitName.equalsIgnoreCase("ltr"))
		    	        		{
			    	      			 Double newstockInLtr = weightForConversionStock*quantityForConversionStock;
			    	      			 Double updateQauantityLtr = newstockInLtr/Double.parseDouble(weight);
		    	      				 newEntry.setQuantity(updateQauantityLtr);
		    	      				 newEntry.setUnit(unit);
		    	      				 newEntry.setTotalKgLtrPieceQuantity(newstockInLtr);
		    	      				 newEntry.setProductId(Long.parseLong(productID));
		    	      			}
		            			
		            			//newEntry.setBatchNum("N/A");
		            			StockDao dao2 = new StockDao();
		            			dao2.registerStock(newEntry);
		            		}
		            	}
		        		
		        	}
        		}
        }
	}
	
	public Map getAllDcNumbers(String supplierId) {
		GoodsReceiveDao dao = new GoodsReceiveDao();
		List list= dao.getAllDcNumbersBySuppliers(supplierId);
		Map  map =  new HashMap();
		for(int i=0;i<list.size();i++)
		{
			Object[] o = (Object[])list.get(i);
			GetPODetailsForGoodsReceive bean = new GetPODetailsForGoodsReceive();
			System.out.println(Arrays.toString(o));
			bean.setDcNum(o[0].toString());
			bean.setInsertDate(o[1].toString());
			//bean.setTotalAmount((Double)o[1]);
			System.out.println("***************"+o[0]);
			map.put(bean.getDcNum(),bean);
			
		}
		return map;
	}

	public Map getPODetails(String dcNum, String supplier) {
		System.out.println("In Helper");
		GoodsReceiveDao dao = new GoodsReceiveDao();
		List list = dao.getPODetailsForGoodsReceive(dcNum,supplier);
		System.out.println(list.size());
		Map  map1 =  new HashMap();
		
		for(int i=0;i<list.size();i++)
		{
			Object[] o = (Object[])list.get(i);
			GetPODetailsForDcUpdate bean = new GetPODetailsForDcUpdate();
			
			
			bean.setPkPOId((BigInteger)o[0]);
			bean.setProductName((String)o[1]);
			bean.setBuyPrice((BigDecimal)o[2]);
			bean.setSalePrice((BigDecimal)o[3]);
			bean.setQuantity((BigInteger)o[4]);
			bean.setWeight((BigDecimal)o[5]);
			/*bean.setTotalAmount((BigDecimal)o[6]);*/
			map1.put(bean.getPkPOId(),bean);
		}
		return map1;
	
	
	}




/*	public List getPurchaseDetailsForSingleDate(HttpServletRequest request,
			HttpServletResponse response) {
		
		String fDate = request.getParameter("fDate");
		System.out.println(fDate+"Single Date");
		
         Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
 		
         GoodsReceiveDao dao = new GoodsReceiveDao();
 		List<PurchaseDetailsFromGoodsReceive> expList = dao.getPurchaseDetailsForSingleDateFromGoodsReceive(fDate);
 		
 		
 		return expList;
		
	}*/




	public List getPurchaseDetailByTwoDate(HttpServletRequest request,
			HttpServletResponse response) {


		String fDate = request.getParameter("fisDate");
        String tDate = request.getParameter("endDate");
		
        Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
        GoodsReceiveDao dao = new GoodsReceiveDao();
		List<PurchaseDetailsFromGoodsReceive> exp1List = dao.getPurchaseReportsBetweenTwoDates(fDate,tDate);
		
		return exp1List;
	
	}
	
	public List GSTSummaryReportBetweenTwoDates(HttpServletRequest request,
			HttpServletResponse response) {


		String fDate = request.getParameter("fisDate3");
        String tDate = request.getParameter("endDate3");
		
        Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
        GoodsReceiveDao dao = new GoodsReceiveDao();
		List<PurchaseDetailsFromGoodsReceive> exp1List = dao.GSTSummaryReportBetweenTwoDates(fDate,tDate);
		
		return exp1List;
	
	}



	public List getStockReportAsPerShop(HttpServletRequest request,
			HttpServletResponse response) {


		
		String fk_shop_id = request.getParameter("fk_shop_id");
		
         Map<Long,StockDetail> map = new HashMap<Long,StockDetail>();
 		
         GoodsReceiveDao dao = new GoodsReceiveDao();
 		List<StockDetail> stockList = dao.getStockReportAsPerShop(fk_shop_id);
 		
 		
 		return stockList;
	
	
	
	
	}






	
	
	public CustomerBean getDetailsById(HttpServletRequest request,
			HttpServletResponse response) {
		
		String key=request.getParameter("key");
		System.out.println(key+"barcode");
		Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
		
		GoodsReceiveDao dao = new GoodsReceiveDao();
		List<CustomerBean> catList = dao.getAllItemDetails(key);
		
		CustomerBean cs = null;
		if(catList!= null && catList.size() > 0 )
		{	
			cs = (CustomerBean)catList.get(0); 
		}
		return cs;
	}

	
	public CustomerBean getPesticideDetailsByBarcode(HttpServletRequest request,
			HttpServletResponse response) {
		
		String key=request.getParameter("key");
		System.out.println(key+"barcode");
		Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
		
		GoodsReceiveDao dao = new GoodsReceiveDao();
		List<CustomerBean> catList = dao.getPesticideDetailByBarocde(key);
		
		CustomerBean cs = null;
		if(catList!= null && catList.size() > 0 )
		{	
			cs = (CustomerBean)catList.get(0); 
		}
		return cs;
	}
	
	
	// get purchase Item By Bill NoWise
		public Map getAllIetmByBillNo(String bill_no,String supplier) {
			// TODO Auto-generated method stub
			
			    GoodsReceiveDao dao = new GoodsReceiveDao();
				List list = dao.getAllIetmByBillNo(bill_no,supplier);
				System.out.println(list.size());
				Map  map1 =  new HashMap();
				
				for(int i=0;i<list.size();i++)
				{
					Object[] o = (Object[])list.get(i);
					GetPurchaseProduct bean = new GetPurchaseProduct();
					
					
					bean.setPk_goods_receive_id((BigInteger)o[0]);
					bean.setSupplier_name((String)o[1]);
					bean.setDc_number((BigInteger)o[2]);
					bean.setProduct_name((String)o[3]);
					bean.setBuy_price((BigDecimal)o[4]);
					bean.setSale_price((BigDecimal)o[5]);
					bean.setWeight((BigDecimal)o[6]);
					bean.setQuantity((Double)o[7]);
					bean.setBatch_no((String)o[8]);
					bean.setFkCategoryId((BigInteger)o[9]);
					BigInteger big1 = new BigInteger("1");
					BigInteger big2 = new BigInteger("2");
					BigInteger big3 = new BigInteger("3");
					System.out.println("cat Id  ===  "+o[9]);
					if(o[9].equals(big1)){
						String ferti = "Fertilizer";
						bean.setCatName(ferti);
					}
					if(o[9].equals(big2)){
						String Pesticide = "Pesticide";
						bean.setCatName(Pesticide);
					}
					if(o[9].equals(big3)){
						String Seed = "Seed";
						bean.setCatName(Seed);
					}
					//bean.setPurchaseDate((Date)o[10]);
					bean.setPurchaseDate1(o[10].toString());
					bean.setMrp((BigDecimal)o[11]);
					bean.setTax_percentage((BigDecimal)o[12]);
					bean.setBarcodeNo((BigInteger)o[13]);
					bean.setCompany_Name((String)o[14]);
					//bean.setDupQuantity((Double)o[15]);
					bean.setDupQuantity1((Double)o[15]);
			
					//Double d=Double.parseDouble(o[15].toString());
						
			/*
			 * if(Double.parseDouble(o[15].toString())>0) {
			 * 
			 * }
			 */
					
					bean.setCatName((String)o[16]);
					/*bean.setFk_unit_id((BigInteger)o[5]);*/
					//bean.setQuantity(0l);
					//System.out.println("***************"+o[0]+"\t"+o[5]);
					bean.setPkPOId(Long.parseLong(o[17].toString()));
				
					System.out.println("pkkkk----------"+o[17].toString());
			/*
			 * bean.setFkCategoryId((BigInteger)o[18]);
			 * System.out.println("cattttt----------"+o[18]);
			 */
					map1.put(bean.getPk_goods_receive_id(),bean);
				}
				return map1;
		}




	//to purchase Return
		
		/////to update in goodsReceive table//////////
		public void returntPurchase(HttpServletRequest request,
				HttpServletResponse response) {
			System.out.println("GoodsReceiveHelper returntPurchase()");
			// TODO Auto-generated method stub
			Integer count = Integer.parseInt(request.getParameter("count"));
			for(int i =0 ; i <count;i++)
			{
				String pkGoodsReceiveId = request.getParameter("pk_goods_receive_id"+i);
				String dupQuantity = request.getParameter("dupQuantity"+i);
				String product_name = request.getParameter("product_name"+i);
				String company_Name = request.getParameter("company_Name"+i);
				String weight = request.getParameter("weight"+i);
				String duplicateQuantity = request.getParameter("duplicateQuantity"+i);
				String tax_percentage = request.getParameter("tax_percentage"+i);
				String buy_price = request.getParameter("buy_price"+i);
				// String Discount = request.getParameter("Discount"+i);
				
				
				System.out.println("currnt qunt : "+ duplicateQuantity);
				System.out.println("minus qunt : "+ dupQuantity);
				HibernateUtility hbu=null;
				Session session = null;
				Transaction transaction = null;
				
				try{
					hbu = HibernateUtility.getInstance();
					session = hbu.getHibernateSession();
					transaction = session.beginTransaction();
					List<Object[]> list2  = null;
					List<GoodsReceiveBean> goodsList = null;
					Double grossTotal = 0.0;
					
					Double quantity = (double)(Double.parseDouble(duplicateQuantity) -  Double.parseDouble(dupQuantity));
					System.out.println("after minus qunt : "+ quantity);
       		        GoodsReceiveBean updateStock = (GoodsReceiveBean) session.get(GoodsReceiveBean.class, new Long(pkGoodsReceiveId));
       		        // updateStock.setQuantity(quantity);
       		        updateStock.setDupQuantity(quantity);
       		        //session.saveOrUpdate(updateStock);
       		        
	       		    Query query2 = session.createSQLQuery("SELECT gross_total,bill_number FROM goods_receive WHERE pk_goods_receive_id="+pkGoodsReceiveId);
	 		        list2 = query2.list();
	 		        goodsList = new ArrayList<GoodsReceiveBean>();
	 		        for (Object[] objects : list2) 
	 		        {
	 					 grossTotal = Double.parseDouble(objects[0].toString());
	 					
	 				 }
	 		        Double buyPrice = Double.parseDouble(buy_price);
			        Double taxPercentage = Double.parseDouble(tax_percentage);
			        Double taxAmount = (taxPercentage/100)*buyPrice;
			        Double newBuyPrice = buyPrice + taxAmount;
			        Double total = Double.parseDouble(dupQuantity) * newBuyPrice;
			        Double newGrossTotal = grossTotal - total;
			        System.out.println("buyPrice = ="+buyPrice);
			        System.out.println("taxAmount = ="+taxAmount);
			        System.out.println("newBuyPrice = ="+newBuyPrice);
			        System.out.println("total = ="+total);
			        System.out.println("newGrossTotal = ="+newGrossTotal);
			        
       		        updateStock.setReturnAmount(total);
       		        updateStock.setTotalAfterReturn(newGrossTotal);
       		        session.saveOrUpdate(updateStock);
       		        transaction.commit();
					
					
       		}
			catch(RuntimeException e){
				try{
					transaction.rollback();
				}catch(RuntimeException rbe)
				{
					Log.error("Couldn't roll back tranaction",rbe);
				}	
			}finally{
				hbu.closeSession(session);
			}				
			}		
		}




		public void returntMinusFromStockPurchase(HttpServletRequest request,
				HttpServletResponse response) {
			// TODO Auto-generated method stub
			Integer count = Integer.parseInt(request.getParameter("count"));
			for(int i =0 ; i <count;i++)
			{
				String pkGoodsReceiveId = request.getParameter("pk_goods_receive_id"+i);
				String dupQuantity = request.getParameter("dupQuantity"+i);
				String product_name = request.getParameter("product_name"+i);
				String company_Name = request.getParameter("company_Name"+i);
				String weight = request.getParameter("weight"+i);
				//String duplicateQuantity = request.getParameter("duplicateQuantity"+i);
				//System.out.println("currnt qunt : "+ duplicateQuantity);
				System.out.println("minus qunt : "+ dupQuantity);
				System.out.println("product_name : "+ product_name);
				System.out.println(" company_Name : "+ company_Name);
				System.out.println(" weight : "+ weight);
				
				HibernateUtility hbu1=null;
				Session session1=null;
				Transaction transaction1 = null;
				
				try
				{ 
				 Long PkStockId;
				 Double quantity;
				 hbu1 = HibernateUtility.getInstance();
				 session1 = hbu1.getHibernateSession();
				 transaction1 = session1.beginTransaction();
				
        		 Query query = session1.createSQLQuery("select PkStockId , Quantity from stock_detail where ProductName=:product_name AND CompanyName=:company_Name And Weight =:weight");
        		 query.setParameter("product_name", product_name);
        		 query.setParameter("company_Name", company_Name);
        		 query.setParameter("weight", weight);
        		 
        		 List<Object[]> list = query.list();
    			 
    			  for (Object[] object : list) {
    			  System.out.println(Arrays.toString(object));  
    			  PkStockId = Long.parseLong(object[0].toString());
    			  quantity = Double.parseDouble(object[1].toString());
    			  System.out.println("PkStockId " +PkStockId);
    			  System.out.println("quantity " +quantity);
    			  
    			  Double updatequnty = (double) (quantity - Double.parseDouble(dupQuantity));
    			  System.out.println("updatequnty " +updatequnty);
          		
         	     Stock Stock = (Stock) session1.load(Stock.class, new Long(PkStockId));
         	     
         	     Stock.setQuantity(updatequnty);
         		 
         		 session1.saveOrUpdate(Stock);
         	     transaction1.commit();
         	    System.out.println("Success ");	 
    		   }
					
			}
				catch(RuntimeException e){
					try{
						transaction1.rollback();
					}catch(RuntimeException rbe)
					{
						Log.error("Couldn't roll back tranaction",rbe);
					}	
				}finally{
					hbu1.closeSession(session1);
				}
	  }

	}
		
/*		//fetching pro details from goods receive for ferti bill
				public CustomerBean getDetailsByProNameForzFertiBill(
						HttpServletRequest request, HttpServletResponse response) {

					
					String proName =request.getParameter("proName");
					String company =request.getParameter("company");
					String weight =request.getParameter("weight");
					
					System.out.println(proName+"pro name in gr helper");
					
					Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
					
					GoodsReceiveDao dao = new GoodsReceiveDao();
					List<CustomerBean> catList = dao.getAllProductDetailsForFrtiBillAsPerProductName(proName,company,weight);
					
					CustomerBean cs = null;
					if(catList!= null && catList.size() > 0 )
					{	
						cs = (CustomerBean)catList.get(0); 
					}
					return cs;
				
				}*/
		 

			//fetching pro details from goods receive for ferti bill
				public CustomerBean getDetailsByProNameForzFertiBill(
						HttpServletRequest request, HttpServletResponse response) {

					
					String proName =request.getParameter("proName");
					String company =request.getParameter("company");
					String weight =request.getParameter("weight");
					
					System.out.println(proName+"pro name in gr helper");
					
					Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
					
					GoodsReceiveDao dao = new GoodsReceiveDao();
					List<CustomerBean> catList = dao.getAllProductDetailsForFrtiBillAsPerProductName(proName,company,weight);
					
					CustomerBean cs = null;
					if(catList!= null && catList.size() > 0 )
					{	
						cs = (CustomerBean)catList.get(0); 
					}
					return cs;
				
				}
				
				//fetching product detail as per batch for seed bill
				public CustomerBean getDetailsByBatchNadStockForSeedBill(
						HttpServletRequest request, HttpServletResponse response) {


					
					String batchNum =request.getParameter("batchNum");
					String stock =request.getParameter("stock");
					System.out.println(" batchNum=== == =in helper"+batchNum);
					System.out.println(" stock=== == =in helper"+stock);
					Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
					
					GoodsReceiveDao dao = new GoodsReceiveDao();
					List<CustomerBean> catList = dao.getAllProductDetailsForSeedBillAsPerBatchAndStock(batchNum,stock);
					
					CustomerBean cs = null;
					if(catList!= null && catList.size() > 0 )
					{	
						cs = (CustomerBean)catList.get(0); 
					}
					return cs;
				
				
				}
				
				public CustomerBean getProductDetailsForFertiBill(HttpServletRequest request, HttpServletResponse response)throws Exception
				{					
					String proName =request.getParameter("proName");
					String company =request.getParameter("company");
					String weight =request.getParameter("weight");
					String batchNum = request.getParameter("batchNum");
					String catId = request.getParameter("catId");
					String subCatId = request.getParameter("subCatId");
					String shopId = request.getParameter("shopId");
					String expiryDate = request.getParameter("expiryDate");
					
					System.out.println(proName+"pro name in gr helper");
					System.out.println(catId+"pro name in gr helper");
					System.out.println(subCatId+"pro name in gr helper");
					
					Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
					
					GoodsReceiveDao dao = new GoodsReceiveDao();
					List<CustomerBean> catList = dao.getAllProductDetailsForFrtiBillAsPerProductName1(proName,company,weight,batchNum,catId,subCatId, shopId, expiryDate);
					
					CustomerBean cs = null;
					if(catList!= null && catList.size() > 0)
					{	
						cs = (CustomerBean)catList.get(0); 
					}
					System.out.println(cs);
					return cs;
				
				
				}
				
				
				
				// Get Details by Product Name
				public CustomerBean getProductDetailsByBarcodeForFertiBill(String barcodeNo)throws Exception
				{					

					System.out.println("IN HELPER Barcode No   : "+barcodeNo);
					Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
					
					GoodsReceiveDao dao = new GoodsReceiveDao();
					List<CustomerBean> catList = dao.getAllProductDetailsForFrtiBillAsPerBarcodeNo(barcodeNo);
					
					CustomerBean cs = null;
					if(catList!= null && catList.size() > 0)
					{	
						cs = (CustomerBean)catList.get(0); 
					}
					System.out.println(cs);
					return cs;
				
				
				}





				public CustomerBean getProductDetailsForSeedBill(
						HttpServletRequest request, HttpServletResponse response) {

					
					String proName =request.getParameter("proName");
					String company =request.getParameter("company");
					String weight =request.getParameter("weight");
					String batchNum = request.getParameter("batchNum");
					
					System.out.println(proName+"pro name in gr helper");
					
					Map<Long,CustomerBean> map = new HashMap<Long,CustomerBean>();
					
					GoodsReceiveDao dao = new GoodsReceiveDao();
					List<CustomerBean> catList = dao.getAllProductDetailsForSeedBillAsPerProductName1(proName,company,weight,batchNum);
					
					CustomerBean cs = null;
					if(catList!= null && catList.size() > 0 )
					{	
						cs = (CustomerBean)catList.get(0); 
					}
					return cs;		
				
				}
				
				public List getSaleDetailsAsPerCategoryForSingleDate(
						HttpServletRequest request, HttpServletResponse response) {
					
					String cat = request.getParameter("cat");
					String fDate = request.getParameter("fDate");
					System.out.println(cat+"Category in Helper");
					
			         Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<SaleReports> expList = dao.getSaleDetailsAsPerCategoryForSingleDate(cat,fDate);
			 		
			 		return expList;
				
				}


				public List getSaleDetailsPerPaymentMode(HttpServletRequest request, HttpServletResponse response) {
					
					String paymentMode = request.getParameter("paymentMode");
					String fk_cat_id = request.getParameter("fk_cat_id");
					System.out.println(paymentMode+"paymentMode in Helper");
					System.out.println(fk_cat_id+"fk_cat_id in Helper");
			         Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<SaleReports> expList = dao.getSaleDetailsAsPerPaymentMode(paymentMode,fk_cat_id);
			 		return expList;
				}


				public List getSaleDetailsPerPaymentModeGorSingleDate(HttpServletRequest request, HttpServletResponse response) {
					
					String singleDate = request.getParameter("enterdate");
					String paymentModeIdForDate = request.getParameter("paymentModeIdForDate");
					System.out.println(singleDate+"singleDate in Helper");
			         Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<SaleReports> expList = dao.getSaleDetailsAsPerPaymentModeForSingleDate(singleDate,paymentModeIdForDate);
			 		return expList;
				}
				
				
	public List getSaleDetailsPerGSTPercentage(HttpServletRequest request, HttpServletResponse response) {
					
				String cat = request.getParameter("fk_cat_id");
					String fDate = request.getParameter("startDate");
						String sDate = request.getParameter("endDate");
			         Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<SaleReports> expList = dao.getSaleDetailsAsGST(cat,fDate,sDate);
			 		return expList;
				}
				
				public List getSaleDetailsAsPerCategoryBetTwoDates(
						HttpServletRequest request, HttpServletResponse response) {

					
					String cat = request.getParameter("fk_cat_id");
					String fDate = request.getParameter("fisDate");
					String sDate = request.getParameter("endDate");
					System.out.println(cat+"Category in Helper");
					System.out.println(fDate+"fDate in Helper");
					System.out.println(sDate+"sDate in Helper");
					
			         Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<SaleReports> expList = dao.getSaleDetailsAsPerCategoryBetweeenTwoDates(cat,fDate,sDate);
			 		
			 		
			 		return expList;
				
				
				
				
				}
				
				public List getSaleDetailsAsPerShopBetTwoDates(
						HttpServletRequest request, HttpServletResponse response) {

					
					String shop = request.getParameter("shop");
					String fDate = request.getParameter("fDate");
					String sDate = request.getParameter("sDate");
					System.out.println(shop+"Category in Helper");
					System.out.println(fDate+"fDate in Helper");
					System.out.println(sDate+"sDate in Helper");
					
			         Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<SaleReports> expList = dao.getSaleDetailsAsPerShopBetweeenTwoDates(shop,fDate,sDate);
			 	
			 		return expList;
				
				}

				public List getSaleDetailsAsPerProNameForSingleDate(
						HttpServletRequest request, HttpServletResponse response) {

					String cat = request.getParameter("cat");
					String productName = request.getParameter("product");
					String fDate = request.getParameter("fDate");
					System.out.println(cat+"Category in Helper");
					
			         Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<SaleReports> expList = dao.getSaleDetailsAsPerProductNameForSingleDate(cat,fDate,productName);
			 		
			 		
			 		return expList;
				
				
				
				
				}
				
				public List getStockDetailsAsPerCategory(HttpServletRequest request,
						HttpServletResponse response) {

					
					String cat = request.getParameter("cat");
					System.out.println(cat+"Category in Helper");
					
			         Map<Long,StockDetail> map = new HashMap<Long,StockDetail>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<StockDetail> stockList = dao.getStockDetailsForReportAsPerCategory(cat);
			 		
			 		System.out.println("@@@ stock report Helper :: "+ stockList);
			 		
			 		return stockList;
			
				}
				

				
				public List getStockDetailsAsCompanyName(HttpServletRequest request,HttpServletResponse response) {
					String companyName = request.getParameter("companyName");
					
			         Map<Long,StockDetail> map = new HashMap<Long,StockDetail>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<StockDetail> stockList = dao.getStockDetailsAsPerCompanyName(companyName);
			 		
			 		return stockList;
				
				}
		
				public List getPurchaseDetailsForSingleDate(HttpServletRequest request,
						HttpServletResponse response) {
					
					String fDate = request.getParameter("fDate");
					System.out.println(fDate+"Single Date");
					
			        Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
			 		
			        GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<PurchaseDetailsFromGoodsReceive> expList = dao.getPurchaseDetailsForSingleDateFromGoodsReceive(fDate);
			 		
			 		return expList;
					
				}
				

				public List getPurchaseDetailsAsPerProduct(HttpServletRequest request,
						HttpServletResponse response) {

					
					String cat = request.getParameter("cat");
					String productName = request.getParameter("proName");
					String company = request.getParameter("company");
					String weight = request.getParameter("weight");
					

					System.out.println(cat+"Category in Helper");
					//System.out.println(product+"product in Helper");
					System.out.println(cat+" "+productName+" "+company+" "+weight);
					
			         Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
			 		
			         GoodsReceiveDao dao = new GoodsReceiveDao();
			 		List<PurchaseDetailsFromGoodsReceive> expList = dao.getPurchaseDetailsAsPerProduct(cat,productName,company,weight);
			 		
			 		
			 		return expList;
				
				
				
				}
//////purchase report for supplier//////////
public List getPurchaseDetailsAsPerSupplierName(HttpServletRequest request,
		HttpServletResponse response) {
	
	String supplier = request.getParameter("supplier");
	System.out.println(supplier+"Supplier in Helper");
	
     Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<PurchaseDetailsFromGoodsReceive> expList = dao.getPurchaseDetailsForSupplier(supplier);
		
		return expList;
}

public List getPurchaseDetailsAsPerShopName(HttpServletRequest request,
		HttpServletResponse response) {
	
	String shop = request.getParameter("shop");
	String fisDate = request.getParameter("fisDate3");
	String endDate = request.getParameter("endDate3");
	System.out.println(shop+"Supplier in Helper");
	
	
	
     Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<PurchaseDetailsFromGoodsReceive> expList = dao.getPurchaseDetailsForShop(shop,fisDate,endDate);
		
		
		return expList;

}


public List getPurchaseDetailsAsPerCategory(HttpServletRequest request,
		HttpServletResponse response) {
	
	String cat = request.getParameter("cat");
	System.out.println(cat+"Category in Helper");
	
     Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<PurchaseDetailsFromGoodsReceive> expList = dao.getPurchaseDetailsForCategory(cat);
		
		
		return expList;


}


public List getSaleDetailsAsPerProductNameBetTwoDates(
		HttpServletRequest request, HttpServletResponse response) {


	
	String cat = request.getParameter("cat");
	String fDate = request.getParameter("fDate");
	String sDate = request.getParameter("sDate");
	String product = request.getParameter("product");
	
	System.out.println(cat+"Category in Helper");
	System.out.println(fDate+"fDate in Helper");
	System.out.println(sDate+"sDate in Helper");
	
     Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.getSaleDetailsAsPerProductNamesBetweeenTwoDates(cat,fDate,sDate,product);
		
		
		return expList;


}

//GST Sale Summary 
public List gstSummaryReportsBetweenTwoDates(
		HttpServletRequest request, HttpServletResponse response) {


	
	String fDate = request.getParameter("gstFisDate1");
	String sDate = request.getParameter("gstEndDate1");
	
	System.out.println(fDate+"fDate in Helper");
	System.out.println(sDate+"sDate in Helper");
	
     Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.gstSummaryReportsBetweenTwoDates(fDate,sDate);	
		
		return expList;
}

//GST Purchase Summary 
public List gstPurchaseSummaryReportsBetweenTwoDates(
		HttpServletRequest request, HttpServletResponse response) {


	
	String fDate = request.getParameter("gstFisDate1");
	String sDate = request.getParameter("gstEndDate1");
	
	System.out.println(fDate+"fDate in Helper");
	System.out.println(sDate+"sDate in Helper");
	
     Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.gstPurchaseSummaryReportsBetweenTwoDates(fDate,sDate);
		
		
		return expList;


}


public List getSaleDetailsAsPerSup(HttpServletRequest request,
		HttpServletResponse response) {

	String fkSupplierId = request.getParameter("fkSupplierId");
	System.out.println(fkSupplierId+"fkSupplierId in Helper");
	
     Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.getSaleDetailsAsPerSupplierName(fkSupplierId);
		
		return expList;

}


public List getTaxDetailsFromPurchaseForSingleDateAsPerCategory(
		HttpServletRequest request, HttpServletResponse response) {

	
	String cat = request.getParameter("cat");
	String fDate = request.getParameter("fDate");
	String sDate = request.getParameter("sDate");
	System.out.println(cat+"Category in Helper");
	System.out.println(fDate+"fDate in Helper");
     Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<PurchaseDetailsFromGoodsReceive> expList = dao.geTaxDetailsAsPerCategoryForSingleDate(cat,fDate,sDate);
		return expList;

}



public List getStockDetailsAsProductName(HttpServletRequest request,HttpServletResponse response) {
	String proName = request.getParameter("proName");
	String company = request.getParameter("company");
	String weight = request.getParameter("weight");
     
	Map<Long,StockDetail> map = new HashMap<Long,StockDetail>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<StockDetail> stockList = dao.getStockDetailsAsPerProductName(proName,weight,company);
		
		return stockList;

}




public List getSaleDetailsPerPaymentModeForTwoDate(HttpServletRequest request,
		HttpServletResponse response) {

	String singleDate = request.getParameter("singleDate");
	String secondDate = request.getParameter("secondDate");
	/*String fk_cat_id = request.getParameter("fk_cat_id");*/
	System.out.println(singleDate+"singleDate in Helper");
	/*System.out.println(fk_cat_id+"fk_cat_id in Helper");*/
     Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
/*		List<SaleReports> expList = dao.getSaleDetailsAsPerPaymentModeForTwoDate(singleDate,fk_cat_id,secondDate);*/
		List<SaleReports> expList = dao.getSaleDetailsAsPerPaymentModeForTwoDate(singleDate,secondDate);
		return expList;

}


public List getSaleDetailsAsPerPaymentModeForRangesHelper(HttpServletRequest request,
		HttpServletResponse response) {

	String fisDateForPay4 = request.getParameter("fisDateForPay4");
	String endDateForPay4 = request.getParameter("endDateForPay4");
	String paymentModeId4 = request.getParameter("paymentModeId4");
	System.out.println(fisDateForPay4+"singleDate in Helper");
	System.out.println(endDateForPay4+"fk_cat_id in Helper");
     Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.getSaleDetailsAsPerPaymentModeForRangesDao(fisDateForPay4,endDateForPay4,paymentModeId4);
		return expList;

}

public Map getSubCatDetails(String fk_cat_id) {
    
	int count=0;
	System.out.println("IN HELPER");
	/*String fk_cat_id = request.getParameter("fk_cat_id");
	
	System.out.println("=== == ==="+fk_cat_id);
	
	
	SubCategoryDetailsBean scdb = new SubCategoryDetailsBean();
	
	scdb.setFk_cat_id(Long.parseLong(fk_cat_id));*/
	
	ProductDetailsDao cdd = new ProductDetailsDao();
	List list=cdd.getAllSubCategory(fk_cat_id);
	
	System.out.println("list ======"+list.size());
	Map  map =  new HashMap();
	for(int i=0;i<list.size();i++)
	{
		System.out.println("IN HELPER");
		Object[] o = (Object[])list.get(i);
		SubCategoryDetailsBean bean = new SubCategoryDetailsBean();
		System.out.println(Arrays.toString(o));
		bean.setSubcatId(Long.parseLong(o[0].toString()));
		bean.setSubcategoryName(o[1].toString());
		//bean.setTotalAmount((Double)o[1]);
		System.out.println("***************"+o[0]);
		map.put(count,bean);
		count++;
	}
	return map;

}

public Map getProNameDetails(String fk_cat_id,String fk_subCat_id) {
    
	int count=0;
	System.out.println("IN HELPER");
	GoodsReceiveDao cdd = new GoodsReceiveDao();
	List list=cdd.getProductName(fk_cat_id,fk_subCat_id);
	
	System.out.println("list ======"+list.size());
	Map  map =  new HashMap();
	for(int i=0;i<list.size();i++)
	{
		System.out.println("IN HELPER");
		Object[] o = (Object[])list.get(i);
		ProductDetailsBean bean = new ProductDetailsBean();
		System.out.println(Arrays.toString(o));
		bean.setProdctId(Long.parseLong(o[0].toString()));
		bean.setProductName(o[1].toString());
		bean.setUnitName(o[2].toString());
		//bean.setTotalAmount((Double)o[1]);
		
		
		System.out.println("product id***************"+o[0]);
		System.out.println("*product name**************"+o[1]);
		System.out.println("**unit name*************"+o[2]);
		
		
		map.put(count,bean);
		count++;
	}
	return map;

}

public List getPurchaseDetailByGST(HttpServletRequest request,
		HttpServletResponse response) {
	String fDate = request.getParameter("gstFisDate");
    String tDate = request.getParameter("gstEndDate");
	
    Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
	
    GoodsReceiveDao dao = new GoodsReceiveDao();
	List<PurchaseDetailsFromGoodsReceive> exp1List = dao.getPurchaseReportsASPerGST(fDate,tDate);
	
	return exp1List;


}

public List getPurchaseReturnDetailsAsPerSupplierName(
		HttpServletRequest request, HttpServletResponse response) {
	String supplier = request.getParameter("supplier");
	String firstDate = request.getParameter("firstDate");
	String secondDate = request.getParameter("secondDate");
	
	System.out.println(supplier+"= = = Supplier in Helper");
	
    Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
    GoodsReceiveDao dao = new GoodsReceiveDao();
	List<PurchaseDetailsFromGoodsReceive> expList = dao.getPurchaseReturnDetailsForSupplier(supplier,firstDate,secondDate);
		
		
	return expList;
}

public List creditCustomerSaleReportBillAndNameWise(HttpServletRequest request, HttpServletResponse response)
{
	
	String creditCustBillNo = request.getParameter("creditCustBillNo");
	//System.out.println("bill_no From Helper === "+creditCustBillNo);
    Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
    GoodsReceiveDao dao = new GoodsReceiveDao();
    List<SaleReports> creditCustSaleReports = dao.creditCustomerSaleReportBillAndNameWise(creditCustBillNo);
    return creditCustSaleReports;
	
}

//GST Sale Summary 
public List saleGstReturnReportsBetweenTwoDates(
		HttpServletRequest request, HttpServletResponse response) {


	
	String fDate = request.getParameter("gstFisDate1");
	String sDate = request.getParameter("gstEndDate1");
	
	System.out.println(fDate+"fDate in Helper");
	System.out.println(sDate+"sDate in Helper");
	
   Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
   GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.saleGstReturnReportsBetweenTwoDates(fDate,sDate);	
		
		return expList;
}

public List purchaseGstReturnReportsBetweenTwoDates(
		HttpServletRequest request, HttpServletResponse response)
{


	System.out.println("helper called");
	
	String supplierId = request.getParameter("supplierId");
	String fDate = request.getParameter("gstFisDate1");
	String sDate = request.getParameter("gstEndDate1");
	
	System.out.println(fDate+"fDate in Helper");
	System.out.println(sDate+"sDate in Helper");
	
   Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
   GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.purchaseGstReturnReportsBetweenTwoDates(supplierId,fDate,sDate);	
		
		return expList;
}


public List billWiseSaleReportForGrossTotalHelper(
		HttpServletRequest request, HttpServletResponse response) {


	
	String fDate = request.getParameter("gstFisDate1");
	String sDate = request.getParameter("gstEndDate1");
	
	System.out.println(fDate+"fDate in Helper");
	System.out.println(sDate+"sDate in Helper");
	
   Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
		
   GoodsReceiveDao dao = new GoodsReceiveDao();
		List<SaleReports> expList = dao.billWiseSaleReportForGrossTotalDao(fDate,sDate);	
		
		return expList;
}

public List cashCustomerSaleReportBillAndNameWise(HttpServletRequest request, HttpServletResponse response)
{
	
	String creditCustBillNo = request.getParameter("cashCustBillNo");
	//System.out.println("bill_no From Helper === "+creditCustBillNo);
    Map<Long,SaleReports> map = new HashMap<Long,SaleReports>();
    
    GoodsReceiveDao dao = new GoodsReceiveDao();
    List<SaleReports> creditCustSaleReports = dao.cashCustomerSaleReportBillAndNameWise(creditCustBillNo);
    return creditCustSaleReports;
	
}

public List rangeWiseBillPaidUnpaidHelper(HttpServletRequest request,
		HttpServletResponse response) {
	
	String FirstDate7 = request.getParameter("FirstDate7");
	String EndDate7 = request.getParameter("EndDate7");	
	
	
     Map<Long,PurchaseDetailsFromGoodsReceive> map = new HashMap<Long,PurchaseDetailsFromGoodsReceive>();
		
     GoodsReceiveDao dao = new GoodsReceiveDao();
		List<PurchaseDetailsFromGoodsReceive> expList = dao.rangeWiseBillPaidUnpaidDao(FirstDate7, EndDate7);
		
		return expList;
}

public Map getProfitAndLosstBetweenTwoDatesHelper(String startDate, String endDate)
{	
 	System.out.println("into helper class");
 	GoodsReceiveDao dao1 = new GoodsReceiveDao();
	List catList = dao1.getProfitAndLosstBetweenTwoDatesDao(startDate, endDate);
	
	Map  map =  new HashMap();
	for(int i=0;i<catList.size();i++)
	{
		Object[] o = (Object[])catList.get(i);
		ProfitAndLoss bean = new ProfitAndLoss();
		
		bean.setId(o[0].toString());
		if(o[1] == null)
		{
			bean.setExpensesAmount("0.00");
		}
		else
		{
			bean.setExpensesAmount(o[1].toString());
		}

		if(o[2] == null)
		{
			bean.setTransAndLabourexpenses("0.00");
		}
		else
		{
			bean.setTransAndLabourexpenses(o[2].toString());
		}
		
		map.put(bean.getId(),bean);
	}
	System.out.println("out of helper return map : "+map);
	return map;
}

public Map getProfitAndLosstBetweenTwoDatesOExpenseHelper(String startDate, String endDate)
{	
 	System.out.println("into helper class");
 	GoodsReceiveDao dao1 = new GoodsReceiveDao();
	List catList = dao1.getProfitAndLosstBetweenTwoDatesOExpenseDao(startDate, endDate);
	
	Map  map =  new HashMap();
	for(int i=0;i<catList.size();i++)
	{
		Object[] o = (Object[])catList.get(i);
		ProfitAndLoss bean = new ProfitAndLoss();
		
		bean.setId(o[0].toString());
		if(o[0] == null)
		{}

		if(o[1] == null)
		{
			bean.setOtherExpenses("0.00");
		}
		else
		{
			bean.setOtherExpenses(o[1].toString());
		}
		
		map.put(bean.getId(),bean);
	}
	System.out.println("out of helper return map : "+map);
	return map;
}

public Map getProfitAndLosstBetweenTwoDatesSaleAmountHelper(String startDate, String endDate)
{	
 	System.out.println("into helper class");
 	GoodsReceiveDao dao1 = new GoodsReceiveDao();
	List catList = dao1.getProfitAndLosstBetweenTwoDatesSaleAmountDao(startDate, endDate);
	
	Map  map =  new HashMap();
	for(int i=0;i<catList.size();i++)
	{
		Object[] o = (Object[])catList.get(i);
		ProfitAndLoss bean = new ProfitAndLoss();
		
		bean.setId(o[0].toString());
		if(o[0] == null)
		{}

		if(o[1] == null)
		{
			bean.setSaleAmount("0.00");
		}
		else
		{
			bean.setSaleAmount(o[1].toString());
		}
		
		map.put(bean.getId(),bean);
	}
	System.out.println("out of helper return map : "+map);
	return map;
}
}
