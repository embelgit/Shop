package com.Fertilizer.helper;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Fertilizer.dao.creditnoteDao;
import com.Fertilizer.hibernate.creditnotebean;


public class CreditnoteGenHelper {


	public void creditNoteGen(HttpServletRequest request, HttpServletResponse response )
	{
		
				Long Txid=0l;
				
				creditnoteDao dao2=new  creditnoteDao();
				List listtxid=dao2.getCreditNoteTxid();
				
				if(listtxid.size()<=0)
				{
					Txid=1l;
				}
				else
				{
				for (int i = 0; i < listtxid.size(); i++) {
					
				creditnotebean bean=(creditnotebean) listtxid.get(i);
				Txid=bean.getTxId();
				Txid++;
				}
			}
		
		
		
		
		String partyname=request.getParameter("partyname");
		String description=request.getParameter("description");
		String amount=request.getParameter("amount");
		
		System.out.println("partyname-------"+partyname);
		System.out.println("description-------"+description);
		System.out.println("amount-------"+amount);
		
		HttpSession session = request.getSession();
		session.setAttribute("partyname", partyname);

		creditnotebean bean=new creditnotebean();

		bean.setTxId(Txid);
		if(partyname!=null)
		{
			bean.setPartyname(partyname);
		}
		else
		{
			bean.setPartyname("N/A");
		}

		if(description!=null)
		{
			bean.setdescription(description);
		}
		else
		{
			bean.setdescription("N/A");
		}

		if(partyname!=null)
		{
			bean.setAmount(Double.parseDouble(amount));
		}
		else
		{
			bean.setAmount(0.0d);
		}
		
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dateobj = new Date();
		System.out.println(dateFormat1.format(dateobj));
		bean.setInsertDate(dateobj);

		creditnoteDao dao=new creditnoteDao();
		dao.insertCreditNote(bean);
	}


}
