package com.Fertilizer.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Fertilizer.hibernate.purchaseReturnBean;
import com.Fertilizer.dao.purchaseReturnDao;


public class PurchaseReturnHelper {
	
	public void  insertPurchaseReturn(HttpServletRequest request,HttpServletResponse response) throws ParseException
	{
     
		//SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		
		int i=0;
		
		Integer count = Integer.parseInt(request.getParameter("count"));
		int flag=0;
		for (i = 0; i < count; i++) 
		{
			
			String fk_supplier_id=request.getParameter("fk_supplier_id");
			String supplierName=request.getParameter("supplierName");
			String product_name=request.getParameter("product_name"+i);
			String company_Name=request.getParameter("company_Name"+i);
			String catName=request.getParameter("catName"+i);
			String pkPOId=request.getParameter("pkPOId"+i);
			String fkCategoryId=request.getParameter("fkCategoryId"+i);
			String batch_no=request.getParameter("batch_no"+i);
			String weight=request.getParameter("weight"+i);
			String buy_price=request.getParameter("buy_price"+i);
			String sale_price=request.getParameter("sale_price"+i);
			String mrp=request.getParameter("mrp"+i);
			String  tax_percentage=request.getParameter("tax_percentage"+i);
			//System.out.println("tax_percentage-----------"+tax_percentage);
			String dupQuantity1=request.getParameter("dupQuantity1"+i);
			//System.out.println("QUantity-----------"+dupQuantity1);
		    String dupQuantity=request.getParameter("dupQuantity"+i);
		    String  taxAmount=request.getParameter("taxAmount"+i);
		    //System.out.println("taxAmount-----------"+taxAmount);
		    
		    String total=request.getParameter("total"+i);
		    //String purchaseDate=request.getParameter("purchaseDate"+i);
		    String barcodeNo=request.getParameter("barcodeNo"+i);
		    
		    
		    System.out.println("fk_supplier_id-----------"+fk_supplier_id);
		    System.out.println("supplierName-----------"+supplierName);
		    System.out.println("product_name-----------"+product_name);
		    System.out.println("company_Name-----------"+company_Name);
		    System.out.println("catName-----------"+catName);
		    System.out.println("pkPOId-----------"+pkPOId);
		    System.out.println("fkCategoryId-----------"+fkCategoryId);
		    System.out.println("batch_no-----------"+batch_no);
		    System.out.println("weight-----------"+weight);
		    System.out.println("buy_price-----------"+buy_price);
		    System.out.println("sale_price-----------"+sale_price);
		    System.out.println("mrp-----------"+mrp);
		    System.out.println("tax_percentage-----------"+tax_percentage);
		    System.out.println("dupQuantity1-----------"+dupQuantity1);
		    System.out.println("dupQuantity-----------"+dupQuantity);
		    System.out.println("taxAmount-----------"+taxAmount);
		    System.out.println("total-----------"+total);
		    System.out.println("barcodeNo-----------"+barcodeNo);
		    
		    
		    

		    purchaseReturnBean bean=new purchaseReturnBean();
		    bean.setFk_supplier_id(Long.parseLong(fk_supplier_id));
		    bean.setSupplier(supplierName);
		    
		    bean.setProduct_name(product_name);
	        bean.setCompany_Name(company_Name);
	        bean.setCatName(catName);
	        bean.setPkPOId(Long.parseLong(pkPOId));
	        bean.setFkCategoryId(Long.parseLong(fkCategoryId));
	        bean.setBatch_no(batch_no);
	        bean.setWeight(Double.parseDouble(weight));
	        bean.setBuy_price(Double.parseDouble(buy_price));
            bean.setSale_price(Double.parseDouble(sale_price));
            bean.setMrp(Double.parseDouble(mrp));
            bean.setTax_percentage(Double.parseDouble(tax_percentage));
            bean.setDupQuantity1(Double.parseDouble(dupQuantity1));
            
			
			/*
			 * if(dupQuantity==0) { bean.setdupQuantity(0d);
			 * 
			 * } else { bean.setDupQuantity(Double.parseDouble(dupQuantity)); }
			 */
			 
            
            bean.setDupQuantity(Double.parseDouble(dupQuantity));
            bean.setTaxAmount(Double.parseDouble(taxAmount));
            bean.setTotal(Double.parseDouble(total));
            //bean.setPurchaseDate(purchaseDate);
            bean.setBarcodeNo(Long.parseLong(barcodeNo));
	        
            
            String purchaseDate=request.getParameter("purchaseDate"+i);
            System.out.println("purchaseDate-----------"+purchaseDate);
            
            
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date purchaseDate1= null;
			
			try {
				purchaseDate1 = format.parse(purchaseDate);
				bean.setPurchaseDate(purchaseDate1);
			}
			catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//bean.setPurchaseDate(purchaseDate1);
	        
			purchaseReturnDao dao=new purchaseReturnDao();
			if(Double.parseDouble(dupQuantity)>0)
			{
				dao.insertPurchaseReturn(bean);
			}
			
		    }
			
		}
	
	}


