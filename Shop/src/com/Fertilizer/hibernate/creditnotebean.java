package com.Fertilizer.hibernate;

import java.util.Date;

public class creditnotebean {


	private Long pk_creditnote_id;
	private Long txId;
	private String partyname;
	private String description;
	private Double amount;
	private Date insertDate;
	
	
	public Long getPk_creditnote_id() {
		return pk_creditnote_id;
	}
	public void setPk_creditnote_id(Long pk_creditnote_id) {
		this.pk_creditnote_id = pk_creditnote_id;
	}
	public Long getTxId() {
		return txId;
	}
	public void setTxId(Long txId) {
		this.txId = txId;
	}
	public String getPartyname() {
		return partyname;
	}
	public void setPartyname(String partyname) {
		this.partyname = partyname;
	}
	public String getdescription() {
		return description;
	}
	public void setdescription(String  description) {
		this.description = description;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

}
